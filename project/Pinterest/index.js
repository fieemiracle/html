// // 获取图片数目
// function imgLocaltion(parent, content) {
//     var cparent = document.getElementById(parent);
//     var ccontent = getChildElement(cparent, content);// 获取图片数目

//     var windowWidth = document.documentElement.clientWidth;//屏幕可视区域宽度
//     var imgWidth = ccontent[0].offsetWidth;//每张图片宽度
//     var number = parseInt(windowWidth / imgWidth);//获取图片数目
//     // console.log(number);
//     // cparent.style.width=number*imgWidth+'px';//修改父容器宽度  一次性只能修改一个属性
//     cparent.style.cssText = `width:${imgWidth * number}px`;//一次性可以修改多个属性，属性之间分号隔开

//     var boxHeightArr = [];
//     for (var i = 0; i < ccontent.length; i++) {
//         if (i < number) {
//             boxHeightArr[i] = ccontent[i].offsetHeight;
//         } else {
//             var minHeight = Math.min.apply(null, boxHeightArr);// 找数组最小高度
//             var minIndex = boxHeightArr.indexOf(minHeight);//最小高度的位置
//             // ccontent[i].style.cssText=`position:absolute;top:${minHeight}px;left:${ccontent[minIndex].offsetLeft}px`;//调整位置
//             ccontent[i].style.position = 'absolute'
//             ccontent[i].style.top = minHeight + 'px'
//             ccontent[i].style.left = ccontent[minIndex].offsetLeft + 'px'
//             boxHeightArr[minIndex] = boxHeightArr[minIndex] + ccontent[i].offsetHeight;
//         }
//     }
// }

// // 获取父容器下的所有子元素，包括不同层次
// function getChildElement(parent, content) {
//     const contentArr = [];
//     var allContent = parent.getElementsByTagName('*');//获取所有标签
//     for (var i = 0; i < allContent.length; i++) {
//         if (allContent[i].className == content) {//筛选目标标签
//             contentArr.push(allContent[i]);
//         }
//     }
//     console.log(contentArr);
//     return contentArr;
// }

// imgLocaltion('container', 'box');

window.onload = function() {
    imgLocation('container', 'box')
  }
  
  // 获取当前有多少张图片要放
  function imgLocation(parent, content) {
    var cparent = document.getElementById(parent)
    var ccontent = getChildElement(cparent, content) // 20个box
    // 谁换行了
    var winWidth = document.documentElement.clientWidth
    var imgWidth = ccontent[0].offsetWidth
    var num = Math.floor(winWidth / imgWidth)
    // cparent.style.width = imgWidth * num + 'px'
    cparent.style.cssText = `width: ${imgWidth * num}px`
  
    var BoxHeightArr = []
    for (var i = 0; i < ccontent.length; i++) {
      if (i < num) {
        BoxHeightArr[i] = ccontent[i].offsetHeight
        // console.log(BoxHeightArr);
      } else {
        // 找数组里最小的值
        var minHeight = Math.min.apply(null, BoxHeightArr)
        var minIndex = BoxHeightArr.indexOf(minHeight)
  
        // ccontent[i].style.cssText = `
        //   position: absolute; 
        //   top: ${minHeight}px; 
        //   left: ${BoxHeightArr[minIndex].offsetLeft}px
        // `
        ccontent[i].style.position = 'absolute'
        ccontent[i].style.top = minHeight + 'px'
        ccontent[i].style.left = ccontent[minIndex].offsetLeft + 'px'
  
        BoxHeightArr[minIndex] = BoxHeightArr[minIndex] + ccontent[i].offsetHeight
      }
    }
  
  }
  
  // 帮取出某容器内的某一层子容器
  function getChildElement(parent, content) {
    const contentArr = []
    var allContent = parent.getElementsByTagName('*')
    for (var i = 0; i < allContent.length; i++) {
      if (allContent[i].className == content) {
        contentArr.push(allContent[i])
      }
    }
    // console.log(contentArr);
    return contentArr
  }