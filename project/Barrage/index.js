window.onload = function () {
    // 把每一条弹幕作为一个对象
    var data = [
        {
            value: '听妈妈的话~~~~~~~',
            time: 5,
            color: 'red',
            speed: 1,
            fontSize: 22
        },
        {
            value: '听妈妈的话~~~~',
            time: 10,
            color: '#333',
            speed: 1,
            fontSize: 32
        },
        {
            value: '听妈妈的话~~',
            time: 7,
            color: '#ff6799',
            speed: 1,
            fontSize: 30
        },
        {
            value: '听妈妈的话~~ 不听',
            time: 15,
            color: '#ff6700',
            speed: 1,
            fontSize: 22
        },
        {
            value: '听妈妈的话~~ 就不听',
            time: 20,
            color: '#bfa',
            speed: 1,
            fontSize: 22
        }
    ]

    // 获取
    var canvas = document.getElementById('canvas');
    var video = document.getElementById('video');

    // 构造类 准备渲染弹幕所需的各种条件
    class CanvasBarrage {
        constructor(canvas, video, options = {}) {//构造器（构造函数）
            if (!canvas || !video) {
                return;
            }
            this.canvas = canvas;//this指向类 CanvasBarrage
            this.video = video;

            // canvas和video，img一样，可直接设置宽高，且不需要单位
            this.canvas.width = video.width;
            this.canvas.height = video.height;

            // 创建画布
            this.ctx = canvas.getContext('2d');//this.canvas
            console.log(this.ctx);

            // 设置默认样式
            let defaultOpts = {
                color: '#e91e63',
                speed: 1.5,
                opacity: 0.5,
                fontSize: 20,
                data: []
            }

            // 合并对象
            Object.assign(this, defaultOpts, options);//后面的会覆盖前面的,options==data:[] ;//this指向类 CanvasBarrage

            // 添加属性，用来判断播放/暂停
            this.isPaused = true;

            // 得到所有的已经初始化的弹幕
            this.barrages = this.data.map((item, index) => new Barrage(item, this));

            this.render();
        }

        // 渲染
        render() {
            this.clear();//清除原来的画布
            this.renderBarrage();
            if (this.isPaused == false) {
                requestAnimationFrame(this.render.bind(this));
            }
        }

        clear() {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        }

        renderBarrage() {
            var time = this.video.currentTime;
            // console.log(time);
            this.barrages.forEach(barrage => {
                if (time >= barrage.time && !barrage.flag) {
                    if (!barrage.isInit) {
                        barrage.init();
                        barrage.isInit = true;
                    }
                    barrage.x -= barrage.speed;
                    barrage.render();

                    if (barrage.x < -barrage.width) {
                        barrage.flag = true;
                    }
                }

            });
        }

        // 添加弹幕
        add(obj) {
            this.barrages.push(new Barrage(obj, this));

        }
    }

    // 初始化每一条弹幕
    class Barrage {
        constructor(obj, ctx) {//obj==value
            this.value = obj.value;//弹幕内容
            this.time = obj.time;
            this.obj = obj;
            this.context = ctx;
        }
        // 初始化弹幕
        init() {//在类里面创建函数
            this.color = this.obj.color || this.context.color;
            this.speed = this.obj.speed || this.context.speed;
            this.opacity = this.obj.opacity || this.context.opacity;
            this.fontSize = this.obj.fontSize || this.context.fontSize;

            // 计算弹幕的宽度
            var p = document.createElement('p');
            p.style.fontSize = this.fontSize + 'px';
            p.innerHTML = this.value;
            document.body.appendChild(p);
            this.width = p.clientWidth;
            document.body.removeChild(p);

            // 设置弹幕出现的位置
            this.x = this.context.canvas.width;
            this.y = this.context.canvas.height * Math.random();

            // 弹幕上下超出设置
            if (this.y < this.fontSize) {
                this.y = this.fontSize;
            } else if (this.y > this.context.canvas.height - this.fontSize) {
                this.y = this.context.canvas.height - this.fontSize;
            }

        }

        // 绘制每一条弹幕
        render() {
            this.context.ctx.font = `${this.fontSize}px Arial`;
            this.context.ctx.fillStyle = this.color;
            this.context.ctx.fillText(this.value, this.x, this.y);
        }
    }

    var canvasbarrage = new CanvasBarrage(canvas, video, { data });
    video.addEventListener('play', () => {
        canvasbarrage.isPaused = false;
        canvasbarrage.render();
        button.addEventListener('click',()=>{
            sendBarrage();
        })
    })

    // 获取弹幕的各值
    var text = document.getElementById('text');
    var button = document.getElementById('button');
    var color = document.getElementById('color');
    var range = document.getElementById('range');

    // 创建一个构造函数
    function sendBarrage() {
        var valueBarrage = {
            value: text.value,
            time:canvasbarrage.video.currentTime,
            color: color.value,
            range:fontSz
        }
        // console.log(valueBarrage.value);
        // console.log(valueBarrage.time);
        // console.log(valueBarrage.color);
        // console.log(valueBarrage.range);
        canvasbarrage.add(valueBarrage);
    }
    // 获取字体大小
    range.addEventListener('click',(e)=>{
        // console.log(e.offsetX);
        // console.log(range.offsetWidth);
        fontSz=`${parseInt(e.offsetX/range.offsetWidth*(range.max-range.min)) +20}px`;
        // console.log(fontSz);
    });












}//window.onload