var source = document.getElementsByClassName('source')[0];
var song = document.querySelectorAll('.song');

// 底部的更改
var cover = document.getElementsByClassName('cover')[0];
var names = document.getElementById('names');

// 顶部的更改
var songN = document.getElementById('song-name');
var singerN = document.getElementById('singer-name');
var picture = document.getElementById('picture');

var fast = document.getElementsByClassName('fast');
var player = document.getElementById('player');

// currentTime 音频当前播放时间
// duration 音频总长度
// ended 音频是否结束
var play = document.getElementsByClassName('play')[0];
var playbar = document.getElementsByClassName('playbar')[0];

// console.log(song);
for (let i = 0; i < song.length; i++) {
    song[i].addEventListener('click', function (e) {
        console.log(e.path[0].querySelector('.names').querySelector('.musicName').innerHTML);
        // console.log(e.srcElement.children[4].innerText);
        console.log(e.srcElement.children[1].innerHTML);

        source.src = e.srcElement.children[4].innerText;
        source.play()

        // 底部
        cover.setAttribute('src', e.srcElement.children[3].innerText);
        names.innerHTML = e.srcElement.children[1].innerHTML;

        // 顶部
        songN.innerHTML = e.path[0].querySelector('.names').querySelector('.musicName').innerHTML;
        singerN.innerHTML = e.path[0].querySelector('.names').querySelector('.musicSinger').innerHTML;
        picture.setAttribute('src', e.srcElement.children[3].innerText);

        // 播放
        player.setAttribute('src', './image/pause.png');

        // 进度条
        // let timer = setInterval(function(){
        //     playbar.style.width=source.currentTime/source.duration*100+"%"
        // },1000);
        timer();
        mouseEvent();
    })
}
function timer() {
    playbar.style.width = source.currentTime / source.duration * 100 + "%";
    let stop = window.requestAnimationFrame(timer);
    if (source.currentTime / source.duration * 100 >= 100) {
        window.cancelAnimationFrame(stop);
        playbar.style.width = 0 + '%';
        player.setAttribute('src', './image/play.png');
    }
}
function mouseEvent() {

    play.addEventListener('mousedown', function (e) {
        console.log(e.offsetX);
        let totalWidth = play.clientWidth;
        console.log(totalWidth);
        playbar.style.width = e.offsetX / totalWidth * 100 + '%';
        console.log(playbar.style.width);
        source.currentTime = e.offsetX / totalWidth * source.duration;
        player.setAttribute('src', './image/pause.png');
        source.play();
        timer();
        // if (e.offsetX / totalWidth * 100 >= 100) {
        //     player.setAttribute('src', './image/play.png');
        // }

    })

}
mouseEvent();


