1、单词超出换行
    overflow-wrap:break-word

2、块级排版上下文
   （1）BFC()
   某些容器会创建一个BFC
     根元素
     浮动、绝对定位、inline-block
     Flex子项和Grid子项
     overflow值不是visible的快盒
     display:flow-root
    （2）BFC排版规则
    盒子从上倒下败方
    垂直margin合并
    BFC内盒子的margin不会与外面的合并
    BFC不会和浮动元素重叠

3、Grid布局
    （1）划分网络
        grid-template-columns:100px 100px 200px
        grid-template-rows:100px 100px

        grid-template-columns:30% 30% auto
        grid-template-rows:100px auto

        grid-template-columns:100px 1fr 1fr 
        grid-template-rows:100px 1fr

    （2）网格线（grid line）
    （3）网格区域（grid area）