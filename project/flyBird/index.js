var headTitle = document.getElementById('headTitle');
var headBird = headTitle.querySelector('#headBird');// 小鸟上下振翅

var grassLand1 = document.getElementById('grassLand1');// 草地左右移动
var grassLand2 = document.getElementById('grassLand2');

var startBtn = document.getElementById('startBtn');
var wrapBg = document.getElementById('wrapBg');// 开始按钮

var blockArr = [];// 存放生成的管道
var score=0;
var num1=document.getElementById('num1');
var num2=document.getElementById('num2');
var num3=document.getElementById('num3');

var result=document.getElementById('result');
var okBtn=document.getElementById('okBtn');
var update=document.getElementById('update');

var Y = 3;//标题上下摆动的幅度
var index = 0;
var imgArr = ['./img/bird0.png', './img/bird1.png'];
var headWaveTimer = setInterval(headWave, 200);
function headWave() {
    Y = -Y;
    index = index % 2;
    headTitle.style.top = headTitle.offsetTop + Y + 'px';
    headBird.src = imgArr[index++];
    // if(index==2){
    //     index=0;
    // }
}

var landTimer = setInterval(landRun, 30);
function landRun() {
    if (grassLand1.offsetLeft <= -grassLand1.offsetWidth) {
        grassLand1.style.left = grassLand1.offsetWidth + 'px';
    }
    if (grassLand2.offsetLeft <= -grassLand1.offsetWidth) {
        grassLand2.style.left = grassLand2.offsetWidth + 'px';
    }
    grassLand1.style.left = grassLand1.offsetLeft - 3 + 'px';
    grassLand2.style.left = grassLand2.offsetLeft - 3 + 'px';

    var blockDistance = baseObj.randomNum(120, 350);
    if (blockArr.length) {
        for (var i = 0; i < blockArr.length; i++) {
            blockArr[i].moveBlock();
            var x = baseObj.rectangleCrashExamine(blockArr[i].upDivWrap, bird.div);
            var y = baseObj.rectangleCrashExamine(blockArr[i].downDivWrap, bird.div);
            var z = bird.div.offsetTop >= 390;
            if (x || y || z) {
                clearInterval(landTimer);
                wrapBg.onclick=null;
                bird.fallSpeed=0;
                result.style.display='block';
            }
        }

        if (blockArr[blockArr.length - 1].downDivWrap.offsetLeft < (450 - blockDistance)) {
            var newBlock = new Block();
            newBlock.createBlock();
            blockArr.push(newBlock);
        }

        // 计算分数
        if(blockArr[0].downDivWrap.offsetLeft == -12){
            score++;
            if(score<10){
                num1.style.backgroundImage=`url(img/${score}.jpg)`;

            }else if(score<100){
                num2.style.display='block';
                num1.style.backgroundImage=`url(img/${parseInt(score/10)}.jpg)`;
                num2.style.backgroundImage=`url(img/${score%10}.jpg)`;

            }else if(score<1000){
                num3.style.display='block';
                num1.style.backgroundImage=`url(img/${parseInt(score/100)}.jpg)`;
                num2.style.backgroundImage=`url(img/${parseInt(score/10)%10}.jpg)`;
                num3.style.backgroundImage=`url(img/${score%10}.jpg)`;
            }
        }
        if(blockArr[0].downDivWrap.offsetLeft < -50){
            wrapBg.removeChild(blockArr[0].upDivWrap);
            wrapBg.removeChild(blockArr[0].downDivWrap);
            blockArr.shift(blockArr[0]);
        }
    }
}

startBtn.addEventListener('click', function () {
    // 隐藏标题
    headTitle.style.display = 'none';
    clearInterval(headWaveTimer);
    // 隐藏开始按钮
    startBtn.style.display = 'none';

    // 小鸟出现
    bird.showBird(wrapBg);
    bird.flyBird();
    bird.wingWave();

    wrapBg.onclick = function () {
        bird.fallSpeed = -8;
    }


    var b = new Block();//构造函数的调用方法，构造函数执行结果是一个对象，自带一个返回值
    b.createBlock();//调用构造函数里面的函数属性
    blockArr.push(b);

    num1.style.display='block';

    okBtn.onclick=function(){
        update.href='./index.html';
    }
});



