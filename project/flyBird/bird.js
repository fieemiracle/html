// 小鸟出现
var bird = {
    div: document.createElement('div'),
    showBird: function (parentObj) {
        this.div.style.width = '40px';
        this.div.style.height = '28px';
        this.div.style.backgroundImage = 'url(./img/bird0.png)';
        this.div.style.backgroundRepeat = 'no-repeat';
        this.div.style.position = 'absolute';
        this.div.style.left = '50px';
        this.div.style.top = '200px';
        this.div.style.zIndex = '1';
        parentObj.appendChild(this.div);
    },
    fallSpeed: 0,
    flyYimer: null,
    wingTimer:null,
    flyBird: function () {
        bird.flyYimer = setInterval(fly, 60);
        function fly() {
            bird.div.style.top = bird.div.offsetTop + bird.fallSpeed++ + 'px';
            if (bird.div.offsetTop > 395) {
                clearInterval(bird.flyYimer);
                clearInterval(bird.wingTimer);
                bird.fallSpeed=0;
            }
            if(bird.div.offsetTop <= 0){
                bird.div.style.top=0;
                bird.fallSpeed=2;
            }
            if(bird.fallSpeed>12){
                bird.fallSpeed=12;
            }
        }
    },
    wingWave:function(){
        const downArr=['url(./img/down_bird0.png)','url(./img/down_bird1.png)'];
        const upArr=['url(./img/up_bird0.png)','url(./img/up_bird1.png)'];
        var downIndex=0,upIndex=0;
        bird.wingTimer=setInterval(wing,120);
        function wing(){
            if(bird.fallSpeed>0){//下落
                downIndex %= 2;
                bird.div.style.backgroundImage=downArr[downIndex++];               
            }
            if(bird.fallSpeed<0){//上升
                upIndex %= 2;
                bird.div.style.backgroundImage=upArr[upIndex++];
            }
        }
    }
}

