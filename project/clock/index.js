// 获取秒针的DOM结构，然后每秒让秒针旋转6deg
var secondHand = document.querySelector('.second-hand');
var minHand=document.querySelector('.min-hand');
var hourHand=document.querySelector(".hour-hand");
// console.log(secondHand);
function setDate(){
    var now=new Date();

    //秒
    var seconds=now.getSeconds();
    var secondDeg=seconds*6+90;
    secondHand.style.transform=`rotate(${secondDeg}deg)`;

    //分
    var minutes=now.getMinutes();
    var minDeg=((minutes*60+seconds)/3600)*360+90;
    minHand.style.transform=`rotate(${minDeg}deg)`;

    // 时
    var hours=now.getHours();
    var hourDeg=((hours*3600+minutes*60+seconds)/(3600*12))*360+90;
    hourHand.style.transform=`rotate(${hourDeg}deg)`;
}
setInterval(setDate,1000)
// setDate();

setInterval(setDate,60000);
// setDate();

setInterval(setDate,3600000);
setDate();