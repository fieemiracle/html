
// // 封装星星生产函数
// function RoundItem(index,x,y,ctx){
//     // 每一颗星星都是一个对象
//     this.index=index;
//     this.x=x;
//     this.y=y;
//     this.ctx=ctx;
//     this.r=Math.random()*2+1;
//     this.color='rgba(255,255,255,1)';
//     console.log(this.color);
// }
// // 封装星星绘制函数
// RoundItem.prototype.draw=function(){
//     //原型可以拿到构造函数内部的属性
//     this.ctx.fillStyle = this.color 
//     this.ctx.beginPath()
//     this.ctx.arc(this.x,this.y,this.r,0,2*Math.PI,false)
//     // CanvasPath.arc(x: number, y: number, radius: number, startAngle: number, endAngle: number, counterclockwise?: boolean | undefined): void
//     this.ctx.closePath()
//     this.ctx.fill()
// }

// RoundItem.prototype.move=function(){
//     this.y -= 3;
//     this.draw();
// }

function RoundItem(index, x, y, ctx) {
    this.index = index
    this.x = x
    this.y = y
    this.ctx = ctx
    this.r = Math.random() * 2 + 1
    this.color = 'rgba(255, 255, 255, 1)'
  }
  
  RoundItem.prototype.draw = function() { // 原型上是可以拿到构造函数内的属性的
    this.ctx.fillStyle = this.color
    this.ctx.beginPath()
    this.ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI, false)
    this.ctx.closePath()
    this.ctx.fill()
  }
  
  RoundItem.prototype.move = function() {
    this.y -= 0.5
    this.draw()
  }