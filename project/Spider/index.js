// 引入https模块
const https = require('https');
const cheerio=require('cheerio');
const fs=require('fs');
// https.get(options[, callback])
// https.get(url[, options][, callback])
// get请求网站链接，在回调函数获取资源
https.get('https://movie.douban.com/top250', (res) => {
    // console.log(res);
    let html = '';
    // res.on类似于addEventListener
    res.on('data', (chunk) => {
        html += chunk;
        // console.log(html);//放在res.on()外边会发生异步

    })
    // 数据获取完毕
    res.on('end',()=>{
        // 操作DOM结构,吧html告诉你的服务器
        const $=cheerio.load(html);
        let allFilems=[];
        $('li .item').each(function(){
            const title=$('.title',this).text();
            // console.log(title);
            const star=$('.rating_num',this).text();
            const pic=$('.pic img',this).attr('src');
            allFilems.push({title,star,pic})

        })
        // console.log(allFilems);
        fs.writeFile('./movieList.json',JSON.stringify(allFilems),(err)=>{
            if(err){
                throw err;
            }else{
                console.log('文件已保存！！！');
            }
        });

    })
})