#什么是前端路由
	描述url和UI之间的映射关系,单向映射
	
	前端路由就是把不同路由对应不同的内容或页面的任务交给前端来做，之前是通过服务端根据 url 不同返回不同的页面来实现。

#实现
	1、如何检测URL发生了变化
	2、如何改变URL却不引起页面的刷新(变成hash的改变)

#hash--用哈希一定有一个#
	改变url中的hash值部分，页面是不会刷新的

#history
只支持前进后退,默认采用history
HTML5提供的一个对象方法
	##修改url,是不会引起页面刷新的
	1、pushState
	2、replaceState
	
	##检测url变化
	3、popState
	
	
###
1、event.stopImmediatePropagation() 方法阻止剩下的事件处理程序被执行。该方法阻止事件在 DOM 树中向上冒泡。
停止当前节点，和所有后续节点的事件处理程序的运行。

2、stopPropagation 会阻止事件向上层元素冒泡。如果同一个元素绑定了多个事件（addEventListener），那么不会阻止其他事件的执行。

