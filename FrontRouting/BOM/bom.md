#window--代表的是整个浏览器的窗口，同时window也是网页中的全局对象 
	Window 对象属性
	属性                描述
	cosed               返回窗口是否已被关闭。
	defaultStatus       设置或返回窗口状态栏中的默认文本。
	document            对Document对象的只读引用。请参阅Document对象
	history             对History对象的只读引用。请参数History对象。
	innerheight         返回窗口的文档显示区的高度。
	innerwidth          返回窗口的文档显示区的宽度。
	length              设置或返回窗口中的框架数量。
	location            用于窗口或框架的Location对象。请参阅Location 对象。
	name                设置或返回窗口的名称。
	Navigator           对 Navigator 对象的只读引用。请参阅Navigator 对象。
	opener              返回对创建此窗口的窗口的引用。
	outerheight         返回窗口的外部高度。
	outerwidth          返回窗口的外部宽度。
	pageXOffset         设置或返回当前页面相对于窗口显示区左上角的X位置。
	pageYOffset         设置或返回当前页面相对于窗口显示区左上角的Y位置。
	parent              返回父窗口。
	Screen              对Screen对象的只读引用。请参阅 Screen对象。
	self                返回对当前窗口的引用。等价于Window属性。
	status              设置窗口状态栏的文本。
	top                 返回最顶层的先辈窗口。
	window              window 属性等价于self属性，它包含了对窗口自身的引用。
	
	方法                    描述
	alert()             显示带有一段消息和一个确认按钮的警告框。
	blur()              把键盘焦点从顶层窗口移开。
	clearIntervalo      取消由 setInterval0设置的 timeout。
	clearTimeout()      取消由setTimeout()方法设置的 timeout。
	cose()              关闭浏览器窗口。
	confirm()           显示带有一段消息以及确认按钮和取消按钮的对话框
	createPopup()       创建一个pop-up窗口。
	focus()             把键盘焦点给予一个窗口。
	moveBy()            可相对窗口的当前坐标把它移动指定的像素。
	moveTo()            把窗口的左上角移动到一个指定的坐标。
	open()              打开一个新的浏览器窗口或查找一个已命名的窗口。
	print()             打印当前窗口的内容。
	prompt()            显示可提示用户输入的对话框。
	resizeBy()          按照指定的像素调整窗口的大小。
	resizeTo()          把窗口的大小调整到指定的宽度和高度。
	scrollby()          按昭指定的像素值来滚动内容。
	scrollTo()          把内容滚动到指定的坐标。
	setInterval()       按照指定的周期(以毫秒计)来调用函数或计算表达式。
	setTimeout()        在指定的毫秒数后调用函数或计算表达式。
#history
	1、length
			属性：可以获取到当前访问的连接数量
			history.length
	2、back()
	        方法：可以用来回退到上一个页面，作用和浏览器的回退按钮一样
			history.back();
	3、forward()
	        方法：可以跳转下一个页面
			history.forward();
	4、go()
			方法：可以用来跳转到指定的页面
			他需要一个参数
				1:表示向前跳转一个页面
				2：表示向前跳转两个页面
				-1：表示向后跳转一个页面
				-2：表示向后跳转两个页面
				
			history.go(1)
	
#location
	Location 对象属性
	    属性 描述
	    hash 设置或返回从井号(#)开始的URL(锚)。
	    host 设置或返回主机名和当前URL的端口号。
	    hostname 设置或返回当前URL的主机名。
	    href 设置或返回完整的URL。
	    pathname 设置或返回当前URL的路径部分。
	    port 设置或返回当前URL的端口号。
	    protocol 设置或返回当前URL的协议。
	    search 设置或返回从问号(?)开始的URL(查询部分)。
	
	Location 对象方法
	    属性 描述
	    assiqn() 加载新的文档。
	    reload() 重新加载当前文档。
	    replace() 用新的文档替换当前文档
#navigator
	 Navigator 
		代表的当前浏览器的信息，通过该对象可以来识别不同的浏览器 
		由于历史原因，Navigator对象中的大部分属性都已经不能帮助我们识别浏览器了
        一般我们只会使用userAgent来判断浏览器的信息,
		
		
		userAgent是一个字符串，这个字符串中包含有用来描述浏览器信息的内容， 
        不同的浏览器会有不同的userAgent
		火狐的userAgent 
            Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:98.0) Gecko/20100101 Firefox/98.0
		Chrome的userAgent 
            Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36
		microsoft edge的userAgent
            Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36 Edg/100.0.1185.29
        
        Navigator 对象属性
        属性            描述
        appCodeName     返回浏览器的代码名。
        appMinorVersion 返回浏览器的次级版本。
        appName         返回浏览器的名称。
        appVersion      返回浏览器的平台和版本信息。
        browserLanguage 返回当前浏览器的语言。
        cookieEnabled   返回指明浏览器中是否启用cookie的布尔值。
        cpuClass        返回浏览器系统的CPU等级。
        onLine          返回指明系统是否处于脱机模式的布尔值。
        platform        返回运行浏览器的操作系统平台。
        systemLanguage  返回 OS 使用的默认语言。
        userAgent       返回由客户机发送服务器的user-agent头部的值。
        userLanquage    返回OS的自然语言设置。