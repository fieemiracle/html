// // 1  解构失败就是undefined
// let [x = 1] = [undefined];
// console.log(x);//1

// let [foo = true] = [];
// console.log(foo);//true

// let [y= 1] = [null];
// console.log(y);//null

// 2
const set1 = new Set([1, 3, 5, 7, 8, 4, 24, 25, 27]);
let res = new Set([...set1].map(x => x * 2));
console.log('集合结构：' + res);//集合结构：[object Set]
console.log(res);//Set(9) { 2, 6, 10, 14, 16, 8, 48, 50, 54 }

// 3
// const a=[[1,2],[3,4]];
// const weakseta=new WeakSet(a);
// console.log(weakseta);//WeakSet { <items unknown> }

// var test=function(){
//     // console.log(123);
// }
// function test(){
//     console.log(456);
// }
// test();

// function t(){}
// console.log(t);//[Function: t]
// var tt=function(){}
// console.log(tt);//[Function: tt]

// 预编译发生在函数执行之前
// function testt(a,b){
//     console.log(a);//1
//     c=0;
//     var c;
//     a=3;
//     b=2;
//     console.log(b);//2
//     function b(){}
//     function d(){}
//     console.log(b);//2
// }
// testt(1);
// AO:{
//     a:undefined 1 3,
//     b:undefined  function 2,
//     c:undefined 0,
//     d:function
// }

