
new Promise(resolve => {
    resolve();
})
    .then(() => {//then1
        new Promise(resolve => {
            resolve();
        })
            .then(() => { console.log(123); })//then1-1
            .then(() => { console.log(234); })//then1-1-1
            // console.log('猜猜我在那打印');
    })
    .then(() => {//then2
        console.log(555);
    })
    .then(() => {//then3
        new Promise(resolve => {
            resolve();
        })
            .then(() => { console.log(345); })//then3-1
            .then(() => { console.log(567); })//then3-1-2
    })

// setTimeout(()=>{
//     console.log('setTimeout');
// },0)

// new Promise((resolve, reject) => {
//     console.log("promise")
//     resolve()
// })//同步代码执行--fulfilled
//     .then(() => {	//return Promise--{<pending>}1
//         console.log("then1")
//         new Promise((resolve, reject) => {
//             console.log("then1promise")
//             resolve()//--fulfilled
//         })
//             .then(() => {//return Promise--{<pending>}3
//                 console.log("then1then1")
//             })
//             .then(() => {//return Promise--{<pending>}4
//                 console.log("then1then2")
//             })
//     })
//     .then(() => {//return Promise--{<pending>}2
//         console.log("then2")
//     })
