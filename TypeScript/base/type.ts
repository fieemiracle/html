let flag:boolean=true;
let count:number=10;
let nickname:string='黄倩';
let a:undefined=undefined;
let b:null=null;

// 严格模式下，这两个不能重新声明类型
flag=undefined;
flag=null;

console.log(flag,nickname,count,a,b);//null 黄倩 10 undefined null


// 枚举类型  通常是一个集合

// 普通枚举
enum Color{//值默认从上往下，从0开始排，可重新赋值，可以赋值为字符串，但是都得放
    red,
    pink,
    blue
}
const red:Color=Color.red;
console.log(red);//0
console.log(Color.pink);
console.log(Color[1]);
// let Color={
//     'red':0,
//     '0':'red'
// }



// 字符串枚举
enum Person{
    name='Lucky',
    age=19,
    sex='female'
}
const pname:Person=Person.name,
      page:Person=Person.age;
console.log(pname,page);

// 常量枚举
const enum Student{
    sname,
    sage,
    ssex
}
const student:Student[]=[Student.sname,Student.sage,Student.ssex];
console.log(student);


