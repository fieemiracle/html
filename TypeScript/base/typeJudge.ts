// 类型推论--声明变量时，不赋给类型，会触发

let x = 1  //let x:number = 1;
// x=true   //不能将类型“boolean”分配给类型“number”

function addd(x = 0, y = 0) {
	return x + y
}
// console.log(addd('a','b'));//类型“string”的参数不能赋给类型“number”的参数

// 类型断言
// interface Bird {
// 	fly();
// 	layEggs();
// }
// interface Fish {
// 	swim();
// 	layEggs();
// }
// function getSmallPet(): Fish | Bird {
// 	return
// }
// let pet = getSmallPet();
// pet.layEggs(); // okay
// pet.swim(); // errors

// 每一个成员访问都会报错
// if (pet.swim) {
// 	pet1.swim();
// }
// else if (pet.fly) {
// 	pet.fly();
// }

// 为了让这段代码工作，我们要使用类型断言
// if ((<Fish>pet).swim) {
// 	(<Fish>pet).swim();
// }
// else {
// 	(<Bird>pet).fly();
// }

// let strr: any = 'hello'
// let strLength: number = (<string>str).length
// let strLength2: number = (<string>str).length
// console.log(strLength);

// interface Objj{
//     a:string,
// }
// interface Objj1{
//     a:number,
// }
// let myObjj:Objj1| Objj={
//     a:'123'
// }
// let strLength1:number = (<string>myObjj.a).length
