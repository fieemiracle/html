// 一、array
// （1）泛型声明
// 语法：type myArr1 = Array<string | number | Record<string, number>>;
const arr:Array<number> = [1, 2, 3, 4, 5];
const arr3:Array<number|string> = [1,2,3, '2'];


// tuple元祖
const tuple1:[number, string]=[1,'hello'];
// const tuple2:[number|string]=[1,'hello'];

// any类型--ts不检测any，相当于就是写js
let value:any=1;
value='2';
value=true;
value={};
value=[];
value=undefined;
value=null;

// function
// （1）void
function say():void{
    console.log('hello');
    
}

// （2）number
function add():number{
    return 123;
}

// never  永远也没有返回类型
// never不能重新赋值，但是可以将其赋值给其他变量
// never是所有类型的子集
function error():never{
    throw new Error('Error');
}

let nev:never;//nev不能被重新赋值

// unknow
let val:unknown=1;
val ='123';
val=true;
val=undefined;
val=null;

// val不能赋值给其他变量
