// 对象类型
let myObject:object = {};
// 非严格模式下，可以这样；严格模式下，myObject不能被重新赋值
myObject=undefined;
myObject=null;

// Object有用内置对象
let bigObject:Object = {};
bigObject=1;
bigObject='a'
bigObject=true
bigObject=undefined
bigObject=null


// 空对象类型
let emptyObject:{} = {};
emptyObject=1
emptyObject='a'
emptyObject=true
emptyObject=undefined
emptyObject=null

// 类
class Teacher{
    name:string
    constructor(name){
        this.name=name
    }

    say():void{
        console.log(`hello, ${this.name}`);
        
    }
}

// 数组
const myArr:number[]=[1,2,3,4,5]
const myArr2:Array<number>=[1,2,3,4,5]

// 函数
function add(x: number, y: number): number {//两个参数
	return x + y
}
function subtract(...number: number[]): number {//多个参数
	let sum = 0;
	for (let i = 0; i < number.length; i++) sum += number[i]
	return sum;
}
const minus = function(x: number, y: number): number {//函数另一个表示方式
	return x - y
}
const multiply = function(x: number, y: number, z?: number): number {//三个参数
	return z ? x * y * z : x * y
}
multiply(1, 2)
function defaultFun(x: number, y: number = 0): number {
	return x / y
}

// 函数重载（同名函数），执行同名函数的最后一个函数体，前面的函数不能由函数体
function reAdd(x:number,y:number):number
function reAdd(x:string,y:string):string
function reAdd(x:any,y:any):any{
    if(typeof x=='number'){
        return x+y
    }else{
        return x+'--'+y
    }
}
console.log(reAdd(2, 3));



// 接口
const hisObj={
    name:'胡琴',
    age:19,
    sex:'boy'
}
hisObj.name='lucky'

// 具有一个和jisObj相同属性的对象
let p1:Object={
    name:'dog',
    age:20
}//这种方式不具有针对性
// （1）接口（具有针对性）
interface Children{
    name:string,
    age:number,
    sex:string
}
let pp1:Children={
    name:'dog',
    age:20,
    sex:'female'
}

// （2）函数接口
interface Push{
    (x:number,y:number):number
}