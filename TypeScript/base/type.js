var flag = true;
var count = 10;
var nickname = '黄倩';
var a = undefined;
var b = null;
// 严格模式下，这两个不能重新声明类型
flag = undefined;
flag = null;
console.log(flag, nickname, count, a, b); //null 黄倩 10 undefined null
// 枚举类型  通常是一个集合
// 普通枚举
var Color;
(function (Color) {
    Color[Color["red"] = 0] = "red";
    Color[Color["pink"] = 1] = "pink";
    Color[Color["blue"] = 2] = "blue";
})(Color || (Color = {}));
var red = Color.red;
console.log(red); //0
// 字符串枚举
var Person;
(function (Person) {
    Person["name"] = "Lucky";
    Person[Person["age"] = 19] = "age";
    Person["sex"] = "female";
})(Person || (Person = {}));
var pname = Person.name, page = Person.age;
console.log(pname, page);
var student = [0 /* Student.sname */, 1 /* Student.sage */, 2 /* Student.ssex */];
console.log(student);
