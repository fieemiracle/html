// unknow类型不能赋值给其他类型，但是其他可以赋值给unkonw类型
export type User = unknown;

export const users: unknown[] = [
	{
		name: 'Max Mustermann',
		age: 25,
		occupation: 'Chimney sweep'
	},
	{
		name: 'Kate Müller',
		age: 23,
		occupation: 'Astronaut'
	}
];

export function logPerson(user: unknown) {
	console.log(` - ${user.name}, ${user.age}`);
}

console.log('Users:');
users.forEach(logPerson);