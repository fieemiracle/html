// boolean
const isRight: boolean = true;
const isleft: boolean = false;
console.log(isRight, isleft);//true false

// number
let numberOne: number = 123;
let numberTwo: number = 999;
console.log(numberOne, numberTwo);//123 999

// string
let yourName: string = 'baby',
	yourGender: string = 'female';
console.log(yourName, yourGender);//baby female
let adult: string = `your name is ${yourName},your sex is ${yourGender}`;//模板字符串
console.log(adult);//your name is baby,your sex is female
let another: string = "my name is " + yourName + "my gender is " + yourGender;//字符串拼接
console.log(another);//my name is babymy gender is female

// Array
let isArray1: string[] = ['1', '2', '3'];//string[]
console.log(isArray1);//[ '1', '2', '3' ]
let isArray2: number[] = [1, 2, 3];//number[]
console.log(isArray2);//[ 1, 2, 3 ]
let isArray3: Array<number> = [1, 2, 3];//数组泛型：Array<number>
console.log(isArray3);//[ 1, 2, 3 ]
let isArray4: Array<string> = ['1', '2', '3'];//数组泛型：Array<string>
console.log(isArray4);//[ '1', '2', '3' ]

// Tuple（元祖）:允许表示一个已知元素数量和类型的数组，各元素的类型不必相同
let myTuple: [string, number];
myTuple = ['hello', 10]; // OK
// myTuple = [10, 'hello']; // error
console.log(myTuple[0]);//hello
myTuple[0] = 'hello world';
console.log(myTuple);//[ 'hello world', 10 ]


// enum（枚举类型）：支持枚举值到枚举名的正反向映射
enum Operate {
	add = '+',
	mult = '*'
}
console.log(Operate['add'] === '+');//true
console.log(Operate['+'] === 'add');//false
let operation: Operate = Operate.add;
console.log(operation);//+

enum Mycolor {
	red, yellow, blue
}
console.log(Mycolor['red'] === 0);//true
console.log(Mycolor[0] === 'red');//true
let myCol: Mycolor = Mycolor.yellow;
let hisCol: string = Mycolor[1]
console.log(myCol, hisCol);//1 yellow

// 默认值
enum People {
	Linda = 1,
	Jhon,
	Lihua
}
let per: string = People[2];
console.log(per);//Jhon

// 字符串枚举
enum Person {
	name = 'Lucky',
	age = 19,
	sex = 'female'
}
const pname: Person = Person.name,
	page: Person = Person.age;
console.log(pname, page);//Lucky 19

// 常量枚举
const enum Student {
	sname,
	sage,
	ssex
}
const student: Student[] = [Student.sname, Student.sage, Student.ssex];
console.log(student);//[ 0, 1, 2 ]