// any：表示任意类型， 可以被任何类型分配，也可以分配给任何类型;任意类型，是所有类型的子类型
//把any类型分配给其他类型
// let otherType1: any
// let otherType2: any = otherType1;
// let otherType3: unknown = otherType1;
// let otherType4: void = otherType1;

// let otherType5: undefined = otherType1;
// let otherType6: null = otherType1;
// let otherType7: number = otherType1;
// let otherType8: string = otherType1;
// let otherType9: boolean = otherType1;
// // 报错:不能将类型“any”分配给类型“never”
// // let val_never:never = val;

// //把其他类型分配给any
// otherType1 = 'hello';
// otherType1 = 110;
// otherType1 = true;
// otherType1 = null;
// otherType1 = undefined;

// otherType1 = unknown;// 报错:“unknown”仅表示类型，但在此处却作为值使用
// otherType1 = never;// 报错:“never”仅表示类型，但在此处却作为值使用
// otherType1 = any;// 报错:“any”仅表示类型，但在此处却作为值使用
// otherType1 = void;// 报错:应为表达式


// undefined
// const und: undefined = undefined;
// console.log(und);
// null
// const nl: null = null;
// console.log(nl);
// unknow：表示未知类型， 可以被任何类型分配，不能分配给任何类型

// 把unknown类型分配给其他类型
let myType: unknown;
let myType1: any = myType;
let myType2: unknown = myType;
let myType3: string = myType;//报错
let myType4: number = myType;//报错
let myType5: boolean = myType;//报错
let myType6: null = myType;//报错
let myType7: undefined = myType;//报错

// 把其他类型分配给unknown类型
myType = '';
myType = 0;
myType = true;
myType = undefined;
val = null;

myType = void;//报错
myType = any;//报错
myType = unknown;//报错
myType = never;//报错


// never：是任何类型的子类型，也可以赋值给任何类型；然而，没有类型是never的子类型或可以赋值给never类型（除了never本身之外）
// 返回never的函数必须存在无法达到的终点
function throwError(message: string): never {
	throw new Error(message);
}
// 返回never的函数必须存在无法达到的终点
function infiniteLoop(): never {
	while (true) {
	}
}