// 基础数据类型
// const str1='string';
const str: string = 'string';
const num: number = 123;
const bool: boolean = true;
const none: null = null;
const defind: undefined = undefined;
console.log(str);//string
console.log(num);//123
console.log(bool);//true
console.log(none);//null
console.log(defind);//undefined