// 函数重载
function getDate(type: 'string', timestamp?: string): string;
interface toGetDate {
	(type: 'string', timestamp?: string): string;
	(type: 'date', timestamp?: string): Date;
	(type: 'string' | 'date', timestamp?: string): Date | string;
}
// 不能将类型“（type:any,timestamp:any）=>string|Date”分配给类型“toGetDate”
// 不能将类型"string|Date"分配给类型"string"
// 不能将类型"Date"分配给类型"string"
const getTime: toGetDate = (type, timestamp) => {
	const date = new Date(timestamp);
	return type == 'string' ? date.toLocaleString() : date
}