// 空类型，表示无赋值
type IEmptyFunction = () => void;

// 任意类型，是所有类型的子类型
type IAnyType = any;

// 枚举类型：支持枚举值到枚举名的正反向映射
enum EnumExample {
	add = '+',
	mult = '*'
}
EnumExample['add'] === '+';
EnumExample['+'] === 'add';

enum ECorlor {
	Mon, Tue, Wed, Thu, Fri, Sat, Sun
}
ECorlor['Mon'] === 0;
ECorlor[0] === 'Mon';

// 泛型
type INumArr = Array<number>;
const numArr: INumArr = [1, 2, 3, 4];
console.log(numArr);