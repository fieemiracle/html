// function add(x, y) {
// 	return x + y;
// }
// const mult = (x, y) => x * y;
// add(1, 2)
// mult(1, 2)

// 函数类型
interface Mymult {
	(x: number, y: number): number
}
const mult: Mymult = (x, y) => x * y;
console.log(mult(2, 3));//6

function add(x: number, y: number): number {
	return x + y;
}
const adder: (x: number, y: number) => number = (x, y) => x * y;
console.log(add(2, 3));//5
console.log(adder(2, 3));//6