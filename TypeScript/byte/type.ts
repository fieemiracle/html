// 别名
// type count=number|number[]
// function hello(val:count){

// }


// 交叉类型
// interface PersonA {
// 	name: string,
// 	age: number
// }

// interface PersonB {
// 	name: string,
// 	gender: string
// }

// let pa:PersonA & PersonB={
// 	name:'dog',
// 	age:19,
// 	gender:'girl'
// }

interface PersonA {
	name: string,
	age: number
}

interface PersonB {
	name: string,
	gender: string
}

function test(params: PersonA & PersonB) {//自相矛盾，ts自动为never
	console.log(params);
}

// test({ name: '陈大佬' })never不能被重新赋值