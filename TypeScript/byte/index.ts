// 类型守卫
interface InObj1 {
	a: number,
	x: string
}
interface InObj2 {
	a: number,
	y: string
}

function isIn(arg: InObj1 | InObj2) {
	if ('x' in arg) console.log(arg.x);
	if ('y' in arg) console.log(arg.y);

}

function isTypeOf(val: string | number){
	if(typeof val==='number') return 'number'
	if(typeof val==='string') return 'string'
}

function createDate(date:Date|string){
	if(date instanceof Date){
		return 'Date'
	}else{
		return 'string'
	}
}