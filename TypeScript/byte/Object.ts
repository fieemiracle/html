interface Myobj {
	// 只读属性：约束属性不可在对象初始化外赋值
	readonly cardId: number;
	name: string;
	sex: 'male' | 'female';
	age: number;
	// 可选属性：定义该属性可以不存在
	hobby?: string;
	// 任意属性：约束属性都必须是该属性的子类型
	[key: string]: any;
}

// 对象类型
const obj: Myobj = {
	cardId: 2020213028,
	name: 'Lucky',
	sex: 'female',
	age: 19,
	hobby: 'reading'
}

// const o:Myobj={//报错：缺少name属性，hobby可缺省
// 	cardId:2020213026,
// 	sex:'female',
// 	age:18
// }
console.log(obj);
// {
//   cardId: 2020213028,
//   name: 'Lucky',
//   sex: 'female',
//   age: 19,
//   hobby: 'reading'
// }
console.log(obj.cardId);//2020213028
console.log(obj.shortcoming = 'Math');//Math
console.log(obj.cardId = 2020213026);//Cannot assign to 'cardId' because it is a read-only property.
