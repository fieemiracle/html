// 类型别名&类型断言
/* 通过type关键字定义了IObjArr的别名类型 */ type I0bjArr = Array<{
	key: string;
	[objKey: string]: any;
}>
function keyBy<T extends I0bjArr>(objArr: Array<T>) {
	/* 未指定类型时，result类型为{} */
	const result = objArr.reduce((res, val, key) => {
		res[key] = val; return res;	
}, {});
/* 通过as关键字，断言result类型为正确类型 */ return result as Record<string, T>;
}