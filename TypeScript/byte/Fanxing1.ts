// 泛型的方式

import { type } from "os"

// 函数泛型
function getValue<T, U>(arg: [T, U]): [T, U] {
	return arg
}
getValue<string, number>(['hello', 123])
getValue(['hello', 123])//类型推断


// 泛型的约束
interface Lengthwish {
	length: number
}
function F<T extends Lengthwish>(args: T): T {
	return args
}

interface KeyValue<T, U> {
	key: T,
	value: U
}
const po1: KeyValue<number, string> = {
	key: 90,
	value: 'hhhh'
}
// 泛型类
class Test<T>{
	value: T;
	add: (x: T, y: T) => T
}
let myTest = new Test<string>()
myTest.value = 'hhhh'

// 泛型字面量
type Cart<T> = { list: T[] } | T[]
let c1: Cart<string> = { list: ['hlll'] }
let c2: Cart<number> = [1]

// 泛型默认值
function createArray<T = string>(length: number, value: T): Array<T> {
	let result: T[] = []
	for (let i = 0; i < length; i++) {
		result[i] = value;
	}
	return result;
}
createArray(3, 'hello')

// 工具类型
let pps = {
	name: 'dog',
	age: 19,
	gender: 'man'
}

type People = typeof pps
function getName(pps: People): string {
	return pps.name
}
getName(pps)

interface Person {
	name: string,
	age: number,
	gender: 'male' | 'female'
}
type PersonKey = keyof Person
type Keys = 'a' | 'b' | 'c'
type Obj = {
	[p in Keys]: any
}

// {a:any,b:any,c:any}




