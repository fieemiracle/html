// 数组类型
// 类型+方括号
type myArr = number[];

// 泛型表示
type myArr1 = Array<string | number | Record<string, number>>;

// 元祖表示
type myArr2 = [number, number, string, string];

// 接口表示
interface myArr3 {
	[key: number]: any
}

const Arr: myArr = [1, 2, 3, 4];
const Arr1: myArr1 = [1, 2, 'name', { age: 19 }];
const Arr2: myArr2 = [1, 2, 'name', 'age'];
const Arr3: myArr3 = [1, () => null, {}, []];

console.log(Arr);//[ 1, 2, 3, 4 ]
console.log(Arr1);//[ 1, 2, 'name', { age: 19 } ]
console.log(Arr2);//[ 1, 2, 'name', 'age' ]
console.log(Arr3);//[ 1, [Function (anonymous)], {}, [] ]




