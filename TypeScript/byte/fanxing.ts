// 泛型
function getRepeatArr(target) {
	return new Array(100).fill(target);
}
type IGetRepeatArr = (target: any) => any[];
// 不预先指定具体的类型，而在使用的时候再指定类型的一种特性
type IGetRepeatArrR = <T>(target: T) => T[];

// 泛型接口&多泛型
interface IX<T, U> {
	key: T;
	value: U;
}

// 泛型类
class IMan<T>{
	instance: T;
}

// 泛型别名
type ITypeArr<T> = Array<T>;

// 泛型的约束：限制泛型必须符合字符串
type IGetRepeatStringArr = <T extends string>(target: T) => T[];
const getStrArr: IGetRepeatStringArr = target => new Array(100).fill(target);
getStrArr('123');/* 报错:类型“number”的参数不能赋给类型“string”的参数 */

/* 泛型参数默认类型 */
type IGetRepeatArrRR<T = number> = (target: T) => T[];
const getRepeatArrRR: IGetRepeatArrRR = target => new Array(100).fill(target);
getRepeatArr('123');/* 报错:类型“string”的参数不能赋给类型“number”的参数 */ 
