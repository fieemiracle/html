// 接口
interface PersonC {
	// 只读属性
	readonly name: string,
	age?: number,
	// 任意属性
	[prop: string]: any
}

let tom: PersonC = {
	name: 'ducky',
	like: 'running'
}
// tom.name = 'dog'//报错

type AAA = {
	name: string,
	say(): void
}
type BBB = {
	name: string,
	say(): void
}

interface bbb extends BBB {
	sex: string
}

let pppp: bbb = {
	name: 'hhhh',
	sex: 'male',
	say(): void {

	}
}