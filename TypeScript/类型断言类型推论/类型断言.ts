// let greet: any = 'hello'
// let greetLength1: number = (<string>greet).length
// let greetLength2: number = (<string>greet).length
// console.log(greetLength1);//5
// console.log(greetLength2);//5

// interface Ob1 {
// 	name: string,
// }
// interface Ob2 {
// 	name: number,
// }
// let myOb: Ob1 | Ob1 = {
// 	name: '123'
// }
// let obLength: number = (<string>myOb.name).length
// console.log(obLength);//3

// function getFullName(fullname: string | number): number {
// 	if (fullname.length) {
// 		return fullname.length;
// 	} else {
// 		return fullname.toString().length;
// 	}
// }

function getFullName(fullname: string | number): number {
	// if ((<string>fullname).length) {
	if (fullname as string) {
		return (<string>fullname).length
	} else {
		return fullname.toString().length
	}
}
getFullName('hhhhhhhhh')


