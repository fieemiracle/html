# 类型推论
	类型推论，顾名思义就是指类型在哪里如何被推断。在TS语法中，如果有些没地方有明确指出类型，类型推论就会帮助提供类型，即声明变量时，不赋给类型，就会触发类型推论。