一、认识TypeScript
	(1)Javascript是一种动态类型的弱类型语言
	Javascript超集：
		A.包含与兼容所有JS特性，支持共存
		B.支持渐进式引入与升级
	(2)TypeScript是一种静态类型的弱类型语言
	静态类型的优点：
		A.可读性增强：基于语法解析TSDoc,ide增强
		B.可维护性增强：在编译阶段暴露大部分错误=>多人合作大型项目中，可以获得更好的稳定性和开发效率
		
	(3)Typescript是Javascript的超集，具有可选的类型并可以编译成纯JS
二、基本语法

三、搭建
	（1）npm i -g typescript
		tsc -v 查看tsc版本
		tsc --init 初始化
	运行：tsc first.ts 可将TS文件转化为JS文件
	（2）npm i -g ts-node
	运行命令：ts-node first.ts 可以直接执行TS文件
	（3）npm i -D tslib @types/node
	如果（2）之后运行还会报错

四、使用TypeScript优点
	（1）增强代码的可维护性，尤其在大型项目开发中效果显著
	（2）友好地在编译器提示错误，在编译阶段就能检查类型，发现大部分错误
	（3）支持最新的JS的特性
	（4）繁荣的生态圈，typescript被普遍使用，多种框架都支持，尤其是Vue3

五、使用TypeScript缺点
	（1）插件库兼容不够完美
	（2）增加前期开发成本

六、tsconfig.json配置文件
	commenjs:
		使用module.exports...抛出
		require(...)引入