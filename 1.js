// var thirdMax = function (nums) {
//     // 思路：
//     // 1、将数组降序排序，方便直接取第三个数    
//     // 2、将排序后的数组转换为集合，并判断集合的长度，方法size
//     // 3、如果集合长度大于等于3，通过集合的entries()/keys()/values()得到值的数组形式，得到第三个数(第三大)
//     // 4、否则，返回最后一个数(最大)
//     nums = nums.sort((a, b) => (b - a));
//     const numsSet = Array.from(new Set(nums));
//     console.log(numsSet);
//     if (numsSet.length < 3) {
//         // return numsSet[numsSet.length];
//         console.log(numsSet.indexOf(0));
//     } else {
//         // return numsSet[2];
//         console.log(numsSet.indexOf(2));
//     }

// };
// thirdMax([3, 2, 1]);

// // 方法一
// let set = new Set([1, 2, 3]);
// set = new Set([...set].map(val => val * 2));
// console.log(set);
// // set的值是2, 4, 6

// // 方法二
// let set1 = new Set([1, 2, 3]);
// set1 = new Set(Array.from(set1, val => val * 2));
// console.log(set1);

// let set2 = new Set([1, 2, 3, 4, 5]);
// set2 = new Set([...set2].filter(x => (x % 2) == 0));
// console.log('result:' + set2);

// // 生成二维数组
// const m = 4;
// const n = 2;
// let arr = Array.from(Array(m), () => new Array(n));
// console.log(arr);

// var addBinary = function (a, b) {
//     let carrySum = "";//空字符串便于将数组转为字符
//     let carry = 0;//判断是否进位
//     // a="11",b="1"
//     for (let i = a.length - 1, j = b.length - 1; i >= 0 || j >= 0; i++, j++) {//遍历循环
//         let sum = carry;
//         sum += i >= 0 ? parseInt(a[i]) : 0;//sum=1   sum=3
//         sum += j >= 0 ? parseInt(b[j]) : 0;//sum=2
//         carrySum += sum % 2;//carrySum=1
//         carry = Math.floor(sum / 2);//1
//     }
//     carrySum += carry == 1 ? carry : "";//1+1=""
//     //     return carrySum.split('').reverse().join('');
//     console.log(carrySum.split(''));
// };
// let a="11";
// let b="1";
// addBinary();

arr1=[1,2,3]
arr2=[1,2,3]
console.log(arr1==arr2);