## Node事件循环
	一、简介
		NodeJS采用V8作为JS的解析引擎，而I/O处理使用libuv，libuv是一个基于事件驱动的跨平台抽象层，封装了不同操作系统一些底层特点，对外提供统一的API，事件循环也在libuv内实现
		NodeJS的运行机制：
		1、v8引擎解析javascript脚本
		2、解析后的代码调用Node API
		3、libuv库负责Node API的执行。将不同的任务分配给不同的线程，形成一个event loop，以异步的方式将任务执行的结果返回给v8引擎
		4、V8引擎将结果返回给用户
	二、六个阶段
		libuv引擎的事件循环分六个阶段，会按照顺序反复执行。每当进入某一个阶段的时候，从对应的回调队列中取出函数去执行，
		当队列为空或者执行的回调函数数量达到系统设定的阈值，就进入下一阶段
		六个阶段：
		（一）timers（定时器检测阶段）：执行setInterval、setTimeout的回调
			由poll阶段控制
		（二）I/O callbacks（I/O事件回调阶段）：处理上一次循环中少数未执行的I/O回调
		（三）idle,prepare（闲置阶段）：仅Node内部使用
		（四）poll（轮询阶段）：获取新的I/O事件，适当的条件下Node将阻塞在这里
			干两件事：
			1、回到timer阶段执行回调
			2、执行I/O回调
			在这个阶段如果没有设定timer：
			（1）若poll队列不为空，遍历回调队列并同步执行，直到队列为空或者达到系统限制
			（2）若poll队列为空：
				A、若有setImmediate回调需要执行，poll阶段会停止并且进入check阶段执行回调
				B、若没有setImmediate回调需要执行，会等待回调被加入到队列中并立即执行回调，这里会有超时间设置防止一直等待
			
		（五）check（检查阶段）：执行setImmediate的回调
		（六）close callbacks（关闭事件回调阶段）：执行socket的close事件回调
			执行顺序：
			外部输入数据-->轮询阶段(poll)-->检查阶段(check)-->关闭事件回调阶段(close callback)-->定时器检测阶段(timer)-->I/O事件回调阶段(I/O callbacks)-->
			闲置阶段(idle, prepare)-->轮询阶段（按照该顺序反复运行）
		
	三、宏任务和微任务
		 macro-task：setTimeout、setInterval、 setImmediate、script（整体代码）、 I/O 操作等。
		 micro-task: process.nextTick、new Promise().then(回调)等
		 
	四、注意点
		（1）setTimeout和setImmediate
			二者非常相似，区别主要在于调用时机不同。
			setImmediate 设计在poll阶段完成时执行，即check阶段；
			setTimeout 设计在poll阶段为空闲时，且设定时间到达后执行，但它在timer阶段执行
			
		（2） process.nextTick
			 process.nextTick()独立于 Event Loop 之外的，它有一个自己的队列，当每个阶段完成后，如果存在 nextTick 队列，就会清空队列中的所有回调函数，并且优先于其他 microtask 执行
			 
	五、Node事件循环和浏览器事件循环
		（一）浏览器环境下，microtask的任务队列是每个macrotask执行完之后执行（宏任务是下一次循环的开始）。
			而在Node.js中，microtask会在事件循环的各个阶段之间执行，也就是一个阶段执行完毕，就会去执行microtask队列的任务
		
		（二）浏览器和Node 环境下，microtask 任务队列的执行时机不同
			Node端，microtask 在事件循环的各个阶段之间执行
			浏览器端，microtask 在事件循环的 macrotask 执行完之后执行
