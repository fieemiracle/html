// 轮询阶段的例子
const fs = require('fs');

console.log('Script started');

// 读取文件
fs.readFile('./text/file.json', (err, data) => {
	// 回调函数将在轮询阶段执行
	console.log('File contents:', JSON.parse(data));
});

console.log('Script ended');