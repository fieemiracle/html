setTimeout(function() {
	console.log('timer1');
	process.nextTick(function setTimeout1() {
		console.log('nextTick1');
	});
	new Promise(function(resolve) {
		console.log('promise1');
		resolve();
	}).then(function() {
		console.log('promise1 callback');
	});
});

process.nextTick(function() {
	console.log('nextTick2');
});

new Promise(function(resolve) {
	console.log('promise2');
	resolve();
}).then(function() {
	console.log('promise2 callback');
});

setTimeout(function setTimeout2() {
	console.log('timer2');
	process.nextTick(function() {
		console.log('nextTick3');
	})
	new Promise(function(resolve) {
		resolve();
	}).then(function() {
		console.log('promise3 callback');
	});
})