let dgram = require('dgram');

let socket = dgram.createSocket("udp4");

socket.on('close', () => {
	console.log('Socket对象已关闭');
});

socket.close();