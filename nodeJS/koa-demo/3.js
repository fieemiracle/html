const Koa = require('koa')
var cors=require('koa-cors')
const app = new Koa()
app.use(cors());

// accept 客户端希望接收到的数据类型
const main = (ctx) => {
  if (ctx.request.accepts('xml')) {
    ctx.response.type = 'xml'
    ctx.response.body = '<data>Hello World</data>'
  } else if (ctx.request.accepts('html')) {
    ctx.response.type = 'html'
    ctx.response.body = '<p>Hello World</p>'
  } else if (ctx.request.accepts('json')) {
    ctx.response.type = 'json'
    ctx.response.body = '{data: hello}'
  } else {
    ctx.response.type = 'text'
    ctx.response.body = 'Hello World'
  }
}

app.use(main)

app.listen(3000, () => {
  console.log('项目已启动');
})



// {
//   request: {
//     method: 'GET',
//     url: '/',
//     header: {
//       host: 'localhost:3000',
//       connection: 'keep-alive',
//       'cache-control': 'max-age=0',
//       'sec-ch-ua': '".Not/A)Brand";v="99", "Google Chrome";v="103", "Chromium";v="103"',
//       'sec-ch-ua-mobile': '?0',
//       'sec-ch-ua-platform': '"macOS"',
//       'upgrade-insecure-requests': '1',
//       'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36',
//       accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
//       'sec-fetch-site': 'none',
//       'sec-fetch-mode': 'navigate',
//       'sec-fetch-user': '?1',
//       'sec-fetch-dest': 'document',
//       'accept-encoding': 'gzip, deflate, br',
//       'accept-language': 'zh-CN,zh;q=0.9,en;q=0.8'
//     }
//   },
//   response: {
//     status: 404,
//     message: 'Not Found',
//     header: [Object: null prototype] {}
//   },
//   app: { subdomainOffset: 2, proxy: false, env: 'development' },
//   originalUrl: '/',
//   req: '<original node req>',
//   res: '<original node res>',
//   socket: '<original node socket>'
// }