const glob=require('glob');

// 阻塞IO
// var result=null;
// console.time('glob');
// result=glob.sync(__dirname+'/**/*');//绝对路径  /**/*通配符
// console.timeEnd('glob');

// console.log(result);//在这些文件被读取到之前，node是干不了背的事的

// 非阻塞IO
var result=null;
console.time('glob')
glob(__dirname+'/**/*',function(err,res){
    result=res;
    // console.log('got result');后于2执行，耗时执行时，一边执行一边读取下一个请求
})
console.timeEnd('glob');

console.log(1+1);//这句不是耗时执行
// 不是2先打印：node事件循环

