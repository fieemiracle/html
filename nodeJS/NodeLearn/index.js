// const lib = require("./lib");
// console.log(lib);//当一个初始化状态的模块被引用后他默认是一个空对象
// 如果引入的那个文件没有抛出东西，就会是一个空对象

// var playerAction = process.argv[process.argv.length - 1];
const game = require('./lib');

let count = 0;

process.stdin.on('data', e => {//on监听
    // console.log(e);//没有结果，没有关闭，输入任意东西(例如rock)，会打印rock的Buffer流
    // crl+c结束进程
    const playerAction = e.toString().trim();
    const result = game(playerAction);
    console.log(result);

    if(result==-1) count++;
    if (count == 3) {
        console.log('你太厉害了,我不玩了');
        process.exit()
    }
});
// game(playerAction)

