const net = require('net')//net创建的是TCP服务

function responseData(str) {
  return `HTTP/1.1 200 OK 
  Connection: keep-alive 
  Date: ${new Date()} 
  Content-Length: ${str.length}
  Content-Type: text/html

  ${str}
  `
}

const server = net.createServer((socket) => {
  socket.on('data', (data) => { 
    const matched=data.toString('utf-8').match(/^GET ([/\w]+) HTTP/)
    if(matched){
        const path=matched[1]
        if(path==='/'){
            socket.write(responseData('<h1>hello world</h1>'))
        }else{
            socket.write(responseData('<h1>not found</h1>',404,'NOT FOUND'))
        }
    }
    // if (/^GET \/ HTTP/.test(data)) {
    //   socket.write(responseData('<h1>hello world</h1>'))
    // }
    console.log(`DATA:\n\n${data}`);
  })

  socket.on('close', () => {
    console.log('连接关闭');
  })
}).on('error', (err) => {
  throw err
})

server.listen({host: '0.0.0.0', port: 8080}, () => {
  console.log('服务已启动在', server.address());
})