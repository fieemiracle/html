const fs=require('fs')
const path=require('path')

let dataCache=null

function loadData(){
    if(!dataCache){
        const file=path.resolve(__dirname,'./data.json')
        const data=JSON.parse(fs.readFileSync(file,{encoding: 'utf-8'}))
        const reports=data.dailyReports
        dataCache={}

        reports.forEach(report=>{
            dataCache[report.updatedDate]=report
        })
    }
    return dataCache
}

function getCoronavirusByDate(date){
    const dailyData=loadData()[date]||{}
    if(dailyData.countries){
        dailyData.countries.sort((a,b)=>{
            return b.confirm-a.confirm
        })
    }
    return dailyData
}

function getCoronavirusByDateIndex(){
    return Object.keys(loadData())
}

module.exports={
    getCoronavirusByDate,
    getCoronavirusByDateIndex
}