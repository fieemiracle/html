const Server=require('../didy-router/lib/server')
const param=require('./aspect/param')
const Router=require('../didy-router/lib/middleware/router')

const app=new Server()
const router=new Router()

app.use(param)
app.use(router.get('/coranavirus/index',async ({route,res},next)=>{
    const {getCoronavirusByDateIndex}=require('./mock/mock')
    const index=getCoronavirusByDateIndex()
    res.setHeader('Content-Type', 'application/json')
    res.body={
        data:index
    }
    await next()
}))
app.use(router.get('/coranavirus/:date',async ({route,res},next)=>{
    const {getCoronavirusByDate}=require('./mock/mock')
    const data=getCoronavirusByDate(route.date)
    res.setHeader('Content-Type', 'application/json')
    res.body={
        data:{data}
    }
    await next()
}))
app.use(router.all('/:*',async ({route,res},next)=>{
    res.setHeader('Content-Type', 'text/html')
    res.body='<h1>NOT FOUND</h1>'
    res.statusCode=404
    await next()
}))

app.listen({port: 3000, host: '127.0.0.1'})