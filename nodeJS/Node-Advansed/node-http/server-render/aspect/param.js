const url=require('url')

// querystring解析GET方法传递的参数
const querystring=require('querystring')

module.exports=async function(ctx,next){
    const {req}=ctx
    const {Query}=url.parse(`http://${req.headers.host}${req.url}`)
    ctx.params=querystring.parse(Query)
    console.log(Query);

    await next()
}