// async (ctx, next) => {
//   do ...
// }

class Interceptor {
  constructor () {
    this.aspects = []  // 拦截到的某种情况
  }

  use (fn) { // 注册拦截切面
    this.aspects.push(fn)
    return this
  }

  async run(context) { // 执行拦截切面
    const aspects = this.aspects // [A, B, C]

    // 将拦截到的切面包装秤一个洋葱模型
    const proc = aspects.reduceRight(function (a, b) {
      return async () => {
        await b(context, a)
      }
    }, () => Promise.resolve())

    try {
      await proc()
    } catch (error) {
      console.log(error);
    }

    return context
  }

}

module.exports = Interceptor




// function wait(ms) {
//   return new Promise((resolve) => {
//     setTimeout(resolve, ms)
//   })
// }

// const inter = new Interceptor()

// const task = function(id) {
//   return async (ctx, next) => {
//     console.log(`task ${id} begin`);
//     ctx.count++
//     await wait(1000)
//     console.log(`count: ${ctx.count}`);
//     await next()
//     console.log(`task ${id} end`);
//   }
// }

// inter.use(task(0))
// inter.use(task(1))
// inter.use(task(2))
// inter.use(task(3))
// inter.use(task(4))

// console.log(inter.aspects);

// inter.run({count: 0})