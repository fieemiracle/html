// // http动态服务
// const Router = require('./lib/middleware/router')
// const Server = require('./lib/server')

// const app = new Server()
// const router = new Router()

// app.use(async ({ res }, next) => {
//     res.setHeader('Content-Type', 'text/html')
//     res.body = '<h1>hello world</h1>'
//     await next()
//     // console.log(1);
// })

// // app.use(async ({ res }, next) => {
// //     console.log(2);
// //     await next()
// // })
// // app.use(async ({ res }, next) => {
// //     console.log(3);
// //     await next()
// // })

// app.use(router.all('*', async ({ res }, next) => {
//     res.setHeader('Content-Type', 'text/html')
//     res.body = '<h1>hello world</h1>'
//     console.log('all');
//     await next()
// }))
// app.use(router.get('/test/:courses/:lecture', async ({ req, res }, next) => {
//     res.setHeader('Content-Type', 'application/json')
//     res.body = req
//     console.log('get');
//     await next()
// }))

// app.listen({ port: 9090, host: '127.0.0.1' })

// const Server = require('./lib/server')
// const Router = require("./lib/middleware/router")

// const app = new Server()
// const router = new Router()


// app.use(async ({ res }, next) => {
//     res.setHeader('Content-Type', 'text/html')
//     res.body = '<h1>hello</h1>'
//     await next()
// })

// app.use(router.all('/test/:course/:lecture', async ({ route, res }, next) => {
//     res.setHeader('Content-Type', 'application/json')
//     res.body = route
//     await next()
// }))

// app.listen({ port: 9090, host: '127.0.0.1' })



const Server = require('./lib/server')
const Router = require('./lib/middleware/router.js')

const app = new Server()
const router = new Router()

app.use(router.all('*', async ({res}, next) => {
  res.setHeader('Content-Type', 'text/html')
  res.body = '<h1>Hello World</h1>'
  await next()
}))

app.use(router.all('/test/:course/:lecture', async ({route, res}, next) => {
  res.setHeader('Content-Type', 'application/json')
  res.body = route
  await next()
}))

app.listen({port: 9090, host: '127.0.0.1'})
