// http动态服务
const http = require('http');
const Interceptor = require('../interceptor');

module.exports = class {
    constructor() {
        const interceptor = new Interceptor()

        this.server = http.createServer(async (req, res) => {
            // 静态就是req.url不一样，直接通过if分支全部写全，比较麻烦
            await interceptor.run({ req, res })
            if (!res.writableFinished) {//响应还没结束
                let body = res.body || '200 OK'
                if (body.pipe) {//说明body一定是流类型
                    body.pipe(res)//流入响应流
                } else {//不是流类型

                    if (typeof body !== 'string' && res.getHeader('Content-Type') == 'application/json') {
                        body = JSON.stringify(body)
                    }
                    res.end(body)//这种方式输出给后端
                }
            }

        })

        this.interceptor = interceptor
    }

    listen(opts,cb=()=>{}){
        if(typeof opts==='number') opts={port:opts}
        opts.host=opts.host||'0.0.0.0'
        console.log(`服务已启动在http://${opts.host}:${opts.port}`);
        this.server.listen(opts,()=>cb(this.server))
    }

    use(aspects){
        return this.interceptor.use(aspects)
    }
}
// const server = http.createServer((req,res)=>{
//     // 静态就是req.url不一样，直接通过if分支全部写全，比较麻烦


// })

// server.listen(7171, () => {
//     console.log('服务已启动在', server.address());
// })