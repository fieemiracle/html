// 静态文件服务--小文件
const http = require('http');
const path = require('path');
const url = require('url');
const fs = require('fs');
const mime = require('mime');

const server = http.createServer((req, res) => {
    // 解析客户端所需要的资源地址
    // console.log(path.resolve(__dirname,path.join('www',url.fileURLToPath(`file:///${req.url}`))));
    // console.log(path.resolve(__dirname, path.join('www', req.url)));
    let filePath = path.resolve(__dirname, path.join('www', req.url));
    if (fs.existsSync(filePath)) {//路径所对应的文件存在
        const stats = fs.statSync(filePath)//fs.statSync(filePath)判断当前资源时文件还是路径
        const isDir = stats.isDirectory()//是文件还是路径

        if (isDir) {//是文件
            filePath = path.join(filePath, 'index.html')
        }

        const { ext } = path.parse(filePath)//文件名后缀
        res.writeHead(200, { 
            'Content-Type': mime.getType(ext) ,
            //设置强缓存
            'Cache-Control': 'max-age=86400'
        })

        // const content = fs.readFileSync(filePath)//直接读取文件资源，处理小文件性能更好
        // return res.end(content)//发送给客户端

        const fileStream = fs.createReadStream(filePath)//把文件读取成流类型，常用于大文件
        fileStream.pipe(res)//把文件流流入响应流对象中
        // if(ext=='.png'){
        //     res.writeHead(200,{'Content-Type': 'image/png'})
        // }else{
        //     res.writeHead(200, { 'Content-Type': 'text/html;charset=utf-8' })
        // }


    } else {
        res.writeHead(404, { 'Content-Type': 'text/html;charset=utf-8' });
        res.end('<h1>NOT FOUND</h1>')
    }

})
server.on('clientError', (err, socket) => {
    socket.end('HTTP/1.1 400 Bad Request\r\n\r\n')
})
server.listen(3030, () => {
    console.log('服务已启动', server.address());
})