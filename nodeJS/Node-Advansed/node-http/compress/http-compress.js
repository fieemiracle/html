// http模块创建web服务
const http = require('http')
const path = require('path')
const fs = require('fs')
const mime = require('mime')

// zlib可拿到浏览器支持的压缩算法
const zlib = require('zlib')

const server = http.createServer(function (req, res) {
    let filePath = path.resolve(__dirname, path.join('source', req.url))
    // console.log(req.url);//得到的是http://localhost:8080后面的路径，如果后面没有，则是/
    // console.log(filePath);
    if (fs.existsSync(filePath)) {//判断文件资源存不存在
        const stats = fs.statSync(filePath);//该路径对应的文件的详细信息
        if (stats.isDirectory()) {//路径本身指向的不是文件，而是文件夹
            filePath = path.join(filePath, 'index.html');
        }

        if (fs.existsSync(filePath)) {
            const { ext } = path.parse(filePath)
            const mimeType = mime.getType(ext)
            // console.log(ext);//.html
            // res.writeHead(200,{//响应头
            //     'Content-Type': mimeType,//读取文件后缀，如.html====>'text/html'
            //     // 设置强缓存
            //     'Cache-Control': 'max-age=86400',
            //     // 'Content-Encoding': 'deflate'
            // })

            const responseHeaders = {
                'Content-Type': mimeType,//读取文件后缀，如.html====>'text/html'
                // 设置强缓存
                'Cache-Control': 'max-age=86400',
                // 'Content-Encoding': 'deflate'
            }

            const acceptEncoding = req.headers['accept-encoding']//获取请求头支持的压缩算法
            console.log(acceptEncoding);

            const compress = /^(text|application)\//.test(mimeType)//以text||application开头的资源则进行压缩
            if (compress) {//压缩
                //以空格，空格的方式分割
                acceptEncoding.split(/\s*,\s*/).some((encoding) => {
                    if (encoding === 'gzip') {
                        responseHeaders['Content-Encoding'] = 'gzip';
                    }
                    if (encoding === 'deflate') {
                        responseHeaders['Content-Encoding'] = 'deflate';
                    }
                    if (encoding === 'br') {
                        responseHeaders['Content-Encoding'] = 'br';
                    }
                    return false
                })
                responseHeaders['Content-Encoding'] = 'deflate'
            }
            res.writeHead(200, responseHeaders)
            // 读取资源文件
            const fileStream = fs.createReadStream(filePath)
            const compressionEncoding = responseHeaders['Content-Encoding']
            if (compressionEncoding) {//存在属性
                let comp;
                if (compressionEncoding === 'gzip') {
                    comp = zlib.createGzip()
                } else if (compressionEncoding === 'deflate') {
                    comp = zlib.createDeflate()
                } else {
                    comp = zlib.createBrotliCompress()
                }

                // 浏览器压缩文件的三种方式Accept-Encoding: gzip, deflate, br
                fileStream.pipe(zlib.createDeflate()).pipe(res)
            } else {//不进行压缩
                fileStream.pipe(res)
            }


        }
    } else {
        res.writeHead(404, { 'Content-Type': 'text/html;charset=utf-8' });
        res.end('<h1>NOT FOUND</h1>')
    }
})

// 监听客户端错误
server.on('clientError', (err, socket) => {
    socket.end('http/1.1 400 Bad Request\n\n')
})

server.listen(8080, () => {
    console.log('服务已启动', server.address())
})