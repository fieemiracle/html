const http=require('http')//http模块创建http服务
const url=require('url')

const responseData={
    ID:'ducky',
    Name:'dddd',
    age:19
}
function toHTML(){
    return `<ul>
    <li><span>账号：</span><span>${responseData.ID}</span></li>
    <li><span>昵称：</span><span>${responseData.Name}</span></li>
    <li><span>注册时间：</span><span>${responseData.RegisterDate}</span></li>
  </ul>`
}
const server=http.createServer((req,res)=>{
    // console.log(req);
    const {pathname} =url.parse(`http://${req.headers.host}${req.url} `)
    console.log(pathname);
    if(pathname==='/'){
        const accept=req.headers.accept;
        if(req.method==='POST'||accept.indexOf('application/json')>=0){
            res.writeHead(200,{'Content-Type':'application/json'})
            // res.end('<h1>hello world</h1>')
            res.end(JSON.parse(responseData))
        }else{
            res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'})
            res.end(toHTML(responseData))
        }
        
    }else{
        res.writeHead(404,{'Content-Type':'text/html'})
        res.end('<h1>not found</h1>')
    }
})

server.on('clientError',(err,socket)=>{
    socket.end('HTTP/1.1 400 Bad Request\n\n')
})

server.listen(8080, () => {
    console.log('服务已启动在', server.address());
})