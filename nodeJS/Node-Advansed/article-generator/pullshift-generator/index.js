import { existsSync, mkdirSync, readFile, readFileSync, writeFileSync } from 'fs'
import { dirname,resolve } from 'path';
import { fileURLToPath } from 'url'

import { generate } from './libs/generator.js';
import { createRandomPicker } from './libs/random.js';
import moment from 'moment';


function loadCorpus(src) { // 取语料库中的内容
  const url = import.meta.url
  const path = resolve(dirname(fileURLToPath(url)),src)
  const data = readFileSync(path,{encoding: 'utf-8'})
  return JSON.parse(data)
}

const corpus = loadCorpus('corpus/data.json');
const pickTitle = createRandomPicker(corpus.title);
const title = pickTitle();
const article = generate(title, {corpus});
// console.log(`${title}\n\n  ${article.join('\n     ')}`);

// 保存到文件夹中
function saveCorpus(title,artcle){
    const outputDir=resolve(dirname('./'),'output');
    // const outputDir=resolve(dirname(fileURLToPath(import.meta.url)),'output');

    const time=moment().format('x');
    const outputFile = resolve(outputDir,`${title}  ${time}.txt`);
    
    if(!existsSync(outputDir)){//文件夹不存在
        // 生成文件夹
        mkdirSync(outputDir);
    }

    const text=`${title}\n\n        ${artcle.join('\n           ')}`;
    writeFileSync(outputFile, text);

    return outputFile;
}

saveCorpus(title,article)
