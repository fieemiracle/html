import { randomInt, createRandomPicker } from './random.js';

// 生成文章
export function generate(title, { corpus, min = 6000, max = 10000 }) {
    //  const pickFamous=createRandomPicker(corpus.famous);
    //  const pickBosh=createRandomPicker(corpus.bosh);
    //  pickFamous();
    //  pickBosh();

    const articleLength = randomInt(min, max);
    const { famous, bosh_before, bosh, conclude, said } = corpus;
    const [pickFamous, pickBosh, pickBoshBefore, pickConclude, pickSaid] = [famous, bosh_before, bosh, conclude, said].map((item) => {
        return createRandomPicker(item);
    })

    // 循环选取段落
    const article = [];
    let totalLength = 0;
    while (totalLength < articleLength ) {
        let section = '';
        let sectionLength = randomInt(200, 500);
        // 若当前段落字数小于段落长度或者当前段落不是以句号或者问号结尾，则继续循环
        while (section.length < sectionLength || /[。？]$/.test(section)) {
            const n = randomInt(0, 100);
            if (n < 20) {
                // 添加名人名言
                section += sentence(pickFamous, { said: pickSaid, conclude: pickConclude })
            } else if (n < 50) {
                // 添加前置废话+废话
                section = sentence(pickBoshBefore, { title }) + sentence(pickBosh, { title })
            } else {
                // 添加废话
                section = sentence(pickBosh, { title })
            }
        }
        article.push(section)
        totalLength += section.length
    }
    return article
    // sentence(pickFamous,{said:pickSaid,conclude:pickConclude});//一条名人名言
    // sentence(pickFamous,{title});//一条废话
}

function sentence(pick, replacer) {
    let ret = pick();
    for (const key in replacer) {
        ret = ret.replace(new RegExp(`{{${key}}}`, 'g'), replacer[key])
    }
    return ret;
}