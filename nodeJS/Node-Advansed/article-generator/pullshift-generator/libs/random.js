// 随机数
export function randomInt(min,max){
    const p=Math.random();
    return Math.floor(min*(1-p)+max*p);
}

// 文章随机长度
const articleLength=randomInt(3000,5000);

// 段落随机长度
const sectionLength=randomInt(200,500);

// 随机选择内容
// let lastPicker=null;
// export function randomPick(arr){
//     // let picked=null;
//     // do{
//     //     const index=randomInt(0,arr.length);
//     //     picked=arr[index];
//     // }while(picked===lastPicker);
//     // lastPicker=picked;
//     // return picked;

//     // 保证每次不取最后一个元素
//     const len=arr.length-1;
//     const index=randomInt(0,len);

//     // 将取值与最后一个元素交换，保证不重复
//     [arr[index],arr[len]]=[arr[len],arr[index]
// }

// 高阶函数:随机取一句话
export function createRandomPicker(arr) {
    arr=[...arr];
    function randomPick(){
        // 保证每次不取最后一个元素
        const len=arr.length-1;
        const index=randomInt(0,len);
        const picked=arr[index];
    
        // 将取值与最后一个元素交换，保证不重复
        [arr[index],arr[len]]=[arr[len],arr[index]];
        return arr[index];
    }
    randomPick();//抛弃第一次选择
    return randomPick;
}


