import {
    randomInt,
    createRandomPicker
  } from './random.js'
  
  export function generate(title, {
    corpus,
    min = 6000,
    max = 10000
  }) {
    // const pickFamous = createRandomPicker(corpus.famous)
    // const pickBosh = createRandomPicker(corpus.bosh)
    // pickFamous() // 随机抽取一条名人名言
    // pickBosh() // 随机抽取一条废话
  
    const articalLength = randomInt(min, max) // 文章目标字数
  
    const {
      famous,
      bosh_before,
      bosh,
      conclude,
      said
    } = corpus
    const [pickFamous, pickBosh, pickBoshBefore, pickConclude, pickSaid] = [famous, bosh, bosh_before, conclude, said].map((item) => {
      return createRandomPicker(item)
    })
    const article = []
    let totalLength = 0
    while (totalLength < articalLength) {
      let section = ''
      const sectionLength = randomInt(200, 500)
      // 如果当前段落小于段落长度，或者当前段落不是以句号或问号结尾
      while (section.length < sectionLength || /[。？]$/.test(section)) {
        const n = randomInt(0, 100)
        if (n < 20) {
          // 添加名人名言
          section += sentence(pickFamous, {
            said: pickSaid,
            conclude: pickConclude
          })
        } else if (n < 50) {
          // 添加前置废话
          section += sentence(pickBoshBefore, {
            title
          }) + sentence(pickBosh, {
            title
          })
        } else {
          // 添加废话
          section += sentence(pickBosh, {
            title
          })
        }
      }
      totalLength += section.length
      article.push(section)
    }
    return article
  }
  
  function sentence(pick, replacer) {
    let ret = pick()
    for (const key in replacer) {
      ret = ret.replace(new RegExp(`{{${key}}}`, 'g'), typeof replacer[key] === 'function' ? replacer[key]() : replacer[key])
    }
    return ret
  }


