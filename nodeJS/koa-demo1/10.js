// compose插件
const Koa = require('koa');
const app=new Koa();

const compose=require('koa-compose');

const logger=(ctx,next)=>{
    console.log(new Date());

    next();
}
const main=(ctx)=>{
    ctx.response.body= 'helloKity';
}

const middlewares=compose([logger,main]);
app.use(middlewares);
app.listen(3000,function(){
    console.log('start');
})