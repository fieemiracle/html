const Koa = require('koa')
var cors = require('koa-cors')
const app = new Koa()
app.use(cors())
const main = (ctx) => {
  if (ctx.request.accepts('application/xml')) {
    ctx.response.type = 'xml'
    ctx.response.body = '<data>hello world</data>'
  } else if (ctx.request.accepts('application/html')) {
    ctx.response.type = 'html'
    ctx.response.body = '<data>hello world</data>'
  } else if (ctx.request.accepts('application/json')) {
    ctx.response.type = 'json'
    ctx.response.body = '{data:hello world}'
  } else {
    ctx.response.type = 'text'
    ctx.response.body = 'hello world'
  }

}
app.use(main)
app.listen(3000, function (params) {
  console.log('项目已启动');
})