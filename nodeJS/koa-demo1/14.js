const Koa = require('koa');
const app=new Koa();

// 添加三个中间件
app.use(async (ctx,next)=>{
    console.log('<<one');
    await next();
    console.log('one>>');
})
app.use(async (ctx,next)=>{
    console.log('<<two');
    await next();
    console.log('two>>');
})
app.use(async (ctx,next)=>{
    console.log('<<three');
    await next();
    console.log('three>>');
})


app.listen(3000,function(){
    console.log('start');
})


// Koa 的洋葱模型指的是以 next() 函数为分割点，先由外到内执行 Request 的逻辑，再由内到外执行 Response 的逻辑。
// 通过洋葱模型，将多个中间件之间通信等变得更加可行和简单。其实现的原理并不是很复杂，主要是 compose 方法。