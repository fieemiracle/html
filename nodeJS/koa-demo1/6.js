// 优化5.js
const Koa = require('koa')
const app = new Koa()
const fs=require('fs') ;
// 路由
const router=require('koa-route')

const main = (ctx) => {
  ctx.response.body = 'hello'
}

const about=(ctx)=>{
    ctx.response.type='html';
    ctx.response.body='<a href="https://koa.bootcss.com/">About</a>'
    // ctx.response.body='<a href="/">About</a>'
}
const other=(ctx)=>{
    ctx.response.type='json';
    ctx.response.body=fs.createReadStream('./6.json')
}
app.use(router.get('/',main));
app.use(router.get('/about',about));
app.use(router.get('/other',other));
// 路由内部有中间件，不需要第二个参数next

app.listen(3000);