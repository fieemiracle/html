// path模块
const Koa = require('koa')
const app = new Koa();
const path=require('path');

const main=ctx=>{
    ctx.response.body=path.join(__dirname);//绝对路径
    ctx.response.body=path.join(__filename);//相对路径
}

app.use(main);
app.listen(3000,function(){
    console.log('start');
});