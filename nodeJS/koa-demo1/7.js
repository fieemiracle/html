const Koa = require('koa');
const app = new Koa();
const fs = require('fs');
const main = (ctx) => {
    ctx.response.type = 'html';
    ctx.response.body = fs.createReadStream('./7.html')
}
const details = (ctx) => {
    let date = new Date();
    ctx.response.body = `<div>${date.toUTCString()}</div>
    <div>${date.toLocaleString()}</div>
    <div>${date.toLocaleDateString()}</div> `;
   
}
// app.use(main);
app.use(details)
app.listen(3000, function () {
    console.log('项目已启动');
})