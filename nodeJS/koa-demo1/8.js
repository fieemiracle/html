const Koa = require('koa');
const app=new Koa();
const fs=require('fs');

// 中间件
const logger=(ctx,next)=>{//打印日志
    let date=new Date();
    console.log(new Date());
    console.log(date.toUTCString());

    next();//让代码逻辑进入下一个中间件函数
}

// 中间件:所有被app.use()掉的函数
const main=(ctx)=>{
    ctx.response.body='hello world!';
}
app.use(logger)
app.use(main);


app.listen(3000,function(){
    console.log('项目已启动,快去浏览器访问localhost:3000');
});
// 404错误码：找不到资源