// console.log('start');

// function foo(){
//     console.log('foo');
// }
// foo()

// setTimeout(()=>{
//     console.log('settimout1');
// },0)
// new Promise(resolve=>{
//     console.log('promise');
//     resolve()
// })
//     .then(()=>{
//         console.log('promise1');
//     })
//     .then(()=>{
//         console.log('promise2');
//     })
// console.log('end');

// new Promise(resolve => {
//     resolve();
// })
//     .then(() => {
//         new Promise(resolve => {
//             resolve();
//         })
//             .then(() => { console.log(111); })
//             .then(() => { console.log(222); });
//     })
//     .then(() => {
//         new Promise((resolve => {
//             resolve()
//         }))
//             .then(() => {
//                 new Promise((resolve) => {
//                     resolve()
//                 })
//                     .then(() => { console.log(333) })
//                     .then(() => { console.log(444) })
//             }).then(() => {
//                 console.log(555);
//             })
//     })
//     .then(() => {
//         console.log(666);
//     })

// new Promise(resolve => {
//     resolve();
// })
//     .then(() => {//第一层微任务队列 1
//         new Promise(resolve => {
//             resolve();
//         })
//             .then(() => { console.log(123); })
//             .then(() => { console.log(234); })//第二层微任务队列 2
//             console.log('///');
//     })
//     .then(() => {//第一层微任务队列 2
//         console.log(555);
//     })
//     .then(() => {//第一层微任务队列 3
//         new Promise(resolve => {
//             resolve();
//         })
//             .then(() => { console.log(345); })//第二层微任务队列 3
//             .then(() => { console.log(567); })//第二层微任务队列 4
//     })

// new Promise((resolve, reject) => {
//     // console.log("promise")
//     resolve()
// })
//     .then(() => {	// 执行.then的时候生成一个promise是给最后一个.then的
//         // console.log("then1")
//         new Promise((resolve, reject) => {
//             // console.log("then1promise")
//             resolve()
//         })
//             .then(() => {// 执行这个.then的时候，生成的promise是下面一个then的
//                 console.log("then1then1")
//             })
//             .then(() => {
//                 console.log("then1then2")
//             })
//     })
//     .then(() => {
//         // 这个
//         console.log("then2")
//     })

// let newPromise = Promise.resolve().then(res => {
//     // 无返回值
// })
// let newPromise2 = Promise.resolve().then(() => {
//     // 返回一个非promise对象
//     return "hello"
// })
// let newPromise3 = Promise.resolve().then(() => {
//     // 返回promise对象
//     return new Promise((resolve, reject) => {
//         resolve("bonjour")
//     })
// })
// console.log(newPromise, newPromise2, newPromise3)//Promise { <pending> } Promise { <pending> } Promise { <pending> }
// 每次调用then都会返回一个新创建的promise对象
// 情况①若then方法中含有return语句，且返回的是一promise对象，则该对象作为then返回的新对象；
// 情况②若返回的是一个非promise对象，则返回的新promise对象的promiseValue为该值；
// 情况③若无返回值则返回的新promise对象promiseValue为undefined。

//  只有返回promsie对象后才会链式调用
// Promise.resolve().then(res => { //then1
//     console.log('1')
//     new Promise((resolve, reject) => {
//         setTimeout(() => {
//             console.log('2')
//             resolve('newPromise')
//         }, 2000);
//     }).then(res => { // then2
//         console.log('3')
//         return "newPromise1"
//         // 返回了一个非promsie对象，对应2中的情况2
//     })
//     // 无return语句，对应2中的情况3 
// }).then(res => { // then3
//     console.log('4', res)
// })


// Promise.resolve().then(res => { //then1
//     console.log('1')
//     // then1中返回一个新的promise实例 只有当then1中返回的promise状态确定后才会进行下一步的链式调用 then3
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             console.log('2')
//             resolve('newPromise')
//         }, 2000);
//     }).then(res => { // then2
//         console.log('3')
//         return "newPromise1"
//     })
//     // 有return语句，且返回了一个promise对象，对象2中的情况1
// }).then(res => { // then3
//     console.log('4', res)
// })

new Promise(resolve => {
    resolve();
})
    .then(function () {
        new Promise(resolve => {
            resolve();
        })
            .then(function () {
                console.log("promise1-1");
            })
            .then(function () {
                console.log("promise1-2");
            })
        console.log("after promise1");
    })
    .then(function () {
        new Promise(resolve => {
            resolve();
        })
            .then(function () {
                console.log("promise2-1");
            })
            .then(function () {
                console.log("promise2-2");
            })


        console.log("after promise2");
    })
    .then(function () {
        new Promise(resolve => {
            resolve();
        })
            .then(function () {
                console.log("promise3-1");
            })
            .then(function () {
                console.log("promise3-2");
            })
        console.log("after promise3");
    });

