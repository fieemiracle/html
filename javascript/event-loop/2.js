console.log('start');

async function async1() {
    console.log(123);

    //   return Promise.resolve().then(()=>{
    //     return async2()
    //   }).then(()=>{
    //      return async3()
    //   })
    //   }).then(()=>{
    //     console.log('async1 end');
    //   })
    await async2();
    await async3();
    console.log('async1 end');//微任务
}

async function async2() {
    console.log('async2 end');
}

async1()

setTimeout(() => {
    console.log('setTimeout');
}, 0)

new Promise(resolve => {
    console.log('Promise');
    resolve()
})
    .then(() => {
        console.log('promise1');
    })
    .then(() => {
        console.log('promise2');
    })
console.log('end');

//start  Promise end async2 async1 promise1 promise2 setTimeout
//start  Promise end async2  promise1 promise2 setTimeout async1//老板浏览器
// 浏览器更新后，为await开辟特殊通道async await会立即执行
// start async2 Promise end async1  promise1 promise2 setTimeout 