// 常见的宏任务有：script（整体代码）/setTimout/setInterval/setImmediate(node 独有)/requestAnimationFrame(浏览器独有)/IO/UI render（浏览器独有）
// 常见的微任务有：process.nextTick(node 独有)/Promise.then()/Object.observe/MutationObserver


// 浏览器的事件循环
// 浏览器的事件循环由一个宏任务队列+多个微任务队列组成。
Promise.resolve().then(() => {
    console.log('第一个回调函数:微任务1')
    setTimeout(() => {
        console.log('第三个回调函数:宏任务2')
    }, 0)
})
setTimeout(() => {
    console.log('第二个回调函数:宏任务1')
    Promise.resolve().then(() => {
        console.log('第四个回调函数:微任务2')
    })
}, 0)
// 浏览器事件循环
// 第一个回调函数：微任务1
// 第二个回调函数：宏任务1
// 第四个回调函数：微任务2
// 第三个回调函数：宏任务2

// node的事件循环
// node的事件循环比浏览器复杂很多。由6个宏任务队列+6个微任务队列组成
// 宏任务
// Timers：定时器setTimeout/setInterval；
// Poll ：获取新的 I/O 事件, 例如操作读取文件等；
// Check：setImmediate回调函数在这里执行；

// 微任务
// process.nextTick;
// promise.then 等;

// 举例1 node的事件循环
console.log('Script开始')//1
setTimeout(() => {
    console.log('第一个回调函数,宏任务1')//3
    Promise.resolve().then(function () {
        console.log('第四个回调函数,微任务2')
    })
}, 0)
setTimeout(() => {
    console.log('第二个回调函数,宏任务2')//4
    Promise.resolve().then(function () {
        console.log('第五个回调函数,微任务3')
    })
}, 0)
Promise.resolve().then(function () {
    console.log('第三个回调函数,微任务1')
})
console.log('Script结束')//2
// Script开始
// Script结束
// 第三个回调函数,微任务1
// 第一个回调函数,宏任务1
// 第二个回调函数,宏任务2
// 第四个回调函数:宏任务2
// 第五个回调函数,微任务3


// 举例2 node的事件循环
console.log('Script开始')

setTimeout(() => {
    console.log('宏任务1:setTimeout')
    Promise.resolve().then(() => {
        console.log('微任务promise2')
    })
}, 0)

setImmediate(() => {
    console.log('宏任务2:setImmediate')
})

setTimeout(() => {
    console.log('宏任务3:setTimeout')
}, 0)

console.log('Script结束')

Promise.resolve().then(() => {
    console.log('微任务promise1')
})

process.nextTick(() => {
    console.log('微任务nextTick')
})
// Script开始
// Script结束
// 微任务nextTick
// 微任务promise1
// 宏任务1:setTimeout
// 宏任务3:setTimeout
// 宏任务2:setImmediate
// 微任务promise2


// 举例3
new Promise((resolve, reject) => {
    console.log(1);
    new Promise((resolve, reject) => {
        console.log(2);
        setTimeout(() => {
            resolve(3)
            console.log(4);
        });
    })
        .then(data => {
            setTimeout(() => {
                console.log(5);
            });
            console.log(data);
        })
    setTimeout(() => {
        resolve(6)
        console.log(7);
    }, 0);
})
    .then(data => {
        console.log(data, '---');
        setTimeout(() => {
            console.log(8);
        });
        console.log(9);
    })