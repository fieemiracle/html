// 浏览器事件循环
// 宏任务微任务分别在自己的队列，先进先出
// 回调函数进栈，先进后出
// 同步模式(Synchronous)和异步模式(Asynchronous)，目的是为了解决耗时任务阻塞执行的问题
// 一般异步任务指的都是ajax请求或者定时器
// 浏览器执行宏任务的次序：task(宏任务)-> rander(渲染)-> task(宏任务)
// 浏览器中的宏任务包括：1、setTimeout, setInterval --定时器
// 2、MessageChannel          --消息通道
// 3. postMessage             --消息通信机制
// 4. setImmediate            --立即定时器，只在IE浏览器可以实现
// 浏览器中的微任务包括：1、promise.then            --Promise的then方法
// 2、async await             --async函数的await之后的内容
// 3. MutationObserver        --监控dom变化，dom变化了就会执行，时间节点是等待所有任务执行完毕再进行监控

// 先执行同步代码，再执行微任务，再检查宏任务是否到达时间，到达时间再执行

// 1
// let name = 'yd';
// setTimeout(function() {
//     name = 'zd';
//     console.log(name);
// }, 1000);
// console.log(name);
//yd
//zd

// 2 消息通道，兼容性不太好
// const channel = new MessageChannel();
// // 在端口号上添加消息, 发送消息
// channel.port1.postMessage('我爱你');
// // 注册接收事件, 后绑定的接收函数，还是可以接收的到，所以可以看出是异步执行
// channel.post2.onmessage = function(e) {
//     console.log(e.data);
// };
// console.log('hello'); // 先走的hello，后出现我爱你.

// 3 dom监控
// const observer = new MutationObserver(() => {
//     console.log('节点已经更新');
//     console.log(document.getElementById('app').children.length);
// });
// observer.observe(document.getElementById('app'), {
//     'childList': true,
// });
// for (let i = 0; i < 20; i++) {
//     document.getElementById('app').appendChild(document.createElement('p'));
// }
// for (let i = 0; i < 20; i++) {
//     document.getElementById('app').appendChild(document.createElement('span'));
// }

// 4
// setTimeout(() => {
//     console.log('timeout');
// }, 0);

// Promise.resolve().then(data => {
//     console.log('then');
// });

// console.log('start');
// start
// then
// timeout


// 5
// setTimeout(() => {
//     console.log('timeout1');
//     Promise.resolve().then(data => {
//         console.log('then1');
//     });
// }, 0);

// Promise.resolve().then(data => {
//     console.log('then2');
//     setTimeout(() => {
//         console.log('timeout2');
//     }, 0);
// });
// then2
// timeout1
// then1
// timeout2

// 6
// const p = new Promise(function(resolve, reject){
//     reject();
//     resolve();
// });
// p.then(function() {
//     console.log('成功');
// }, function() {
//     console.log('失败');
// });//失败，promise只会打印一种情况

// 7
// const promise = new Promise((resolve, reject) => {
//     console.log(1);
//     resolve();
//     console.log(2);
// });
// promise.then(() => {
//     console.log(3);
// });
// new Promise传入的函数是同步代码，立刻就会被执行.当代吗自行结束，会清空微任务1队列
// 1 2 3

// 8
Promise.resolve(1)
    .then(res => 2)
    .catch(err => 3)
    .then(res => console.log(res));//返回的是resolve，所以会走resolve  2

// 9
Promise.resolve(1)
    .then((x) => x + 1)
    .then(x => { throw new Error('My Error') })
    .catch(() => 1)
    .then(x => x + 1)
    .then(x => console.log(x))
    .catch(console.error);


