// 1 定时器
// let name = 'yd';
// setTimeout(function() {
//     name = 'zd';
//     console.log(name);
// }, 0);
// console.log(name);

// 2 Promise
// const myPromise = new Promise((resolve, reject) => {
//     console.log('start');
//     resolve('loading')
//     console.log('next');
// })
//     .then(result => { console.log(result); })

// 3 Async-await
// async function async1() {
//     console.log(123);
//     //   return Promise.resolve().then(()=>{
//     //     return async2()
//     //   }).then(()=>{
//     //      return async3()
//     //   })
//     //   .then(()=>{
//     //     console.log('async1 end');
//     //   })

//     await async2();
//     await async3();
//     console.log('async1 end');//微任务
// }
// async function async2() {
//     console.log('async2 end');
// }
// async function async3() {
//     console.log('async3 end');
// }
// async1();
// setTimeout(() => {
//     console.log('setTimeout');
// }, 0)

// 4
// setTimeout(() => {
//     console.log('timeout1');
//     Promise.resolve().then(data => {
//         console.log('then1');
//     });
// }, 0);

// Promise.resolve().then(data => {
//     console.log('then2');
//     setTimeout(() => {
//         console.log('timeout2');
//     }, 0);
// });

// 6
// console.log('start');//1

// async function async1() {
//     console.log(123);//2
//     await async2();
//     await async3();
//     console.log('async1 end');
// }
// async function async2() {
//     console.log('async2 end');//3
// }
// async function async3() {
//     console.log('async3 end');
// }

// async1();

// setTimeout(() => {
//     console.log('setTimeout');
// }, 0)

// new Promise(resolve => {
//     console.log('Promise');
//     resolve()
// })
//     .then(() => {
//         console.log('promise1');
//     })
//     .then(() => {
//         console.log('promise2');
//     })
// console.log('end');

// new Promise(resolve => {
//     resolve();//
//     console.log(123);
// })
//     .then(() => {
//         new Promise(resolve => {
//             resolve();//
//         })
//             .then(() => { console.log(123); })
//             .then(() => { console.log(234); })
//     })
//     .then(() => {
//         console.log(555);
//     })
//     .then(() => {
//         new Promise(resolve => {
//             resolve();
//         })
//             .then(() => { console.log(345); })
//             .then(() => { console.log(567); })
//     })

// let myPromise=new Promise((resolve,reject) => {
//     console.log('promise1');
//     setTimeout(() => {
//         resolve();
//         console.log(123);
//     })
// })
//     .then(() => { 
//         console.log(234); 
//     })
//     .then(()=>{console.log('error');})

Promise.resolve().then(() => {
    console.log('promise1');
    setTimeout(() => {
        console.log('timeout1')
    }, 0)
})

new Promise(resolve => {
    resolve();
})
    .then(function () {
        new Promise(resolve => {
            resolve();
        })
            .then(function () {
                console.log("promise2");
            })
            .then(function () {
                console.log("promise3");
            })
        console.log("then1");
    })
    .then(function () {
        new Promise(resolve => {
            resolve();
        })
            .then(function () {
                console.log("promise4");
            })
            .then(function () {
                console.log("promise5");
            })


        console.log("then2");
    })
    .then(function () {
        new Promise(resolve => {
            resolve();
        })
            .then(function () {
                console.log("promise6");
            })
            .then(function () {
                console.log("promise7");
            })
        console.log("then3");
    });

setTimeout(() => {
    console.log('timeout2')
    Promise.resolve().then(() => {
        console.log('promise8')
    })
}, 0)

