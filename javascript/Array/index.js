// 数组创建
const myArray=[1,2,3];
// const myArray1=new Array().fill(1);//[]
const myArray1=new Array(3).fill(1);//[1,1,1]
console.log(myArray1[1]);

// 遍历数组
// forEach()没有返回值，map（）有返回值
myArray1.forEach((item,index,value)=>{
    console.log(item,index,value);
});
let newArray=myArray1.map((item,index,value)=>{
    // console.log(item,index,value);
    return item==='b';
});
console.log(newArray);

// 矩阵 --二维数组
const herArray=[
    [1,2,3],
    [2,3,4],
    [3,4,5]
];

const hisArray=new Array(7).fill([]);
hisArray[0][0]=1;
console.log(hisArray);//[[1],[1],[1],[1],[1],[1],[1]]

const itsArray=new Array(7);
for(let i=0;i<itsArray.length;i++){
    itsArray[i].push('[]');
}
console.log(itsArray);

for(let i=0;i<herArray.length;i++){
    for(let j=0;j<herArray[i].length;j++){
        console.log(arr[i][j]);
    }
}
