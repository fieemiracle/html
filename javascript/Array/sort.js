// 数组排序

// sort() 方法以字母顺序对数组进行排序
var fruits = ["Banana", "Orange", "Apple", "Mango"];
console.log(fruits.sort());           // [ 'Apple', 'Banana', 'Mango', 'Orange' ]

// reverse() 方法反转数组中的元素
console.log(fruits.reverse());      //[ 'Orange', 'Mango', 'Banana', 'Apple' ]

// 比值函数对数组进行排序
// 比较函数的目的是定义另一种排序顺序。
// 比较函数应该返回一个负，零或正值，这取决于参数：
// 当 sort() 函数比较两个值时，会将值发送到比较函数，并根据所返回的值（负、零或正值）对这些值进行排序
var nums=[1,3,2,4,6,5,7,9,8,0];
var result=nums.sort((a,b)=>(a-b));//升序排序
console.log(result);

var res=nums.sort((a,b)=>(b-a));//降序排序
console.log(res);
