// 数组迭代--对每个数组项进行操作
// 1、Array.forEach(value,index,array),参数分别是项目值value、项目索引index、数组本身array
// （1）不会更改原始数组
const myArray=[1,2,3,4,5];
let alterArray=myArray.entries(value=>{
  value = value*2;
})
console.log(myArray);//[ 1, 2, 3, 4, 5 ]
console.log(alterArray);//Object [Array Iterator] {}

// 2、Array.map(value,index,array),参数分别是项目值value、项目索引index、数组本身array
// （1）通过对每个数组元素执行函数来创建新数组
// （2）不会对没有值的数组元素执行函数
// （3）不会更改原始数组
let alterArray1=myArray.map(value=>{
    return value*2;
});
console.log(myArray);//[ 1, 2, 3, 4, 5 ]
console.log(alterArray1);//[ 2, 4, 6, 8, 10 ]

// 3、Array.filter(value,index,array),参数分别是项目值value、项目索引index、数组本身array
// （1）创建一个包含通过测试的数组元素的新数组
// （2）不会更改原始数组
let alterArray2=myArray.filter(value=>{
    return value%2==0;
});
console.log(myArray);//[ 1, 2, 3, 4, 5 ]
console.log(alterArray2);//[ 2, 4 ]

// 4、Array.reduce(total, value, index, array)，除了参数total:总数（初始值/先前返回的值）,其余参数同Array.forEach()
// （1）在每个数组元素上运行函数，以便生成（减少它）单个值
// （2）在数组中从左到右工作
// （3）不会减少原始数组
// （4）可接受一个初始值
let alterArray3=myArray.reduce((total,value)=>{
    return total+value;
});
console.log(myArray);//[ 1, 2, 3, 4, 5 ]
console.log(alterArray3);//15

let alterArray4=myArray.reduce((total,value)=>{
    return total+value;
},100);
console.log(alterArray4);//115

// 5、Array.reduceRight(total, value, index, array)，参数同Array.reduce()
// （1）在每个数组元素上运行函数，以便生成（减少它）单个值
// （2）在数组中从右到左工作
// （3）不会减少原始数组
let alterArray5=myArray.reduce((total,value)=>{
    return total+value;
});
console.log(myArray);//[ 1, 2, 3, 4, 5 ]
console.log(alterArray5);//15

// 6、Array.every(value, index, array)，参数同Array.forEach()
// （1）检查所有数组是否通过测试
let alterArray6=myArray.every((total,value)=>{
    return value%2==1;
});
console.log(alterArray6);//false

// 7、Array.some(value, index, array)，参数同Array.forEach()
// （1）检查某些数组值是否通过测试
let alterArray7=myArray.some((total,value)=>{
    return value%2==1;
});
console.log(alterArray7);//true

// 8、Array.indexOf(item, start)，参数item：必需。要检索的项目；start：可选。从哪里开始搜索。负值将从结尾开始的给定位置开始，并搜索到结尾。
// （1）如果未找到项目，Array.indexOf() 返回 -1。
// （2）如果项目多次出现，则返回第一次出现的位置。
let alterArray8=myArray.indexOf(10);
console.log(alterArray8);//-1   

// 9、Array.lastIndexOf(item, start)
// （1）item  必需。要检索的项目。
// （2）start 可选。从哪里开始搜索。负值将从结尾开始的给定位置开始，并搜索到开头。
let alterArray9=myArray.indexOf(10);
console.log(alterArray9);//-1  

// 10、Array.find(value, index, array)，参数同Array.entries()
// （1）返回通过测试函数的第一个数组元素的值
let alterArray10=myArray.find(value=>{
    return value>3;
});
console.log(alterArray10);//4

// 11、Array.findIndex(value, index, array)，参数同Array.entries()
// （1）返回通过测试函数的第一个数组元素的索引
let alterArray11=myArray.findIndex(value=>{
    return value>3;
});
console.log(alterArray11);//3