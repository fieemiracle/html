// 数组方法

// toString() 把数组转换为数组值（逗号分隔）的字符串。
var fruits = ["Banana", "Orange", "Apple", "Mango"];
console.log( fruits.toString());

// join() 方法也可将所有数组元素结合为一个字符串。行为类似 toString()，但是还可以规定分隔符
console.log(fruits.join(" * ")); 

// pop() 方法从数组中删除最后一个元素并返回该值
console.log(fruits.pop());//Mango

// push() 方法（在数组结尾处）向数组添加一个新的元素,并返回新数组的长度：
console.log(fruits.push("huanglixin"));//4

// shift() 方法会删除首个数组元素，并把所有其他元素“位移”到更低的索引
console.log(fruits.shift());//Banana

//unshift() 方法（在开头）向数组添加新元素，并“反向位移”旧元素

// 既然 JavaScript 数组属于对象，其中的元素就可以使用 JavaScript delete 运算符来删除
console.log(delete fruits[1]);//fruits[1]=undefined  true

// splice() 方法可用于向数组添加新项
// 第一个参数（2）定义了应添加新元素的位置（拼接）。
// 第二个参数（0）定义应删除多少元素。
// 其余参数定义要添加的新元素。
// splice() 方法返回一个包含已删除项的数组
fruits.splice(2,0,"1","2");
console.log(fruits);

// 能够使用 splice() 在数组中不留“空洞”的情况下移除元素
// 第一个参数（0）定义新元素应该被添加（接入）的位置。
// 第二个参数（1）定义应该删除多个元素。
// 其余参数被省略。没有新元素将被添加

// concat() 方法通过合并（连接）现有数组来创建一个新数组
// concat() 方法不会更改现有数组。它总是返回一个新数组。
// concat() 方法可以使用任意数量的数组参数
// concat() 方法也可以将值作为参数

// slice() 方法用数组的某个片段切出新数组。
// 比如从数组元素 1 （"Orange"）开始切出一段数组
const person=["kiki"];
var arr=fruits.concat(person);
console.log(arr);

console.log(fruits.slice(3));

// 自动 toString() 如果需要原始值，则 JavaScript 会自动把数组转换为字符串
console.log(fruits.toString());