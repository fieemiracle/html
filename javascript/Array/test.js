// const arr=[1,2,3,4,5]
// console.log(arr.reverse());//[ 5, 4, 3, 2, 1 ]

// const strArr=['apple','pear','peach','watermelon','strawberry']
// console.log(strArr.reverse());//[ 'strawberry', 'watermelon', 'peach', 'pear', 'apple' ]

// // 一个参数时，比如从数组元素 （"myself"）开始切出一段数组
// const person=['grandfather','grandmother','father','mother','brother','sister','myself','son','dauther'];
// console.log(person.slice(person.indexOf('myself')));//从起点开始切到最后 [ 'myself', 'son', 'dauther' ]
// console.log(person.slice(-2));//反方向切2个元素  [ 'son', 'dauther' ]
// console.log(person);//slice() 方法创建新数组，不改变原数组

// // 两个参数时，比如从数组元素“grandfather”切到“myself”
// console.log(person.slice(person.indexOf('grandfather'),person.indexOf('myself')));//切左不切右
// //[ 'grandfather', 'grandmother', 'father' ,'mother','brother','sister']


// // 第一个参数:添加新元素的位置（拼接）。
// // 第二个参数:删除多少元素。
// // 其余参数:要添加的新元素。
// // splice() 方法返回一个包含已删除项的数组
// const myArr=['you','are','a','big','banana','!']
// // 现在在"are"后面添加"not"
// myArr.splice(2,0,'not')
// console.log(myArr);//会改变原数组   [ 'you', 'are', 'not', 'a', 'big', 'banana', '!' ]

// // 能够使用 splice() 在数组中不留空洞的情况下移除元素
// // 第一个参数:新元素应该被添加（接入）的位置。
// // 第二个参数:删除几个元素。
// // 现在删除末尾的感叹号"!"
// console.log(myArr.splice(myArr.length-1,1));//返回被删除的元素 [ '!' ]
// console.log(myArr);//会改变原数组  [ 'you', 'are', 'not', 'a', 'big', 'banana' ]

// // toString() 把数组转换为数组值（逗号分隔）的字符串。
// var fruits = ["Banana", "Orange", "Apple", "Mango"];
// console.log( fruits.toString());//Banana,Orange,Apple,Mango

// // join() 方法也可将所有数组元素结合为一个字符串。行为类似 toString()，但是可以规定分隔符
// console.log(fruits.join("")); //BananaOrangeAppleMango

// // Array.from(),将其他类型转为数组
// var str='abcdef';
// console.log(Array.from(str));//[ 'a', 'b', 'c', 'd', 'e', 'f' ]

let arr=[11,54,23,45,67,12,34];
arr=arr.sort((a,b)=>a-b);//升序 [11,12,23,45,54,67]
// arr=arr.sort((a,b)=>b-a);//降序 [67, 54, 45, 34,23, 12, 11]
console.log(arr);