// 连接特定的 DNS 后缀 . . . . . . . :ipconfig
//    IPv6 地址 . . . . . . . . . . . . : 240e:370:d52:2aa1::2ab
//    IPv6 地址 . . . . . . . . . . . . : 240e:370:d52:2aa1:6942:61de:be72:f34c
//    临时 IPv6 地址. . . . . . . . . . : 240e:370:d52:2aa1:c1c:3579:a6e5:a5ea
//    本地链接 IPv6 地址. . . . . . . . : fe80::6942:61de:be72:f34c%13
//    IPv4 地址 . . . . . . . . . . . . : 192.168.31.28
//    子网掩码  . . . . . . . . . . . . : 255.255.255.0
//    默认网关. . . . . . . . . . . . . : fe80::5264:2bff:fe33:7cc%13
//                                        192.168.31.1

// 找重复次数
var lines = ['192.168.3.1', '192.118.1.1', '192.138.8.1', '192.168.3.1', '192.168.3.1'];
var max=1;
var res=null;
function findAgain(array) {
    var obj = {};
    for (let i = 0; i < array.length; i++) {
        if (!obj[array[i]]) {
            obj[array[i]] = 1;
        }else{
            obj[array[i]] ++;
            if(obj[array[i]]>max){
                max=obj[array[i]];
                res=array[i];
            }
        }
    }
    console.log(res); 
    // return res;
}
// let result=findAgain(lines);
// console.log(result);
findAgain(lines);


// 两数相加
var nums=[2,7,11,15];
var target=9;
var twoSum=function(nums,target){
    for(let i=0;i<nums.length;i++){
        if(target-nums[i]===0){
            let j=target-nums[i];
            var search=nums.indexOf(j,i+1);
            if(search!==-1){
                return [i,search];
            }
        }
    return [null,null]
    }
}
let res1=twoSum(nums,target);
console.log(res1);

// 找中位数
var arr=[4,5,1,2,3,6,7,2,8];
function findMid(array){
    let mid=array.length/2;
    if(array.length/2==0){
        array.sort((a,b)=>(a-b));
        return array[mid];
    }else{
        array.sort((a,b)=>(a-b));
        return array[Math.floor(mid)];
    }
}
let result1=findMid(arr);
console.log(result1);