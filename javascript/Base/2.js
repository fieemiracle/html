// Math 数学对象
console.log(Math.min(2,6));
console.log(Math.max(2,6));
console.log(Math.max('4','6'));

console.log(Math.abs(-23.5));

console.log(Math.ceil(-23.5));//向上取整-23
console.log(Math.ceil(23.4));//向上取整24

console.log(Math.floor(23.4));//向下取整.23
console.log(Math.floor(-23.4));//向下取整,-24

console.log(Math.round(-23.5));//四舍五入,-22

console.log(Math.random()*(20-10)+10);//(10,20)之间随机数
console.log(Math.random()*(30-15)+15);//（15,30）之间随机数

console.log(Math.pow(2,3));//幂次方，8

console.log(Math.sqrt(16));//开根