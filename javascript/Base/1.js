// 判断语句
// if
var i = 10;
if (i > 10) {
    console.log('1');
}
else if (i < 10) {
    console.log('2');
} else {
    console.log('none');
}

// switch
var a = 3;
switch (a) {
    case 1:
        console.log('yes');
        break;

    case 2:
        console.log('no');
        break;

    case 3:
        console.log('either');
        break;
    default:
        console.log('none');

}

// for
var arr=[1,2,3,5,4,7,6,5];
for(let i=0;i<arr.length;i++){
    if(arr[i]===5){
        continue;//跳出当前循环，去到下次循环
        // break 跳出整个循环
    }else{
    arr[i]=arr[i]+'a';
    }
}
console.log(arr);

// while
var i=9;
while(i<12){
    console.log('hello world');
    i++;
}