// 词法作用域
// eval（）
function foo(str,a){
    'use strict'//严格模式
    eval(str)//欺骗
    console.log(a,b);//1 4
}
var b=2;
foo('var b=4',1);

// with
let obj={
    a:1,
    b:2,
    c:3
}
// 让obj里面的值加1
with(obj){
    a:2,
    b:3,
    c:3
}
console.log(obj);

function bug(){
    with(obj){
        a=2
    }
}
var obj1={
    a:3
}
var obj2={
    b:4
}
bug(obj1)
console.log(obj2.a);//undefined
console.log(a);//2 