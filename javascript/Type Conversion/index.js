// 类型转换：
// 指将一个数据类型强制转换为其他的数据类型
// 类型转换主要指，将其他的数据类型，转换为：
// String,Number,Boolean
// 1、将其他的数据类型转换为String
// 方式一：调用被转换数据类型的toString()方法
//         该方法不会影响到原变量，它会将转换的结果返回
//         但是注意，null  undefined这两个值没有toString()方法
// 方法二：使用String()函数，并将被转换的数据作为参数传递递给函数
//         使用toString()函数类型强制类型转换时，
//         对于Number和Boolean实际上就是调用的toString()方法
//         但是对于null和undefined，就不会调用toString()方法
//         它会将null直接转换为“null”        
//         将undefined直接转换为“undefined”

// 2、 将其他类型转换为Number
// 方法一：使用Number()函数
//     字符串->数字    
//     1、如果纯数字的字符串，则直接将其转换为数字
//     2、如果字符串中有非数字的内容，则转换为NaN
//     3、如果字符串是一个空串或者是一个全是空格的字符串,则转换为0
//     布尔->数字
//     true 转成1
//     false 转成0
//     null  转成0
//     undefined 转成NaN
// 方法二：parseInt(),parseFloat()
//     这种方式专门用来对付字符串
//     parseInt()把一个字符串转换为一个整数
//     parseFloat()把一个字符串转换为一个浮点数
//     -如果对非String使用parseInt()或parseFloat()，把各种类型的值转十进制
//     它会先将其转换为String,然后在操作
// 3、其他进制的数字
// 1、八进制数字：0开头
// 2、二进制数字：0b开头
// 3、十六进制数字：0x开头
// 4、将其他数据类型转换为Boolean
// 方法一：使用Boolean()函数
//     -数字->布尔
//         -除了0和NaN,其余都是true
//     -字符串->布尔
//         -除了空串，其余都是true
//     -null和undefined都会转换为false
//     -对象也会转换为true
// 方法二：隐式类型转换
//     -为任意的数据类型做两次非运算，既可将其转换为布尔值
//     -例子：
//         var a="hello"
//         a=!!a;

var a=123;
//  调用a的toString()方法，即调用x的y()方法，就是x.y()
//    var b= a.toString();
//    console.log(typeof b);
//    console.log(b);
a=a.toString();
console.log(typeof a);
console.log(a);
// var b=null;
var b=undefined;
b=String(b);
console.log(typeof b);
console.log(b);
var c="";
c=Number(c);
console.log(typeof c);
console.log(c);

var d=true;
d=parseInt(d);
console.log(typeof d);
console.log(d);
var e=070;
e=parseInt(e,8);
// 可以在parseInt()中传递一个第二个参数，来指定数字的进制
var f=123;
f=Infinity;
f=Boolean(f);
console.log(typeof f);
console.log(f);