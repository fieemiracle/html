1、一元操作符
当+运算作为一元操作符时，会调用ToNumber()处理该值
2、二元操作符
v1+v2
（1）lprim=ToPrimitive(v1)
（2）rprim=ToPrimitive(v2)
（3）如果lprim或者rprim是字符串，那么返回ToString(Lprim)和ToString(rprim)的拼接结果
（4）返回ToNumber(Lprim)和ToNumber(rprim)的相加结果
3、二元运算符
（1）如果x和y是同一类型：a.x是undefined,返回true
                       b.x是null,返回true
                       c.x是数字，x为NaN,返回false
                       d.x和y指向同一个对象（内存地址），返回true:let x={};let y=x;x==y  =>true;否则返回false:{}=={} =>false;[]==[]  =false

（2）如果x和y不是同一类型：a.null==undefined =>true
                         b.x是String类型，ToNumber(x)
                         c.x是Boolean类型,ToNumber(x)
                         d.x不是String/Number,y是Object  true=={a:123}  =>false