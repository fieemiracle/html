// 将其他数据类型转换为Boolean
// 方法一：使用Boolean()函数
//     -数字->布尔
//         -除了0和NaN,其余都是true
//     -字符串->布尔
//         -除了空串，其余都是true
//     -null和undefined都会转换为false
//     -对象会转换为true
// 方法二：隐式类型转换
//     -为任意的数据类型做两次非运算，既可将其转换为布尔值
//     -例子：
//         var a="hello"
//         a=!!a;

// 使用Boolean()函数转换，结果为false的六种情况
// 规律：在js中，只有 0 、NaN 、空字符串、null 、undefined 和false(布尔值本身)这个6个值转换成布尔为false，其余都转换为true
// console.log(Boolean(0));
// console.log(Boolean(''));
// console.log(Boolean(null));
// console.log(Boolean(undefined));
// console.log(Boolean(NaN));
// console.log(Boolean(false));

// 隐式类型转换，为任意的数据类型做两次非运算
// let a='active';
// !:取反  先把其他数据类型转换为布尔类型，再取反
// b=!a;
// console.log(b);
// console.log(typeof b);
// !!:取两次反，等价于没有取反，直接转换为布尔类型
// c=!!a;
// console.log(c);
// console.log(typeof c);


// 将其他数据类型转换为数字
// console.log(Number('11'));
// 方法一：使用Number()函数
// 规则:使用Number()函数强制转换，实际上是调用内部的ToNumber(value)方法
//     字符串->数字    
//     1、如果纯数字的字符串，则直接将其转换为数字
//     2、如果字符串中有非数字的内容，则转换为NaN
//     3、如果字符串是一个空串或者是一个全是空格的字符串,则转换为0
//     布尔->数字
//     1、true 转成1
//     2、false 转成0
//     此外
//     null  转成0
//     undefined 转成NaN
// console.log(Number('123'));
// console.log(Number('hello world'));
// console.log(Number(''));
// console.log(Number('   '));
// console.log(Number(true));
// console.log(Number(false));
// console.log(Number(null));
// console.log(Number(undefined));
// null和undefined区别：
// 转换为数字后的不同，null转换数字是0，undefined转换数字是NaN
// null一般是意料之中的没有，暂时没有，使用时一般先手动赋值为null，后面使用的时候再次会赋值
// undefined 不是人为手动控制的，大部分都是浏览器自主为空，后面可以赋值也可以不复制

// 方法二：parseInt(),parseFloat()
    // 这种方式专门用来对付字符串
    // parseInt()把一个数字字符串转换为一个整数
    // parseFloat()把一个字符串转换为一个浮点数
    // -如果对非String使用parseInt()或parseFloat()，可以在parseInt()中传递一个第二个参数，来指定数字的进制把各种类型的值转十进制
    // 它会先将其转换为String,然后再操作
// console.log(parseInt('123'));
// console.log(parseInt(70,8));//56
// console.log(parseInt(070));//70是8进制，转换成十进制=>56
// console.log(parseInt(070,8));

// 将其他数据类型转换为字符串
// 方式一：调用被转换数据类型的toString()方法
//         该方法不会影响到原变量，它会将转换的结果返回
//         但是注意，null  undefined这两个值没有toString()方法
// console.log((123).toString());
// console.log((true).toString());
// console.log(({}).toString());//'[object Object]'
// console.log((null).toString());//TypeError: Cannot read property 'toString' of null
// console.log((undefined).toString());//TypeError: Cannot read property 'toString' of undefined

// 方法二：使用String()函数，并将被转换的数据作为参数传递递给函数
// 规则:使用String()函数强制转换，实际上是调用内部的ToString()方法
//         使用String()函数类型强制类型转换时，
//         对于Number和Boolean实际上就是调用的toString()方法
//         但是对于null和undefined，就不会调用toString()方法
//         它会将null直接转换为“null”        
//         将undefined直接转换为“undefined”
// console.log(String(true));
// console.log(String(190));
// console.log(String(null));
// console.log(String(undefined));


// 将原始数据类型转换为对象
// String(),Number(),Boolean()包装类
// [[PrimitiveValue]]
// let string=new String('hello')
// console.log(typeof string,string);
// let number=new Number(123)
// console.log(typeof number,number);
// let boolean=new Boolean(false)
// console.log(typeof boolean,boolean);

// 对象转布尔值：结果都为TRUE
// console.log(Boolean({}));
// console.log(Boolean([]));
// console.log(Boolean(Function));
// console.log(Boolean(new Boolean(false)));

// 对象转数字和字符串
// toString()(),valueOf()对象都可以使用的方法，可以吧对象往原始类型转换
// let obj={a:'name'}
// Object.prototype.toString.call(obj)
// obj.toString()

// 规则：内部启用toPrimitive(input,String/Number)方法
// String(input)
// Object=>toPrimitive(input,String)
// console.log(String(obj));
// let obj=new String('hello')
// String(obj)
// console.log(obj.toString());
// console.log(typeof obj);



// ToPrimitive(input,PreferredType),PreferredType:String/Number

// PreferredType不存在,input是Date类型,相当于PreferredType==String
// 对象转原始类型一定会调用ToPrimitive(obj,Number/String)方法，
// ToPrimitive(obj,Number)
// 1、如果obj是基本类型，直接返回
// 2、否则，调用valueOf()方法，如果得到一个原始类型，则返回
// 3、否则，调用toString()方法，如果得到一个原始类型，则返回
// 4、否则，报错
// Number({})//NaN
// ({}).valueOf()
// ({}).toString()
// Number(new Date(2022,7,11))//时间戳
// Number([])//0

// ToPrimitive(obj,String)
// 1、如果obj是基本类型，直接返回
// 2、否则，调用toString()方法，如果得到一个原始类型，则返回
// 3、否则，调用valueOf()方法，如果得到一个原始类型，则返回
// 4、否则，报错

// let obj={
//     name:'duck'
// }
// [object Object]

// let data={
//     'name':'ducky'
// }
// JSON.stringify()
// JSON.parse(JSON.stringify(data))
var obj3={name:"Lucky",age:45,gender:"man"};
console.log(JSON.stringify(obj3));

var json='{"name":"Lucky","age":89,"gender":"woman"}';