// 浅拷贝：会受拷贝对象（本体）的影响
// 1、对象的赋值
// let a={
//     name:'lucky'
// }
// let b=a;
// a.name='dog'
// console.log(b.name);//dog

// 2、Object.assign()
// let person={
//     name:'lucky',
//     // 如果这个对象里面还有一个属性是对象，那么会深拷贝
//     hobby:{
//         like:'running'
//     }
// }
// let copyPerson=Object.assign({},person)
// person.name='mouse'
// person.hobby.like='singing'
// console.log(copyPerson);//{ name: 'lucky', hobby: { like: 'singing' } }

// 扩展运算符
// let arr=['name', 'hobby', 'person',{age:19}]
// let copyArr=[...arr];
// arr[0]='nickname';
// arr[arr.length-1].age=20;
// console.log(copyArr);//[ 'name', 'hobby', 'person', { age: 20 } ]

// Array.prototype.concat()和Array.prototype.slice()
// let myArr=['old',null,undefined,true,{hobby:undefined}];
// let copyMyarr1=myArr.concat();
// let copyMyarr2=myArr.slice();
// myArr[0]='nickname';
// myArr[myArr.length-1].hobby='books'
// console.log(copyMyarr1);//[ 'old', null, undefined, true, { hobby: 'books' } ]
// console.log(copyMyarr2);//[ 'old', null, undefined, true, { hobby: 'books' } ]

let newArr = [
	[{
		name: 'lucky'
	}],
	['age']
]
let copyNewarr = newArr.slice()
newArr[0].name = 'chicken'
console.log(copyNewarr); //[ [ { name: 'lucky' }, name: 'chicken' ], [ 'age' ] ]

// 深拷贝：不会受拷贝对象（本体）的影响
// 1、JSON.parse(JSON.stringify(obj))
// let a={
//     name:'lucky',
//     hobby:{
//         like:'dancing'
//     },
//     birth:undefined,
//     title:Symbol('Banana')
// }
// let c=JSON.parse(JSON.stringify(a));
// a.name='duck';
// a.hobby.like='reading'
// console.log(c);//{ name: 'lucky', hobby: { like: 'dancing' } }

// 2、Object.assign()
