// jQuery.extend()
// $.extend(deepCopy, target, object1, [objectN])//第一个参数为true,就是深拷贝
function jQueryExtend(target,{}, object1, objectN) {
    let deep=false;
    let length=arguments.length;
    let target=arguments[0]||{};
    let i=1;
    let options;

    // 用户开启深拷贝
    if(typeof target ==='boolean'){
        deep=target;
        target=arguments[i++]||{};
    }

    // 用户没有开启深拷贝
    if(typeof target !=='object'){
        target={}

    }

    for(;i<length;i++){
        options=arguments[i];

        if(options!=null){
            for(let key in options){
                // 目标属性值
                let src=target[key];
                // 要复制的对象属性值
                let copy=options[key];

                if(copy && deep && typeof copy=="object"){
                    target[key]=jQueryExtend(deep,src,copy);
                }else if(copy!=undefined){
                    target[key]=copy;
                }
        }
    }
}
return target;
}

let obj1={
    name:'lucky',
    age:20,
    hobby:{
        first:'books',
        second:'movie'
    }
}
let obj2={
    name:'duck',
    age:19,
    hobby:{
        first:'song',
        second:'dance'
    }
}
jQueryExtend()