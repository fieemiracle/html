// 1、Object.assign()
let person = {
	name: 'lucky',
	// 如果这个对象里面还有一个属性是对象，那么会深拷贝
	hobby: {
		like: 'running'
	}
}
// let copyPerson = Object.assign({}, person)
// person.name = 'mouse'
// person.hobby.like = 'singing'
// console.log(copyPerson); //{ name: 'lucky', hobby: { like: 'singing' } }

// let copyPerson = {
// 	...person
// }
// person.hobby.like = 'swimming'
// console.log(copyPerson); //{ name: 'lucky', hobby: { like: 'swimming' } }


// // 循环
// let b = {

// }
// let a = {
// 	b: b
// }

// b.a = a
// let c = JSON.parse(JSON.stringify(a)) //TypeError: Converting circular structure to JSON
// console.log(c);

// 序列
let a = {
	A: 'AAA',
	b: 'aaa',
	a: 444,
	B: 111
}
console.log(a); //{ '1': 111, '4': 444, A: 'AAA', a: 'aaa' }
let s = JSON.parse(JSON.stringify(a))
console.log(s); //{ '1': 111, '4': 444, A: 'AAA', a: 'aaa' }
