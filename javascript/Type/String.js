// String 对象方法
// 方法	    描述
// anchor()	创建 HTML 锚。
// big()	用大号字体显示字符串。
// blink()	显示闪动字符串。
// bold()	使用粗体显示字符串。
// charAt()	返回在指定位置的字符。
// charCodeAt()	返回在指定的位置的字符的 Unicode 编码。
// concat()	连接字符串。
// fixed()	以打字机文本显示字符串。
// fontcolor()	使用指定的颜色来显示字符串。
// fontsize()	使用指定的尺寸来显示字符串。
// fromCharCode()	从字符编码创建一个字符串。
// indexOf()	检索字符串。
// italics()	使用斜体显示字符串。
// lastIndexOf()	从后向前搜索字符串。
// link()	将字符串显示为链接。
// localeCompare()	用本地特定的顺序来比较两个字符串。
// match()	找到一个或多个正则表达式的匹配。
// replace()	替换与正则表达式匹配的子串。
// search()	检索与正则表达式相匹配的值。
// slice()	提取字符串的片断，并在新的字符串中返回被提取的部分。
// small()	使用小字号来显示字符串。
// split()	把字符串分割为字符串数组。
// strike()	使用删除线来显示字符串。
// sub()	把字符串显示为下标。
// substr()	从起始索引号提取字符串中指定数目的字符。
// substring()	提取字符串中两个指定的索引号之间的字符。
// sup()	把字符串显示为上标。
// toLocaleLowerCase()	把字符串转换为小写。
// toLocaleUpperCase()	把字符串转换为大写。
// toLowerCase()	把字符串转换为小写。
// toUpperCase()	把字符串转换为大写。
// toSource()	代表对象的源代码。
// toString()	返回字符串。
// valueOf()	返回某个字符串对象的原始值。

// 58题
var lengthOfLastWord = function(s) {
    let length=0;
    for(let i=s.length-1;i>=0;i--){
        if(s.charAt(i)!=' '){
            length++;
        }else if(length!=0){
            return length;
        }
    }
    return length;
};

var s='lorem o w pif g fffffh hjr';
console.log(lengthOfLastWord(s));//3,返回最后一个单词的长度

var str='abcdefgey';
var str1='hijklmn';
console.log(str.length);//7

// 返回指定下标的字符
console.log(str[3]);//d
console.log(str.charAt(3));//d

// 合并字符串
console.log(str.concat(str1));//abcdefghijklmn
console.log(str+str1);//abcdefghijklmn

// 返回指定字符的位置
console.log(str.indexOf("e"));//4,找第一次出现的位置
console.log(str.lastIndexOf('e'));//7,找最后一次出现的位置

// 替换
console.log(str.replace('f','q'));//后者新值，若有多个旧值需要替换，只会替换第一次找到的那个,不修改元字符串

// 切割
console.log(str.slice(1,3));//bc,取左不取右，不修改元字符串
console.log(str.slice(4,));//efgey

// 切割并转换成数组
console.log(str.split());//将字符串放在一个数组里   [ 'abcdefgey' ]
console.log(str.split(''));//默认以逗号隔开每个字符 ['a', 'b', 'c','d', 'e', 'f','g', 'e', 'y']
console.log(str.split('e'));//把e切掉，并分割      [ 'abcd', 'fg', 'y' ]

// 切割
console.log(str.substr(2,5));//从2开始切割，切5个元素,cdefg
console.log(str.substring(2,5));//从2开始切，切到第五个元素截止不包括第五个，cde

// 转换大小写
var str3='ABCDEFGH'
console.log(str.toLocaleUpperCase());
console.log(str3.toLocaleLowerCase());

// 去除空格
var str4='    rtj gkd    ';
console.log(str4.trim());//去除收尾的空格,rtj gkd

console.log(parseInt(2.5));
console.log(parseInt(2.8));