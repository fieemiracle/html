// Number  数字
var num=123;
var num1=12.3456;

// 指定小数位数
console.log(num1.toFixed());//默认四舍五入保留整数
console.log(num1.toFixed(2));//保留两位小数

// 转换为字符串
console.log(num.toString());