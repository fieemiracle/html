// Symbol 定义独一无二的值
var a= Symbol('aa');
var b= Symbol(123);
console.log(a);
console.log(b);

var c='cc';
var d='cc';
console.log(c===d);//true

var c1=Symbol('cc');
var d1=Symbol('cc');
console.log(c1===d1);//false
