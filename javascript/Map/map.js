// 基本的 Map() 方法
// Method	Description
// new Map()	创建新的 Map 对象。
// set()	为 Map 对象中的键设置值。
// get()	获取 Map 对象中键的值。
// entries()	返回 Map 对象中键/值对的数组。
// keys()	返回 Map 对象中键的数组。
// values()	返回 Map 对象中值的数组。
// clear()	删除 Map 中的所有元素。
// delete()	删除由键指定的元素。
// has()	如果键存在，则返回 true。
// forEach()	为每个键/值对调用回调。

// Map() 属性
// Property	Description
// size	获取 Map 对象中某键的值。

// 创建对象
const apples = {name: 'Apples'};
const bananas = {name: 'Bananas'};
const oranges = {name: 'Oranges'};

// 创建新的 Map
const fruits = new Map();

// Add new Elements to the Map
fruits.set(apples, 500);
fruits.set(bananas, 300);
fruits.set(oranges, 200);
console.log(apples);
console.log(fruits);

//数组算法219题
var containsNearbyDuplicate = function (nums, k) {
    const map=new Map();
    for(let i=0;i<nums.length;i++){
        if(map.has(nums[i]) && Math.abs(i - map.get(nums[i]))<=k){
            return true;
        }
        map.set(nums[i],i);
    }
    return false;
};

const arr=[1,2,3,1,2,3];
const k=3;
console.log(containsNearbyDuplicate(arr,k));