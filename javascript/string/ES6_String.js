// es6新增字符方法
// 单字节表示:\u0000~\uFFFF之间的字符
console.log('\u2d2d');//z
// 双字节表示：超出\u0000~\uFFFF的字符
console.log('\uD842\uDFB7');

// 错误解读
console.log('\u20BB7');//\u20BB+7  打印空格+7
// 解决方法：将码点放入大括号，就能正确解读该字符
console.log("\u{20BB7}");
console.log("\u{41}\u{42}\u{43}");

// 字符的6种表示方式
console.log( '\z' === 'z'    );  // true
console.log( '\172' === 'z'  );  // true
console.log( '\x7A' === 'z'  );  // true
console.log( '\u007A' === 'z' ); // true
console.log( '\u{7A}' === 'z' ); // true

