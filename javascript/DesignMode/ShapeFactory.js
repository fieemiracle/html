// 抽象工厂

// 耦合

// function Factory(name,age,career){
//     let work;
//     switch(career){
//         case 'coder':
//             work=['写代码','改bug','马产品'];
//             break;
//         case 'product manager':
//             work=['定会议室','画原型图','催更'];
//             break;
//         case 'boss':
//             work=['喝茶','看报','见客户'];
//             break;
//     }

//     return new User(name,age,work,career,work)
// }

// 没有遵守开放封闭原则
// 对扩展开放 对修改封闭

class ShapeFactory{//抽象工厂
    // 操作系统
    createOS(){
        throw new Error("抽象工厂方法不允许直接调用，需要重写")
    }
    // 硬件
    createHardWare(){
        throw new Error("抽象工厂方法不允许直接调用，需要重写")
    }
}

class Product extends ShapeFactory{
    createOS(){
        // return new AndroidOS();
        return new QualcommOS();
    }
    createHardWare() {
        // return new QualcommHardware();
        return new MiHardware();
    }
}


// 抽象就是把不变的尽量分离出来
// function AndroidOS(){

// }

// function QualcommHardware(){

// }
class OS{
    controlHardware() {
        throw new Error("抽象工厂方法不允许直接调用，需要重写")
    }
}
class AndroidOS extends OS{
    controlHardware(){
        console.log('我会用AndroidOS方式创造这个产品操作系统');
    }
}
class QualcommOS extends OS{
    controlHardware(){
        console.log('我会用QualcommOS方式创造这个产品操作系统');
    }
}

class HardWare{
    operateByOrder(){
        throw new Error("抽象工厂方法不允许直接调用，需要重写")
    }
}
class MiHardware extends HardWare{
    operateByOrder(){
        console.log('我会使用MiHardware方式创造这个产品的硬件系统');
    }
}
class QualcommHardware extends HardWare{
    operateByOrder(){
        console.log('我会使用QualcommHardware方式创造这个产品的硬件系统');
    }
}

const myPhone=new Product();
const myOS=myPhone.createOS();
const myHardware=myPhone.createHardWare();
myOS.controlHardware();
myHardware.operateByOrder();