const obj1 = {
	name: 'tt',
	age: 18,
	gender: 'male',
	birth: '2001.06.12',
	career: 'care',
	//......后续有更多的属性
}
//后续有更多的程序员


const obj2 = {
	name: 'ttt',
	age: 18,
	career: 'care'
}

// 使用构造函数
// function User(name, age,career) {
//     this.name = name;
//     this.age = age;
//     this.career =career;
//     this.work=['写代码','改bug','马产品'];
//     this.work=['定会议室','画原型图','催更'];
// }
// const user = new User(name, age,career);

function Coder(name, age) {
	this.name = name;
	this.age = age;
	this.career = 'coder';
	this.work = ['写代码', '改bug', '马产品'];
}

function ProductManager(name, age) {
	this.name = name;
	this.age = age;
	this.career = 'product manager';
	this.work = ['定会议室', '画原型图', '催更'];
}

// 工厂诞生
function Factory(name, age, career) {
	switch (career) {
		case 'coder':
			return new Coder(name, age);
			break;
		case 'product manager':
			return new ProductManager(name, age);
			break;
	}
}


// 改进后的工厂（有构造函数的地方）
function User(name, age, career, work) {
	this.name = name;
	this.age = age;
	this.career = career;
	this.work = work;
}

function Factory(name, age, career) {
	switch (career) {
		case 'coder':
			work = ['写代码', '改bug', '马产品'];
			break;
		case 'product manager':
			work = ['定会议室', '画原型图', '催更'];
			break;
	}

	return new User(name, age, work, career)
}
