// 原型链继承
SuperType.prototype.getSuperValue = function() {
	return this.property;
}

function SuperType() {
	this.property = true,
		this.like = {
			spoet: 'running',
			book: 'html'
		}
}
//原型链继承
Type.prototype = new SuperType()

function Type() {
	this.typeproperty = false;
}

var instance = new Type()
var instance1 = new Type()

console.log(instance.property);
console.log(instance.getSuperValue());
console.log(instance1.like.sport = 'swimming');









// 原型链继承
SuperType.prototype.greet = function() {
	return 'hello~,' + this.name;
}

function SuperType() {
	this.name = '美羊羊',
		this.hobby = {
			boy: '喜洋洋',
			book: '白雪公主和七个小矮人'
		}
}

// 让构造函数的原型等于另外一个构造函数的实例，会继承另外一个构造函数上的属性和方法
ParentType.prototype = new SuperType();

function ParentType() {
	this.name = '懒洋洋'
}

let child1_type = new ParentType()
let child2_type = new ParentType()


console.log(child1_type.greet()); //hello~,懒洋洋
console.log(child2_type.name); //懒洋洋
console.log(child1_type.hobby.boy = '暖洋洋'); //暖洋洋
console.log(child2_type); //SuperType { name: '懒洋洋' }
console.log(child2_type.hobby); //{ boy: '暖洋洋', book: '白雪公主和七个小矮人' }
