// 寄生组合式
Super.prototype.say = function() {
	console.log(this.name);
}

function Super(name) {
	this.name = name,
		this.colors = ['red', 'blue', 'green']
}

function Son(name, age) {
	this.age = age;
	Super.call(this, name)
}

var another = Object.create(Super.prototype);
another.constructor = Son;

var another = Object.assign(Son.prototype(), Super.prototype())

// Son.prototype = another;

var instance1 = new Son('duck', 19)
instance1.
instance1.colors.push('pink')
var instance2 = new Son('cat', 18)

// SuperType.prototype.sayName = function() {
// 	console.log(this.name);
// }
// SuperType.prototype.like = {
// 	a: 1,
// 	b: 2
// }

// function SuperType(name) {
// 	this.name = name
// 	this.colors = ['red', 'blue', 'green']
// }

// function Type(name, age) {
// 	this.age = age
// 	SuperType.call(this, name)
// }
// var anotherPrototype = Object.assign(Type.prototype, SuperType.prototype)
// // anotherPrototype.constructor = Type
// Type.prototype = anotherPrototype // new SuperType()
// var instance1 = new Type('Tom', 12)
// // instance1.colors.push('pink')
// instance1.like.a = 11
// var instance2 = new Type('jerry', 10)
// // instance1.sayName()
// console.log(instance2.like);
