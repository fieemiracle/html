// class继承
class Parent {
	constructor(name, gender) {
		this.name = name;
		this.gender = gender;
		this.greet = function() {
			console.log('greet');
		};
	}
	speak() {
		console.log("parent speak")
	}

	static speak() {
		console.log("static speak")
	}
}

//class 子类 extends 父类
class Son extends Parent {
	//在子类的构造方法中调用父类的构造方法
	constructor(name, gender, hobby) {
		super(name, gender);
		this.hobby = hobby;
	}
	//子类中声明的方法名和父类中的方法名相同时，子类中的方法将覆盖继承于父类的方法
	speak() {
		console.log("Son speak");
	}
}
const grandson = new Son('lucky', 'male', 'reading');
console.log(grandson.name, grandson.gender, grandson.hobby); //lucky male reading
grandson.greet(); //greet
grandson.speak(); //Son speak
Son.speak(); //static speak
