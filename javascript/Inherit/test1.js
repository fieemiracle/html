// 寄生组合式
Super.prototype.say = function() {
	console.log(this.name);
}

function Super(name) {
	this.name = name,
		this.colors = ['red', 'blue', 'green']
}

function Son(name, age) {
	this.age = age;
	Super.call(this, name)
}

var another1 = Object.create(Super.prototype);
another1.constructor = Son;
console.log(another1);

var another2 = Object.assign(Son.prototype, Super.prototype)
console.log(another2);
// Son.prototype = another;

var instance1 = new Son('duck', 19)
instance1.colors.push('pink')
console.log(instance1);
var instance2 = new Son('cat', 18)
console.log(instance2);
