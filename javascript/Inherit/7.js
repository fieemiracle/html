// // 寄生式继承
// // 1、借用构造函数对目标对象进行浅复制
// function helperFun(targetObj) {
// 	function newFun() {};
// 	newFun.prototype = targetObj;
// 	return newFun();
// }

// // 2、封装继承过程函数
// function packInherit(originObj) {
// 	let evalObj = helperFun(originObj);
// 	evalObj.greet = function() {
// 		console.log('hello~,' + this.name);
// 	}
// 	return evalObj;
// }

// let initObj = {
// name: '虹猫',
// like: ['reading', 'running'],
// age: 20
// }
// let newObj1 = packInherit(initObj);
// let newObj2 = packInherit(initObj);
// console.log(newObj1);
// newObj1.greet()
// newObj2.name = '蓝兔';
// newObj2.like.push('虹猫');
// newObj2.greet()

function helperFun(targetObj) {
	function newFun() {};
	newFun.prototype = targetObj;
	return new newFun();
}

function packInherit(originObj) {
	let evalObj = helperFun(originObj);
	evalObj.greet = function() {
		console.log('hello~,' + this.name);
	};
	return evalObj;
}

let initObj = {
	name: '虹猫',
	like: ['reading', 'running'],
	age: 20
}

let newObj1 = packInherit(initObj);
console.log(newObj1); //[ 'reading', 'running', '蓝兔' ]
newObj1.like.push("蓝兔");
console.log(newObj1.like); //[ 'reading', 'running', '蓝兔' ]
newObj1.greet(); //hello~,虹猫

let newObj2 = packInherit(initObj);
newObj2.name = '蓝兔';
console.log(newObj2); //{ greet: [Function (anonymous)], name: '蓝兔' }
newObj2.greet(); //hello~,蓝兔
console.log(newObj2.like); //[ 'reading', 'running', '蓝兔' ]
