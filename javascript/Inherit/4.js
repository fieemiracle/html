// 原型式继承
function object(o) {
	function F() {

	}
	F.prototype = o;
	return new F()

}

var person = {
	name: 'dog',
	age: 19,
	like: {
		hobby: 'singing'
	}
}
var obj = object(person)
obj.like.hobby = 'dancing'
var obj2 = Object.create(person)
console.log(obj2);



// 原型式继承
// 借用构造函数
function helperFun(targetObj) {
	function newFun() {}
	newFun.prototype = targetObj;
	return new newFun();
}

let myObj = {
	username: 'dog',
	age: 14,
	other: {
		hobby: 'reading',
		taste: 'sore'
	}
}

console.log(myObj);
let newObj = helperFun(myObj)
newObj.username = 'cat';
newObj.other.hobby = 'sleeping';
console.log(newObj);



// Object.create()
let presentIbj = {
	username: '熊大',
	sex: 'male',
	age: 19,
	greet() {
		console.log('name is  ' + this.username);
	}
}

let newObj1 = Object.create(presentIbj);
let newObj2 = Object.create(presentIbj);
newObj1.username = '熊二';
console.log(newObj1);
console.log(newObj2);
