// 伪造对象
SuperType.prototype.value = 'hello world';

function SuperType() {
	this.colors = ['red', 'green', 'blue'];
	// console.log(this); //指向全局

}

function Type() {
	SuperType.call(this) //改变this的指向
}

var instance = new Type()
instance.colors.push('pink')
var instance1 = new Type()

console.log(instance.colors); //[ 'red', 'green', 'blue' ,'pink']
console.log(instance1.colors); //[ 'red', 'green', 'blue' ]
console.log(instance1.value); //undefined  继承不到父类原型上的属性



// 经典继承（伪造对象）
SuperType.prototype.greet = function() {
	return 'hello~' + this.person.name;
}

function SuperType(name) {
	this.person = {
		name: name || '大耳朵图图',
		gender: 'boy',
		age: 19
	}
}

function ParentType(name) {
	SuperType.call(this, name)
}

let child1_type = new ParentType('张小丽');
let child2_type = new ParentType();

child1_type.person.gender = 'female';
console.log(child1_type.person); //{ name: '张小丽', gender: 'female', age: 19 }

console.log(child2_type.person); //{ name: '大耳朵图图', gender: 'boy', age: 19 }
console.log(child2_type.greet()); //TypeError: child2_type.greet is not a function 继承不到父类原型上的属性
