// 组合继承
Super.prototype.sayName = function() {
	console.log(this.name);
}

function Super(name) {
	this.name = name;
	this.colors = ['red', 'green', 'blue'];
}

function Son(age, name) {
	this.age = age;
	// 经典继承，直接那父类在自己函数体调用
	Super.call(this, name)
}

// 原型链继承
Son.prototype = new Super() //Son.prototype被重写成为一个实例对象，没有constructor

// 给Super弥补一个constructor
Son.prototype.constructor = Son;
Son.prototype.sayAge = function() {
	console.log(this.age);
}
var instance = new Son(19, 'dog');
instance.sayAge() //19
instance.sayName() //dog





// 组合继承（伪经典继承）
SuperType.prototype.greet = function() {
	return 'hello~,' + this.card.name;
}

function SuperType(gender) {
	this.card = {
		name: '佩奇',
		age: 20,
		gender: gender
	}
}

// 利用原型链继承
ParentType.prototype = new SuperType();
// 弥补一个constructor
ParentType.prototype.constructor = ParentType();
ParentType.prototype.sayHobby = function() {
	return 'I like ' + this.hobby;
}

function ParentType(gender, hobby) {
	this.hobby = 'singing';

	// 利用经典继承，借用构造函数
	SuperType.call(this, gender)
}

let child1_type = new ParentType('female')
let child2_type = new ParentType('male')

console.log(child1_type.greet()); //hello~,佩奇
console.log(child1_type.sayHobby()); //I like singing
console.log(child1_type.card); //{ name: '佩奇', age: 20, gender: 'female' }

child2_type.card.name = '黑猫警长';
console.log(child2_type.card); //{ name: '黑猫警长', age: 20, gender: 'male' }
