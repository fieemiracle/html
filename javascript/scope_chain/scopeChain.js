// 作用域链
function test(){//函数被创建的那一刻，就携带name,prototype属性

}
console.log(test.name);//test
console.log(test.prototype);//{}
// console.log(test[[scope]]);访问不到,作用域属性，也称为隐式属性

// test() --->AO:{}执行完毕会回收
// test() --->AO:{}执行完毕会回收
function a(){
    function b(){
        var b=222;
    }
    var a=111;
    console.log(a);
}
a();
// a定义 a.[[scope]]---->0:GO{}
// a执行 a.[[scope]]---->0:AO{}  1:GO{}  后访问的在前面
// b定义 b.[[scope]]---->0:bAO{} 1:aAO{}  2:GO{}
// a的AO

