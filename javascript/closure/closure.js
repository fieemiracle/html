// 闭包
// 作用：全局变量能够通过闭包实现局部（私有）
// 闭包指的是有权访问父作用域的函数，即使在父函数关闭之后
// 闭包的作用
// 保护函数的私有变量不受外部的干扰。形成不销毁的栈内存。
// 保存，把一些函数内的值保存下来。闭包可以实现方法和属性的私有化

// // 自调函数
// var add = (function () {
//     var counter = 0;
//     return function () {return counter += 1;}
// })();

// add();
// add();
// add();

// // 计数器目前是 3 

// 闭包经典使用场景--return一个函数
var n = 10;
function fn() {
    var n = 20;
    function f() {
        n++;
        console.log(n);
    }
    f();
    return f;
}
var x = fn();//21
x();//22

// 闭包经典使用场景--函数作为参数
var a = '林一一';
function foo() {
    var a = 'foo';
    function fo() {
        console.log(a);
    }
    return fo;
}

function f(p) {
    var a = 'f';
    p();
}
f(foo());//foo

// 闭包经典使用场景--自执行函数
var n = '林一一';
(function p() {
    console.log(n)
})();//林一一

// 闭包经典使用场景--循环赋值 
for (var i = 0; i < 10; i++) {
    (function (j) {
        setTimeout(function () {
            console.log(j);
        }, 1000)
    })(i)
}

// 闭包经典使用场景--使用回调函数
// window.name = '林一一'
// setTimeout(function timeHandler() {
//     console.log(window.name);
// }, 1000)
// 打印一次，林一一


// 闭包经典使用场景--节流防抖
// // 节流
// function throttle(fn, timeout) {
//     let timer = null
//     return function (...arg) {
//         if (timer) return
//         timer = setTimeout(() => {

//             fn.apply(this, arg)
//             timer = null;
//         }, timeout)
//     }
// }

// // 防抖
// function debounce(fn, timeout) {
//     let timer = null
//     return function (...arg) {
//         clearTimeout(timer)
//         timer = setTimeout(() => {
//             fn.apply(this, arg)
//         }, timeout)
//     }
// }



// 闭包经典使用场景--柯理化实现
// function curry(fn, len = fn.length) {
//     return _curry(fn, len)
// }

// // 
// function _curry(fn, len, ...arg) {
//     return function (...params) {
//         let _arg = [...arg, ...params]
//         if (_arg.length >= len) {
//             return fn.apply(this, _arg)
//         } else {
//             return _curry.call(this, fn, len, ..._arg)
//         }
//     }
// }

// let fn = curry(function (a, b, c, d, e) {
//     console.log(a + b + c + d + e)
// })

// fn(1, 2, 3, 4, 5)  // 15
// fn(1, 2)(3, 4, 5)
// fn(1, 2)(3)(4)(5)
// fn(1)(2)(3)(4)(5)
