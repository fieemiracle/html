// 1
// function test(){
//     var arr=[];
//     for(var i=0;i<10;i++){
//         arr[i]=function(){
//             console.log(i);
//         }
//     }
//     return arr;
// }
// var myArr=test();
// for(var j=0;j<10;j++){
//     myArr[j]();
// }

// 2
// function a(){
//     function b(){
//         var bb=345;
//         console.log(aaa);
//     }
//     var aaa=123;
//     return b;//b出生在a里面，但是被保存出去，这里b还没有被调用执行，而是返回
// }
// var global=100;
// var demo=a();
// demo();

// 3
// count是全局变量，污染全局
// var count=0;
// function add(){
//     return count++;
// }
// console.log(add());//0
// console.log(add());//1
// console.log(add());//2

// // count不是全局变量
// function add(){
//     var count;
//     function a(){
//         count++:
//         return count;
//     }
//     return a;
// }
// var res=add();

// function fruit(){
//     var food='apple';
//     var obj={
//         eatFood:function(){
//             if(food!=''){
//                 console.log('I am eating'+food);
//                 foog='';
//             }else{
//                 console.log('There is nothing');
//             }
//         },
//         pushFood:function(myFood){
//             food=myFood
//         }
//     }
//     return obj;
// }
// var person=fruit();
// person.eatFood();
// person.pushFood('pear');
// person.eatFood();

function test() {
    var arr = [];
    for (var i = 0; i < 10; i++) {
        (
            function (j) {
                arr[j] = function () {
                    console.log(j);
                }//被保存到了外部，自执行函数执行完一次以后不会被销毁
            }
        )(i);
    }
    return arr;
}
var myArr = test();
for (var i = 0; i < 10; i++) {
    myArr[i]();
}

function foo() {
    for (var i = 0; i < 10; i++) {
        // (function (j) {
        //     setTimeout(function () {
        //         console.log(j);
        //     }, 1000 * j)
        // })(i);
        setTimeout(function () {
            console.log(j);
        }, 1000, i)
    }
}

foo();