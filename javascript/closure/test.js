// function outer(){
//     var name="来碗盐焗星球";//局部变量
//     function inner(){//内部函数
//         console.log(name);
//     }
//     inner();
// }
// var name="小新没蜡笔";//全局变量
// outer();

// function doConsole(){
//     var name="来碗盐焗星球"
//     setTimeout(()=>{
//         console.log(name);
//     },1000);
// }
// doConsole();

// let name = "来碗盐焗星球";
// setTimeout(function timeHandler() {
//     console.log(name);
// }, 1000)

// //请求开始动画
// $(document).ajaxSend(function () {
//     alert(1);
//     if ($('#ajaxloader').length == 0) {
//         $('body').append(loader('请稍候。。。'));

//         $('#ajaxloader,#ajaxloader_zz').fadeIn('normal');
//     }
// });

// 闭包经典使用场景--自执行函数
// var name = '来碗盐焗星球';
// (function doName() {
//     console.log(name)
// })();

// for(let i=1;i<6;i++){
//     setTimeout(()=>{
//         console.log(i);
//     },i*1000);
// }

// for(var i=0;i<=5;i++){
//     let j=i;
//     setTimeout(function timer(){
//         console.log(j);
//     },j*1000)
// }

// var name = '来碗盐焗星球';
// function fn() {
//     var name = '蜡笔小新';
//     function f() {
//         name += '?'
//         console.log(name);
//     }
//     f();
//     return f;
// }
// var doName = fn();
// doName();

// var a = '碗盐来焗星球';
// function outer() {
//     var a = '小猪佩奇';
//     function inner() {
//         console.log(a);//小猪佩奇
//     }
//     return inner;
// }

// function other(func) {//形参是一个函数
//     var a = '小狗汪汪队';
//    func();
// }
// other(outer());//小猪佩奇

// function add() {
//     var count=0;
//     function a() {
//         count++;
//         // return count;
//         console.log(count);//1
//     }
//     return a;
// }
// var res = add();
// res()

// function outer(){
//     var name="来碗盐焗星球";
//     function inner(){
//         console.log(name);
//     }
//     // inner();
//     return inner;
// }
// var doOuter=outer():
// doOuter();

// function fruit() {
//     var food = 'apple'
//     var people = {
//       eatFood: function() {
//         if (food !== '') {
//           console.log('还有 ' + food);
//           food = ''
//         } else {
//           console.log('没水果了');
//         }
//       },
//       pushFood: function(myfood) {
//         food = myfood
//       }
//     }
//     return people
//   }
//   var person = fruit()
//   person.eatFood()
//   person.eatFood()
//   person.pushFood('banana')
//   person.eatFood()

// 模块
function aModule(){
    var name="来碗盐焗星球"
    function inner(){
        console.log(name);
    }

    return {inner:inner}
}
var doModule=aModule();
doModule.inner();