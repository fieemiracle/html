<!-- 闭包 -->
当一个函数被保存到了外部，将会产生闭包。
闭包的概念：

<!-- 内存泄漏 -->
可用空间变小的现象
闭包的缺陷：闭包会导致原有的作用域链不释放，导致内存泄漏
            function b(){
                var a=123;
                b();
            }
            b();

<!-- 闭包的作用 -->
1、实现共有变量（模块化）:全局变量能够通过闭包实现局部（私有）,例如：累加器
        // count是全局变量，污染全局
            var count=0;
            function add(){
                return count++;
            }
            console.log(add());//0
            console.log(add());//1
            console.log(add());//2

            // count不是全局变量
            function add(){
                var count;
                function a(){
                    count++:
                    return count;
                }
                return a;
            }
            var res=add();
    2、做缓存  4.js
    3、可以实现封装，实现私有化
    4、模块化开发，防止污染全局变量

