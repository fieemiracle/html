// // 函数中的原型对象：每个函数都有一个特殊的属性--原型（prototype）

// // 创建构造函数
// function Person(){};
// console.log(Person.prototype);//{}
// var Student=function(){};
// console.log(Student.prototype);//{}

// // 构造函数原型添加属性
// Person.prototype.sex='female';
// console.log(Person.prototype);//{ sex: 'female' }
// Student.prototype.grade='2020级';
// console.log(Student.prototype);//{ grade: '2020级' }

// // 调用构造函数创建实例化对象
// // 构造函数的调用方法：在正常调用函数时，在函数名的前面加上一个 new 前缀
// var person=new Person();
// console.log(person);
// var student=new Student();

// // 给实例化对象添加属性
// person.father='Person()';
// console.log(person);//Person { father: 'Person()' }
// student.father='Student()';
// console.log(student);//Student { father: 'Student()' }

// //得出结论--实例化对象的__proto__属性就是构造函数的prototype
// console.log(person.__proto__==Person.prototype);//true
// console.log(student.__proto__==Student.prototype);//true

// // 规则
// console.log(person.sex);//female
// // 实例化对象person并没有sex属性，但是居然有打印值？？？
// // 这就是原型链了：当访问person的sex属性，浏览器首先搜索person本身，没有？浏览器继续寻找person.__proto__(person的爹爹===Person.prototype),有了！他的(Person.prototype)就是我的(person)！
// // 还是没有咋整？
// // 假设我想要访问student.class,告诉我(student)怎么找吧~
// // 目标：student.class
// // 浏览器:你有class这个属性嘛?
// // student:不好意思，我没有~你去问问我父亲大人-->person.__proto__(Student.prototype)吧~
// // 浏览器：他没有。。。
// // student:那你要不问问我祖父-->person.__proto__.__proto__(Student.prototype.__proto__===window.Object.prototype)?
// // 浏览器：你逗我呢？他也没有！
// // student:那你再问问我太祖父-->person.__proto__.__proto__.__proto__(Object.prototype.__proto__)，相信我，这次肯定有结果的。
// // 浏览器:真的栓Q!他都不在世上！
// // student:大哥，不好意思了，那找遍了都没有那就真没有了...
// // 浏览器:行吧,累死人了。那我要把class属性是undefined记录了。

// function Teacher(name,age,sex,wage){
//     this.name=name;
//     this.age=age;
//     this.sex=sex;
//     this.wage=wage;
// }
// var teacher=new Teacher('Lucky','23','female','15k');
// console.log(teacher);//Teacher { name: 'Lucky', age: '23', sex: 'female', wage: '15k' }

// console.log(Object.prototype);//[Object: null prototype] {}
// console.log(Object.__proto__);//{}

Fruit.prototype.classify=function(){
    console.log('Fruits');
}
Fruit.prototype.price='￥10';
function Fruit(name){
    this.name=name;
}
var fruit=new Fruit('pear');

//添加color属性
fruit.color='green';
console.log(fruit.color);//green 给我自己添加成功了
console.log(Fruit.color);//undefined Fruit.prototype.color不存在！

// 改变price值
fruit.price='$9';//本来想改变Fruit.prototype.price=$9
console.log(fruit.price);//只是自己的fruit.price=$9
var myFruit=new Fruit('pear');
console.log(myFruit.price);//没有改变Fruit.prototype.price=￥10

// 删除颜色属性
delete fruit.price;
console.log(fruit.price);//￥10,好家伙没删除成功！
delete Fruit.prototype.price;
console.log(fruit.price);//undefined，居然成功了！

// 查找name属性和price属性
console.log(fruit.name);//查到啦！pear
console.log(fruit.price);//没有诶~，undefined

// 原型的增删改：不能通过实例对象修改，只能通过原型对象修改

