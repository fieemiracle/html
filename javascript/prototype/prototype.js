// 原型和原型链
// 引用类型：Object、Array、Function、Date、RegExp
// 1、引用类型，都具有对象特性，即可自由扩展属性。
// 2、引用类型，都有一个隐式原型 __proto__ 属性，属性值是一个普通的对象。
// 3、引用类型，隐式原型 __proto__ 的属性值指向它的构造函数的显式原型 prototype 属性值。
// 4、当你试图得到一个对象的某个属性时，如果这个对象本身没有这个属性，那么它会去它的隐式原型 __proto__（也就是它的构造函数的显式原型 prototype）中寻找。

var A = function () { };
A.prototype.n = 1;
var b = new A();
A.prototype = {
  n: 2,
  m: 3
}
var c = new A();
console.log(b.n);//1
console.log(b.m);//undefined

console.log(c.n);//2
console.log(c.m);//3

// 构造函数的原型
Person.prototype.lastName='Pan';
Person.prototype.name='Lucky';
Person.prototype.say=function(){
  console.log('Oh, My God!');
}
function Person(){
  this.name='Ducky'
}
var person=new Person();
console.log(person.name);//Ducky
console.log(person.say);//[Function (anonymous)]
//[[Prototype]]  __proto__

// 增删改查
// 改
var person1=new Person('周丹');
person.lastName='La';
console.log(person1.lastName);//Pan
//删
delete person.lastName;
console.log(person.lastName);//Pan
delete Person.prototype.lastName;
console.log(person.lastName);//undefined

// constructor--构造器
Car.prototype={
  constructor:Bus
}
function Car(){

}
Bus.prototype.name='Lucky';
function Bus(){

}
var car=new Car();
console.log(car.constructor);//[Function: Bus]
console.log(Car.prototype.constructor);
console.log(car.name);//undefined



// 原型链
function Student(){

}
var student=new Student();
console.log(student.__proto__===Student.prototype);//true




