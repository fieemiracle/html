// 构造函数Foo--上溯：Object()
function Foo(name){
    this.name=name;
};
console.log(Foo.prototype);//{constructor: ƒ}constructor: ƒ Foo(name)[[Prototype]]: Object
console.log(Foo.prototype.constructor);//[Function: Foo]

// 实例化对象f1,f2--上溯：Foo()
var f1=new Foo();
console.log(f1.__proto__);//{constructor: ƒ}constructor: ƒ Foo(name)[[Prototype]]: Object==Foo.prototype
console.log(f1.__proto__==Foo.prototype);//true


console.log(Foo.prototype.__proto__);//constructor: ƒ Object()

//对象
function Object(){};
console.log(Object.prototype);//constructor: ƒ Object()==Foo.prototype.__proto__
console.log(Object.prototype.constructor);//ƒ Object() { [native code] }

var o1=new Object();
console.log(o1.__proto__==Object.prototype);//true

console.log(Object.prototype.__proto__);//null