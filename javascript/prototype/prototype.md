#原型
prototype   函数的原型（显示原型）
__proto__   对象原型（隐式原型）

#原型的定义：原型是函数function对象的一个属性，它定义了构造函数制造出来的对象（var person=new Person()）的公共祖先,通过构造函数产生的对象可以继承到该原型的属性和对象，原型也是对象，原型也是对象。

#利用原型的特点和概念，可以实现公有属性。

#原型上的增删改查
    删改：不能通过实例对象修改，只能通过原型对象修改

#constructor---继承
    为了让构造函数构造出来的所有的对象都能找到自己的构造器

#原型链
1、定义：在原型上再加一个原型，再加一个原型...把原型连成链，访问顺序也是按照这个链的顺序跟作用域链一样，叫做原型链

#所有的对象都会继承自Object.prototype?答：不会
    var obj1=Object.create(null);

理解对象的原型（可以通过Object.getPrototypeOf(obj)或者已被弃用的__proto__属性获得）与构造函数的prototype属性之间的区别是很重要的。
前者是每个实例上都有的属性，后者是构造函数的属性。也就是说，Object.getPrototypeOf(new Foobar())和Foobar.prototype指向着同一个对象。
