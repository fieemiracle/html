// 对象的原型
// 对象可以转换成给字符串
Person.prototype.name='wn';//构造函数的显式原型 Person.prototype
function Person(){
    var this={
        __proto__:Person.prototype
    }
    
}
var obj={
    name:'Dubi'
}
var person=new Person();
console.log(person.__proto__);//{ name: 'wn' }//实例对象的隐式原型 Person.prototype
Person.prototype=obj;
console.log(person.__proto__);//{ name: 'Dubi' }
// 实例对象的隐式原型==构造函数的显式原型

var obj1=Object.create(null);//obj1没有隐式原型,更不会继承自Object.prototype
// obj1.__proto__
var obj2=Object.create(obj1);
obj1.__proto__={//手动添加隐式原型
    name:'lucky'
}
console.log(obj1.name);//undefined  undefined没有原型，也就没有对象上的toString()方法


