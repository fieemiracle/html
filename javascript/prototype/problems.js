Object.prototype.a = "a";
Function.prototype.a = "a1";
function Person() {}
const yideng = new Person();

console.log(Person.a);  // Person.__proto__=Function.prototype  a1
console.log(yideng.a);  // yideng.__proto__=Person.prototype->Person.proto==Object.prototype a
console.log(yideng.__proto__); //Person.prototype
console.log(yideng.__proto__.__proto__); //Person.prototype.proto=Object.prototype
console.log(yideng.__proto__.__proto__.constructor);//Object.prototype.constructor     f Object{}
console.log(yideng.__proto__.__proto__.constructor.constructor);//Function:Function
console.log(yideng.__proto__.__proto__.constructor.constructor.constructor);//Function:Function
console.log(yideng.__proto__.__proto__.constructor.constructor.constructor.constructor);//Function:Function
console.log(yideng.__proto__.__proto__.constructor.constructor.constructor.constructor.constructor);//Function:Function

console.log(yideng.__proto__ === Person.prototype);//true
console.log(Person.prototype.__proto__ === Object.prototype);//true
// 构造函数
console.log(Object.prototype.constructor === Object);//true
// constructor 构造函数
console.log(Function.constructor === Function);//true

// Function.__proto__=Object.prototype
// =Function.prototype

// function Function(){}
// Function.__proto__=Function.prototype
// Function.prototype.constructor=Function:Function(函数体本身)
// Function.prototype.constructor.prototype=Function.prototype=Function.__proto__