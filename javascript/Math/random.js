// 求得是 N - M 之间的一个随机数公式
// let random = Math.floor(Math.random() * (10 - 1 + 1)) + 1
// console.log(random)
// 封装一个随机数函数  min 到  max  
function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}
let random = getRandom(1, 10)
console.log(random)
let random1 = getRandom(1, 50)
console.log(random1)

// 随机点名
// 声明一个数组
let arr = ['赵云', '黄忠', '关羽', '张飞', '马超', '刘备', '曹操', 'pink老师', 'like']

// 生成1个随机数 作为数组的索引号
let random2 = getRandom(0, arr.length - 1)
console.log(random2)
console.log(arr[random2]);
// document.write(arr[random2])

// 之后删除这个 人的名字，实现不重复点名
// arr.splice(从哪里开始删， 删几个)
arr.splice(random2, 1)
console.log(arr)


// 猜数字
// 生成 1~10 之间的随机数
let random3= getRandom(1, 10)
// console.log(random)
// 3. 用户输入 不断的弹框直到输入正确为止
while (true) {
    let num = +prompt('请您输入一个数字：')
    // 如果输入的大于随机数就提示猜大了
    if (random3 < num) {
        alert('私密马赛，你猜大了')
    } else if (random3 > num) {
        alert('比亚乃， 你猜小了')
    } else {
        alert(' 萨卡迪卡，正确')
        break   // break 退出循环  return 退出函数
    }
    // 如果输入的小于随机数就提示猜小了
    // 如果输入的正好就提示正确
}