function toUpperCase(str) {
    return str.toUpperCase()
}
function hello(x){
    return 'hello ' + x;
}

let greet=function(x){
    return hello(toUpperCase(x))
}
console.log(greet('duck'));

// 组合函数
var compose=function(){
    var args=arguments;
    var start=args.length-1;
    return function(){
        var i=0;
        var result=args[start].apply(this,arguments);
        while(i--){
            result=args[i].apply(this,result);
        }
        return result;
    }
}



