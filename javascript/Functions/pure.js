// 纯函数
// function pure(a,b,c){
//     // return a*b*c;
//     console.log(a*b*c);
// }
// pure(1,2,3);
// pure(1,2,3);
// pure(1,2,3);
// pure(1,2,3);

// 非纯函数
const start = 1;
const isPure=function(num){
    // return start+num;
    console.log(start + num);
}
isPure(2);//3

// 非纯函数
function fn(num){
    return num + Math.random();
}
console.log(fn(1));
console.log(fn(1));
console.log(fn(1));