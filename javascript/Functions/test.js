// 正常函数1
// function add(a, b) {
//     return a + b;
// }
// console.log(add('my name is ','cat'));
// console.log(add('my name is ','dog'));
// console.log(add('my name is ','duck'));
// console.log(add('my name is ','mouse'));
// console.log(add('my name is ','hen'));
// console.log(add('my name is ','bird'));
// 规律：第一个参数全都是相同，重复输入了6次。参数少的情况还好办，那参数多的时候就非常不方便了


// function concat(a, b) {
//     // return a+b;
//     console.log(a + b);
// }

// function productPartial(fn, a) {
//     return function (b) {
//         return fn(a, b);
//     }
// }

// // 固定一个参数
// var partial = productPartial(concat, 'my name is ');
// // 其他参数在调用的时候传入
// partial('apple');
// partial('pear');
// partial('peach');
// partial('grape');

var subtract = function(a, b) { return b - a; };
sub5 = _.partial(subtract, 5);
sub5(20);
// => 15

// Using a placeholder
subFrom20 = _.partial(subtract, _, 20);
subFrom20(5);
// => 15