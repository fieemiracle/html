// 纯函数
function pure(a,b,c){
    // return a*b*c;
    console.log(a*b*c);
}
pure(1,2,3);
pure(1,2,3);
pure(1,2,3);
pure(1,2,3);

// 纯函数
var arr = [1,2,3,4,5,6];
var myArr = arr.filter(function(item){
    return item > 3;
});
console.log(myArr);
console.log(arr);

// 非纯函数
function fn(num){
    return num + Math.random();
}
console.log(fn(1));
console.log(fn(1));
console.log(fn(1));

// 纯函数的优点
// I.不会改变外部环境的变量，就像上面所说的一样，非纯函数会改变外部环境的变量，多次调用得到不同的结果
var newArr = [1,2,3,4,5,6,7,8,9];
newArr.splice(2,3);
console.log(newArr);
newArr.splice(2,3);
console.log(newArr);
newArr.splice(2,3);
console.log(newArr);

//I.相同的输入永远对应相同的输出