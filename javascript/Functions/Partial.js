let _={}
function partial(fn){
    // let args=Array.from(arguments)
    let args=[].slice.call(arguments, 1);

    return function(){
        let index=0;
        for(let i=0; i<args.length; i++){
            args[i]=args[i]===_?arguments[index++]:args[i];//将数组中的_替换成正确的参数
        }
        // 判断参数是否用完
        if(index<arguments.length){//有参数没用完
            args.concat([].slice.call(arguments,index))
        }
        // let newArgs=args.concat([].slice.call(arguments));
        return fn.apply(this, args);
    }
}
function add(a,b,c){
    // return a+b+c;
    console.log(a+b+c);
}
let add1=partial(add,_,1,_);
add1(2)
add1(2)
add1(2)





// var subtract = function(a, b) { return b - a; };
// sub5 = _.partial(subtract, 5);
// sub5(20);

// // Using a placeholder
// subFrom20 = _.partial(subtract, _, 20);
// subFrom20(5);