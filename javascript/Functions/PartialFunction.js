// 正常函数1
function add(a, b) {
    return a + b;
}
// console.log(add('my name is ','cat'));
// console.log(add('my name is ','dog'));
// console.log(add('my name is ','duck'));
// console.log(add('my name is ','mouse'));
// console.log(add('my name is ','hen'));
// console.log(add('my name is ','bird'));
// 规律：第一个参数全都是相同，重复输入了6次。参数少的情况还好办，那参数多的时候就非常不方便了

// 正常函数2
function ajax(url, data, callback) { }
function ajaxTest1(data, callback) {
    ajax('http://www.test.com/test1', data, callback)
}
function ajaxTest2(data, callback) {
    ajax('http://www.test.com/test2', data, callback)
}
function ajaxTest3(data, callback) {
    ajax('http://www.test.com/test3', data, callback)
}


// 解决：对于相同的参数，只输入一次（也就是把他们固定住），对于其他的参数在调用的时候输入就行
// 入参函数
function concat(a, b) {
    // return a+b;
    console.log(a + b);
}
// 生产偏函数的工厂
function productPartial(fn, a) {
    return function (b) {
        return fn(a, b);
    }
}

// 固定一个参数
var partial = productPartial(concat, 'my name is ');
partial('apple');
partial('pear');
partial('peach');
partial('grape');


// 固定多个参数
function add(a, b, c, d, e) {
    return a + b + c + d + e;
}
function partial(fn, a, b, c) {
    return function (d, e) {
        return fn(a, b, c, d, e);
    }
}
var parAdd = partial(add, 1, 2, 3);
