// const arr1=[1,2,3]
// const arr2=[1,2,3]
// console.log(arr1===arr2);//false

// 1
// async function async1() {
//     console.log('async1 start')
//     await async2()
//     console.log('async1 end')//第一个微任务
// }

// async function async2() {
//     console.log('async2')
// }

// console.log('script start');

// setTimeout(function () {
//     console.log('setTimeout')//第一个宏任务
// }, 0)

// async1();

// new Promise(function (resolve) {
//     console.log('promise1')
//     resolve();
// }).then(function () {
//     console.log('promise2')//第二个微任务
// })
// script start
// async1 start
// async2
// promise1
// async1 end
// promise2
// setTimeout

// 2
// async function async1() {
//     console.log('async1 start');
//     await async2();
// };

// async function async2() {
//     console.log('async2');
// }

// console.log('script start');

// setTimeout(function () {
//     console.log('setTimeout');//第一个宏任务
// }, 0);

// async1();

// new Promise(function (resolve) {
//     console.log('promise1');
//     resolve();
// }).then(function () {
//     console.log('promise2');//第一个微任务
// });

// console.log('script end2');
// script start
// async1 start
// async2
// promise1
// script end2
// promise2
// setTimeout

// 3
new Promise((resolve, reject) => {
    console.log(1);
    new Promise((resolve, reject) => {
        console.log(2);
        setTimeout(() => {
            resolve(3)
            console.log(4);
        });
    })
        .then(data => {
            setTimeout(() => {
                console.log(5);
            });
            console.log(data);// 3
        })
    setTimeout(() => {
        resolve(6)
        console.log(7);
    }, 0);
})
    .then(data => {
        console.log(data, '---');// 6 ---
        setTimeout(() => {
            console.log(8);
        });
        console.log(9);
    })
