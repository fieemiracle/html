// Object.prototype.toString().call()
let str='123';
console.log(Object.prototype.toString.call(str));//'[object String]'
console.log(Object.prototype.toString.apply(str));//'[object String]'

// 生成变量O，让O变为一个对象，让class成为内部属性
// let O={
//     [[class]]:String,
// }

if(Object.prototype.toString.call(str).slice(8,-1)==='String'){
    console.log('yes');
}


// let classType={};
// ['Boolean', 'Array', 'Object', 'String', 'Number', 'Date', 'Function', 'Undefined', 'Null',
// 'Symbol', 'Bigint'].map((item,index)=>{
//     classType["[object"+item+"]"]=item.toLocaleLowerCase()
// })
// function JudgeType(obj){
//     return typeof obj==='object'||typeof obj==='function'?classType[Object.prototype.toString.call(obj)]||'object':typeof obj
// }
// JudgeType(obj)


// 自带方法
Array.isArray()

// instanceof实现原理
function instance_of(L,R){
    let O=R.prototype
    L=L._proto_;
    while(L!=null){
        if(L===O) return true
        L=L._proto_
    }

    return false
}
instance_of(arr,Array)
instance_of(arr,Object)