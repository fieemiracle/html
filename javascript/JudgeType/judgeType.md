1、原始类型：String,Number,Boolean,Null,Undefined,Symbol,Bigint
2、引用类型：Object,Array,Date,Function,RegExp

3、特例：
    typeof(null)----'object'

4、typeof
    （1）除了Null，能正确判断原始类型（基本类型）
    （2）判断引用类型时，除了能正确Function,其他类型都是Object
    所以，typeof只能判断原始类型，且除了Null;对于引用类型，typeof能正确判断Function

5、Object.prototype.toString().call()内部逻辑
    （1）如果this值是Undefined，就会返回[object Undefined]
    （2）如果this值是Null，就会返回[object Null]
    （3）生成变量O，让O成为ToObject(this)的结果
    （4）让class成为O内部属性[[class]]的值
    （5）最后返回由'[object'和class和']'三个部分组成的字符串
    Object.prototype.toString().call()能判断各种类型