// setTimeout(_ => console.log(4))

// new Promise(resolve => {
//     resolve();
//     console.log(1);
// }).then(_ => {
//     console.log(3);
// })

// console.log(2)


new Promise((resolve, reject) => {
    console.log(1);
    new Promise((resolve, reject) => {
        console.log(2);
        setTimeout(() => {
            resolve(3);
            console.log(4);
        });
    }).then(data => {
            setTimeout(() => {
                console.log(5);//第三
            });
            console.log(data);// 3  第一微
        })
    setTimeout(() => {
        resolve(6)
        console.log(7);
    }, 0);
}).then(data => {
        console.log(data, '---');// 6 --- 第二微
        setTimeout(() => {
            console.log(8);//第四
        });
        console.log(9);//第二微
    })



// new Promise(resolve => {
//     resolve();
// })
//     .then(() => {
//         new Promise(resolve => {
//             resolve();
//         })
//             .then(() => { console.log(111); })
//             .then(() => { console.log(222); });
//     })
//     .then(() => {
//         new Promise((resolve => {
//             resolve()
//         }))
//             .then(() => {
//                 new Promise((resolve) => {
//                     resolve()
//                 })
//                     .then(() => { console.log(333) })
//                     .then(() => { console.log(444) })
//             }).then(() => {
//                 console.log(555);
//             })
//     })
//     .then(() => {
//         console.log(666);
//     })