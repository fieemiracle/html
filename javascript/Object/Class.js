// 类的包装
// 原始类型是不能添加属性或者方法的
// var str = 'hello'
// var str = new String('hello')  // 字符串对象
// // 'hello'
// console.log(str.length);



// var obj = {
//   a: 1
// }
// console.log(obj.b);



// var num = new Number(123)
// num.abc = 'aaa'
// console.log(num);


// var num = 4
// num.len = 3

// // var num = new Number(4).len = 3
// // delete num.len
// console.log(num.len);

var arr=[1,2,3,4,5];
arr.length=2;
console.log(arr);//2

var str='abcd';
str.length=2;
console.log(str.length);//4 字符串对象天生就具备的属性

// var array=new Array();

// 面试题
var str='abc';
str +=1;
var test=typeof(str);//String  var test=new String('String');
if(test.length==6){
    test.sign='typeof的返回结果可能是String';//test是字符串类型，sign属性加不进去 delete test.sign
}
console.log(test.sign);//在这里，相当于test挂载了sign属性但是没有值  undefined