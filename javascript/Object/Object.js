
// Object 对象类型
var obj={
    name:'宇航员',
    age:20,
    like:{
        n:'running'
    }
}
obj.sex='boy';

// 变量
var height='yourheight';
obj[height]=185;

// 删除
delete obj.age;

console.log(obj);
console.log(obj.name);
console.log(obj.like.n);

// 对象创建
var name={};//对象字面量 || 对象直接量
var age=new Object();
Object.create(obj);

// 构造函数与普通函数的区别
function Car(){
    this.name='BMW',
    this.height='140',
    this.width='490',
    this.weight='1000',
    this.health=100,
    this.run=function(){
        this.health--;
    }
}
var car=new Car('black');
car.name='BNW-2';
Car.price='50W';
var car1=new Car();
car1.run();
console.log(car1);
console.log(Car);