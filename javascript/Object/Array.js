// Array
var arr=[1,'a',4,'ad',true];

// 访问
console.log(arr[4]);

// 增加
arr.push(3);//数组尾部增加
arr.unshift('start');//数组头部增加
arr.splice(1,0,'b');//指定位置添加
console.log(arr);

// 删除
arr.pop();//尾部删除
arr.shift();//首部删除
console.log(arr.splice(1,1));//指定位置删除,并返回删除元素

// 转字符串
console.log(arr.join());//默认逗号拼接,b,a,4,ad,true
console.log(arr.join(''));//没有符号拼接，ba4adtrue

// 反转
console.log(arr.reverse());//[ true, 'ad', 4, 'a', 'b' ]

// 合并
var arr1=['you','are','a','banana'];
console.log(arr.concat(arr1));

// 排序
var arr2=['d','b','f','c','a'];
var arr3=[12,54,56,34,25,17,18];
console.log(arr2.sort());//字符串默认升序
console.log(arr3.sort((a,b)=> a-b));//升序
console.log(arr3.sort((a,b)=> b-a));//降序
