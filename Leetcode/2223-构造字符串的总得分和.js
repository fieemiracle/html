/**
 * @param {string} s
 * @return {number}
 */
const sumScores = function(s) {
	let answer = answer_function(s)
	return answer.reduce((pre, cur) => pre + cur) + s.length
};
const answer_function = (s) => {
	let answer = new Array(s.length).fill(0)
	for (let i = 1, left = 0, right = 0; i < s.length; i++) {
		if (i <= right && answer[i - left] < right - i + 1) {
			answer[i] = answer[i - left]
		} else {
			answer[i] = Math.max(0, right - i + 1)
			while (i + answer[i] < s.length && s[answer[i]] === s[i + answer[i]]) answer[i]++
		}
		if (i + answer[i] - 1 > right) {
			left = i
			right = i + answer[i] - 1
		}
	}
	return answer
}
const s = "azbazbzaz"
sumScores(s)