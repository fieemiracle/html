const myArray=[1,2];
// 添加
myArray.unshift('happy');
myArray.push('dog');
console.log(myArray);//[ 'happy', 1, 2, 'dog' ]

// splice会改变原数组
myArray.splice(1,1)
console.log(myArray);//[ 'happy', 2, 'dog' ]

// slice不会改变原数组
console.log(myArray.slice(1));//一个参数，直接切到最后
console.log(myArray.slice(1,2));//两个参数，切到第二个参数，取右不取左边
