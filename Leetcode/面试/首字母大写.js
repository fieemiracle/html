// 写一个通用方法，把字符串中所有单词首字母转为大写
// eg：hello world ===> Hello World

function localToUpper(str) {
	// 方式一
	let strToArr = str.split(" ")
	for (let i = 0; i < strToArr.length; i++) {
		strToArr[i] = strToArr[i].charAt(0).toUpperCase() + strToArr[i].substring(1)
	}
	console.log(strToArr.join(" "));

	// 方式二
	let resStr = str
		.split(' ')
		.map(word => word.charAt(0).toUpperCase() + word.slice(1))
		.join(' ');
	console.log(resStr);
}
let str = "hello jerry and jane"
localToUpper(str)
