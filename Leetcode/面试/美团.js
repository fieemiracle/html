var MyLinkedList = function() {
	this.size = 0;
	this.head = new ListNode(0);
};

MyLinkedList.prototype.get = function(index) {
	if (index < 0 || index >= this.size) {
		return -1;
	}
	let cur = this.head;
	for (let i = 0; i <= index; i++) {
		cur = cur.next;
	}
	return cur.val;
};

MyLinkedList.prototype.addAtHead = function(val) {
	this.addAtIndex(0, val);
};

MyLinkedList.prototype.addAtTail = function(val) {
	this.addAtIndex(this.size, val);
};

MyLinkedList.prototype.addAtIndex = function(index, val) {
	// let cur = this.head
	// for (let i = 0; i < this.size; i++) {
	// 	if (i == index) {
	// 		let temp = new ListNode(val)
	// 		let curNext = cur.next
	// 		cur.next = temp
	// 		temp.next = curNext
	// 	}
	// 	cur = cur.next
	// }
};

MyLinkedList.prototype.deleteAtIndex = function(index) {
	// let cur = this.head
	// for (let i = 0; i < this.size; i++) {
	// 	if (index == 0 && i == index) { //头结点

	// 	}
	// 	if (index == this.size - 1 && i == index) { //尾结点

	// 	}
	// 	if (i + 1 == index && cur.next.next != null) {
	// 		cur.next = cur.next.next
	// 	}
	// 	cur = cur.next
	// }
};

function ListNode(val, next) {
	this.val = (val === undefined ? 0 : val)
	this.next = (next === undefined ? null : next)
}