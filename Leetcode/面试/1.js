instanceof操作符的问题在于， 它假定只有一个全局环境。 如果网页中包含多个框架， 那实际上就存在两个以上不同的全局执行环境， 从而存在两个以上不同版本的Array构造函数。
如果你从一个框架向另一个框架传入一个数组， 那么传入的数组与在第二个框架中原生创建的数组分别具有各自不同的构造函数。
var iframe = document.createElement('iframe');
document.body.appendChild(iframe);

var arr = [1, 2, 3];
xArray = window.frames[0].Array; //iframe中的构造函数
var arrx = new xArray(4, 5, 6);

console.log(arrx instanceof Array); //false
console.log(arrx.constructor == Array); // false

console.log(Array.prototype == xArray.prototype); //false
console.log(arr instanceof xArray); //false

console.log(arrx.constructor === Array); // false
console.log(arr.constructor === Array); // true
console.log(arrx.constructor === xArray); // true
console.log(Array.isArray(arrx)); //true


let num = 0.411523

function findMax(arr) {

}