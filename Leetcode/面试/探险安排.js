// 小明要为n个人计划一次火星的探险， 其中一个重要的任务是为每个参与者安排食物。 仓库里面有m个能用一天的食物包裹， 每个食物包裹有不同的类型ai。
// 每个人每天必须用且只能用一个食物包裹。 由于某些原因， 在整个过程中， 每个人只能用同一种类型的食物包裹， 但是不同的人用的食物包裹可以不一样。
// 给出人数以及食物包裹的情况， 请你求出这趟探险最多可以持续多少天
// 第一行两个整数，n和m，表示人数和食物包裹的个数。
// 第二行m个整数，表示每个食物包裹的类型。
// 满足1 <= n <= 100,1 <= m <= 100,1 <= ai <= 100。

const confimExporeTimes = (n, m, a) => {
	const foodsMap = new Map()
	for (let item of a) {
		if (!foodsMap.has(item)) {
			foodsMap.set(`${item}`, 1)
		}
		foodsMap.get(`${item}`) = foodsMap.get(`${item}`) = 1
	}
	console.log(foodsMap);
}
let n = 4,
	m = 10;
const a = [1, 5, 2, 1, 1, 1, 2, 5, 7, 2]
confimExporeTimes(n, m, a)
