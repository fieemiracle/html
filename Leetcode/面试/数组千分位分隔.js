// 78819534849.8259 => 78,819,534,849.825,9
// 4种JavaScript实现千位分隔符的方法


let num = 78819534849.825956

function formatNumber(num) {
	// 方法一：Number.prototype.toLocaleString,小数部分会根据四舍五入只留下三位
	console.log(num.toLocaleString()); //78,819,534,849.826

	// 方法二：Intl.NumberFormat(),可以把普通的数字，转换成不同的货币和格式样式字符串
	let formatNum = new Intl.NumberFormat('en-US')
	console.log(formatNum.format(num)); //78,819,534,849.826

	console.log(new Intl.NumberFormat('de-DE', {
		style: 'currency',
		currency: 'EUR'
	}).format(num)); //78.819.534.849,83 €

	console.log(new Intl.NumberFormat('ja-JP', {
		style: 'currency',
		currency: 'JPY'
	}).format(num)); //￥78,819,534,850

	console.log(new Intl.NumberFormat('en-IN', {
		maximumSignificantDigits: 3
	}).format(num)); //78,80,00,00,000


	// 方式三：循环和正则表达式
	let numToStr = num.toString().split('.')
	let pattern1 = /(-?\d+)(\d{3})/
	while (pattern1.test(numToStr[0])) {
		numToStr[0] = numToStr[0].replace(pattern1, "$1,$2")
		numToStr[1] = numToStr[1].split("").reverse().join("").replace(pattern1, "$1,$2")
	}
	console.log(numToStr.join(".")); //78,819,534,849.82,596

	// 方式四：正则表达式
	let parts = num.toString().split('.')
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")
	parts[1] = parts[1].split("").reverse().join("").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
	console.log(parts.join("."));
}
formatNumber(num) //78,819,534,849.825,9
