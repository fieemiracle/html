系统会随机生成id为jsLayout的 m x n 表格(m >= 3, n >= 3)， 请按照如下需求实现bind函数
1、 bind 函数为每个td节点绑定click事件， 当某个td节点被点击时class变为current， 同时以该td为中心的九宫格td节点class变为wrap
2、 每次click后， 请清空所有不需要变动的td节点的class
3、 请不要手动调用bind函数
4、 当前界面为系统生成 10 * 10 表格， 执行 bind 函数， 并点击第一个td后的效果
5、 请不要手动修改html和css
6、 不要使用第三方插件
7、 请使用ES5语法
