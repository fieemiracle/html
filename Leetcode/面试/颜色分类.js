// 问题描述: 给定一个包含红色、白色和蓝色，一共 n 个元素的数组，原地对它们进行排序，使得相同颜色的元素相邻，并按照红色、白色、蓝色顺序排 列。 
// 此题中，我们使用整数 0、 1 和 2 分别表示红色、白色和蓝色

function colorsClassify(nums) {
	// 冒泡排序
	for (let i = 0; i < nums.length; i++) { //控制循环次数
		for (let j = 0; j < nums.length; j++) { //控制内层循环
			if (nums[j] > nums[j + 1]) {
				let temp = nums[j];
				nums[j] = nums[j + 1]
				nums[j + 1] = temp
			}
		}
	}
	console.log(nums);
}
const colors = [2, 0, 2, 1, 1, 0]
colorsClassify(colors)

// 官方解析
function swap(nums, i, j) {
	let t = nums[j];
	nums[j] = nums[i];
	nums[i] = t;
}

function sortColors(nums) {
	let ptr = 0;
	for (let i = 0; i < nums.length; i++) {
		if (nums[i] === 0) {
			swap(nums, i, ptr);
			++ptr;
		}
	}
	for (let i = 0; i < nums.length; i++) {
		if (nums[i] === 1) {
			swap(nums, i, ptr);
			++ptr;
		}
	}
	return nums;
}
var line;
while (line = read_line()) {
	line = line.split(' ');
	var lines = line.map(Number)
	print(JSON.stringify(sortColors(lines)));
}
