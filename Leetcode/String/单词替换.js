// 648.单词替换
/**
 * @param {string[]} dictionary
 * @param {string} sentence
 * @return {string}
 */
// 方法一：两层for循环
// var replaceWords = function(dictionary, sentence) {
// 	const helper = sentence.split(' ');
// 	// console.log(helper);
// 	for (let root of dictionary) {
// 		for (let i = 0; i < helper.length; i++) {
// 			let word = helper[i].slice(0, root.length);
// 			if (root == word) {
// 				helper[i] = root;
// 				break;
// 			}
// 		}
// 	}
// 	console.log(helper.join(' '));
// };

// 方法二：哈希表
var replaceWords = function(dictionary, sentence) {
	let rootSet = new Set();
	// 将词根加入集合
	for (let root of dictionary) {
		rootSet.add(root)
	}

	const helper = sentence.split(' ');
	for (let i = 0; i < helper.length; i++) {
		for (let j = 0; j < helper[i].length; j++) {
			if (rootSet.has(helper[i].substring(0, j + 1))) {
				helper[i] = helper[i].substring(0, j + 1)
				break;
			}
		}
	}
	return helper.join(' ')
}

let dictionary = ["cat", "bat", "rat"],
	sentence = "the cattle was rattled by the battery";
replaceWords(dictionary, sentence)
