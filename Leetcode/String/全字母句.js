/**
 * @param {string} sentence
 * @return {boolean}
 */
var checkIfPangram = function(sentence) {
	if (sentence.length < 26) return false;

	let arr = sentence.split('')
	// 去重
	// 方式一
	// let res = Array.from(new Set(arr))
	// return res.length == 26

	// 方式二
	// let len = arr.length
	// for (let i = 0; i < len; i++) {
	// 	for (let j = i + 1; j < arr.length; j++) {
	// 		if (arr[i] == arr[j]) {
	// 			arr.splice(j, 1)
	// 			j--;
	// 			len--
	// 		}
	// 	}
	// }
	// return len == 26

	// 方式三
	// let res = []
	// for (let i = 0, len = arr.length; i < len; i++) {
	// 	if (res.indexOf(arr[i]) === -1) {
	// 		res.push(arr[i])
	// 	}
	// }
	// return res.length == 26

	// 方式四
	// let res = []
	// for (let i = 0, len = arr.length; i < len; i++) {
	// 	if (!res.includes(arr[i])) {
	// 		res.push(arr[i])
	// 	}
	// }
	// return res.length == 26

	// 方式五
	let res = arr.filter((item, index) => arr.indexOf(item, 0) == index)
	return res.length == 26

};
