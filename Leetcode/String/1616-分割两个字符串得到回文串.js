/**
 * @param {string} a
 * @param {string} b
 * @return {boolean}
 */
var checkPalindromeFormation = function(a, b) {
	let n = a.length
	if (n == 0 || n == 1) return true
	b = b.split('').reverse().join('')
	let i = 0;
	while (i < n) {
		let charA = a[i];
		let charB = b[i];
		if (charA == charB) {
			console.log(true, '///');
			return true
		}
		i++
	}
	console.log(false, '//');
	return false
};
let a = "abdef",
	b = "fecab";
checkPalindromeFormation(a, b)



var checkPalindromeFormation = function(a, b) {
	return checkConcatenation(a, b) || checkConcatenation(b, a);
}

const checkConcatenation = (a, b) => {
	const n = a.length;
	let left = 0,
		right = n - 1;
	while (left < right && a[left] === b[right]) {
		left++;
		right--;
	}
	if (left >= right) {
		return true;
	}
	return checkSelfPalindrome(a, left, right) || checkSelfPalindrome(b, left, right);
}

const checkSelfPalindrome = (a, left, right) => {
	while (left < right && a[left] === a[right]) {
		left++;
		right--;
	}
	return left >= right;
};
