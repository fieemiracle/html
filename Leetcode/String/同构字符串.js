// 205.同构字符串
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
 var isIsomorphic = function(s, t) {
    let sSymbol=[],
        tSymbol=[];
    for (let i=0; i<s.length; i++) {
        if(sSymbol.indexOf(s[i])==tSymbol.indexOf(t[i])) {
            sSymbol.push(s[i]);
            tSymbol.push(t[i]);
        }else return false;
    }

    return true;
};