/**
 * @param {string} s1
 * @param {string} s2
 * @return {boolean}
 */
var CheckPermutation = function(s1, s2) {
	if (s1.length !== s2.length) {
		return false;
	}
	let obj = {};
	for (let i = 0; i < s1.length; i++) {
		if (!obj[s1[i]]) {
			obj[s1[i]] = 1;
			continue;
		}
		obj[s1[i]]++;
	}

	for (let j = 0; j < s1.length; j++) {
		if (!obj[s2[j]]) {
			return false;
		} else {
			obj[s2[j]]--;
		}
	}

	for (let key in obj) {
		if (obj[key] != 0) {
			return false;
		}


	}
	return true;
};
