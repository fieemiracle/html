/**
 * @param {string} ransomNote
 * @param {string} magazine
 * @return {boolean}
 */
// charCodeAt() 方法可返回指定位置的字符的 Unicode 编码。这个返回值是 0 - 65535 之间的整数。
// 方法 charCodeAt() 与 charAt() 方法执行的操作相似，只不过前者返回的是位于指定位置的字符的编码，而后者返回的是字符子串。
// 语法
// stringObject.charCodeAt(index)

var canConstruct = function(ransomNote, magazine) {
	// 分别统计两个字符串中字符存在的个数
	let initArr = new Array(26).fill(0);
	for (let char of magazine) {
		initArr[char.charCodeAt() - 'a'.charCodeAt()]++;
	}
	for (let char of ransomNote) {
		initArr[char.charCodeAt() - 'a'.charCodeAt()]--;
		if (initArr[char.charCodeAt() - 'a'.charCodeAt()] < 0) return false;
	}
	return true;
};

let ransomNote = 'a',
	magazine = 'b';
canConstruct(ransomNote, magazine);
