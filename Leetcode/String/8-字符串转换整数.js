const {
	match
} = require("assert");

/**
 * @param {string} s
 * @return {number}
 */
var myAtoi = function(s) {
	// 去除首尾空格
	s = s.trim()
	let sign = 1 //符号
	let index = 0; //从当前小标开始
	let res = 0; //结果
	while (index < s.length) {
		// 判断符号
		if (s.charAt(index) == '-' || s.charAt(index) == '+') {
			sign = s.charAt(index) == '+' ? 1 : -1
			index++
		}
		for (; index < s.length; index++) {
			let item = s.charAt(index) - '0'
			// 在0-9范围
			if (item < 0 || item > 9) break;
			// 判断溢出
			if (res < Math.pow(-2, 31)) return res = Math.pow(-2, 31)
			if (res > Math.pow(2, 31) - 1) return res = Math.pow(2, 31) - 1
			res = res * 10 + item * sign
			console.log(res)
		}
	}
	// console.log(res*sign)
	return res
};
var myAtoi = function(str) {
	// let str = s.trim().match(/^[-|+]{0,1}[0-9]+/)
	let str = s.trim().match(/d+[0-9]+/)
	console.log(str);
	// 范围判断
	if (str !== null) {
		if (str[0] > Math.pow(2, 31) - 1) {
			return Math.pow(2, 31) - 1
		} else if (str[0] < Math.pow(-2, 31)) {
			return Math.pow(-2, 31)
		}
		return str[0]
	}
	return 0;
};
