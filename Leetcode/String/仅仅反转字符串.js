// 917.仅仅反转字符串
/**
 * @param {string} s
 * @return {string}
 */
var reverseOnlyLetters = function (s) {
    let reg = /[a-zA-Z]+$/;
    // (/^[a-zA-Z]+$/.test(s[left])))
    // console.log(reg.test('a'));
    let low = 0, high = s.length - 1;
    s = s.split('');
    console.log(s);
    while (low < high) {

        while (reg.test(s[low])===false) {
            console.log(reg.test(s[low]));
            low++
        }
        while (reg.test(s[high]) ===false) {
            console.log(reg.test(s[high]));
            high--;
        }

        let temp = s[low];
        s[low] = s[high];
        s[high] = temp;
        low++;
        high--;
    }
    // return s.join('');
    console.log(s.join(''));

    // const n = s.length;
    // const arr = [...s];
    // let left = 0, right = n - 1;
    // while (true) {
    //     while (left < right && !(/^[a-zA-Z]+$/.test(s[left]))) { // 判断左边是否扫描到字母
    //         left++;
    //     }
    //     while (right > left && !(/^[a-zA-Z]+$/.test(s[right]))) { // 判断右边是否扫描到字母
    //         right--;
    //     }
    //     if (left >= right) {
    //         break;
    //     }
    //     swap(arr, left, right);
    //     left++;
    //     right--;
    // }
    // // return arr.join('');
    // console.log(arr.join(''));

};

const swap = (arr, left, right) => {
    const temp = arr[left];
    arr[left] = arr[right];
    arr[right] = temp;
}
// let s="Test1ng-Leet=code-Q!"
let s = 'ab-cd'
reverseOnlyLetters(s)