var maximalNetworkRank = function(n, roads) {
	const connect = new Array(n).fill(0).map(() => new Array(n).fill(0));
	const degree = new Array(n).fill(0);
	for (const v of roads) {
		connect[v[0]][v[1]] = true;
		connect[v[1]][v[0]] = true;
		degree[v[0]]++;
		degree[v[1]]++;
	}

	let maxRank = 0;
	for (let i = 0; i < n; i++) {
		for (let j = i + 1; j < n; j++) {
			let rank = degree[i] + degree[j] - (connect[i][j] ? 1 : 0);
			maxRank = Math.max(maxRank, rank);
		}
	}
	return maxRank;
};


/**
 * @param {number} n
 * @param {number[][]} roads
 * @return {number}
 */
var maximalNetworkRank = function(n, roads) {
	// 保存每个图节点的度数
	const degrees = new Array(n).fill(0)
	// 记录所有的城市对
	let maxRank = 0;
	const citys = new Array(n).fill(0).map(() => new Array(n).fill(0))
	for (let road of roads) {
		let leftCity = road[0];
		let rightCity = road[1];
		citys[leftCity][rightCity] = true
		citys[rightCity][leftCity] = true
		degrees[leftCity]++
		degrees[rightCity]++
	}
	// console.log(degrees)
	// console.log(citys)
	for (let i = 0; i < n; i++) {
		for (let j = i + 1; j < n; j++) {
			let rank = degrees[i] + degrees[j] - (citys[i][j] ? 1 : 0)
			// console.log(degrees[i],degrees[j],citys[i][j])
			maxRank = Math.max(rank, maxRank)
			// if (i == 2 && j == 5) console.log(citys[i][j])
		}
	}
	return maxRank;
};
