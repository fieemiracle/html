/**
 * @param {number[]} nums
 * @return {number[][]}
 */
// 超出时间限制
var myThreeSum = function(nums) {
	if (nums.length < 3) return [null];
	let result = []
	nums = nums.sort((a, b) => a - b)
	for (let i = 0; i < nums.length; i++) {
		let tempI = nums[i]
		if (tempI > 0) break
		for (let j = i + 1; j < nums.length - 1; j++) {
			let tempJ = nums[j]
			let tempRest = nums.slice(j + 1)
			let target = (tempI + tempJ) == 0 ? tempI + tempJ : -(tempI + tempJ)
			// console.log(tempRest, target, '//');
			if (tempRest.indexOf(target) > -1) {
				// console.log(tempRest.indexOf(target), '//////');
				let tempK = tempRest[tempRest.indexOf(target)]
				result.push([tempI, tempJ, tempK])
				// console.log(result, '----------------');
			}
			continue;
		}
	}
	var obj = {}
	result.forEach(item => {
		obj[item] = item
	})
	console.log(Object.values(obj))
	return Object.values(obj)
};

// 官方解答
var threeSum = function(nums) {
	let ans = [];
	const len = nums.length;
	if (nums == null || len < 3) return ans;
	nums.sort((a, b) => a - b); // 排序
	for (let i = 0; i < len; i++) {
		if (nums[i] > 0) break; // 如果当前数字大于0，则三数之和一定大于0，所以结束循环
		if (i > 0 && nums[i] == nums[i - 1]) continue; // 去重
		let L = i + 1;
		let R = len - 1;
		while (L < R) {
			const sum = nums[i] + nums[L] + nums[R];
			if (sum == 0) {
				ans.push([nums[i], nums[L], nums[R]]);
				while (L < R && nums[L] == nums[L + 1]) L++; // 去重
				while (L < R && nums[R] == nums[R - 1]) R--; // 去重
				L++;
				R--;
			} else if (sum < 0) L++;
			else if (sum > 0) R--;
		}
	}
	return ans;
};

// 学以致用
let threeSum2 = function(nums) {
	let ans = []
	if (nums.length < 3) return ans;
	nums = nums.sort((a, b) => a - b)
	for (let i = 0; i < nums.length; i++) {
		if (nums[i] > 0) break; //第一个数就是一个正数
		if (i > 0 && nums[i] == nums[i - 1]) continue; // 去重
		let j = i + 1;
		let k = nums.length - 1;
		while (j < k) {
			let threeSum = nums[i] + nums[j] + nums[k]
			if (threeSum == 0) {
				ans.push([nums[i], nums[j], nums[k]])
				while (nums[j] == nums[j + 1]) j++
				while (nums[k] == nums[k - 1]) k--
				j++
				k--
			} else if (threeSum < 0) {
				j++
			} else {
				k--
			}
		}

	}
	console.log(ans);
	return ans
}
let nums = [-1, 0, 1, 2, -1, -4];
threeSum2(nums)
