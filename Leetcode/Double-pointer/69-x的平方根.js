//69. x的平方根
var mySqrt = function (x) {
    // 二分查找法
    let low = 0, high = x;
    while (low <= high) {
        let mid = parseInt(low + (high - low) / 2);
        if (x > mid * mid) {
            low = mid + 1;
        } else if (x < mid * mid) {
            high = mid - 1;
        } else {
            console.log(mid);
            // return mid;
        }
    }
    // return high;
    console.log(high);
};
mySqrt(8);