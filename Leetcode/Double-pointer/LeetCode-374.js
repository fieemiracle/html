/**
 * @param {number} n
 * @return {number}
 */
 var guessNumber = function(n,pick) {
    // 二分查找法
    let low=1,high=n;
    if(guess(low)==0) return low;
    if(guess(high)==0) return high;
    while(low<=high){
        let num=parseInt(low+(high-low)/2) ;
        if(guess(num)==-1){
            high=num-1;
        }else if(guess(num)==1){
            low=num+1;
        }else{
            return num;
        }
    }
    return high;
};

function guess(num){
    if(pick<num){
        return -1;
    }else if(pick>num){
        return 1;
    }else{
        return 0;
    }
}
guessNumber(10,8);