// 11.盛最多水的容器
/**
 * @param {number[]} height
 * @return {number}
 */
 var maxArea = function(height) {
    let left=0,right=height.length-1;
    let minHeight=Math.min(height[left],height[right]);
    let maxSqure=(right-left)*minHeight;
    // 思路：1、从两端开始，取得高度最小值和两端的距离相乘，循环跳出条件是left>=right 
    while(left<right && height.length>2){
        if(height[left]<=height[right]){// 2、如果左边的高度<右边高度，左边++
            left++;
            minHeight=Math.min(height[left],height[right]);
            maxSqure=Math.max(maxSqure,(right-left)*minHeight);
        }else{// 3、如果左边的高度>右边高度，右边--
            right--;
             minHeight=Math.min(height[left],height[right]);
             maxSqure=Math.max(maxSqure,(right-left)*minHeight);
        }
    }
    return maxSqure;//这里不仅包括经过遍历后替换最大面积的结果，还包括height.length<=2的情况，两种情况
};