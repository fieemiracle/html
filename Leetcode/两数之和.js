// 1.两数之和
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function (nums, target) {
    // 1、两层for循环
    /*for(let i=0;i<nums.length;i++){
        for(let j=i+1;j<nums.length){
            if(nums[i]+nums[j]==target){
                console.log([i,j]);
            }
        }
    }
    console.log([null,null]);*/

    // 2、一层for循环
    // for (let i = 0; i < nums.length; i++) {
    //     var j = target - nums[i];
    //     var search = nums.indexOf(j, i + 1);
    //     if (search !== -1) {
    //         console.log([i, search]);
    //     }
    // }
    // console.log([null, null]);

    // 3、
    const diffs={};
    for (let i = 0; i < nums.length; i++) {
        if(diffs[target - nums[i]]!=undefined){
            // return [diffs[target - nums[i]],i];
            console.log([diffs[target - nums[i]],i]);
        }else{
            diffs[nums[i]]=i;
        }
    }

};
const nums = [2, 7, 11, 15];
let target = 9
twoSum(nums, target)