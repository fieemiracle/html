// 13.罗马数字转整数
/**
 * @param {string} s
 * @return {number}
 */
 var romanToInt = function (s) {
    // <!-- 创建一个Map对象，存放罗马数字，罗马数字为键，对应的大小为值 -->
    let map = new Map([
        ['I', 1],
        ['V', 5],
        ['X', 10],
        ['L', 50],
        ['C', 100],
        ['D', 500],
        ['M', 1000]
    ]);
    let result = 0;
    for (let i = 0; i < s.length ; i++) {
        // <!-- 判断遍历字符串s得到的字符的大小 -->
        if (map.get(`${s[i]}`) < map.get(`${s[i+1]}`)) {
            result -= map.get(`${s[i]}`);
            // <!-- 当i+1==s.length-1时，i+1的下一项必须存在，可以没有值，所以循环的条件是s.length而不是s.length-1
            // 以便可以加到最后一项的值。    
        } else {
            result += map.get(`${s[i]}`) ;
        }
    }
    // return result;
    console.log(result);
};
