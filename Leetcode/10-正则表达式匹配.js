/**
 * @param {string} s
 * @param {string} p
 * @return {boolean}
 */
var isMatch = function(s, p) {
	// 匹配单个字符
	const matchChar = (char, s) => {
		if (s.indexOf(char) > -1) return true
		return false
	}
	// 匹配0个或多个连续字符
	const matchChars = (char, s) => {
		let firstIndex = s.indexOf(char)
		let lastIndex = s.lastIndexOf(char)
		if (s.indexOf(char)) { //没有该字符
			return true
		} else if (firstIndex > -1 && lastIndex > -1 && firstIndex !== lastIndex) { //匹配多次
			return true
		} else {
			return false
		}
	}
	let dotIndex = 0
	let starIndex = 0
	for (let i = 0; i < p.length; i++) {
		dotIndex = p.indexOf('.')
		starIndex = p.indexOf('*')
	}
};
let s = "aa",
	p = "a";

let s = "aa",
	p = "a*"

let s = "ab",
	p = ".*"