// 88.合并两个有序数组
/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
 var merge = function(nums1, m, nums2, n) {
    // nums1.length=m;
    // nums1=nums1.concat(nums2).sort((a,b)=>a-b);
    // console.log(nums1);

    // let p1=m-1;
    // let p2=n-1;
    // let p=m+n-1;
    // while(p2>=0){
    //     if(p1<0){
    //         nums1[p--]=nums2[p2--];
    //     }
    //     if(nums1[p1]>=nums2[p2]){
    //         nums1[p]=nums1[p1];
    //         p1--;
    //     }else{
    //         nums1[p]=nums2[p2];
    //         p2--;
    //     }
    //     p--;
    // }

    for(let i=0;i<nums2.length;i++){
        var arr=[...nums1]
        nums1.length=m;
        for(let j=0;j<nums1.length;j++){
            if(nums2[i]>nums1[j]){
                if(j>=m-1){
                    nums1.push(nums2[i])
                }
                continue;

            }else{
                arr.splice(j,0,nums2[i]);
                break;
            }
        }
    }
    console.log(nums1);
 };
 const nums1=[1,2,3,0,0,0];
 const nums2=[2,5,6];
 merge(nums1,3,nums2,3);