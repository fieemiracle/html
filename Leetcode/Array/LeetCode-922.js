// 922.按奇偶排序数组II
/**
 * @param {number[]} nums
 * @return {number[]}
 */
 var sortArrayByParityII = function(nums) {
    let len=nums.length;
    const result = new Array(len);

    // 偶数
    let i=0;
    for(let item of nums){
        if(item%2==0){
            result[i]=item;
            i+=2
        }
    }

    // 奇数
    i=1;
    for(let item of nums){
        if(let item of nums){
            if(item%2==1){
                result[i]=item;
                i+=2
            }
        }
    }

    return result;
};