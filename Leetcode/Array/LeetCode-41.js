// 41.缺失的第一个正数
/**
 * @param {number[]} nums
 * @return {number}
 */
 var firstMissingPositive = function (nums) {
    // 过滤0和负数
    nums = [...new Set(nums.filter(item => item > 0).sort((a, b) => a - b))];
    let max = Math.max.apply(Math, nums);
    let min = Math.min.apply(Math, nums);
    if (!nums.length) {
        return 1
    }
    for (let i = 0; i < nums.length; i++) {
        if (min > 1) {
            return 1;
        } else if (max - min == 1) {
            return max + 1;
        } else if (Math.abs(nums[i] - nums[i + 1]) == 1) {
            continue;
        } else {
            return nums[i] + 1;
        }
    }
};