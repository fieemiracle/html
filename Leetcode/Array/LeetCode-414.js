var thirdMax = function(nums) {
    // 思路：
    // 1、将数组降序**排序**，方便直接取指定下标的数  （升序也可以）  
    // 2、将排序后的数组转换为集合，目的是**去重**，通过`Array.from()`将集合转换为一个**新数组**，这样方便直接获取数组指定下标
    // 3、通过数组的length**判断数组长度**，集合长度大于等于3，，得到第三个数(第三大)，下标为2（降序）；下标为length-1(升序)
    // 4、否则，返回最后一个数(最大)，下标为0（降序）；下标为length-3(升序)
    nums=nums.sort((a,b)=>{return b-a});
    let numsSet=Array.from(new Set(nums)) ;
    if(numsSet.length<3){
        // return numsSet[0];
        console.log(numsSet[0]);
    }else{
        // return numsSet[2];
        console.log(numsSet[2]);
    }
};

