// 905.按奇偶排序数组
/**
 * @param {number[]} nums
 * @return {number[]}
 */
 var sortArrayByParity = function(nums) {
    // 方法一
    // const oldArray=nums.filter((value)=>{
    //     return value%2==1;
    // })
    // console.log(oldArray);
    // const evalArray=nums.filter((value)=>{
    //     return value%2==0
    // })
    // console.log(evalArray);
    // console.log(evalArray.concat(oldArray));

    // 方法二
    let index=0;
    const result=Array.from(nums.length).fill(0);
    for(let value of nums){
        if(value%2==0){
            result[index++]=value;
        }
        
    }
    for(let value of nums){
        if(value%2==1){
           result[index++]=value; 
        }
        
    }
    console.log(result);
};
const nums=[3,1,2,4];
sortArrayByParity(nums)