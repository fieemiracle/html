// 剑指 Offer 17. 打印从1到最大的n位数
/**
 * @param {number} n
 * @return {number[]}
 */
var printNumbers = function(n) {
	let nums = new Array(Math.pow(10, n) - 1)
	let newNums = nums.fill(0).map((value, index, arr) => {
		value = index + 1
		return value
	})
	return newNums

};
