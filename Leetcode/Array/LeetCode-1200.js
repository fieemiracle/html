// 1200.最小绝对差
var minimumAbsDifference = function (arr) {
    // 升序排序
    arr = arr.sort((a, b) => a - b);
    // 定义一个变量保存最小差值
    let minDiff = Infinity;
    // 定义一个数组保存最小差值数组对
    let diffArr = [];
    // 先找最小差值
    for (let i = 0; i < arr.length - 1; i++) {
        minDiff = Math.min(minDiff, Math.abs(arr[i] - arr[i + 1]));
    }
    // 再找数字对
    for (let j = 0; j< arr.length - 1; j++) {
        if (Math.abs(arr[j] - arr[j + 1]) ==minDiff) {
            diffArr.push([arr[j], arr[j + 1]]);
        }
    }
    // return diffArr;
    console.log(diffArr);
};
