// 34.在排序数组中查找元素的第一个和最后一个位置
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
 var searchRange = function(nums, target) {

};

// 二分查找
const binarySearch(nums,target){
    let slow=nums[0],high=nums[nums.length-1];

    while (slow < high) {
        const mid=(slow+high)/2;
        if (nums[mid] < target) {
            slow = mid;
        } else if (nums[mid] > target) {
            high =  mid;

    }
}