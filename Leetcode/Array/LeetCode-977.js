// 977.有序数组的平方
/**
 * @param {number[]} nums
 * @return {number[]}
 */
 var sortedSquares = function(nums) {
    let result=nums.map((value)=>{
        return Math.pow(value,2)
    })
    result=result.sort((a,b)=>a-b);
    console.log(result);
};
const nums=[-4,-1,0,3,10];
sortedSquares(nums)