// 148.排序链表
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */

//  方法一：自顶向下归并排序
//  对链表自顶向下归并排序的过程如下。

//  找到链表的中点，以中点为分界，将链表拆分成两个子链表。寻找链表的中点可以使用快慢指针的做法，快指针每次移动 2步，慢指针每次移动 1步，
// 当快指针到达链表末尾时，慢指针指向的链表节点即为链表的中点。

//  对两个子链表分别排序。

//  将两个排序后的子链表合并，得到完整的排序后的链表。可以使用「21. 合并两个有序链表」的做法，将两个有序的子链表进行合并。

//  递归的终止条件是链表的节点个数小于或等于 11，即当链表为空或者链表只包含 11 个节点时，不需要对链表进行拆分和排序。



var sortList = function (head) {
    return sortTwoLists (head,null)
};

// 将链表分成两个链表
const sortTwoLists = function (head, tail) {
    if(!head){
        return head;
    }
    if(head.next===tail){
        head.next = null;
        return head;
    }

    let fast=slow=head;
    // while(fast!=slow){
    //     fast=fast.next.next;
    //     slow=slow.next;
    // }

    while (fast !== tail) {
        slow = slow.next;
        fast = fast.next;
        if (fast !== tail) {
            fast = fast.next;
        }
    }

    const middle = head.next;
    return mergeTwoLists(separateList(head, middle),separateList(middle, tail));

}

// 合并两个链表
const mergeTwoLists = function (list1, list2) {
    let head = new ListNode();
    let p = head;

    while (list1 && list2) {
        if (list1.val <= list2.val) {
            p.next = list1;
            p = p.next;
            list1 = list1.next;
        } else {
            p.next = list2;
            p = p.next;
            list2 = list2.next;
        }

    }

    if (list1) {
        p.next = list1;
    } else {
        p.next = list2
    }

    return head.next;
}

// const merge = (head1, head2) => {
//     const dummyHead = new ListNode(0);
//     let temp = dummyHead, temp1 = head1, temp2 = head2;
//     while (temp1 !== null && temp2 !== null) {
//         if (temp1.val <= temp2.val) {
//             temp.next = temp1;
//             temp1 = temp1.next;
//         } else {
//             temp.next = temp2;
//             temp2 = temp2.next;
//         }
//         temp = temp.next;
//     }
//     if (temp1 !== null) {
//         temp.next = temp1;
//     } else if (temp2 !== null) {
//         temp.next = temp2;
//     }
//     return dummyHead.next;
// }

// const toSortList = (head, tail) => {
//     if (head === null) {
//         return head;
//     }
//     if (head.next === tail) {
//         head.next = null;
//         return head;
//     }
//     let slow = head, fast = head;
//     while (fast !== tail) {
//         slow = slow.next;
//         fast = fast.next;
//         if (fast !== tail) {
//             fast = fast.next;
//         }
//     }
//     const mid = slow;
//     return merge(toSortList(head, mid), toSortList(mid, tail));
// }

// var sortList = function(head) {
//     return toSortList(head, null);
// };