// 206. 反转链表
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
 var reverseList = function(head) {
    let current=head;
    let pre=null;

    let n=getLength(head);
    for(let i=0;i<n;i++) {
        // 改变current.next的指向
        let temp=new ListNode(0,current.next);
        current.next=pre;
        pre=current;
        current=temp;

    }
    return pre.next;
};

// 遍历得到链表的长度
const getLength=function(hend){
    let n=0;
    while(head.next){
        n++;
        head = head.next;
    }
    return n;
}