// {
//     val:1;//数据域
//     // 指针域，指向下一个节点
//     next:{
//         val:2;
//         next:...
//     }
// }

// 创建链表节点
function ListNde(val){
    this.val=val;
    this.next=null;
}

const node=new ListNde(1);
const nnode2=new ListNde(2);
node.next=node2;

// 添加
const node3=new ListNde(3);
node3.next=node1.next;
node1.next=node3;

// 删除
node1.next=node3.next;

// 深入
const arr=['hello',1,{a:1}];
// 数组元素不是都一种类型，根据哈希表映射，存储是不连续的

// 链表的弊端
const index=10;
let node=head;
for(let i=0;i<index&&node;i++){
    node=node.next;
}