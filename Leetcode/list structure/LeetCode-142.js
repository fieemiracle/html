// 142. 环形链表 II

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
 var detectCycle = function(head) {
    let mySet=new Set();
    let current=head;
    while (current) {
        if(mySet.has(current)) {
            return current;
        }
        mySet.add(current);
        current=current.next;
    }

    return null;
};