// 24.两两交换链表中的节点
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
 var swapPairs = function(head) {
    // 创建虚拟节点
    let dummy = new ListNode(0, head);
    let  current = dummy;

    while (current.next && current.next.next) {
        let temp1 = current.next,temp2 = current.next.next;
        current.next=temp2;
        temp1.next = temp2.next;
        temp2.next = temp1;
        // current.next = temp2;
        current = temp1;
    }
    return dummy.next;
};