// 234.回文链表
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {boolean}
 */

// 法一
 var isPalindrome = function(head) {
    // 判断回文链表思路：1、将链表反转，得到新链表；2、判断新链表和原链表是否相等
    // 得到反转后的链表
    let newHead=reverseList(head);
    // 遍历两个链表，并判断一一对应的值是否相等
    while(head&&newHead){
        if(head.val==newCurrent.val){
            head=head.next;
            newHead=newHead.next;
        }else{
            return false;
        }
    }
    return true;
};

// 反转链表
let reverseList=function(head){
    // 先获取链表长度
    let getLength=function(head){
        let listLength=0;
        while(head){
            listLength++;
            head=head.next;
        }
        return listLength;
    }

    // 反转链表
    let current=head;
    let dummy=null;
    let listLen=getLength(head);
    for(let i=0; i<listLen; i++){
        let save=current.next;
        // 改变指针指向
        current.next=dummy;
        dummy=current;
        current=save;
    }
    return dummy;
}


// 法二
var isPalindrome = function(head) {
    const vals = [];
    while (head !== null) {
        vals.push(head.val);
        head = head.next;
    }
    for (let i = 0, j = vals.length - 1; i < j; ++i, --j) {
        if (vals[i] !== vals[j]) {
            return false;
        }
    }
    return true;
};