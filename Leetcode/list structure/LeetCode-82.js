// 82.删除排序链表中的重复元素 II

// 连头结点都要删除的话，考虑创建虚拟节点

/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
 var deleteDuplicates = function(head) {
    // 创建虚拟节点
    let dummy=new ListNode();
    dummy.next=head;
    let current=dummy;

    while(current.next && current.next.next){
        if(current.next.val===current.next.next.val){
            // 保存重复的值
            let val=current.next.next.val;

            while(current.next && current.next.val===val){
                current.next=current.next.next;
            }
        }else{
                current=current.next;
            }
    }

    return dummy.next;

};