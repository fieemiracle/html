// 160.相交链表
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} headA
 * @param {ListNode} headB
 * @return {ListNode}
 */
var getIntersectionNode = function (headA, headB) {
    // 方法1
    let mySet = new Set();
    let currentA = headA;
    let currentB = headB;
    while (currentA) {
        mySet.add(currentA);
        currentA = currentA.next;
    }
    while (currentB) {
        if (mySet.has(currentB)) {
            return currentB;
        }
        currentB = currentB.next;
    }

    return null;
};