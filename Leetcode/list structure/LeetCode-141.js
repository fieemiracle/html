//141. 环形链表
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {boolean}
 */
 var hasCycle = function(head) {
    // 遍历
    // while(head){
    //     if(head.title){
    //         return true;
    //     }else{
    //         head.title=true;
    //         head=head.next;
    //     }
    // }
    // return false;


    // 快慢指针：{}=={}？FALSE
    var i = head
    if(!head){
        return false
    }
    while(i.next){
        i = i.next
        if(i.next){
            i = i.next
        }else{
            return false
        }
        head =head.next
        if(head == i){
            return true
        }
    }
    return false
    
};