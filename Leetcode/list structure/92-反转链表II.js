/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} left
 * @param {number} right
 * @return {ListNode}
 */
function ListNode(val, next) {
	this.val = (val === undefined ? 0 : val)
	this.next = (next === undefined ? null : next)
}
var reverseBetween = function(head, m, n) {
	// function getListLength(head){
	//     let n=0;
	//     while(head){
	//         head=head.next;
	//         n++
	//     }
	//     return n;
	// }
	//m==n不需要反转
	if (m == n) return head;
	//只有一个节点
	if (head == null && head.next == null) return head;

	let curPointer = head;
	let endPointer = null;
	let startPointer = null;

	for (let i = 1; i <= n; i++) {
		if (i == m - 1) {
			startPointer = curPointer;
		}
		curPointer = curPointer.next
	}
	endPointer = curPointer;

	let prePointer = null;
	let tempPointer = null;
	if (startPointer) { //起始节点不是头结点
		curPointer = startPointer.next;
		while (curPointer !== endPointer) {
			tempPointer = curPointer.next;
			curPointer.next = prePointer;
			prePointer = curPointer;
			curPointer = tempPointer;
		}
		startPointer.next.next = endPointer;
		startPointer.next = prePointer;
	} else { //起始节点是头结点
		curPointer = head;
		while (curPointer !== endPointer) {
			tempPointer = curPointer.next;
			curPointer.next = prePointer;
			prePointer = curPointer;
			curPointer = tempPointer;
		}
		head.next = endPointer;
		head = prePointer;
	}
	console.log(head);
	return head;
};
const head = [1, 2, 3, 4, 5];
let left = 2,
	right = 4;
reverseBetween(head, left, right)
