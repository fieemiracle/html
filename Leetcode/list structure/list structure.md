链表
    （1）有序链表,线性结构（有且只有一个前驱，有且只有一个后继）
    （2）节点的分部在内存中是离散型的
数组
    （1）数组的增删会导致后序所有元素的位置变动，所以数组的增删操作时间复杂度就是O（n）
    （2）如果数组的元素都是基本类型的，那么他就是真正意义上的数组
    （3）如果数组的元素有引用类型，那么他实际上不是真正的数组
    （4）数组中查找某个元素的时间复杂度O（1）

链表优点（存在意义）
    （1）链表的增删不会导致后续所有元素的位置变动
    （2）对比数组，链表在添加和删除元素的时候不需要移动多余的元素（性能比较高），是按复杂度O（1）

链表的弊端
    （1）链表的遍历比较复杂，必须从链表的头部开始遍历
    （2）查找某个元素的时间复杂度O（n）

数组与链表的区别：
    （1）存储方式:数组的存储是连续的；链表的存储是离散的
    （2）遍历方式：数组的遍历时间复杂度O（1）；链表的遍历时间复杂度O（n）
    （3）增删方式：数组的增删会改变后续元素的位置；链表的增删不会改变后续元素的位置

模式识别:
    一旦需要定位链表中的特殊位置，可使用快慢指针
    碰到单链表删除结点的问题，我们需要找到该结点的前驱