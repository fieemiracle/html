// 2.两数相加
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function (l1, l2) {
    let sum = 0, carry = 0;
    let newList=null,current=null;
    let getData = (list) => { return list ? list.val : 0 };
    let getPointer = (list) => { return list ? list.next : list };
    while (l1 && l2) {
        sum = getData(l1) + getData(l2) + carry;
        if (sum > =10) {
            if (!current) {
                newList = new ListNode(sum);
                current = current.next;
            } else {
                current.next = new ListNode(sum);
                current = current.next.next;
            }
        }
        l1 = getPointer(l1.next);
        l2 = getPointer(l2.next);
    }

    let carry = new ListNode(carry);

    return newList;

};
const l1 = [2, 4, 3];
const l2 = [5, 6, 4];
addTwoNumbers(l1, l2);