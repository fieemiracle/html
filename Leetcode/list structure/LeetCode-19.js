// 19.删除链表的倒数第N个结点
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
 var removeNthFromEnd = function(head, n) {
    // 暴力解法
    // let len=getListLength(head);

    // // 创建虚拟结点
    // let dummy=new ListNode(null,head);
    // dummy.next = head;
    // let current=dummy;

    // for(let i=0;i<len-n+1;i++){
    //     current=current.next;
    // }
    // current.next=current.next.next;
    // return dummy.next;

    // 快慢指针
    let fast=(slow=head);

    // 判空
    if(!head){
        return head;
    }

    while(slow.next){
        // 让快指针先走n步
        while(n>0){
            fast=fast.next;
            n--;
        }

        // 如果快指针超出链表长度
        if(!fast.next){
            return head.next;
        }

        // 如果快指针没有超出链表长度
        while(fast.next){
            // 让快慢指针一起走
            fast=fast.next;
            slow=slow.next;
        }

        // 删除结点
        slow.next=slow.next.next;
    }

    return head;

};

// 计算链表的长度
const getListLength=function(head){
    let len=0;
    while(head.next){
        len++;
        head=head.next;
    }
    return len;
}