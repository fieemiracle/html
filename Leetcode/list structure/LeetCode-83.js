// 83.删除排序链表中的重复元素
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
// function ListNode(val, next) {
//     this.val = (val === undefined ? 0 : val)
//     this.next = (next === undefined ? null : next)
// }
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
var deleteDuplicates = function (head) {
    // 新建一个链表
    let list=new ListNode();
    let current=list;

    // 当下一个数据域存在
    while(current.next){
        // 判断
        if(current.val===current.next.val){
            current.next = current.next.next;
            continue;
        }

        // 更新当前位置
        current=current.next;
    }
    return list.next;

};
const head = [1, 1, 2];
deleteDuplicates(head);