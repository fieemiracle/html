// 203.移除链表元素
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} val
 * @return {ListNode}
 */
 var removeElements = function(head, val) {
    // 头结点也要删除，创建虚拟节点
    let dummy=new ListNode(null,head);
    let current=dummy;

    while(current.next){
        if(current.next.val===val){
            current.next=current.next.next;
            continue;
        }
        current=current.next;
    }

    return dummy.next;
};