// 青蛙跳台阶
/**
 * @param {number} n
 * @return {number}
 */
var numWays = function(n) {
	// 规律：1个楼梯--1个方法；2个楼梯--2个方法；3个楼梯--3个方法；4个楼梯--5个方法；5个楼梯--7个方法
	// 从3个楼梯开始，第n个楼梯是前两个楼梯的方法累加
	if (n == 0) return 1;
	// 1个楼梯
	if (n == 1) return 1;
	// 2个楼梯
	if (n == 2) return 2;
	// 多个楼梯
	let front = 1,
		back = 2,
		temp;
	for (let i = 3; i < n + 1; i++) {
		temp = front;
		front = back;
		back = (temp + back) % (1e9 + 7);
	}
	return back;
};
