/**
 * @param {number[]} nums
 * @return {boolean}
 */
var findSubarrays = function(nums) {
	const dp = new Map()
	for (let i = 0; i < nums.length - 1; i++) {
		if (dp.get(nums[i] + nums[i + 1])) return true
		dp.set(nums[i] + nums[i + 1], true)
	}
	return false
};