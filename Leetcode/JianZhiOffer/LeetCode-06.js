// 06.从尾到头打印打印链表
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {number[]}
 */
 var reversePrint = function (head) {
        // 得到链表的长度
    // let len = 0;
    // while (head) {
    //     len++;
    //     head = head.next;
    // }
    // console.log(len)

    // 遍历链表，使用队列
    let dqueue = [];
    while(head){
        dqueue.unshift(head.val)
        head=head.next;
    }
    return dqueue;

};