// 231.2的幂
/**
 * @param {number} n
 * @return {boolean}
 */
  // n为奇数 return false
 //  n==1   return true
//  n为偶数 m=n/2  
 var isPowerOfTwo = function(n) {
    if(n<=0)  return false;
    if(n==1)  return true;
    if(n%2==1){
        return false;
    }else{
        return isPowerOfTwo(n/2);
    }
  
  
};
isPowerOfTwo(1);