// 342.4的幂
/**
 * @param {number} n
 * @return {boolean}
 */
// var isPowerOfFour = function (n) {
    // 分情况：
    // 1、小于1
    // 2、奇数
    // 3、偶数
    // if (n < 1) return false;//console.log(false);
    // if (n == 1) return true;//console.log(true);
    // if (n % 4 !== 0) {
    //     //console.log(false);
    //     return false;
    // } else {
    //     return isPowerOfFour(n / 4);
    // }
   

// };

// 4的幂满足：n>0+2的幂+n%3==1
// var isPowerOfFour = function(n) {
//     return n > 0 && (n & (n - 1)) === 0 && n % 3 === 1;
// };

// 4的幂满足：n>0+2的幂+n&（10101010101010101010101010101010）
var isPowerOfFour = function(n) {
    return n > 0 && (n & (n - 1)) === 0 && (n & 0xaaaaaaaa) === 0;
};

