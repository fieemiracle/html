// 84.柱状图中最大的矩形
/**
 * @param {number[]} heights
 * @return {number}
 */
 var largestRectangleArea = function (heights) {
    // 初始化单调递增栈
    const sortStack = [{ index: -1, height: -1 }];
    // 初始化最大面积
    let maxSquare = 0;

    // 遍历数组
    // 得到单调递增栈
    for (let i = 0; i < heights.length; i++) {
        while (sortStack[sortStack.length - 1].index !== -1 && heights[i] < sortStack[sortStack.length - 1].height) {
            let peek = sortStack.pop();
            maxSquare = Math.max(maxSquare, (i - 1 - sortStack[sortStack.length - 1].index) * peek.height)
        }
        sortStack.push({ index: i, height: heights[i] });
    }

   // 计算单调递增栈中的最大面积
   let high = sortStack[sortStack.length - 1].index;
   while (sortStack[sortStack.length - 1].index !== -1 ) {
       let peek = sortStack.pop();
       maxSquare = Math.max(maxSquare, (high - sortStack[sortStack.length - 1].index) * peek.height);
   }
    // return maxSquare;
    console.log(maxSquare);
};
const heights = [2,1,5,6,2,3];
largestRectangleArea(heights)