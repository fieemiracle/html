// 225.用队列实现栈
var MyStack = function() {
    this.queue1=[];
    this.queue2=[];
};

/** 
 * @param {number} x
 * @return {void}
 */
MyStack.prototype.push = function(x) {
    this.queue1.push(x);


};

/**
 * @return {number}
 */
MyStack.prototype.pop = function() {
    // 如果队列1不为空
    while(this.queue1.length > 1) {
        // 将队列个元素添加到队列2
        this.queue2.push(this.queue1.shift());
    }

    let pop=this.queue1.shift();
    while(this.queue2.length ) {
        this.queue1.push(this.queue2.shift());
    }

    return pop;

};

/**
 * @return {number}
 */
MyStack.prototype.top = function() {
    return this.queue1.slice(-1)[0];

};

/**
 * @return {boolean}
 */
MyStack.prototype.empty = function() {
    return !this.queue1.length;
};

/**
 * Your MyStack object will be instantiated and called as such:
 * var obj = new MyStack()
 * obj.push(x)
 * var param_2 = obj.pop()
 * var param_3 = obj.top()
 * var param_4 = obj.empty()
 */