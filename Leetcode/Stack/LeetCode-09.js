// 剑指offer 09.用两个栈实现队列
var CQueue = function() {
	// 使用两个栈
	this.stack1 = [];
	this.stack2 = [];
};

/** 
 * @param {number} value
 * @return {void}
 */
CQueue.prototype.appendTail = function(value) {
	this.stack1.push(value);
};

/**
 * @return {number}
 */
CQueue.prototype.deleteHead = function() {
	if (this.stack2.length <= 0) {
		if (this.stack1.length <= 0) {
			return -1;
		}
		while (this.stack1.length !== 0) {
			this.stack2.push(this.stack1.pop())
		}
	}
	return this.stack2.pop();
};

/**
 * Your CQueue object will be instantiated and called as such:
 * var obj = new CQueue()
 * obj.appendTail(value)
 * var param_2 = obj.deleteHead()
 */


var CQueue = function() {
	this.stack1 = [];
	this.stack2 = [];
};

CQueue.prototype.appendTail = function(value) {
	this.stack1.push(value);
};

CQueue.prototype.deleteHead = function() {
	if (!this.stack2.length) {
		if (!this.stack1.length) {
			return -1;
		}
		this.in2out();
	}
	return this.stack2.pop();
};

CQueue.prototype.in2out = function() {
	while (this.stack1.length) {
		this.stack2.push(this.stack1.pop());
	}
};
