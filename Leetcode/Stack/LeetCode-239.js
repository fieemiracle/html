// 239.滑动窗口的最大值
// 凡是设计滑动窗口，首先想到指针

// 求数组最大值math.max.apply(math,arr)


/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
// 方法一：双指针
//  var maxSlidingWindow = function (nums, k) {
//     // 取最大值
//     const result = [];
//     let left = 0, right = k - 1;
//     while (right < nums.length) {
//         const max = getMax(nums, left, right);
//         result.push(max)
//         left++;
//         right++;
//     }
//     // console.log(result);
//     return result;
// }
// // 窗口滑动
// function getMax(array, left, right) {
//     let max = array[left];
//    for(let i=left;i<right+1;i++){
//        max=array[i]>max?array[i]:max;
//    }
//    return max;
// }
let nums = [1, 3, -1, -3, 5, 3, 6, 7], k = 3;
// 方法二：双端队列
var maxSlidingWindow = function (nums, k) {
    const len = nums.length;
    const result = [];
    // 双端队列
    const dqueue = [];

    // 遍历数组
    for (let i = 0; i < len; i++) {
        // dqueue维护一个单调递减队列
        while (dqueue.length && nums[i] > nums[dqueue[dqueue.length - 1]]) {
            dqueue.pop();
        }
        // 队列存放下标，便于计算差值
        dqueue.push(i);
        console.log(dqueue);

        // 当对头元素的索引超出滑动窗口
        while (dqueue.length && dqueue[0] <= i - k) {
            dqueue.shift()
        }

        // 判断滑动窗口状态,更新数组
        if (i >= k - 1) {
            result.push(dqueue[0])
        }
    }
    // return result;
    console.log(result);
}
maxSlidingWindow(nums, k)