// 1047.删除字符串中的所有相邻重复项
/**
 * @param {string} s
 * @return {string}
 */
 var removeDuplicates = function(s) {
    // const stack=[];
    // for(let value of s){
    //     // 如果栈中找不到，进栈
    //     if(value!==stack[stack.length-1]){
    //         stack.push(value);
    //     }else{
    //         stack.pop();
            
    //     }

        
    // }
    // console.log(stack.join(''));

    // 双指针模拟栈
    s = [...s];
    let top = -1; // 指向栈顶元素的下标
    for(let i = 0; i < s.length; i++) {
        if(top === -1 || s[top] !== s[i]) { // top === -1 即空栈
            s[++top] = s[i]; // 入栈
        } else {
            top--; // 推出栈
        }
    }
    s.length = top + 1; // 栈顶元素下标 + 1 为栈的长度
    // return s.join('');
    console.log(s.join(''));

};
let s='abbaca';
removeDuplicates(s);