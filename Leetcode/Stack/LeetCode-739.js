// 739.每日温度
/**
 * @param {number[]} temperatures
 * @return {number[]}
 */
var dailyTemperatures = function (temperatures) {
    // 初始化结果数组各项起初都为0
    const result=new Array(temperatures.length).fill(0);
    // console.log(result);
    // 定义单调栈，保存索引
    const stack=[];

    // 遍历数组
    for(let i=0;i<temperatures.length;i++){
        // 当栈不为空，且数组元素比栈顶元素大时
        while(stack.length&& temperatures[stack[stack.length-1]]<temperatures[i]){
            // 得到差值
            let diff=i-stack[stack.length-1];
            result[stack[stack.length-1]]=diff;
            stack.pop();

        }
        stack.push(i)
    }
    console.log(result);
};
const temperatures = [73, 74, 75, 71, 69, 72, 76, 73];
dailyTemperatures(temperatures)

