// 90.子集II
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
 var subsetsWithDup = function(nums) {
    let result = [];
    let path=[];

    const backTracing=function(startIndex){
        result.push([...path]);

        for(let i=startIndex; i<nums.length; i++) {
            if(i>startIndex&&nums[i]==nums[i-1]){
               continue
            }

            path.push(nums[i]);
            backTracing(i+1)
            path.pop();
        }   
    }

    nums=nums.sort((a,b)=>a-b)
    backTracing(0);
    // let obj={}
    // result.forEach(item=>obj[item]=item)
    // result=Object.values(obj)
    return result;
};