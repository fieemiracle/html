// 5.最长回文子串
// 给你一个字符串 s，找到 s 中最长的回文子串。
/**
 * @param {string} s
 * @return {string}
 */
var longestPalindrome = function(s) {
	// 字符串为空
	// if (!s.length) return '';
	// let result = '';
	// // 遍历字符串，需要遍历到每一个字符
	// for (let i = 0; i < s.length; i++) {
	// 	helper(i, i);
	// 	helper(i, i + 1);
	// }

	// function helper (low, high) {
	// 	while (low >= 0 && high < s.length && s[low] === s[high]) {
	// 		low--;
	// 		high++;
	// 	}
	// 	// 得到字符串
	// 	let path = s.slice(low + 1, high + 1-1);
	// 	// 对比
	// 	if (result.length < path.length) {
	// 		result = path;
	// 	}
	// }
	// return result;

	let result = ''

	for (let i = 0; i < s.length; i++) {
		// 分奇偶， 一次遍历，每个字符位置都可能存在奇数或偶数回文
		helper(i, i)
		helper(i, i + 1)
	}

	function helper(low, high) {
		// 定义左右双指针
		while (low >= 0 && high < s.length && s[low] === s[high]) {
			low--
			high++
		}
		// 拿到回文字符， 注意 上面while满足条件后多执行了一次，所以需要l+1, r+1-1
		const path = s.slice(low + 1, high + 1 - 1);
		// 取最大长度的回文字符
		if (path.length > result.length) result = path
	}
	return result
};
