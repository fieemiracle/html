// 82.含有重复元素的组合
/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
 var combinationSum2 = function (candidates, target) {
    let result = [],
        path = [],
        sum = 0;

    const backTracing = function (candidates, target, startIndex, sum) {
        if (sum == target) {
            result.push([...path]);
            return;
        }

        for (let i = startIndex; i < candidates.length; i++) {
            if (sum + candidates[i] > target) {//剪枝
                return;
            }
            if (i > startIndex && candidates[i] == candidates[i - 1]) {//去重
                continue
            }
            path.push(candidates[i]);
            backTracing(candidates, target, i + 1, sum+candidates[i]);
            path.pop();
            // sum -= path.pop()
        }
    }

    candidates = candidates.sort((a, b) => a - b);
    backTracing(candidates, target, 0, 0)
    return result;
};