// 77.组合
/**
 * @param {number} n
 * @param {number} k
 * @return {number[][]}
 */
 var combine = function (n, k) {
    let result = []
    let path = []
    const combineHelper = (n, k, startIndex) => {
        if (path.length === k) {//出口
            result.push([...path])
            return
        }
        for (let i = startIndex; i <= n - (k - path.length) + 1; ++i) {//剪枝
            path.push(i)
            combineHelper(n, k, i + 1);//回溯
            path.pop()
        }
    }

    combineHelper(n, k, 1)
    return result;
};