/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function(nums) {
	let sum = nums[0];
	let current = nums[0];
	for (i = 1; i < nums.length; i++) {
		if (nums[i] + current > nums[i]) {
			current += nums[i];
		} else {
			current = nums[i];
		}
		if (current > sum) {
			sum = current;
		}
	}
	console.log(sum);
	return sum;
};
const nums = [-2, 1, -3, 4, -1, 2, 1, -5, 4];
maxSubArray(nums)
