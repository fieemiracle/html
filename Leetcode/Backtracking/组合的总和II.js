// 40.组合的总和II
/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
 var combinationSum2 = function(candidates, target) {
    let result = [];
    let path=[];

    const backTracing=function(startIndex,target) {
        if(target==0){
            result.push(path)
            return;
        }

        for(let i=startIndex; i<candidates.length; i++) {
            if(target-candidates[i]<0){//剪枝
                return;
            }

            if (i > startIndex && candidates[i] == candidates[i - 1]) {//去重
                continue;
            }

            path.push(candidates[i]);
            backTracing(i+1,target-candidates[i])
            path.pop()
        }
    }
    candidates=candidates.sort((a,b)=>a-b);
    backTracing(0,target)
    return result
};