// 84.柱状图中的最大矩形
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var permuteUnique = function(nums) {
	let result = [];
	let path = [];
	let used = [];

	const backTracing = (used) => {
		if (path.length == nums.length) {
			result.push([...path])
			return;
		}

		for (let i = 0; i < nums.length; i++) {
			if (i > 0 && nums[i] == nums[i - 1] && used[nums[i]] == false) {
				continue;
			}

			if (used[nums[i]]) continue;
			used[nums[i]] = true;

			path.push(nums[i]);
			backTracing(i + 1);
			path.pop();
			used[nums[i]] = false;
		}
	}
	nums = nums.sort((a, b) => a - b)
	backTracing(used);
	return result;
};
