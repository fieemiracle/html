// 216.组合的总和III
/**
 * @param {number} k
 * @param {number} n
 * @return {number[][]}
 */
 var combinationSum3 = function(k, n) {
    let result = [];
    let path= [];
    let sum=0;

    const backTracing=function(startIndex,sum){
        if(sum==n&&path.length==k){
        result.push([...path]);
            return;
        }
        // console.log(123)
        for(let i=startIndex;i<=nums.length;i++){
            if(sum+i>n) return;

            path.push(i);
            // console.log(sum)
            backTracing(i+1,sum+i);
            path.pop();
        }
    }

    backTracing(1,0);
    return result;

};