// 剑指 Offer 12. 矩阵中的路径
/**
 * @param {character[][]} board
 * @param {string} word
 * @return {boolean}
 */
var exist = function(board, word) {
	const dfs = (i, j, k) => {
		if (i >= m || i < 0 || j >= n || j < 0 || board[i][j] !== word[k])
			return false;
		if (k === word.length - 1)
			return true;
		board[i][j] = '';
		let res = dfs(i - 1, j, k + 1) || dfs(i + 1, j, k + 1) || dfs(i, j + 1, k + 1) || dfs(i, j - 1, k + 1);
		board[i][j] = word[k];
		return res;
	}

	// 原数组 board 每个方格都可以看做是开始遍历的起点
	let m = board.length;
	n = board[0].length;
	for (let i = 0; i < m; i++) {
		for (let j = 0; j < n; j++) {
			if (dfs(i, j, 0))
				return true;
		}
	}
	return false;


	// 方法二
	if (!matrix.length || !matrix[0].length) {
		return [];
	}
	let arr = []
	let left = 0
	let right = matrix[0].length - 1
	let top = 0
	let bottom = matrix.length - 1
	while (left <= right && top <= bottom) {
		for (let i = left; i <= right; i++) {
			arr.push(matrix[top][i])
		}
		for (let i = top + 1; i <= bottom; i++) {
			arr.push(matrix[i][right])
		}
		if (left < right && top < bottom) {
			for (let i = right - 1; i > left; i--) {
				arr.push(matrix[bottom][i])
			}
			for (let i = bottom; i > top; i--) {
				arr.push(matrix[i][left])
			}
		}
		left++
		right--
		top++
		bottom--

	}
	return arr
};
