// 38.字符串的排列
/**
 * @param {string} s
 * @return {string[]}
 */

 var permutation = function(s) {
    s=s.split('').sort((a, b) => a - b);
    let result=[],
        path=[],
        used=[];
    
    const backtracing=function(used){
        if(path.length==s.length){
            result.push(path.slice().join());
            return;
        }

        for(let i=0;i<s.length;i++){
            if(used[i]) continue;
            used[i]=true;
            path.push(s[i]);
            backtracing(i+1);
            path.pop();
            used[i]=false;
        }
    }

    
    backtracing(used);
    return result;
};