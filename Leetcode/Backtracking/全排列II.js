// 47.全排列II
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
 var permuteUnique = function(nums) {
    let result = [];
    let path = [];
    let used = [];


    const backTracing = function (used) {
        if (path.length==nums.length) {
            result.push([...path]);
            return;
        }

        for (let i = 0; i < nums.length; i++) {
            if (i > 0 && nums[i] == nums[i - 1] && used[i] == false) {//去重
                continue
            }
            if (used[i]) continue;
            used[i] = true;

            path.push(nums[i]);
            backTracing(used);
            path.pop();
            used[i] = false;
        }
    }
    nums = nums.sort((a, b) => a - b)
    backTracing(used);
    return result;
};