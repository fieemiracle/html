// 491.递增子序列
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var findSubsequences = function(nums) {
	let result = [];
	let path = [];
	const backTracing = (startIndex) => {
		if (path.length > 1) {
			result.push([...path]);
			// return;
		}
		let used = {};

		for (let i = startIndex; i < nums.length; i++) {
			if ((i > startIndex && nums[i] < path[path.length - 1]) || used[nums[i]]) { //去重
				continue;
			}
			used[nums[i]] = true;
			path.push(nums[i]);
			backTracing(i + 1);
			path.pop();
		}
	}
	backTracing(0);
	console.log(result);
	return result;
};
const arr = [1, 2, 3, 4]
findSubsequences(arr)
