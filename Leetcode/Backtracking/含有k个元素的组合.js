// 80.含有k个元素的组合
/**
 * @param {number} n
 * @param {number} k
 * @return {number[][]}
 */
 var combine = function(n, k) {
    // 结果数组
    let result=[];
    // 回溯函数
    const backTracing=function(n,k,startIndex,path){
        // 回溯出口
        if(path.length==k){
            result.push([...path]);
            return;
        }

        for(let i=startIndex;i<=n;i++){
            // 处理节点
            path.push(i);
            // 回溯
            backTracing(n,k,i+1,path);
            // 撤销回溯
            path.pop()
        }
    }

    backTracing(n,k,1,[]);
    return result;
};