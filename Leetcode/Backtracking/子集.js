// 78.子集
/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var subsets = function (nums) {

    // 字典排序（二进制）
    // 计算子集的数目
    // let len=1<<nums.length;
    // // 定义一个空数组存放所有子集
    // const result=[];

    // for(let i=0;i<len;i++){
    //     // 这里是子集
    //     let evalSubsets=[];
    //     for(let j=0;j<nums.length;j++){
    //         if(i & (1<<j)){
    //             evalSubsets.push(nums[j]);
    //         }
    //     }
    //     // 将得到的子集一个一个加入到result
    //     result.push(evalSubsets);
    // }
    // return result;



    // 回溯
    let result = [];
    let path = [];

    function backTracing(startIndex) {
        result.push(path);

        for (let i = startIndex; i <= nums.length; i++) {
            path.push(nums[i]);
            backTracing(i + 1);
            path.pop();
        }
    }

    backTracing(1);
    return result;
};

// [[],[1],[1]]