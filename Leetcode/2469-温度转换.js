/**
 * @param {number} celsius
 * @return {number[]}
 */
var convertTemperature = function(celsius) {
	let celsius2Kelvin = (celsius) => {
		return celsius + 273.15
	}
	let celsius2Fahrenheit = (celsius) => {
		return celsius * 1.80 + 32.00
	}
	console.log([celsius2Kelvin(celsius), celsius2Fahrenheit(celsius)]);
	return [celsius2Kelvin(celsius), celsius2Fahrenheit(celsius)]
};
let celsius = 36.50
convertTemperature(celsius)
