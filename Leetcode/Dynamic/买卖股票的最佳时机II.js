// 122.买卖股票的最佳时机II
/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function(prices) {
    // 动态规划
    let max=0;
    for(let i=0;i<prices.length-1;i++){
        if(prices[i]<prices[i+1]){
            max=max+prices[i+1]-prices[i]
        }
    }
    return max;
};