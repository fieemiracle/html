/**
 * @param {number[]} nums
 * @return {number}
 */
 var rob = function(nums) {
    // 动态规划
    if(nums.length==0) return 0;
    if(nums.length==1) return nums[0]
    // 一开始最高金额0
    let max=0;
    for(let i=2;i<nums.length;i++){
        if(nums[i-2]>max){
            max=nums[i-2];
        }
        nums[i]=nums[i]+max;
    }
    return Math.max(nums[nums.length-1],nums[nums.length-2])

};