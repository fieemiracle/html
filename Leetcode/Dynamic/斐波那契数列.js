// 剑指offer 01.斐波那契数列
/**
 * @param {number} n
 * @return {number}
 */
var fib = function(n) {
	// if (n == 0) return 0;
	// if (n == 1) return 1;
	// if (n == 2) return 1;

	// return fib(n - 1) + fib(n - 2);

	// if (n === 0 || n === 1) {
	//     return n;
	// }
	// const mod = Math.pow(10, 9) + 7;
	// const dp = new Array(n + 1);
	// dp[0] = 0;
	// dp[1] = 1;
	// for (let i = 2; i <= n; i++) {
	//     dp[i] = (dp[i - 1] + dp[i - 2]) % mod;
	// }
	// return dp[n];

	const MOD = 1000000007;
	if (n < 2) {
		return n;
	}
	let p = 0,
		q = 0,
		r = 1;
	for (let i = 2; i <= n; ++i) {
		p = q;
		q = r;
		r = (p + q) % MOD;
	}
	return r;

};
