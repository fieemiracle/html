// 55.跳跃游戏
/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canJump = function (nums) {
    // let index = 0;
    // while (index < nums.length) {
    //     let step = 0;
    //     while (step <= nums[index]) {
    //         index += step;

    //     }
    // }
    // // return index===nums.length-1;
    // console.log(index === nums.length - 1);

    // let index=0;
    // while(index<nums.length){
    //     let step=1;
    //     if(step<nums[index]){
    //         index+=step;

    //     }

    // }
    // console.log(index === nums.length - 1);

    let maxSteps=0;
    for(let i=0;i<nums.length;i++){
        maxSteps=Math.max(i+nums[i],maxSteps)
        if(maxSteps<i){
            return false;
        }
    }
    return true;
};
const arr = [2, 3, 1, 1, 4]
canJump(arr)