// 买卖股票的最佳时机III
/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function(prices) {
    // 0  第一次买 -price
    // 1  第一次卖 0
    // 2  第二次买 -price
    // 3  第二次卖 0

    // 动态规划
    // 创建一个二维数组
    const dynamicArray=new Array(prices.length).fill(0).map(item=>new Array(4).fill(0));

    // 初始化第一天的状态收益
    dynamicArray[0][0]=-prices[0];
    dynamicArray[0][2]=-prices[0];

    for(let i=1;i<prices.length;i++){
        dynamicArray[i][0]=Math.max(dynamicArray[i-1][0],-prices[i])
        dynamicArray[i][1]=Math.max(dynamicArray[i-1][1],dynamicArray[i-1][0]+prices[i])
        dynamicArray[i][2]=Math.max(dynamicArray[i-1][2],dynamicArray[i-1][1]-prices[i])
        dynamicArray[i][3]=Math.max(dynamicArray[i-1][3],dynamicArray[i-1][2]+prices[i]);
    }
    return dynamicArray[dynamicArray.length-1][3]
};