// 打家劫舍II
// /**
//  * @param {number[]} nums
//  * @return {number}
//  */
var rob = function (nums) {
    if(nums.length<=3){
        return Math.max.apply(Math,nums)
    }

    // 去头
    const nums1=nums.slice(1,nums.length);
    console.log(nums1);
    let max1=0;
    for(let i=2;i<nums1.length;i++){
        if(nums1[i-2]>max1){
            max1=nums1[i-2];
        }
        console.log(max1);
        nums1[i]=max1+nums1[i];
    }
    max1=Math.max(nums1[nums1.length-1],nums1[nums1.length-2]);
    console.log(max1);

    // 去尾
    const nums2=nums.slice(0,nums.length-1);
    console.log(nums2);
    let max2=0;
    for(let i=2;i<nums2.length;i++){
        if(nums2[i-2]>max2){
            max2=nums2[i-2];

        }

        nums2[i]=max2+nums2[i];
    
    }
    max2=Math.max(nums2[nums2.length-1],nums2[nums2.length-2]);
    console.log(max2);

    return Math.max(max1,max2)

}
const nums = [1,2,1,1];
rob(nums);