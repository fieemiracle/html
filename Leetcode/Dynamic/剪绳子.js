/**
 * @param {number} n
 * @return {number}
 */
// var cuttingRope = function(n) {
// 	let dp = new Array(n + 1).fill(0);

// 	// 初始化
// 	dp[2] = 1;

// 	for (let i = 3; i <= n; i++) {
// 		for (let j = 1; j < i - 1; j++) {
// 			dp[i] = Math.max(dp[i], (i - j) * j, dp[i - j] * j);
// 		}
// 	}
// 	return dp[n];
// }

var cuttingRope = function(n) {
	//将数组分为m段后，每段的长度l，
	if (n == 2) {
		return 1
	}
	let max = 0

	function getL(m) {
		for (let i = 0; i < n; i++) {
			if (m * i == n) {
				s = Math.pow(i, m)
			}
			if (m * i < n && m * (i + 1) > n) {
				let a = n - m * i
				s = Math.pow(i, m - a) * Math.pow(i + 1, a)
			}
		}
		return s
	}
	for (let m = 2; m < n; m++) {
		max = Math.max(max, getL(m))
	}
	return max
};
