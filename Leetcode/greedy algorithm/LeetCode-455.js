// 455.分发饼干
/**
 * @param {number[]} g
 * @param {number[]} s
 * @return {number}
 */
 var findContentChildren = function(g, s) {
    // 升序排序
    // g = g.sort((a, b) => a - b)
    // s = s.sort((a, b) => a - b)
    // let result = 0
    // let index = s.length - 1
    // for(let i = g.length - 1; i >= 0; i--) {
    //     if(index >= 0 && s[index] >= g[i]) {
    //         result++
    //         index--
    //     }
    // }
    // return result

    // 降序排序
    g=g.sort((a, b) => b - a);
    s=s.sort((a, b) => b - a);
    let count=0;
    let index=0;
    for(let i = 0; i < g.length; i++) {
        if(s[index] >= g[i]){
            count++;
            index++;
        }
    }
    console.log(count);
};
const g=[1,2,3]
const s=[1,2]
findContentChildren(g, s)