// 860.柠檬树找零
/**
 * @param {number[]} bills
 * @return {boolean}
 */
var lemonadeChange = function (bills) {
    let n = bills.length
    let dp = [0, 0]
    for (let i = 0; i < n; i++) {
        if (bills[i] == 5) {
            dp[0]++
        }
        if (bills[i] == 10) {
            dp[1]++
            dp[0]--
            if (dp[0] < 0) {
                return false
            }
        }
        if (bills[i] == 20) {
            dp[1]--
            dp[0]--
            if (dp[1] < 0 || dp[0] < 0) {
                dp[1]++
                dp[0]++
                dp[0] = dp[0] - 3
                if (dp[0] < 0) {
                    return false
                }
            }
        }
    }
    return true
};