// 使用对象定义二叉树
function TreeNode(val){
    this.val = val;
    this.left = null;
    this.right = null;
}

const node=new TreeNode(1);