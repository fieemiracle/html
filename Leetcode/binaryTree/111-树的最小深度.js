/**
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
// 最小深度：从根节点到最近的叶子结点的最短路径上的节点数量
var minDepth = function(root) {
	// 根节点为null
	if (!root) return 0
	// 根节点的左右子树为null
	if (!root.left && !root.right) return 1
	// 根节点的左右子树不都为null
	let minPath = Infinity
	if (root.left) minPath = Math.min(minPath, minDepth(root.left))
	if (root.right) minPath = Math.min(minPath, minDepth(root.right))
	return minPath + 1
};