// 106.从中序与后序遍历序列构造二叉树
if(!preorder.length) return null;

    let rootVal=preorder[0];
    let root=new TreeNode(rootVal);
    let position=inorder.indexOf(rootVal);
    root.left=buildTree(preorder.slice(1,1+position),inorder.slice(0,position));
    root.right=buildTree(preorder.slice(1+position),inorder.slice(position+1));
    return root;
