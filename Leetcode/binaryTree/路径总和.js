/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} targetSum
 * @return {boolean}
 */
// 方法一：广度优先遍历
var hasPathSum = function(root, targetSum) {
	// 方法：广度优先遍历（层序遍历）
	// 树为空
	if (root == null) return false;

	//节点队列
	const queueNode = [];
	queueNode.push(root);
	// 节点对应的值队列
	const queueVal = [];
	queueVal.push(root.val);

	while (queueNode.length) { //长度不为0
		let popNode = queueNode.shift();
		let popVal = queueVal.shift();

		// 不存在左右子树
		if (!popNode.left && !popNode.right) {
			if (popVal == targetSum) {
				return true;
			}
			continue;
		}

		// 存在左子树
		if (popNode.left) {
			queueNode.push(popNode.left);
			queueVal.push(popVal + popNode.left.val);
		}

		// 存在右子树
		if (popNode.right) {
			queueNode.push(popNode.right);
			queueVal.push(popVal + popNode.right.val);
		}

	}
	return false;
};
