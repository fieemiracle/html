/**
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
// 二叉树的深度为根节点到最远叶子节点的最长路径上的节点数
var maxDepth = function(root) {
	if (root != null) {
		return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1
	} else {
		return 0
	}
};