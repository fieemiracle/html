/**
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */

var levelOrder = function(root) {
	// 借用队列
	let res = []
	if (!root) return res;
	let queue = []
	queue.push(root) //先进
	while (queue.length) {
		let level = []
		let queueLen = queue.length
		for (let i = 0; i < queueLen; i++) {
			let cur = queue.shift() //先出
			level.push(cur.val)
			if (cur.left) queue.push(cur.left)
			if (cur.right) queue.push(cur.right)
		}
		res.push(level)
	}
	return res
}