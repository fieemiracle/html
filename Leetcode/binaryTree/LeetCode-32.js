// 32.从上到下打印二叉树II
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
 var levelOrder = function(root) {
    let result=[];
    if(!root){
        return result;
    }

    // 借助队列
    let queue=[];
    queue.push(root);
    while(queue.length){
        let level=[];
        let len=queue.length;
        for(let i=0;i<len;i++){
            let first=queue.shift();
            level.push(first.val);

            if(first.left){
                queue.push(first.left);
            }
            if(first.right){
                queue.push(first.right);
            }
        }
        result.push([...level]);
    }
    return result;
};