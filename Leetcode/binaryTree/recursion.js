// function fb(n){
//     if(n==0) return 0;
//     if(n==1) return 1;
//     if(n==2) return 1;

//     // return fb(n-1)+fb(n-2);
//     console.log(fb(n-1)+fb(n-2));

    
// }
// fb(15)

const root={
    val:12,
    left:{
        val:34,
        left:{
            val:30
        }
    },
    right:{
        val:35,
        left:{
            val:40
        },
        right:{
            val:45
        }
    }
}

// 前序遍历
console.log('前序遍历');
function preorder(root){
    if(!root) return 
    console.log('当前节点值是：',root.val);

    preorder(root.left);
    preorder(root.right);
}
preorder(root) 

// 中序遍历
console.log('中序遍历');
function inorder(root){
    if(!root) return 
    inorder(root.left);
    console.log('当前节点值是：',root.val);
    inorder(root.right)
}
inorder(root);

// 后序遍历
console.log('后序遍历');
function postorder(root){
    if(!root) return 
    postorder(root.left);
    postorder(root.right);
    console.log('当前节点值是：',root.val);
}
postorder(root);