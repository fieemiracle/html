// 94.二叉树的中序遍历
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
 var inorderTraversal = function(root) {
    const result=[];
    const fun=(root)=>{
        if(!root){
            return;
        }
        fun(root.left);//遍历左子树
        result.push(root.val);//添加值
        fun(root.right);//遍历右子树
    }
    fun(root);//调用
    return result;
};