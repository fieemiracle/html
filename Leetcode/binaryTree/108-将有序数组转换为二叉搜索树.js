 // 二叉排序树（ 二叉查找树Binary Search Tree）: 又称二叉搜索树、 有序二叉树
 // 二叉搜索树可以为空； 如果不为空， 满足以下性质：
 // 非空左子树的所有键值小于其根节点的键值。
 // 非空右子树的所有键值大于其根节点的键值。
 // 左、 右子树本身也都是二叉搜索树
 // 特点：
 // 相对较小的值总是保存在左节点上， 相对较大的值总是保存在右节点上， 使得二叉搜索树的查找效率非常高
 // 高度平衡 二叉树是一棵满足「每个节点的左右两个子树的高度差的绝对值不超过 1 」的二叉树

 function TreeNode(val, left, right) {
 	this.val = (val === undefined ? 0 : val)
 	this.left = (left === undefined ? null : left)
 	this.right = (right === undefined ? null : right)
 }
 var sortedArrayToBST = function(nums) {
 	// 把有序数组当做二叉搜索树，根节点是最中间的那个值
 	// 构建树
 	let left = 0,
 		right = nums.length - 1
 	const createTree = (left, right) => {
 		if (left > right) return null
 		let midIndex = Math.floor((right - left) / 2 + left)
 		let leftSubTree = nums.slice(0, midIndex),
 			rightSubTree = nums.slice(midIndex + 1)
 		let root = new TreeNode(nums[midIndex], createTree(left, midIndex - 1), createTree(midIndex + 1,
 			right))
 		return root
 	}
 	console.log(createTree(left, right));
 	return createTree(left, right)
 }
 const nums = [-10, -3, 0, 5, 9]
 sortedArrayToBST(nums)