// 32.从上到下打印二叉树III
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
 var levelOrder = function (root) {
    let result = [];
    if (!root) {
        return result;
    }
    let queue = [];
    let count = 1;
    queue.push(root);
    while (queue.length > 0) {
        let level = [];
        let len = queue.length;
        for (let i = 0; i < len; i++) {
            if (count % 2 == 0) {
                let first = queue.pop();
                level.push(first.val);
                if (first.right) {
                    queue.unshift(first.right);
                }
                if (first.left) {
                    queue.unshift(first.left);
                }
            } else {
                let first = queue.shift();
                level.push(first.val);
                if (first.left) {
                    queue.push(first.left);
                }
                if (first.right) {
                    queue.push(first.right);
                }
            }
        }
        count += 1;
        result.push([...level]);
    }
    return result;
};