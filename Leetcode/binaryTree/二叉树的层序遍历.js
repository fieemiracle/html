// 102.二叉树的层序遍历
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var levelOrder = function (root) {
    // 结果数组
    const res = [];
    // 遍历边界
    if(!root){
        return res;
    }

    // 广度优先遍历--队列
    const queue=[];
    queue.push(root);
    while (queue.length > 0){
        const level=[];//存放每一层的节点
        const len=queue.length;
        for (let i=0; i<len; i++){
            const top=queue.shift();
            level.push(top.val);
            if(top.left){
                queue.push(top.left);

            }
            if(top.right){
                queue.push(top.right);
            }
        }
        res.push(level);
    }
    return res;
};