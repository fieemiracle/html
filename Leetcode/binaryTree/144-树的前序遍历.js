// 144.二叉树的前序遍历
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */

// 递归
var preorderTraversal = function(root) {
	const preOrder = [];

	function doOrder(root) {
		if (root == null) {
			return;
		}
		preOrder.push(root.val);
		doOrder(root.left);
		doOrder(root.right);
	}
	doOrder(root);
};

// 借用栈
function preOrder(root) {
	let res = []
	let stack = []
	if (!root) return res
	stack.push(root)
	while (stack.length) {
		let cur = stack.pop()
		res.push(cur.val)
		if (cur.right) stack.push(cur.right)
		if (cur.left) stack.push(cur.left)
	}
	console.log(res);
	return res
}