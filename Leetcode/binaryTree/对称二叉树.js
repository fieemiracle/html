// 101.对称二叉树
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
 var isSymmetric = function (root) {
    //思路
    // 1、判断根节点的左右子树是否都为null，是则true,否则false
    // 2、将左右子树看做两棵树，构造一个函数，判断这两棵树**对称**是否相等，递归思想
    function isSame(leftTrees, rightTrees) {
        if (leftTrees == null && rightTrees == null) return true
        if (leftTrees == null || rightTrees == null) return false;
        if (leftTrees.val !== rightTrees.val) return false;
        return isSame(leftTrees.left, rightTrees.right) && isSame(leftTrees.right, rightTrees.left);
    }
    if (!root) return true;
    return isSame(root.left, root.right);
};