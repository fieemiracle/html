// 144.二叉树的前序遍历
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
 var preorderTraversal = function (root) {
    let preOrder = [];
    const doOrder=function(root) {
        if (root == null) {
            return;
        }
        preOrder.push(root.val);
        doOrder(root.left);
        doOrder(root.right);
    }
    doOrder(root);
    return preOrder;
};