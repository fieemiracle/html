/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number[]} preorder
 * @param {number[]} inorder
 * @return {TreeNode}
 */
 var buildTree = function(preorder, inorder) {
    let result=[];
    if(!root){
        return result;
    }

    // 借助队列
    let queue=[];
    queue.push(root);
    while(queue.length){
        let level=[];
        let len=queue.length;
        for(let i=0;i<len;i++){
            let first=queue.shift();
            level.push(first.val);

            if(first.left){
                queue.push(first.left);
            }
            if(first.right){
                queue.push(first.right);
            }
        }
        result.push(...level);
    }
    return result;
};