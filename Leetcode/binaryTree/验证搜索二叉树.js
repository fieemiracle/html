// 98.验证二叉搜索树
// 要知道中序遍历下，输出的二叉搜索树节点的数值是有序序列。
// 有了这个特性，验证二叉搜索树，就相当于变成了判断一个序列是不是递增的了
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */
 var isValidBST = function(root) {
    // 中序遍历二叉树
    // 1、递归遍历
    let result=[];
    const inOrder=(root)=>{
        if(!root){
            return result;
        }
        inOrder(root.left);//遍历左子树
        result.push(root.val);
        inOrder(root.right);//遍历右子树
    }

    inOrder(root);
    console.log(result);
    // 判断结束数组result是否为升序序列
    for(let i=0;i<result.length;i++) {
        if(result[i]<result[i+1]) {
            continue;
    }else{
        return false;
    }
}
    return true;
};