function TreeNode(val, left, right) {
	this.val = (val === undefined ? 0 : val)
	this.left = (left === undefined ? null : left)
	this.right = (right === undefined ? null : right)
}


var buildTree = function(preorder, inorder) {
	if (!preorder.length) return null;
	let rootVal = preorder[0] //根节点
	let rootIndex = inorder.indexOf(rootVal) //根节点在中序数组的位置
	let root = new TreeNode(rootVal)
	root.val = rootVal
	let inorderLeft = inorder.slice(0, rootIndex),
		inorderRight = inorder.slice(rootIndex + 1)
	let preorderLeft = preorder.slice(1, rootIndex + 1),
		preorderRight = preorder.slice(1 + rootIndex)
	// 递归
	root.left = buildTree(preorderLeft, inorderLeft)
	root.right = buildTree(preorderRight, inorderRight)
	console.log(root);
	return root
}
const preorder = [3, 9, 20, 15, 7],
	inorder = [9, 3, 15, 20, 7]
buildTree(preorder, inorder)