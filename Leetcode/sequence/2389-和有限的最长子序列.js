var answerQueries = function(nums, queries) {
	// 二分法
	const binarySearch = (target, nums) => {
		let left = 0,
			right = nums.length - 1;
		while (left < right) {
			let mid = Math.floor((right - left) / 2 + left)
			if (target < nums[mid]) {
				left = mid + 1
			} else if (target > nums[mid]) {
				right = mid
			} else {
				return mid
			}
		}
		return left
	}

	let n = nums.length,
		m = queries.length;
	nums = nums.sort((a, b) => a - b)
	let everySum = new Array(n + 1).fill(0)
	for (let i = 0; i < n; i++) {
		everySum[i + 1] = everySum[i] + nums[i]
	}
	console.log(everySum);
	const answer = new Array(m).fill(0)
	for (let j = 0; j < m; j++) {
		answer[j] = binarySearch(queries[j], everySum)
	}
	console.log(answer);
	return answer
}
const nums = [4, 5, 2, 1],
	queries = [3, 10, 21]
answerQueries(nums, queries)
