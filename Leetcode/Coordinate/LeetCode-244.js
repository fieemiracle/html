// 244.搜索二维矩阵
/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
var searchMatrix = function(matrix, target) {
	if (matrix.length < 0) return false;
	let row = matrix.length - 1;
	let colmun = 0;
	while (row >= 0 && colmun < matrix[0].length) {
		if (matrix[row][colmun] == target) {
			return true;
		} else if (matrix[row][colmun] > target) {
			row--;
		} else if (matrix[row][colmun] < target) {
			colmun++;
		}
	}
	return false;
};

let matrix = [
		[1, 4, 7, 11, 15],
		[2, 5, 8, 12, 19],
		[3, 6, 9, 16, 22],
		[10, 13, 14, 17, 24],
		[18, 21, 23, 26, 30]
	],
	target = 5;

searchMatrix(matrix, target);
