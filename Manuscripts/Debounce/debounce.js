// 防抖（闭包）
function debounce(fn, timeout) {
    let timer = null
    return function (...arg) {
        clearTimeout(timer)
        timer = setTimeout(() => {
            fn.apply(this, arg)
        }, timeout)
    }

    // return function(){
    //     const context=this;
    //     // 清除上一次的定时器
    //     clearTimeout(timer);
    //     timer=setTimeout(function(){
    //         fn.apply(context)
    //     }, timeout);
    // }

    // return function () {
    //     const context = this;
    //     var args = arguments;
    //     // 清除上一次的定时器
    //     clearTimeout(timer);
    //     timer = setTimeout(function () {
    //         fn.apply(context,args)
    //     }, timeout);
    // }
}