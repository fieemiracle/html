class EventEmitter {
	constructor() {
		this.cache = {}
	}
	on(eventName, callback) { //订阅
		if (!this.cache[eventName]) {
			this.cache[eventName] = [callback]
		} else { //存在
			this.cache[eventName].push(callback)
		}
	}
	emit(eventName, ...args) { //发布
		// this.cache[eventName] && this.cache[eventName].forEach(callback => callback(...args))
		if (this.cache[eventName]) {
			this.cache[eventName].forEach(item => item())
		} else {
			this.cache[eventName] = []
		}
	}
	once(eventName, callback) { //只订阅一次

	}
	cancel(eventName, callback) { //取消订阅
		// if (this.cache[eventName]) {
		// 	this.cache[eventName].filter(item => item != callback)
		// }
		if (this.cache[eventName]) {
			const index = this.cache[eventName].findIndex(item => item === callback)
			if (index >= 0) {
				this.cache[eventName].splice(index, 1)
			}
		}
	}
}
let event = new EventEmitter()
event.on('hello', () => {
	console.log('订阅成功');
})
event.cancel('hello', () => {
	console.log('取消订阅');
})
event.on('hello', () => {
	console.log('hello world');
})
event.on('hello', () => {
	console.log('hello world');
})
event.once('hello', () => {
	console.log('只能订阅一次');
})
event.emit('发布成功')
