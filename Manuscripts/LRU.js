class LRUcACHE {
	constructor(size) {
		this.size = size
		this.cache = new Map()
	}
	get(key) {
		const hasKey = this.cache.has(key)
		if (hasKey) { //key存在
			const val = this.cache.get(key)
			// 调整位置
			this.cache.delete(key)
			this.cache.set(key, val)
			return val
		} else {
			return -1
		}
	}
	put(key, value) {
		const hasKey = this.cache.has(key)
		if (hasKey) { //存在
			// 更新
			this.cache.delete(key)
		} else {
			if (this.cache.size >= this.size) { //容量超出
				// 找出最久未使用的
				this.cache.delete(this.cache.keys().next().value) //最早被存放的key
			}
		}

		this.cache.set(key, value)
	}
}
