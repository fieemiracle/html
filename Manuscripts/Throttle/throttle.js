// 节流
function throttle(fn, timeout) {
    // let timer = null
    // return function (...arg) {
    //     if (timer) return
    //     timer = setTimeout(() => {
    //         fn.apply(this, arg)
    //         timer = null;
    //     }, timeout)
    // }


    var pre=0;
    return function(){
        const context=this;
        var args=arguments;
        // 记录时间戳
        // var now=new Date().getTime();
        var now=+new Date();
        // var now=new Date().now();
        if(now-pre>timeout){
            fn.apply(context, args);
            pre=now;
        }
    }
}