页面上有三个按钮， 分别为 A、 B、 C， 点击各个按钮都会发送异步请求且互不影响， 每次请求回来的数据都为按钮的名字。
请实现当用户依次点击 A、 B、 C、 A、 C、 B 的时候，
最终获取的数据为 ABCACB。


var a = 0;
var b = async () => {
	a = a + await 10; //a=0被保存
	console.log('2', a);
}

b()
a++
console.log('1', a);


var a = 0;
var b = async () => {
	a = await 10 + a;
	console.log('2', a);
}

b()
a++
console.log('1', a);
