// // 挂在原型上的方法，实例对象可以访问并且使用
// MyPromise.prototype.then = function(onResolved, onRejected) {
// 	//.then接收两个函数类型的形参，分别处理Promise为成功和失败状态时的情况
// }
// MyPromise.prototype.catch = function(onRejected) {
// 	//.catch接收一个函数类型的形参，相当于.then的第二个回调函数
// }
// //...还有一些其他方法
// function MyPromise(executor) {
// 	// Promise的初始状态为pending
// 	let self = this;
// 	this.status = 'pending'
// 	//executor包含两个函数类型的形参，即resolve和reject
// 	function resolve() {}

// 	function reject() {}

// 	//其他具体操作
// }
(function(window) {
	MyPromise.prototype.then = function(onResolved, onRejected) {
		// 把回调用对象包裹，存放在callbacks中
		let self = this
		return new MyPromise((resolve, reject) => {
			if (self.status = 'pending') { //状态还未改变，.then有两个回调函数
				self.callbacks.push({
					onResolved,
					onRejected
				})
			} else if (self.status = 'fullfilled') { //状态为成功，执行onResolved
				//定时器模拟.then的异步执行
				setTimeout(() => {
					const result = onResolved(self.data)
					if (result instanceof MyPromise) { //如果Promise内部没有其他return
						result.then( //为了将result的状态变更成resolved
							(res) => {
								resolve(res)
							},
							(err) => {
								reject(err)
							}
						)
						return result
					} else { //如果Promise内部没有其他return，改变状态
						resolve(result)
					}
				})
			} else { //状态为成功，执行onRejected
				setTimeout(() => {
					onRejected(self.data)
				})
			}
		})
	}
	MyPromise.prototype.catch = function(onRejected) {
		// let self = this
		if (this.status == 'pending') {
			this.callbacks.push({
				onRejected
			})
		} else if (this.status == 'rejected') {
			setTimeout(() => {
				onRejected(this.data)
			})
		}
	}

	function MyPromise(executor) {
		let self = this //保存this对象
		self.status = 'pending' //初始状态为pending
		self.data = undefined
		self.callbacks = []

		// 实现resolve
		function resolve(value) {
			//必须是pending状态,Promsie的状态只会被改变一次
			if (self.status !== 'pending') {
				return
			}
			// 将状态变为fullfilled
			self.status = 'fullfilled'
			self.data = value

			//有没有待执行的回调函数
			if (self.callbacks.length > 0) {
				//如果有，必须都执行
				self.callbacks.forEach(callbackObj => {
					callbackObj.onResolved(value)
				})
			}
		}

		function reject(value) {
			// 同样也必须是pending状态
			if (self.status !== 'pending') {
				return
			}
			// 将状态变为rejected
			self.status = 'rejected'
			self.data = value

			// 有没有待执行的回调
			if (self.callbacks.length > 0) {
				//如果有，必须都执行
				self.callbacks.forEach(callbackObj => {
					callbackObj.onRejected(value)
				})
			}
		}
		try {
			executor(resolve, reject)
		} catch (error) {
			reject(error)
		}
	}
	window.MyPromise = MyPromise
})(window)

let promise = new MyPromise((resolve, reject) => {
	// resolve('成功啦~')
	reject('失败啦~')
}).then(res => {
	console.log(res);
}, err => {
	console.log(err);
})
