let classType = {};
['Boolean', 'Array', 'Object', 'String', 'Number', 'Date', 'Function', 'Undefined', 'Null',
    'Symbol', 'Bigint'].map((item, index) => {
        classType["[object" + item + "]"] = item.toLocaleLowerCase()
    })
function JudgeType(obj) {
    return typeof obj === 'object' || typeof obj === 'function' ? classType[Object.prototype.toString.call(obj)] || 'object' : typeof obj
}
JudgeType(obj)








// instanceof实现原理
function instance_of(L, R) {
    let O = R.prototype
    L = L._proto_;
    while (L != null) {
        if (L === O) return true
        L = L._proto_
    }

    return false
}
instance_of(arr, Array)
instance_of(arr, Object)