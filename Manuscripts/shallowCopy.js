// 浅拷贝
// function shallowCopy(obj){
//     let newObj ={}
//     for(let key in obj){
//         newObj[key] = obj[key]
//     }
//     // return newObj;
//     console.log(newObj);
// }
// let obj={
//     name:'duck',
//     like:{
//         hobby:'reading'
//     }
// }
// let arr=['old', 'new']
// shallowCopy(arr)
// obj.name='dog'
// obj.like.hobby='dancing'

function shallowCopy(obj) {
    if(typeof obj!=='object') return;

    let newObj=Array.isArray(obj) ?[]:{};
    // let newObj=obj instanceof Array ?[]:{};
    // let newObj=obj instanceof Object ?[]:{};//这个不能判断

    for(let key in obj) {
        if(obj.hasOwnProperty(key)) {//自己本身具有的属性
            newObj[key]=obj[key];
        }
    }
    // return newObj;
    console.log(newObj);
}
let obj={
    name:'duck',
    like:{
        hobby:'reading'
    }
}
let arr=['old', 'new']
shallowCopy(arr)
arr[0]='elder'
obj.name='dog'
obj.like.hobby='dancing'