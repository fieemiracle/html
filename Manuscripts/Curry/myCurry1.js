var sub_curry = function (fn) {
    // 让args具备数组的slice方法
    var args = [].slice.call(arguments, 1);//arguments.slice(1)
    return function () {
        var newArgs = args.concat([].slice.call(arguments));
        return fn.apply(this, newArgs);//fn的this指向window
    }
}
function curry(fn, length) {
    length = length || fn.length;
    // var slice=Array.prototype.slice.call(arguments, length);
    var slice = Array.prototype.slice;
    // 返回一个新的函数为了柯里化
    return function () {
        if (arguments.length < length) {
            // 参数还没接收完，还需return function
            var combined = [fn].concat(slice.call(arguments));
            return curry(sub_curry.apply(this, combined), length - arguments.length)
        } else {
            return fn.apply(this, arguments);
        }
    }
}
function add(a, b, c) {
    // return a + b + c;
    console.log(a+b+c);
}
var fn = curry(add);
fn(1, 2, 3);
fn(1, 2)(3);
fn(1)(2)(3);
fn(1)(2, 3);