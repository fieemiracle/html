var curry=function(fn){
    // 让args具备数组的slice方法
    var args=[].slice.call(arguments, 1);//arguments.slice(1)

    return function(){
        var newArgs=args.concat([].slice.call(arguments));
        return fn.apply(this,newArgs);//fn的this指向window
    }
}

function add(a,b,c){
    // return a+b;
    console.log(a+b+c);
}
var fn=curry(function(a,b,c){
    console.log(a+b+c);
});
fn(1,2,3);
fn(1,2)(3);
fn(1)(2)(3);
fn(1)(2,3);
// var addCurry=curry(add,1,2);
// addCurry();

// var addCurry=curry(add,1);
// addCurry(2);

// var addCurry=curry(add);
// addCurry(1,2);