// 闭包经典使用场景--柯理化实现
// function curry(fn, len = fn.length) {
//     return _curry(fn, len)
// }
// function _curry(fn, len, ...arg) {
//     return function (...params) {
//         let _arg = [...arg, ...params]
//         if (_arg.length >= len) {
//             return fn.apply(this, _arg)
//         } else {
//             return _curry.call(this, fn, len, ..._arg)
//         }
//     }
// }
// let fn = curry(function (a, b, c, d, e) {
//     console.log(a + b + c + d + e)
// })
// fn(1, 2, 3, 4, 5)  // 15
// fn(1, 2)(3, 4, 5)
// fn(1, 2)(3)(4)(5)
// fn(1)(2)(3)(4)(5)


// 终极
function curry(fn,args){
    var length=fn.length;
    args=args||[];

    return function(){
        var _args=args.slice();
        var arg;

        for(let i=0;i<arguments.length;i++){
            arg=arguments[i];
            _args.push(arg);
        }

        if(_args.length<length){
            return curry.call(this,fn,_args);
        }else{
            return fn.apply(this,_args);
        }
    }
}
var fn=curry(function(a,b,c){
    console.log([a,b,c]);
})
fn('a')('b')('c')
fn('a','b')('c')
fn('a','b','c')
fn('a')('b','c')