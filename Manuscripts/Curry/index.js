// function add(a,b){
//     return a+b;
// }
// let addCurry=curry(add);
// addCurry(1)(2)

// function sum(a, b, c) {
//     return a + b + c
// }
// function curry(fn) {
// }
// let _sum = curry(sum)
// let A = _sum(1)
// let B = A(2)
// let res = B(3)
// console.log(res);


// ajax发请求
// function ajax(type,url,data){

// }
// 发三次请求就要三次调用

// 颗粒化实现发请求
// let ajaxCurry=curry(ajax);
// let post=ajaxCurry('post');//不同之处在于：只要是post请求，那么post不需要使用多次
// post('www.test.com','name=wn');
// post('www.test.com','name=wn');
// post('www.test.com','name=wn');
// // 页面发请求
// let postFromTest=post('www.test.com');
// postFromTest('name=wn');


// 柯里化作用1
var person=[{name:'John'},{name:'Smith'},{name:'Doe'}];
// var name=person.map(function((item){
//     return item.name;
// }))
// 柯里化实现拿到数组的name
var prop=curry(function(key,obj){//可以复用
    return obj[key];
})
var name=person.map(prop('name'))