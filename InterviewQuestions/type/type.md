# js类型

# js类型有哪些？
	基本数据类型：string,number,null,undefined,bigint,Symbol,boolean
	引用类型：Object-Array,Set,Map,RegExp,Date,Math
	函数：Function
# -大数相加，相乘的算法题


# 类型判断
## typeof 
## instanceof
## Object.prototype.toString.call()
## Array.isArray()
## isNaN()---isNaN(null)->false;isNaN(NaN|undefined)->true
	NaN不是一种类型，判断的时候会将数据转换为数字number，如果不能转换，则为true；否则，返回false
## Number.isNaN()
	只能判断NaN
	
# 类型转换
## 强制转换
	Number()
	toString()
	Boolean()
## 隐式转换
### 对象转原始类型
	valueOf()->toString()
	1+{}===>'1[object Object]'
### 在四则运算中，其中一方是字符串，就会把另有一方也转成字符串
### 在四则运算中，只要其中一方是数字，另一方也要转换成数字
### []!=[]===>true