function _instanceof(L, R) {
	L = L._proto_
	R = R.prototype

	while (L !== null) {
		if (L === R) {
			return true
		} else {
			L = L._proto_
		}
	}
}

let arr = [1, 2, 3]
console.log(_instanceof(arr, Object));
