// jq ajax

function ajax(options) {
	let {
		url,
		method = 'GET',
		data = null,
		dataType = 'JSON',
		async = true,
		cache = true,
		success,
		error,
	} = options
	let xhr = new XMLHttpRequest()
	// 处理参数
	if (method.toUpperCase() == 'GET') {
		if (data) {
			url += '?'
			for (let key in data) {
				url += `${key}=${data[key]}&`
			}
		}
		xhr.open(method, url, async)
		xhr.send()
	} else { // post
		xhr.open(method, url, async)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
		xhr.send(data)
	}


	xhr.onreadystatechange = () => {
		if (xhr.readyState === 4) {
			if (!/^(2|3)\d{2}$/.test(xhr.status)) {
				error && error(xhr.statusText, xhr)
			}
			let result = handleDataType(xhr) // 格式化
			success && success(result, xhr)
		}
	}

	function handleDataType(xhr) {
		dataType = dataType.toUpperCase()
		let result = xhr.responseText
		switch (dataType) {
			case 'TEXT':
				break;
			case 'JSON':
				result = JSON.parse(result)
				break;
			case 'XML':
				result = xhr.responseXML;
				break;
		}
		return result
	}
}
