#http请求方式
GET：从服务器获取数据（向服务器推送数据）
POST：向服务器推送数据（从服务器获取数据）
DELETE：删除服务器端的某些内容（与发POST请求效果一样）
PUT:向服务器存放一些内容
HEAD：只要获取服务器返回的响应头信息，不要响应体
OPTIONS:一般使用它向服务器发送一个探测性请求，如果返回的信息，说明客户端和服务端建立了连接，可以继续请求
TRACE:基于cross-domin进行跨域请求的时候

#get和post的区别?
	1、传参位置：get参数拼接在URL后面，post参数在请求体中
	2、可控缓存：get请求的url会产生不可控制的缓存，post不会
	3、长度限制：get请求的url地址是有长度限制的，post没有
	4、编码方式：get请求只能进行url编码，post支持多种编码
	
#readyState五种状态
	0：刚刚创建xhr（new XMLHttpRequest()）
	1：执行了open这个操作
	2：请求已经发送，响应头已经被客户端接受
	3：响应主题正在返回
	4：响应主体已经被客户端接受
	
#http状态码
	1、1XX:请求已经接受，正在处理
	2、2XX:成功，请求被成功接受
	3、3XX:成功，资源已经重定向
	4、4XX:客户端错误
	5、5XX:服务端错误
	
	(1)1** 信息状态码，服务器收到请求，需要请求者继续执行操作
		常用状态码：
		100：Continue 继续。客户端应继续其请求。
		101：Switching Protocols 切换协议。服务器根据客户端的请求切换协议。只能切换到更高级的协议，如切换到 HTTP 的新版本协议。
	(2)2** 成功状态码，操作被成功接收并处理	
		常用状态码：
		200：OK 请求成功。一般用于 GET 与 POST 请求。
		201：Created 已创建。成功请求并创建了新的资源。
		202：Accepted 已接受。已经接受请求，但未处理完成。
		203：Non-Authoritative Information    非授权信息。请求成功。但返回的 meta 信息不在原始的服务器，而是一个副本。
		204：No Content 无内容。服务器成功处理，但未返回内容。在未更新网页的情况下，可确保浏览器继续显示当前文档。
		205：Reset Content    重置内容。服务器处理成功，用户终端（例如：浏览器）应重置文档视图。可通过此返回码清除浏览器的表单域。
		206：Partial Content     部分内容。服务器成功处理了部分 GET 请求。
		
	(3)3** 重定向状态码，需要进一步的操作以完成请求				
		常用状态码：
		300：Multiple Choices 多种选择。请求的资源可包括多个位置，相应可返回一个资源特征与地址的列表用于用户终端（例如：浏览器）选择。
		301：Moved Permanently 永久移动。请求的资源已被永久的移动到新 URI，返回信息会包括新的 URI，浏览器会自动定向到新 URI。今后任何新的请求都应使用新的 URI 代替。
		302：Found 临时移动，与 301 类似。但资源只是临时被移动。客户端应继续使用原有URI。
		303：See Other    查看其它地址。与 301 类似。使用 GET 和 POST 请求查看。
		304：Not Modified    未修改。所请求的资源未修改，服务器返回此状态码时，不会返回任何资源。客户端通常会缓存访问过的资源，通过提供一个头信息指出客户端希望只返回在指定日期之后修改的资源。
		305：Use Proxy    使用代理。所请求的资源必须通过代理访问。
		306：Unused 已经被废弃的 HTTP 状态码。
		307：Temporary Redirect 临时重定向。与 302 类似。使用 GET 请求重定向。
		
	(4)4** 客户端错误状态码，请求包含语法错误或无法完成请求		
		常用状态码
		400：Bad Request 客户端请求的语法错误，服务器无法理解。
		
		
		401：Unauthorized 请求要求用户的身份认证。
		
		
		402：Payment Required    保留，将来使用。
		
		
		403：Forbidden    服务器理解请求客户端的请求，但是拒绝执行此请求。
		
		
		404：Not Found 服务器无法根据客户端的请求找到资源（网页）。通过此代码，网站设计人员可设置"您所请求的资源无法找到"的个性页面。
		
		
		405：Method Not Allowed 客户端请求中的方法被禁止。
		
		
		406：Not Acceptable 服务器无法根据客户端请求的内容特性完成请求。
		
		
		407：Proxy Authentication Required 请求要求代理的身份认证，与 401 类似，但请求者应当使用代理进行授权。
		
		
		408：Request Time-out 服务器等待客户端发送的请求时间过长，超时。
		
		
		409：Conflict 服务器完成客户端的 PUT 请求时可能返回此代码，服务器处理请求时发生了冲突。
		
		
		410：Gone 客户端请求的资源已经不存在。410 不同于 404，如果资源以前有现在被永久删除了可使用 410 代码，网站设计人员可通过 301 代码指定资源的新位置。
		
		
		411：Length Required 服务器无法处理客户端发送的不带 Content-Length 的请求信息。
		
		
		412：Precondition Failed 客户端请求信息的先决条件错误。
		
		
		413：Request Entity Too Large 由于请求的实体过大，服务器无法处理，因此拒绝请求。为防止客户端的连续请求，服务器可能会关闭连接。如果只是服务器暂时无法处理，则会包含一个 Retry-After 的响应信息。
		
		
		414：Request-URI Too Large    请求的 URI 过长（URI通常为网址），服务器无法处理。
		
		
		415：Unsupported Media Type    服务器无法处理请求附带的媒体格式。
		
		
		416：Requested range not satisfiable    客户端请求的范围无效。
		
		
		417：Expectation Failed    服务器无法满足 Expect 的请求头信息。
	(5)5** 服务器错误状态码，服务器在处理请求的过程中发生了错误	
		常用状态码：
		500：Internal Server Error 服务器内部错误，无法完成请求。
		501：Not Implemented 服务器不支持请求的功能，无法完成请求。
		502：Bad Gateway 作为网关或者代理工作的服务器尝试执行请求时，从远程服务器接收到了一个无效的响应。
		503：Service Unavailable 由于超载或系统维护，服务器暂时的无法处理客户端的请求。延时的长度可包含在服务器的Retry-After头信息中。
		504：Gateway Time-out 充当网关或代理的服务器，未及时从远端服务器获取请求。
		505：HTTP Version not supported 服务器不支持请求的HTTP协议的版本，无法完成处理。
	
	
#HTTP 的重定向
	URL 重定向，也称为 URL 转发，是一种当实际资源，如单个页面、表单或者整个 Web 应用被迁移到新的 URL 下的时候，保持（原有）链接可用的技术。HTTP 协议提供了一种特殊形式的响应—— HTTP 重定向（HTTP redirects）来执行此类操作。
	
	重定向可实现许多目标：
		1、站点维护或停机期间的临时重定向。
		2、永久重定向将在更改站点的 URL，上传文件时的进度页等之后保留现有的链接/书签。
		3、上传文件时的表示进度的页面。
#什么是 AJAX？
	AJAX = Asynchronous JavaScript And XML.
	AJAX 并非编程语言。
	AJAX 仅仅组合了：
		浏览器内建的 XMLHttpRequest 对象（从 web 服务器请求数据）
		JavaScript 和 HTML DOM（显示或使用数据）
		Ajax 是一个令人误导的名称。Ajax 应用程序可能使用 XML 来传输数据，但将数据作为纯文本或 JSON 文本传输也同样常见。
		Ajax 允许通过与场景后面的 Web 服务器交换数据来异步更新网页。这意味着可以更新网页的部分，而不需要重新加载整个页面。
		
#AJAX 如何工作？
	浏览器（发生某个时间...->创建XMLHttpRequest对象->发送HttpRequest）
	|Internet
	服务器（处理HttpRequest->创建响应并将数据返回浏览器）
	|Internet
	浏览器（使用JS处理被返回的函数->更新页面内容）
	
	1、网页中发生一个事件（页面加载、按钮点击）
	2、由 JavaScript 创建 XMLHttpRequest 对象
	3、XMLHttpRequest 对象向 web 服务器发送请求
	4、服务器处理该请求
	5、服务器将响应发送回网页
	6、由 JavaScript 读取响应
	7、由 JavaScript 执行正确的动作（比如更新页面）