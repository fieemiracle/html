#渲染页面
1、HTML代码解析生成DOM树
2、CSS代买解析生成CSSOM树
3、结合DOM树 和CSSOM树，生成一个render树（重排）
4、布局,将渲染树的所有节点进行平面合成（页面依然没有渲染）（重绘）
5、绘制页面到屏幕上（render-UI）

#渲染
网页生成的时候，至少渲染一次

#回流（重排）
	如果发生重排就一定会发生重绘，但是如果发生重绘，不一定会发生重排
		发生重排的操作：
			1、window大小被修改
			2、增加或者删除DOM结构
			3、元素尺寸发生变化
			4、offsetWidth和offsetHeight,offset...;clientWidth,clientHeight,client...;scrollHeight,scrollTop,scroll...;
		减少重排操作
			1、元素脱离文档流--改变样式--回归文档流

	所有导致元素几何信息发生变化的操作

#重绘
	所有导致元素非几何信息发生变化的操作

#浏览器的优化
	当改变元素的几何信息导致重排发生，浏览器提供一个渲染队列用于临时存储该次重排，浏览器继续执行代码，如果还有几何信息修改，继续入队，直到没有样式修改。
	然后浏览器会按照浏览器渲染队列批量优化重排过程
	offsetWidth和offsetHeight,offset...;clientWidth,clientHeight,client...;scrollHeight,scrollTop,scroll...会强制刷新渲染队列（立即执行修改任务）

## 1、什么情况下会发生重排？
    （1）页面首次渲染
         浏览器窗口大小发生改变
         元素尺寸或位置发生改变
         元素内容变化（文字数量或图片大小等等）
         元素字体大小变化
         添加或者删除可见的DOM元素
         激活CSS伪类（例如：:hover）
         查询某些属性或调用某些方法

    一些常用且会导致回流的属性和方法：
        clientWidth、clientHeight、clientTop、clientLeft
        offsetWidth、offsetHeight、offsetTop、offsetLeft
        scrollWidth、scrollHeight、scrollTop、scrollLeft
        scrollIntoView()、scrollIntoViewIfNeeded()
        getComputedStyle()
        getBoundingClientRect()
        scrollTo()
## 2、重排和重绘哪个更耗能？
## 3、如何减少页面重排与重绘？
    （1）CSS：
			避免使用table布局。
			尽可能在DOM树的最末端改变class。
			避免设置多层内联样式。
			将动画效果应用到position属性为absolute或fixed的元素上。
			避免使用CSS表达式（例如：calc()）。

    （2）JavaScript:
			1、避免频繁操作样式，最好一次性重写style属性，或者将样式列表定义为class并一次性更改class属性。
			2、避免频繁操作DOM，创建一个documentFragment，在它上面应用所有DOM操作，最后再把它添加到文档中。
			3、也可以先为元素设置display: none，操作结束后再把它显示出来。因为在display属性为none的元素上进行的DOM操作不会引发回流和重绘。
			4、避免频繁读取会引发回流/重绘的属性，如果确实需要多次使用，就用一个变量缓存起来。
			5、对具有复杂动画的元素使用绝对定位，使它脱离文档流，否则会引起父元素及后续元素频繁回流。

