# 什么是Generator函数？
	Generator 函数是 ES6 提供的一种异步编程解决方案，语法行为与传统函数完全不同。Generator 函数的调用方法与普通函数一样，也是在函数名后面加上一对圆括号。不同的是，调用
Generator 函数后，该函数并不执行，返回的也不是函数运行结果，而是一个指向内部状态的指针对象，也就是遍历器对象（Iterator Object）。
	