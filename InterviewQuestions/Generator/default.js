// let axios = require('axios');

// function* getData() {
// 	let url = 'https://www.fastmock.site/mock/726ff1264270a66fb14cd2981c00b6f6/article/relatedList'
// 	let result = yield axios(url)
// 	console.log('result=', result); //result= undefined
// }
// let g = getData()
// console.log('next1=', g.next()); //next1= { value: Promise { <pending> }, done: false }
// console.log('next2=', g.next()); //next2= { value: undefined, done: true }

var fetch = require('node-fetch') //npm install node-fetch@2npm install node-fetch@2

function* getData() {
	var url = 'https://www.fastmock.site/mock/39ac87de3060aa2bb2ba20a0ff375c81/cat-movie/mostLike'
	var result = yield fetch(url)
	console.log(result);
}
var g = getData()
var res = g.next()

res.value.then((data) => {
	return data.json()
}).then((data) => {
	g.next(data)
})
