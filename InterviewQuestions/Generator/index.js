// 认识Generator函数
function* foo() {
	yield 'hello'
	yield 'dear'
	return 'world'
}
const result = foo()
console.log(result); //Object [Generator] {}

// 必须调用遍历器对象的next方法，使得指针移向下一个状态（每次调用next方法，内部指针就从函数头部或上一次停下来的地方开始执行）
console.log(result.next()); //{ value: 'hello', done: false }
console.log(result.next()); //{ value: 'dear', done: false }
console.log(result.next()); //{ value: 'world', done: true }
console.log(result.next()); //{ value: undefined, done: true }


console.log('--------------------------------------------------------');
// 
function* myGenerator(x) {
	var num = yield x + 2
	console.log('num=', num); //num= 5
	return num
}
let g = myGenerator(1)
console.log('next1=', g.next()); //next1= { value: 3, done: false }   
console.log('next2=', g.next(5)); //next2= { value: 5, done: true } 覆盖上一个next执行的结果

console.log('--------------------------------------------------------');
// 
function* myGenerator1(x) {
	var num1 = 0;
	console.log(num1); //0
	num1 = yield x + 2
	console.log('num1=', num1); //num1= undefined
	// yield 100
	return num1
}
let g1 = myGenerator1(1)
console.log('next1=', g1.next()); //next1= { value: 3, done: false }
console.log('next2=', g1.next()); //next2= { value: undefined, done: true }
