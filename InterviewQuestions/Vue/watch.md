## watch和watchEffect有什么区别？
（1）watch(侦听一个或多个响应式数据源，并在数据源变化时调用所给的回调函数):watch:{key,value} 
    key是需要侦听的响应式组件实例属性（通过data,computed声明的属性）；
    value是相应的回调函数（两个参数，旧值和新值）或methods里声明的函数的函数名的字符串或包含额外选项的对象（回调函数应该生命在handler中）
    额外的选项包括：
    A.immediate:在侦听器创建时立即触发回调。第一次调用时，旧值将为 undefined;
    B.deep：如果源是对象或数组，则强制深度遍历源，以便在深度变更时触发回调;
    C.flush：调整回调的刷新时机;
    D.onTrack / onTrigger：调试侦听器的依赖关系;

（2）watchEffect(立即运行一个函数，同时响应式地追踪其依赖，并在依赖更改时重新执行):watchEffect(param1,param2)
    param1（参数1）:要运行的副作用函数，副作用函数的参数也是一个函数，用来注册清理回调，清理回调会在该副作用下次执行前被调用，可用来清理无效的副作用（等待中异步请求）
    param2（参数2）:
