// 引入规范
const Koa = require('koa');
const app = new Koa();

const main = (ctx, next) => {
	console.log(ctx.query.name);
	// 往前端输出
	ctx.body = 'hi,good afternoon'
}

app.use(main)

// 监听一个端口
app.listen(3000, () => {
	console.log('start');
})
