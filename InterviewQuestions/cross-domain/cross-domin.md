#什么是跨域？
	跨域只会出现在浏览器上,小程序和APP开发不会有跨域问题
	
	
##同源策略--浏览器的一个重要安全策略
	协议号-域名-端口号（都相同才符合同源策略）
	eg.
	一个域名地址的组成：
	https://   www.baidu.com  :8080      /userInfo
	 协议号       域名         端口号        路径
	 
	例1：(协议不同，不允许跨域通信)
	http://www.baidu.com:8080/user
	https://www.baidu.com:8080/user
	
	例2：(端口不同，不允许跨域通信)
	http://www.baidu.com:8080/user
	http://www.baidu.com:9090/user
	 
	 
##跨域通常发生在？
	后端响应回来的数据，在浏览器接收到时被跨域机制拦截下来
	
	
##解决跨域方案
	（1）JSONP--利用script标签中的src属性加载资源时不受同源策略的影响这一特性解决跨域（需要前后端的契合）
		JSONP的做法是：当需要跨域请求时，不使用AJAX，转而生成一个script元素去请求服务器，由于浏览器并不阻止script元素的请求，这样请求可以到达服务器。服务器拿到请求后，响应一段JS代码，这段代码实际上是一个函数调用，调用的是客户端预先生成好的函数
		并把浏览器需要的数据作为参数传递到函数中，从而间接的把数据传递给客户端.
		理解：（JSONP只能用在发get请求方式的情况下）
		前端:
			封装一个JSONP方法:
			const jsonp=(...)=>{
				return new Promise((...)=>{
					...
					准备一个callback函数
					function callback(data){
						resolve(data)
					}
				})
			}
			
		后端：
			callback()//调用回调函数
			
		HTML5中，有三个标签允许跨域加载资源，也就是不会出现跨域
			<link rel="stylesheet" href="xxx.css">
			<script src="xxx.js?callback"></script>//只能走get请求
			<img src="xxx.xxx" alt="">
			
	（2）Cors（Cross-Origin Resouce Sharing）(后端开启==给前端一个通行证)
		仅适用于开发环境
		
	（3）node代理（vue.config.js）
		同源策略是浏览器需要遵循的标准，而如果是服务器向服务器请求就无需遵循同源策略。
		
				  请求数据                 响应请求
					-->						<--
		浏览器				代理服务器					服务器
					<--						-->
					转发响应                转发请求
		适用于开发环境
		
		
#### JSONP

```
//动态创建 script
var script = document.createElement('script');

// 设置回调函数
function getData(data) {
    console.log(data);
}

//设置 script 的 src 属性，并设置请求地址
script.src = 'http://localhost:3000/?callback=getData';

// 让 script 生效
document.body.appendChild(script);

```

**JSONP 的缺点**:
JSON 只支持 get，因为 script 标签只能使用 get 请求； JSONP 需要后端配合返回指定格式的数据。

#### CORS

**CORS** CORS(Cross-origin resource sharing)跨域资源共享 服务器设置对CORS的支持原理：服务器设置**Access-Control-Allow-Origin** HTTP响应头之后，浏览器将会允许跨域请求

**proxy代理** 目前常用方式,通过服务器设置代理 但是一般情况还是后台解决跨域，因为前台是上线之后是没有服务器的概念的

**document.domain** 基础域名相同 子域名不同

**window.name** 利用在一个浏览器窗口内，载入所有的域名都是共享一个window.name

**window.postMessage()** 利用h5新特性window.postMessage()