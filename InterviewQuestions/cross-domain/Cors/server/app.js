// 引入规范
// const Koa = require('koa');
// const app = new Koa();
// const cors = require('@koa/cors');

// // 后端开启cors,允许跨域操作
// app.use(cors())

// const main = (ctx, next) => {
// 	console.log(ctx.query);
// 	// 往前端输出
// 	ctx.body = 'hi,good afternoon'
// }

// app.use(main)

// // 监听一个端口
// app.listen(3000, () => {
// 	console.log('start');
// })


// 原生JS实现
const http = require('http');

const server = http.createServer((req, res) => {
	// 开启cors
	// writeHead()后端在响应头中设置
	res.writeHead(200, {
		// 允许请求源，将域名地址加入白名单，例如http://127.0.0.1:5500
		"Access-Control-Allow-Origin": "*",
		// 允许发请求的方式
		"Access-Control-Allow-Methods": "GET,POST,PUT,OPTIONS",
		// 允许请求头的类型
		// 不管向浏览器返回什么类型都可以，浏览器不会拦截
		"Access-Control-Allow-Headers": "Content-Type"
	})
	res.end('hello cors');
})

server.listen(3000, () => {
	console.log('cors项目已经启动');
})
