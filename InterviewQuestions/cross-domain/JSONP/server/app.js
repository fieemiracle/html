// 引入规范
const Koa = require('koa');
const app = new Koa();

const main = (ctx, next) => {
	console.log(ctx.query);
	const {
		name,
		age,
		cb
	} = ctx.query;
	const userInfo = `${name} is ${age} years old!`;
	const str = `${cb}(${JSON.stringify(userInfo)})`; //'callback()'
	// 往前端输出
	// ctx.body = 'hi,good afternoon'
	ctx.body = str;
}

app.use(main)

// 监听一个端口
app.listen(3000, () => {
	console.log('start');
})
