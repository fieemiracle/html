function foo() {
	var name = 'dog';
	let test1 = 1;
	const test2 = 2;

	var innerBar = {
		getName: function() {
			console.log(test1);
			return name;
		},
		setName: function(newName) {
			name = newName;
		}
	}
	return innerBar;
}
var bar = foo();
bar.setName('dunny');
console.log(bar.getName());


// 累加器解决办法
for (var i = 0; i < 6; i++) {
	setTimeout(() => {
		console.log(i);
	})
}

// (1)let和闭包
//（2）块作用域
for (var i = 0; i < 6; i++) {
	{
		setTimeout(() => {
			console.log(i);
		})
	}
}
// (3)自执行函数
for (var i = 0; i < 6; i++) {
	(function(j) {
		setTimeout(() => {
			console.log(j);
		})
	})(i)
}
//（4）利用定时器第三个参数 
for (var i = 0; i < 6; i++) {
	setTimeout((j) => {
		console.log(j);
	}, 0, i)
}
