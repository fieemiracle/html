const promise1 = new Promise(function (resolve, reject) {
    setTimeout(() => {
        resolve('ok1');
        console.log('timer1');
    }, 1000)

    console.log(1111);
});

const promise2 = promise1.then(function () {
    throw new Error('promise2');
})

console.log('promise1', promise1);
console.log('promise2', promise2);

setTimeout(function () {
    console.log('timer2');
    console.log('promise1', promise1);
    console.log('promise2', promise2);
},2000)