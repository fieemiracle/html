Promise.resolve().then(() => {
    console.log('promise1')

    const timer2 = setTimeout(() => {
        console.log('timer2');
    },1000)
});

const timer2 = setTimeout(() => {
    console.log('timer1')

    Promise.resolve().then(() => {
        console.log('promise2');
    },1000)
});
console.log('start');