const promise1 = new Promise(function (resolve, reject) {
    console.log('promise1');
    resolve('ok');
})

const promise2=promise1.then(res=>{
    console.log(res);
})

console.log('1',promise1);
console.log('2',promise2);

// promise1
// 1 Promise { 'ok' }
// 2 Promise { <pending> }
// ok