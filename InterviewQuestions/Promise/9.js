
setTimeout(() => {
    console.log('timer1');

    Promise.resolve().then(()=>{
        console.log('promise');
    }).then(()=>{
        console.log(333);
    })
},1000)
setTimeout(() => {
    console.log('timer2');
},1000)
console.log(1);

// 每一个宏任务都必须是下一个事件循环的开始，而微任务不会去到下一次事件循环的开始，会留在当前事件循环的微任务队列