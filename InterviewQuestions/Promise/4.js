let myPromise=new Promise((resolve,reject) => {//myPromise
        console.log('promise1');
        resolve('ok');
   })

const myPromise2=myPromise.then(res=>{
    console.log(res);
})

myPromise2.then(res1=>{
    console.log('3',res1);
})
   
   //只会打印promise1,因为myPromise的状态不确定，所以不会执行下面的.then和.catch。如果状态不确定，我们在
   //事件循环中，可以先挂起，等待前一个Promise的状态确定以后再执行

// then(res=>{
//     res
//     return new Promise((resolve, reject) =>{
//         resolve()
//     })
// })