# Promise 的三种状态
    （1）pending
    （2）resolved
    （3）rejected
当Promise状态为pending状态的时候，为同步代码，如果后面接.then，不会执行它的回调函数；当状态为resolved和rejected是，为异步代码。