// 手写Promise构造函数
(function(window) {
	// 挂在原型上的方法，实例对象可以访问并且使用
	MyPromise.prototype.then = function(onResolved, onRejected) {
		// 把回调用对象包裹存放在callbacks中

		// (1)
		// let obj = {}
		// obj.onResolved = onResolved
		// obj.onRejected = onRejected

		// (2)相当于（1）
		// this.callbacks.push({
		// 	onResolved,
		// 	onRejected
		// })

		// （3）改善走到then，状态仍为pending状态的情况
		let self = this
		if (self.status === 'pending') {
			self.callbacks.push({
				onResolved,
				onRejected
			})
		} else if (self.status === 'fullfilled') {
			// 异步
			setTimeout(() => {
				const result = onResolved(self.data)
				if (result instanceof MyPromise) { //如果data有值,且没有额外的return
					result.then( //为了将result的状态变更成resolved
						(val) => {
							console.log(val);
						},
						(err) => {
							console.log(err);
						}
					)
				} else { //例如return 123||return '123'
					resolve(result)
				}
			})
		} else {
			// 异步
			setTimeout(() => {
				onResolved(self.data)
			})
		}
	}
	MyPromise.prototype.catch = function(onRejected) {

	}

	function MyPromise(executor) {
		// 保存Promise的this
		let self = this;
		// Promise初始状态（默认状态）pending
		self.status = 'pending';
		// data用来存储（resolve或者reject抛出）返回的数据
		self.data = undefined;
		// callbacks用来存储.then和.catch的回调
		self.callbacks = [];

		function resolve(value) {
			// 必须是pending状态
			if (self.status != 'pending') {
				return;
			}
			// 改变状态
			self.status = 'fullfilled';
			self.data = value;
			// 有没有待执行的callback函数
			if (self.callbacks.length > 0) {
				// 异步
				setTimeout(() => {
					self.callbacks.forEach(callbackObj => {
						// callbackObj(value);
						console.log(callbackObj);
						callbackObj.onResolved(value)
					})
				})
			}
		}

		function reject(value) {
			// 必须是pending状态
			if (self.status != 'pending') {
				return;
			}
			// 改变状态
			self.status = 'rejected';
			self.data = value;
			// 有没有待执行的callback函数
			if (self.callbacks.length > 0) {
				// 异步
				setTimeout(() => {
					self.callbacks.forEach(callbackObj => {
						// callbackObj(value);
						console.log(callbackObj);
						callbackObj.onRejected(value)
					})
				})
			}
		}

		try {
			// 执行器
			executor(resolve, reject)
		} catch (error) {
			reject(error)
		}
	}

	// Promise.resolve=function(){}
	// Promise.reject=function(){}

	window.MyPromise = MyPromise
})(window)


// 测试
let promise = new MyPromise((resolve, reject) => {
	setTimeout(() => {
		resolve(1)
	}, 100)
})

promise.then(
	(val) => {
		console.log('onResolved', val);
	},
	(err) => {
		console.log('onRejected', err);
	}
)
