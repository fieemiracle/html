const p = new Promise((resolve, reject) => { //p为Promise的实例
	// setTimeout(() => {
	// 	//设置 p 对象的状态为失败，并设置失败的值 
	// 	reject("出错啦!"); //reject()方法的作用，等同于抛出错误
	// }, 1000)

	throw new Error('出错啦'); //从这抛出错误，catch()指定的回调函数也可以捕获
});
// p.then(function(value){},function(reason){
// // console.error(reason); 
// console.log(reason);
// });
​
p.catch(function(reason) { //相当于上面的.then(...)
	console.log(reason, 'reason'); //捕获reject状态的值
});
