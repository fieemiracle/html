// let p = new Promise((resolve, reject) => {
// 	resolve('ok')
// 	throw Error('err')
// })

// p.then( //两个回调
// 	(res) => {
// 		console.log(res);
// 	}
// 	(err) => {
// 		console.log(err);
// 	}
// )

// p.catch( //相当于then的第二个回调
// 	// (res) => {
// 	// 	console.log(res);
// 	// },
// 	(err) => {
// 		console.log(err);
// 	})


// function add(a, b) {

// }

// add(sub)

// function Sub() {
// 	console.log(123);
// }




(function(window) {
	MyPromise.prototype.then = function(onResolved, onRejected) {
		// 把回调用对象包裹，存放在callbacks中
		let self = this
		return new MyPromise((resolve, reject) => {
			if (self.status === 'pending') {
				self.callbacks.push({
					onResolved,
					onRejected
				})
			} else if (self.status === 'resolved') {
				setTimeout(() => {
					const result = onResolved(self.data)
					if (result instanceof MyPromise) { // 没有额外的return
						result.then( // 为了将result的状态变更成resolved
							(val) => {
								resolve(val)
							},
							(err) => {
								reject(err)
							}
						)
						return result
					} else {
						resolve(result)
					}
				})
			} else {
				setTimeout(() => {
					onRejected(self.data)
				})
			}
		})
	}

	MyPromise.prototype.catch = function(onRejected) {
		let self = this
		if (self.status == 'pending') {
			self.callbacks.push({
				onRejected
			})
		} else if (self.status == 'rejected') {
			setTimeout(() => {
				onRejected(self.data)
			})
		}
	}

	function MyPromise(executor) {
		let self = this
		self.status = 'pending'
		self.data = undefined
		self.callbacks = []

		function resolve(value) {
			// 必须是pending状态
			if (self.status !== 'pending') {
				return
			}
			self.status = 'resolved'
			self.data = value
			// 有没有待执行的callback函数

			if (self.callbacks.length > 0) {
				setTimeout(() => {
					self.callbacks.forEach(callbackObj => {
						callbackObj.onResolved(value)
					})
				})
			}
		}

		function reject(value) {
			// 必须是pending状态
			if (self.status !== 'pending') {
				return
			}
			self.status = 'rejected'
			self.data = value
			// 有没有待执行的callback函数
			if (self.callbacks.length > 0) {
				setTimeout(() => {
					self.callbacks.forEach(callbackObj => {
						callbackObj.onRejected(value)
					})
				})
			}
		}

		try {
			executor(resolve, reject)
		} catch (error) {
			reject(error)
		}
	}
	window.MyPromise = MyPromise
})(window)



// let promise = new MyPromise((resolve, reject) => {
// 	// resolve('成功啦~')
// 	return 'sucess'
// }).then(res => {
// 	console.log(res); //成功啦~
// })
// let promise = new MyPromise((resolve, reject) => {
// 	// resolve('成功啦~')
// 	reject('失败啦~')
// }).then(res => {
// 	console.log(res);
// }, err => {
// 	console.log(err);
// })
// let promise = new MyPromise((resolve, reject) => {
// 	reject('失败啦')
// }).catch(err => {
// 	console.log(err); //失败啦
// })
let MyPromiseTest = new MyPromise((resolve, reject) => {
		resolve('成功啦');
	}).then(res => {
		console.log(res);

	})
	.then(res => {
		console.log('猜猜我打印什么');
	})
