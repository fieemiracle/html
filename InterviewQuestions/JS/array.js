// 数组扁平化
var array=[[1,2,3],[3,4,5],[6,7,8,[9,10,11]]]
// [1,2,3,4,5,6,7,8,9,10,11]

function flattern(arr){
    let str=arr.toString();
    str='['+str+']';
    // return JSON.parse(str);
    console.log(JSON.parse(str));
}
flattern(array)

function flattern1(arr){
    // let str=arr.toString();
    // console.log(str.split(','));

    // 递归
    // return arr.reduce((pre,item,index,arr)=>{
    //     return pre.concat(Array.isArray(item)?flattern1(item):item)
    // },[])
    console.log(arr.reduce((pre,item,index,arr)=>{
        return pre.concat(Array.isArray(item)?flattern1(item):item)
    },[]));

}
flattern1(array)