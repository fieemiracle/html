['1','2','3','4','5'].map(parseInt);//[1, NaN, NaN, NaN, NaN]

parseInt('1',0);//10
parseInt('2',1);//NaN

// parseInt(string, radix) 解析一个字符串并返回指定基数的十进制整数，radix 是 2-36 之间的整数，表示被解析字符串的基数。