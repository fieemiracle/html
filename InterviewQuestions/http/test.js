(async () => {
	setTimeout(() => {
		console.log(1);
	}, 0);

	function foo() {
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				reject(2)
				resolve(3)
			}, 0)
			console.log(4);
		})
	}

	const res = await foo()
	console.log(res);
})()
