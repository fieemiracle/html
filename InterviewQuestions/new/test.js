// 1、创建一个构造函数
function Vehicle(name, price) {
	this.name = name
	this.price = price

	// // 第一步：在构造函数内部，创建一个this对象
	// let this = {
	// 	name = name,
	// 	price = price
	// }

	// // 第二步：返回this对象
	// return this;

	// // 第三步：将实例对象的参数与this对象匹配
	// this.name = name
	// this.price = price

}

// 2、new一个实例对象
let truck = new Vehicle()
console.log(truck); //Vehicle { name: undefined, price: undefined }
console.log(Object.prototype.toString.call(truck)); //[object Object]

// 传入参数
let car = new Vehicle('car', '￥9999999')
console.log(car);
//Vehicle { name: 'car', price: '￥9999999' }

console.log(typeof Vehicle); //function
