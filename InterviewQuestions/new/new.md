#new
## -new做了那些事？
	function Person(name, age) {
		// 第一步
		// let this={
		// 	name:name,
		// 	age:age
		// }
	
		// 第二步
		return this;
	
		// 第三步
		this.name = name
		this.age = age
	}
## -new 返回不同类型时有哪些表现？
## -new 的实现原理？
## function Person(name, age) {
	// 第一步
	// let this={
	// 	name:name,
	// 	age:age
	// }

	// 第二步
	// return this;

	// 第三步
	this.name = name
	this.age = age

	// return null||undefined||123||'123' 返回的依然是对象Person { name: undefined, age: undefined }
	// return null; //Person { name: undefined, age: undefined }
	// return undefined; //Person { name: undefined, age: undefined }
	// return {}  //{}
	// return function() {}//[Function (anonymous)]
}
// let p = new Person()
// console.log(p);

// new的实现原理
function newPerson() {
	// 先return一个对象
	var obj = {};
	var constructor = Array.prototype.shift.call(arguments); //把数组的shift方法借给constructor使用
	// 实例对象的隐式原型===构造函数的显示原型
	obj._proto_ = constructor.prototype;
	var result = constructor.apply(obj, arguments);
	return typeof result === 'object' && result != 'null' ? result : obj;
}
let p = newPerson(Person, 'hunny')
console.log(p); //{ _proto_: {}, name: 'hunny', age: undefined }
