function Person(name, gender) {
	console.log('赋值前的this=', this); //赋值前的this= Person {}
	this.name = name
	this.gender = gender
	console.log('赋值后的this=', this); //赋值后的this= Person { name: '小灰灰', gender: 'boy' }
}
// let child = new Person('小灰灰', 'boy') //Person { name: '小灰灰', gender: 'boy' }
// console.log(child);


function Student(id, name) {
	this.id = id
	this.name = name

	// return null||undefined||123||'123' 返回的依然是对象Person { name: undefined, age: undefined }
	// return null; //Person { name: undefined, age: undefined }
	// return undefined; //Person { name: undefined, age: undefined }
	// return {}  //{}
	// return function() {}//[Function (anonymous)]

	// 返回基本类型的值时：返回的结果依然是对象Student {name:xxx,age:xxx}
	// return null   //Student { id: '1001', name: 'cat' }
	// return undefined //Student { id: '1001', name: 'cat' }
	// return 123        //Student { id: '1001', name: 'cat' }
	// return 'hello world'   //Student { id: '1001', name: 'cat' }
	// return true  //Student { id: '1001', name: 'cat' }
	return Symbol('abc') //Student { id: '1001', name: 'cat' }

	// 返回引用类型时：
	// return {}  //{}
	// return function() {} //[Function (anonymous)]
	// return [] //[]
	// return new Date() //2022-10-24T04:44:18.581Z
	// return new RegExp() ///(?:)/
	// return new Error() //Error...


}

let student = new Student('1001', 'cat')
console.log(student); //Student { id: '1001', name: 'cat' }
