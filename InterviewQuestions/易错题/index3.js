// var name = 'World'
// (function() {
// 	if (typeof name === 'undefined') {
// 		var name = 'Jack' //会在函数体内部提升
// 		console.log(name);
// 	} else {
// 		console.log(name);
// 	}
// })()

// var END = Math.pow(2, 53)
// var START = END - 100
// var count = 0;
// for (var i = START; i <= END; i++) { //i加到END时，封顶，已经达到安全值2*53，再大也识别不出来，所以这个循环会一直进行
// 	count++
// }
// console.log(count); //无穷大

// const arr = [1, 2, 3]
// arr[10] = 10
// let res = arr.filter(x => { //filter会跳过empty
// 	return x === undefined
// })
// console.log(arr[3]);
// console.log(res);


console.log(new String('A')); //[String: 'A']
