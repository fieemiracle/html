// let a = {
// 	n: 1
// }
// let b = a
// a.x = a = {
// 	n: 2
// }
// console.log(a.x);
// console.log(b.x);


// let a = 0,
// 	b = 0;

// function A(a) {
// 	A = function(b) {
// 		console.log(a + b++);
// 	}
// 	console.log(a++);
// }
// A(1)
// A(2)

let obj = { //对象中存在length属性，则称该对象为类数组
	'2': 3,
	'3': 4,
	'length': 2,
	'splice': Array.prototype.splice,
	'push': Array.prototype.push
}
obj.push(1)
obj.push(2)
console.log(obj);
console.log(obj.splice(0, 3));
