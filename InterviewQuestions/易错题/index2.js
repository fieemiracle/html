// var b = 10;
// (function b() {
// 	'use strict'
// 	b = 20
// 	console.log(b);
// })()
//自执行函数的具名函数不允许被修改



var b = 10;
(function b() {
	// 'use strict'
	b = 20
	console.log(window.b);
	console.log(b);
})()

// var b = 10;
// function b() {
// 	console.log(window.b);//undefined 先在自己作用域找
// }

// var b = 10;
// (function b() {
// 	console.log(window.b);//10  直接去找window
// })()
