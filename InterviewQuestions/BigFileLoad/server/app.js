const http = require('http')
// 中间件，创建一个FormData对象
const multiparty = require('multiparty')
const path = require('path')
// 比fs跟好用
const fse = require('fs-extra')

const server = http.createServer()
// 创建一个目录存放切片
const UPLOAD_DIR = path.resolve(__dirname, '.', 'qiepian')

server.on('request', async (req, res) => {
	// 处理跨域问题，允许所有的请求头和请求源
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Headers', '*')

	if (req.url === '/upload') { //前端访问的地址正确
		// 解析FormData对象
		const multipart = new multiparty.Form()
		multipart.parse(req, async (err, fields, files) => {
			if (err) { //解析失败
				console.log(err);
				return
			}
			console.log('fields=', fields);
			// console.log('files=', files);
			const [file] = files.file
			const [fileName] = fields.fileName
			const [chunkName] = fields.chunkName

			const chunkDir = path.resolve(UPLOAD_DIR, `${fileName}-chunks`)

			if (!fse.existsSync(chunkDir)) { //文件夹不存在，新建该文件夹
				await fse.mkdirs(chunkDir)
			}

			// 把切片移动进chunkDir
			await fse.move(file.path, `${chunkDir}/${chunkName}`)
			res.end(JSON.stringify({ //向前段输出
				code: 0,
				message: '切片上传成功'
			}))
		})
	}

	if (req.url === '/merge') { // 该去合并切片了
		const data = await resolvePost(req) //解析post请求传递的参数
		const {
			fileName,
			size
		} = data
		const filePath = path.resolve(UPLOAD_DIR, fileName)

		// 合并切片
		await mergeFileChunk(filePath, fileName, size)
		res.end(JSON.stringify({
			code: 0,
			message: '文件合并成功'
		}))
	}
})


// 合并
async function mergeFileChunk(filePath, fileName, size) {
	const chunkDir = path.resolve(UPLOAD_DIR, `${fileName}-chunks`)

	let chunkPaths = await fse.readdir(chunkDir) //读取所有切片的目睹，该方法会返回一个数组
	console.log('chunkPaths=', chunkPaths);
	chunkPaths.sort((a, b) => a.split('-')[1] - b.split('-')[1]) //升序排序

	const arr = chunkPaths.map((chunkPath, index) => {
		return pipeStream(
			path.resolve(chunkDir, chunkPath),
			// 在指定的位置创建可写流
			fse.createWriteStream(filePath, {
				start: index * size,
				end: (index + 1) * size
			})
		)
	})
	await Promise.all(arr) //保证四个切片合并完成
	fse.rmdirSync(chunkDir)
}

// 将切片转换成流进行合并
function pipeStream(path, writeStream) {
	return new Promise(resolve => {
		// 创建可读流，读取所有切片
		const readStream = fse.createReadStream(path)
		readStream.on('end', () => {
			fse.unlinkSync(path) // 读取完毕后，删除已经读取过的切片路径，即让路径失效，保证每个切片只被读取一次
			resolve()
		})
		readStream.pipe(writeStream) //将可读流流入可写流
	})
}

// 解析POST请求传递的参数
function resolvePost(req) {
	// 解析参数
	return new Promise(resolve => {
		let chunk = ''
		req.on('data', data => { //req接收到了前端的数据
			chunk += data //将接收到的所有参数进行拼接
		})
		req.on('end', () => {
			resolve(JSON.parse(chunk)) //将字符串转为JSON对象
		})
	})
}

server.listen(3030, () => {
	console.log('服务已启动');
})
