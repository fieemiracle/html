class A {
	constructor(name) {
		this.name = name
	}
	say() {
		console.log('hello' + this.name);
	}
}

class a extends A {
	constructor(age) {
		super();
		this.age = age;
	}
	run() {
		console.log('run');
	}
}
let demo1 = new a()
demo1.say() //hello
console.log(demo1); //a { name: undefined, age: undefined }
let demo2 = new a('kitty', 19);
demo2.say()
console.log(demo2);

// ES5
// Super.prototype.say = function() {
// 	console.log('hello');
// }

// function Super(name) {
// 	this.name = name;
// }

// function Sub(age) {
// 	this.age = age;
// }

// Sub.prototype.run = function() {}
// Sub.prototype = Object.assign(Sub.prototype)
// let ss = new Sub()
// ss.say()
