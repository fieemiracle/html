// 打印顺序
async function async1() {
    console.log('async1 start'); // 2
    await async2();
    // await 后面都可看作是 callback 里的内容，同步代码执行完毕 再执行异步内容
    console.log('async1 end'); // 5
  };
  async function async2() {
    console.log('async2'); // 3
  }
   
  console.log('script start'); // 1
  async1();
  console.log('script end'); // 4
   
   
   
  // 打印顺序--进阶
//   async function async1() {
//     console.log('async1 start'); // 2
//     await async2();
//     // await 后面都可看作是 callback 里的内容，同步代码执行完毕 再执行异步内容
//     console.log('async1 end'); // 5
//     await async3();
//     // 下面是回调的内容
//     console.log('async1 end end'); // 7
//   };
//   async function async2() {
//     console.log('async2'); // 3
//   }
   
//   async function async3() {
//     console.log('async3'); // 6
//   }
   
//   console.log('script start'); // 1
//   async1();
//   console.log('script end'); // 4