// 面试题
// function getJSON() {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             console.log(2);
//             resolve(2)
//         }, 2000)
//     })
// }
// async function testAsync() {
//     await getJSON()
//     console.log(3);
// }
// testAsync()

// 打印顺序
// 将async await语句解析翻译为promise


function getContent() {
    console.log('getContent');
    return 1
}

async function getAsyncContent() {

    //内部一定会return Promise((resolve,reject)=>{console.log('getAsyncContent');    return 1})
    console.log('getAsyncContent');

    return 1
}

async function getPromise() {

    //     内部一定会return Promise((resolve,reject)=>{
    //         return new Promise((resolve,reject)=>{
    //             console.log(' getPromise');

    //                 resolve(1)
    //             })
    // }

    return new Promise((resolve, reject) => {
        console.log(' getPromise');

        resolve(1)
    })
}

async function test() {

    // 内部编译原理
    // return Promise.resolve().then(()=>{
    //      let a = 2;
    //      let c = 1
    //      return getContent()
    // })
    // .then(()=>{
    //     let d=3;
    //     return getPromise()
    // })
    // .then(()=>{
    //     let e=4;
    //     return getAsyncContent()
    // })
    // .then(()=>{
    //     
    //     return 2
    // })

    let a = 2;
    let c = 1
    await getContent()
    let d = 3
    await getPromise()
    let e = 4;
    await getAsyncContent()
    return 2


}
test()