优点：
    更加贴近于原生代码

1、await不能单独出现，其函数前面一定要有async
2、await会干两件事，第一，将写在await后面的代码放到async创建的那个promise里面执行
                   第二、将写在await下面的代码放到前一个创建的那个promise对象的.then里面执行
3、await返回的也是promise对象，他只是吧await下面的代码放到了await返回的promise的.then里面执行

