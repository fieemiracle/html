async function testError() {
    await Promise.reject('出错了')
        .catch(error => console.log(error));//这里捕获错误，不会影响下一个await执行

    return await Promise.resolve('hello world');
}

testError()
    .then(result => console.log(result))
