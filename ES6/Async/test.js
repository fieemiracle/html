// // async返回的是Promise对象
// async function testAsync() {
//     // await await等待还是promise对象
//     return 'hello'
// }
// // const result = testAsync()
// // console.log(result);//async===return new Promise()
// // //Promise { 'hello' }
// testAsync()
//     .then((result)=>{
//         console.log(result);//hello
//     })
//     .catch((error)=>{
//         console.log(error);
//     })


// async函数内部抛出错误
// async function testError(){
//     throw new Error('出错啦~~');
// } 
// testError()
//     // .then(()=>{},(error)=>{console.log(error);})
//     .catch(error=>{console.log('reject'+error);})

// // await
// async function getName(){
//     // return '来自星星的你';
//     return await '来自星星的你';
// }
// getName()
//     .then(result=>{console.log(result);})

// 没有async
// function testAwait(){
//     return await '西红柿炒辣椒'
// }
// testAwait()
//     .catch(error=>{
//         console.log(error);
//     })

// promise原理
// async function fn1() {
//     return '喜羊羊与灰太狼'
//     // const data = await fn1();//接收data值
// }
// fn1()//执行async函数，返回的是一个Promise对象
//     .then(data => {
//         console.log('content =', data)
//     })

// await---.then()
// async function getName(){
// 	const res2=Promise.resolve('白雪公主')//相当于上面例子的res1 也就是fn1()
// 	const name= await res2 //await相当于Promise的then  res1.then(data=>{})
// 	console.log('name:',name)
// }
// getName();
// ( async function(){
// 	const person=await '七个小矮人' //await Promise.resolve(400) await后面不跟Promise，也会被封装成Promise
// 	console.log('person:',person)//400
// })()

// try...catch
// !(async function () {
//     const testError = Promise.reject('出错啦~~~')//rejected状态
//     // const testError=throw new Error('出错啦~~~');
//     try {
//         const result = await testError; //await相当于then，但是reject不会触发then
//         console.log('success:'+result) //不会输出，因为const result = await testError被报错，被catch捕获
//     } catch (error) {
//         console.error('error:'+error)//try...catch 相当于Promise的catch
//     }

// })()

// async function testError() {
//     await Promise.reject('出错了')
//         .catch(error => console.log(error));
//     return await Promise.resolve('hello world');
// }

// testError()
//     .then(result => console.log(result))

// async function testOrder() {
//     await Promise.reject('出错了')
//     await Promise.resolve('hello world'); // 不会执行
// }
// testOrder();

// 面试题
// function getJSON() {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             console.log(2);
//             resolve(2)
//         }, 2000)
//     })
// }
// async function testAsync() {
//     await getJSON()
//     console.log(3);
// }
// testAsync()

// function getJSON() {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             console.log(2);
//             resolve(2)
//         }, 2000)
//     })
// }
// // 编译城promise原理
// function testAsync() {
//     return Promise.resolve().then(() => {
//         return getJSON();
//     })
//         .then(() => {
//             console.log(3);

//         })
// }
// testAsync()

// 回调地狱
//地狱回调
// setTimeout(function () {  //第一层
//     console.log(1);//等4秒打印1，然后执行下一个回调函数
//     setTimeout(function () {  //第二层
//         console.log(2);//等3秒打印2，然后执行下一个回调函数
//         setTimeout(function () {   //第三层
//             console.log(3);//等2秒打印3，然后执行下一个回调函数
//             setTimeout(function () {   //第三层
//                 console.log(4);//等1秒打印4
//             }, 1000)
//         }, 2000)
//     }, 3000)
// }, 4000)

// // Promise解决方式
// function doCallback(n) {
//     var myPromise = new Promise(function (resolve, reject) {   
//         //处理异步任务
//         var flag = true;
//         setTimeout(function () {
//             if (flag) {
//                 resolve(n)
//             }
//             else {
//                 reject('失败')
//             }
//         },0)
//     })
//     return promise;
// }

// doCallback(1)
//     .then((res) => { //then是成功执行的方法 返回的还是一个promise对象
//         console.log(res);//打印张三  res是执行
//         return fn(2);
//     })
//     .then((res) => {
//         console.log(res);
//         return fn(3)
//     })
//     .then((res) => {
//         console.log(res);
//         return fn(4)
//     })
//     .then((res) => {
//         console.log(res);
//     })
//     .catch((res) => { //catch是失败执行的方法
//         console.log(res);
//     })


//封装一个返回promise的异步任务
function doCallback(str) {
    var myPromise = new Promise(function (resolve, reject) {
        var flag = true;
        setTimeout(function () {
            if (flag) {
                resolve(str)
            } else {
                reject('处理失败')
            }
        })
    })
    return myPromise;
}

//封装一个执行上述异步任务的async函数
async function testAsync() {
    var result1 = await doCallback(1);  //await直接拿到fn()返回的promise的数据，并且赋值给res
    var result2 = await doCallback(2);
    var result3 = await doCallback(3);
    var result4 = await doCallback(4);
    console.log(result1);
    console.log(result2);
    console.log(result3);
    console.log(result4);
}
//执行函数
testAsync();