// async原理
// 1
async function testAsync() {
    // await await等待还是promise对象
    return 'hello'
}
const result = testAsync()
console.log(result);//asyns===return new Promise()
//Promise { 'hello' }

// 2
function getJSON() {
    setTimeout(() => {
        console.log(2);
    }, 2000)
}
async function testAsync() {

    // 编译城promise原理
    //  function testAsync() {
    // return Promise((resolve, reject) => {
    //     return getJSON()

    // })
    //   .then(()=>{
    // console.log(3);

    //   })
    // }

    await getJSON()
    console.log(3);
}
testAsync()

function getJson() {
    return new Promise((resolve, reject) => {
        setTimeout(function () {
            console.log(2);
            resolve(2)
        }, 2000)
    })
}

async function testAsync() {
    await getJson()
    console.log(3);
}

testAsync()