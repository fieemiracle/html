// set 没有重复元素

// new Set()	创建新的 Set 对象。
// add()	    向 Set 添加新元素。
// clear()	    从 Set 中删除所有元素。
// delete()	    删除由其值指定的元素。
// entries()	返回 Set 对象中值的数组。
// has()	    如果值存在则返回 true。
// forEach()	为每个元素调用回调。
// keys()	    返回 Set 对象中值的数组。
// values()	    与 keys() 相同。
// size	        返回元素计数。

// Set.prototype.add(value)：   添加某个值，返回 Set 结构本身。
// Set.prototype.delete(value)：删除某个值，返回一个布尔值，表示删除是否成功。
// Set.prototype.has(value)：   返回一个布尔值，表示该值是否为Set的成员。
// Set.prototype.clear()：      清除所有成员，没有返回值

var set=new Set();
var letters=new Set(['apple','purple','green']);
console.log( typeof letters);//object
console.log(letters instanceof Set);//true
letters.add('banana');
console.log(letters);//Set(4) { 'apple', 'purple', 'green', 'banana' }
console.log(letters.has('pear'));//false

// 接收数组作为参数
const set1 =new Set([1,2,3,3,4,5,4,5,6,6])
console.log([...set1]);
console.log(set1.size);//6

// 添加
const s = new Set();
[2, 3, 5, 4, 5, 2, 2].forEach(x => s.add(x));
for (let i of s) {
  console.log(i);
}

// 去除数组的重复成员
// [...new Set(array)]
let set2=new Set([1,2,1,3,1,4,1,5,1,6]);
let res=[...set2];
console.log('res='+res);

// 去除字符串重复字符
// [...new Set(string)].join('')
let str=new Set('abcdefabcd');
let result=[...str].join('');
console.log(result);

// 在set内部，两个NaN是相等的
// 两个对象总是不相等的
let testSet=new Set();
testSet.add(NaN);
testSet.add(NaN);
testSet.add({});
testSet.add({});
console.log(testSet);//Set(3) { NaN, {}, {} }

// 将set结构转换为数组,也可用来为数组去重
const items = new Set([1, 2, 3, 4, 5, 4]);
const array = Array.from(items);
console.log(array);//[ 1, 2, 3, 4, 5 ]
