// WeakSet 结构与 Set 类似，也是不重复的值的集合。但是，它与 Set 有两个区别。
// weakset内部的对象个数在垃圾回收机制运行前后个数不一样，因此weakset不可遍历
// WeakSet.prototype.add(value)：向 WeakSet 实例添加一个新成员。
// WeakSet.prototype.delete(value)：清除 WeakSet 实例的指定成员。
// WeakSet.prototype.has(value)：返回一个布尔值，表示某个值是否在 WeakSet 实例之中

// 1、WeakSet 的成员只能是对象，而不能是其他类型的值
let weakset=new WeakSet();
const a=[[1,2],[3,4]];
const b=[1,2,3,4];
let weakseta=new WeakSet(a);
console.log(weakseta);//WeakSet { <items unknown> }
// let weaksetb=new WeakSet(b);//Invalid value used in weak set,WeakSet的成员是对象，而且数组和类似数组都可以，但是他们的成员也要是一个对象

// 2、不可遍历
// WeakSet的举例
// 保证了Foo的实例方法，只能在Foo的实例上调用。
// 这里使用 WeakSet 的好处是，foos对实例的引用，
// 不会被计入内存回收机制，所以删除实例的时候，不用考虑foos，也不会出现内存泄漏
const foos = new WeakSet();
class Foo {
  constructor() {
    foos.add(this)
  }
  method () {
    if (!foos.has(this)) {
      throw new TypeError('Foo.prototype.method 只能在Foo的实例上调用!');
    }
  }
}