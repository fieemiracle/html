// 集合的遍历
// Set.prototype.keys()：返回键名的遍历器
// Set.prototype.values()：返回键值的遍历器
// Set.prototype.entries()：返回键值对的遍历器
// Set.prototype.forEach()：使用回调函数遍历每个成员

// 1、keys()，values()，entries()
let set = new Set(['apple', 'pear', 'peach', 'huang', 'li', 'xin']);
for (let item of set.keys()) {
    console.log(item);
}
for (let item1 of set.values()) {
    console.log(item1);
}
for (let item2 of set.entries()) {//键值对，键==值
    console.log(item2);
}
for (let item3 of set) {//相当于set.values()
    console.log(item3);
}

// 2、forEach(),参数是一个处理函数，没有返回值
set.forEach((value, key) => console.log(key + ':' + value));

// 3、扩展运算符（...）
const arr = [...set];
console.log(arr);//[ 'apple', 'pear', 'peach', 'huang', 'li', 'xin' ]直接将集合转换为数组
const arr1 = Array.from(set);
console.log(arr1);//[ 'apple', 'pear', 'peach', 'huang', 'li', 'xin' ]也可以将集合转换为数组

// 4、map(),filter()(数组方法)，参数都是处理函数
let sett = new Set([1, 2, 3]);
sett = new Set([...sett].map(val => val * 2));
console.log(sett);
// let ress = [...sett].map(x => x * 2);
// console.log('数组结构：' + ress);//数组结构：2,4,6

// 5、filter()去除偶数
let set2 = new Set([1, 2, 3, 4, 5]);
set2 = new Set([...set2].filter(x => (x % 2) == 0));
console.log(set2);

// 6、交集，并集，差集
let setone = new Set([2, 4, 7, 4, 8, 9]);
let setsecond = new Set([3, 4, 6, 5, 7, 8]);
// 交集
let interact = new Set([...setone].filter(x => setsecond.has(x)));
console.log(interact);
// 差集
let difference = new Set([...setone].filter(x => !setsecond.has(x)));
console.log(difference);
// 并集
let union = new Set([...setone, ...setsecond]);
console.log(union);


// 一般在遍历的过程不可能修改集合的结构，但是有两种变通方法
// 方法一
let setmap = new Set([1, 2, 3]);
setmap = new Set([...setmap].map(val => val * 2));
console.log(setmap);
// set的值是2, 4, 6

// 方法二
let setfrom = new Set([1, 2, 3]);
setfrom = new Set(Array.from(setfrom, val => val * 2));
console.log(setfrom);


