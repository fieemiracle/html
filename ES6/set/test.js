// const myset=new Set([1,2,3,4,5,6,7]);
// for(let item of myset){
//     console.log(item);
// }

// 垃圾回收机制
// global.gc()
// console.log(process.memoryUsage());

let obj={
    name:'haha',
    age:new Array(5*1024*1024)
}

let ws=new WeakSet();
ws.add(obj);

// obj=null;//该对象不再被引用

global.gc()
console.log(process.memoryUsage());

// node --expose-gc test.js