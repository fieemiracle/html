1、for...in
    （1）返回的都是数据结构的键名
    （2）遍历数组返回的是下标
    （3）能遍历所有数据结构
    （4）还会遍历到原型上的属性值
    （5）对象的数字属性会被优先遍历，且按照顺序遍历（现有的规范，再有的javascript语言）；ECMAScript规范定义了数字属性应该按照索引值的大小升序排序，字符串属性根据创建时的顺序排列

2、for...of
    （1）for...of循环可以使用的范围包括数组、Set 和 Map 结构、某些类似数组的对象（比如arguments对象、DOM NodeList 对象）、后文的 Generator 对象，以及字符串
    （2）对象Object天生不具备迭代器Iterable属性
    （3）只能遍历具有Symbol.iterator属性的数据结构
    （4）不会遍历到原型上的数组(一般使用for...of遍历数组)

3、for...of和for...in的区别


4、什么是可枚举？
可枚举属性是指那些内部 “可枚举” 标志设置为 true 的属性，对于通过直接的赋值和属性初始化的属性，该标识值默认为即为 true，对于通过 Object.defineProperty 等定义的属性，该标识值默认为 false。可枚举的属性可以通过 for...in 循环进行遍历（除非该属性名是一个 Symbol）。属性的所有权是通过判断该属性是否直接属于某个对象决定的，而不是通过原型链继承的。一个对象的所有的属性可以一次性的获取到。有一些内置的方法可以用于判断、迭代/枚举以及获取对象的一个或一组属性，下表对这些方法进行了列举。对于部分不可用的类别，下方的示例代码对获取方法进行了演示。

5、Symbol
symbol 是一种基本数据类型（primitive data type）。Symbol() 函数会返回 symbol 类型的值，该类型具有静态属性和静态方法。它的静态属性会暴露几个内建的成员对象；它的静态方法会暴露全局的 symbol 注册，且类似于内建对象类，但作为构造函数来说它并不完整，因为它不支持语法："new Symbol()"。

每个从 Symbol() 返回的 symbol 值都是唯一的。一个 symbol 值能作为对象属性的标识符；这是该数据类型仅有的目的。更进一步的解析见

6、什么是枚举
    可枚举可以理解为是否可以被遍历被列举出来，可枚举性决定了这个属性能否被for…in查找遍历到。
    js中基本包装类型的原型属性是不可枚举的（不可被 for…in… 遍历），比如：
    Boolean,Number和String三个的原型属性，或是 Boolean,Number值，都是不可枚举的，即是基本类型，也是引用类型。基本包装类型还可以像引用类型一样访问它自带的一些方法，但是不能像引用类型那样自定义方法。


7、并不是所有的属性都会在for-in循环中显示。数组的 length 属性和 constructor 属性就不会被显示。

8、Object.propertyIsEnumerable（）
propertyIsEnumerable( ) 方法返回一个布尔值，表示属性是否可以枚举
每个对象都有一个propertyIsEnumerable方法。该方法可以确定对象中的指定属性是否可以通过for…in循环枚举，但通过原型链继承的属性除外。如果对象不具有指定的属性，则此方法返回false。
语法
obj.propertyIsEnumerable(prop)，prop：表示要测试的属性名称
返回值：Boolean类型
//例一：
var obj = {}
obj.propertyIsEnumerable('a') //false

//例二：
var obj = {a：1}
obj.propertyIsEnumerable('a') //true

简单来说，用户定义的属性都是可枚举的，而内置对象不可枚举。
一种情况除外：当属性是继承于其它对象原型时，这时用户定义的属性就是不可枚举的


9、
对象的每一个属性都有一个描述对象，用来描述和控制该属性的行为。

用Object.getOwnPropertyDescriptor方法来获取该描述对象，用Object.defineProperty方法来设置。

获取到的描述对象中的enumerable属性，称为可枚举性，ture为可枚举，false即不可枚举。当属性不可枚举时，就表示某些操作会忽略当前属性。

四个会忽略enumerable为false的属性的操作。


for…in循环：只遍历对象自身的和继承的可枚举的属性。
Object.keys()：返回对象自身的所有可枚举的属性的键名。
JSON.stringify()：只串行化对象自身的可枚举的属性。
Object.assign()： 忽略enumerable为false的属性，只拷贝对象自身的可枚举的属性。

10、
for in 用来循环数组不是一个合适的选择 ，使用for...in可以遍历数组，但会存在以下问题：

index索引为字符串型数字（数字，非数字），不能进行集合运算
遍历顺序有可能不是按照实际数组的内部顺序（可能按照随机顺序）
使用for-in会遍历数组所有的可枚举属性，包括原型。原型方法method和name属性都会被遍历出来，通常需要配合hasOwnProperty()方法判断某个属性是否该对象的实例属性，来将原型对象从循环中剔除。
如果迭代的对象的变量值是null或者undefined, for in不执行循环体，建议在使用for in循环之前，先检查该对象的值是不是null或者undefined

11、
for..of适用遍历数/数组对象/字符串/map/set等拥有迭代器对象的集合.但是不能遍历对象,因为没有迭代器对象.

for...of循环不会循环对象的key，只会循环出数组的value，因此

如果实在想用for...of来遍历普通对象的属性的话，可以通过和Object.keys()搭配使用，先获取对象的所有key的数组