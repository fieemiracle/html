// 对象的key值唯一
// const obj = {
//     name:'ducky',
//     name:function() { return 'ducky'; },
// }
// console.log(obj);

// 1 向数组原型添加属性
// const myArray = [1,2,3,4,5,6,7,8,9,10];
// Array.prototype.c='c';
// console.log(myArray.c);//c 原型上面的属性也能遍历到
// for(let i in myArray){
//     console.log(myArray[i]);

// }


// 2 向对象原型添加属性
// const obj={
//     name:'lucky',
//     age:19,
//     gender:'female'
// }
// Object.prototype.title='Object';
// for(let key in obj){
//     console.log(obj[key]);
// }

// 3 遍历对象得到的属性值
function MyObject(){
    this[1]='text-1';
    this['a']='test-a';
    this['A']='test-A';
    this[90]='test-90';
    this['B']='test-B';
    this['56']='test-56';
}

let myobj= new MyObject();
for(let key in myobj){
    console.log(`key:${key},value:${myobj[key]}`);

}