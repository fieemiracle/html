//遍历数组 Array
// Array.prototype.testIn = function () { console.log('index'); }
// Array.prototype.age='18'
// const myArray = [1, 2, 3, '7', 'gender'];
// myArray.name = 'lucky'
// for (let index in myArray) {
//     console.log('index:'+index,'value:'+myArray[index]);
// }


// 遍历字符串
// String.prototype.testIn = function () { console.log('index')};
// String.prototype.type='String'
// let s='one';
// s.name='duck'
// for(let index in s){
//     console.log('index:'+index,'value:'+s[index]);
// }

// 遍历对象
// Object.prototype.testIn = function () { console.log('key')};
// Object.prototype.status='student'
// const myObj={
//     name: 'duck',
//     age:19,
//     gender:'female'
// }
// myObj.grade='2020';
// for(let key in myObj){
//     console.log('key:'+key,'value:'+myObj[key]);
// }

// hasOwnProperty()方法
// Object.prototype.testIn = function () { console.log('key')};
// Object.prototype.status='student'
// const myObj={
//     name: 'duck',
//     age:19,
//     gender:'female'
// }
// myObj.grade='2020';
// for (let key in myObj) {
//     if (myObj.hasOwnProperty(key)) {
//         console.log('key:'+key,'value:'+myObj[key]);
//     }
// }

// 遍历数组/字符串
// Array.prototype.testOf=function(){};
// const myArray=['red','green','yellow'];
// for(const vlaue of myArray) {
//     console.log('valueofArray:'+vlaue);
// }

// let s='wow'
// for(var vlaue of s) {
//     console.log('valueofString:'+vlaue);
// }

// 遍历对象
// const obj={
//     name:'duck',
//     age:19
// }
// for(let vlaue of obj) {
//     console.log(value);
// }

// 遍历Set和Map
// const mySet=new Set()
// mySet.add('name');
// mySet.add('age');
// for(let value of mySet) {
//     console.log('valueofSet:'+value);
// }

// const myMap=new Map();
// myMap.set('name', 'duck');
// myMap.set('age', '19');
// console.log(myMap);
// for(let value of myMap) {
//     console.log('valueofMap:'+value);

// }

// 3 遍历对象得到的属性值
// function MyObject(){
//     this[1]='text-1';
//     this['a']='test-a';
//     this['A']='test-A';
//     this[90]='test-90';
//     this['B']='test-B';
//     this['56']='test-56';
// }

// let myobj= new MyObject();
// for(let key in myobj){
//     console.log(`key:${key},value:${myobj[key]}`);

// }

// 遍历arguments
// (function() {
//     for (let argument of arguments) {
//       console.log(argument);
//     }
//   })(1,'2', 'luck');

// 迭代 TypedArray
// let iterable = new Uint8Array([0x00, 0xff]);

// for (let value of iterable) {
//   console.log(value);
// }


// 迭代 DOM 集合
// 迭代 DOM 元素集合，比如一个NodeList对象：下面的例子演示给每一个 article 标签内的 p 标签添加一个 "read" 类。

//注意：这只能在实现了 NodeList.prototype[Symbol.iterator] 的平台上运行
// let articleParagraphs = document.querySelectorAll("article > p");

// for (let paragraph of articleParagraphs) {
//   paragraph.classList.add("read");
// }

// 关闭迭代器
// 对于for...of的循环，可以由 break, throw 或 return 终止。在这些情况下，迭代器关闭。
// function* foo(){
//   yield 1;
//   yield 2;
//   yield 3;
// };

// for (let o of foo()) {
//   console.log(o);
//   break; // closes iterator, triggers return
// }

const arr=[1,2,3,4,5]
for(let item of arr){
    console.log(item);
    break;
}