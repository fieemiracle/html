class Student{
    constructor(){
        return Object.create(null);//Object.create(null)创造的对象没有原型
    }
    say(){
        console.log('hello');
    }
}

let student = new Student('cat');
console.log(student);//[Object: null prototype] {}
console.log(student._proto_==Student.prototype);//false



class IncreasingCounter{
    constructor(){
        this._count=0;
    }

    // 也可以在类内部任意位置直接定义一个属性
    // 这个属性实际上是直接定义在实例对象身上的
    // _count=0;

   get value(){//函数前面加一个get,调用的时候我们不需要value()
        return this._count;
    }
    // _count=0;

    increment(){
        this._count++;
    }
    // _count=0;

}
let n=new IncreasingCounter();
n.increment();
console.log(n.value);//1

class Teacher{
    constructor(){

    }

    get prop(){//get相当于拦截取值操作
        return 'getter';
    }

    set prop(val){//set相当于拦截设值操作
        console.log('setter:'+val);
    }
}
let teacher=new Teacher();
teacher.prop=123;
console.log(teacher.prop);//setter:123  getter 先走set再走get

// 类相当于实例的原型
class Children{
    method(){
        return 'hello';
    }

    // 类可以在内部设置静态方法
    static hobby(){//静态方法只允许构造函数自己使用，不会出现在prototype上，也就不会被继承，更不会在实例对象删出现
        return 'ice-cream';
    }

    static homework(){
        this.math();//构造函数的this指向的实例对象，构造函数原型上的方法的this也是指向实例对象
    }
    math(){
        console.log('Math Homework');
    }

}
let child=new Children();
console.log(child.method());//hello
// console.log(child.hobby());//TypeError: child.hobby is not a function
console.log(Children.hobby());//ice-cream
// Children.homework();//TypeError: this.math is not a function

// 静态方法和静态属性（私有）
class Family{
    static elder='grandParent';
    static parent(){
        console.log('parent');
    }
}
let family=new Family();
console.log(family);//Family {},静态方法和静态属性实例对象都不能继承到