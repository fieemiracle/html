// JavaScript 语言中，生成实例对象的传统方法是通过构造函数。
// Person.prototype.say=function(){
//     console.log('hello world');
// }
// function Person(name,age){
//     this.name = name;
//     this.age = age;
// }
// var person = new Person('duck','19');
// console.log(person.name);//duck
// person.say();//hello world


// ES5~6的Class(类)
class Person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    say(){
        console.log('hello world');
    }
}

Object.assign(Person.prototype, {
    eat(){
        console.log('food');
    },
    run(){
        console.log('running');
    }
});

var person = new Person('duck','19');
console.log(person.name);//duck
person.say();//hello world
person.eat();//food