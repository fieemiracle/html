// ES5的继承
// 继承在前实例在后
function Parent(value){
    this.value = value;
}
let parent = new Parent('myself');
Children.prototype= parent;
function Children(count){
    this.count = count;
}
let children = new Children('son');
console.log(children.value);//myself

// 类的继承
// 1、先将父类的属性和方法加到一个对象上
// 2、再拿这个对象在作为子类的实例
// 父类
class Point{
    // count=1;
    constructor(count){
        // this.count =1;
        // this.count= count;
    }
    foo(){
        console.log('foo');
    }
}
// 子类
class ColorPoint extends Point{
    // constructor(){
    //     super();
    // }不接受参数时，undefined

    constructor(prop){
        super(prop);//super会将父类实例化，即new Point(prop)
    }
}
let cp=new ColorPoint(123);
console.log(cp.foo);//[Function: foo]
console.log(cp);//ColorPoint { count: 123 }