// 在ES5中,this永远指向最后调用它的那个对象
// 1
var name = "windowsName";
function a() {
    var name = "Cherry";

    console.log(this.name);          // windowsName

    console.log("inner:" + this);    // inner: Window
}
a();//最后调用a的地方，等价于window.a,严格模式下，全局对象为undefined（node端没有window对象）
    // 严格模式下全局作用域中函数中的 this 是 undefined，但是在浏览器的控制台，输出结果为windowsName
console.log("outer:" + this)         // outer: Window

// 2
var name1 = "windowsName";
var a = {
    name1: "Cherry",
    fn: function () {
        console.log("name1=" + this.name1);      // Cherry
    }
}
a.fn();//最后调用对象是a，等价于window.a.fn,则this.name1="Cherry"

// 3
var name2 = "windowsName";
var a1 = {
    name2: "Cherry",
    fn: function () {
        console.log("name2"+this.name2); 
    }
}
window.a1.fn();//严格模式下，undefined;浏览器下name2=Cherry

// 严格模式
// 'use strict'
// num = 10 
// console.log(num)//严格模式下不允许使用未声明的变量

// var num2 = 1;
// delete num2;//严格模式不允许删除变量

// function fn() {
//  console.log(this); // 严格模式下全局作用域中函数中的 this 是 undefined
// }
// fn();  

// function Star() {
// 	 this.sex = '男';
// }
// // Star();严格模式下,如果构造函数不加new调用, this 指向的是undefined 如果给他赋值则 会报错.
// var ldh = new Star();
// console.log(ldh.sex);

// setTimeout(function() {
//   console.log(this); //严格模式下，定时器 this 还是指向 window
// }, 2000);  

// 4
var name3 = "windowsName";
var a2 = {
    name3 : null,
    // name3: "Cherry",
    fn : function () {
        console.log("name3="+this.name3);      //浏览器下， windowsName；严格模式下undefined
    }
}

var f = a2.fn;//从上往下，将对象a的fn赋值给f，但是并没有调用
f();//此处调用为window.fn(),严格模式下，undefined;浏览器下，windowsName

// 5
var name4 = "windowsName";
function fn() {
    var name4 = 'Cherry';
    innerFunction();
    function innerFunction() {
        console.log("name4="+this.name4); 
    }
}
fn();//严格模式下，undefined；浏览器下，windowsName


// 对象属性引用链只有上一层或者说最后一层在调用位置中起作用
