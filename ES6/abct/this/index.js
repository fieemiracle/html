// 默认绑定
function A(){
    console.log(this.a);
}
var a=2;
A();//2

function foo(){
    console.log(this.a);
}
var obj={
    a:2,
    foo:foo
}
var obj1={
    a:4,
    obj:obj
}
// console.log(obj.foo());//2 显示绑定
obj1.obj.foo();//2

function Fn(){
    console.log(this.a);
}
var o1={
    a:2
}
Fn.call(o1);//2
Fn.apply(o1);
var bar=Fn.bind(o1)
bar();

// 箭头函数没有this
var 