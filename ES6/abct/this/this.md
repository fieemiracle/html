<!-- this -->
1、默认绑定--独立的函数调用
    -this所处的词法作用域在哪里生效了，this就绑定在哪里
    -在严格模式下，全局作用域无法进行默认绑定，所以导致this只能绑定在undefined上
    // 默认绑定
        function A(){
            console.log(this.a);
        }
        var a=2;
        A();//浏览器:2;node:undefined

2、隐式绑定
    当函数引用(非调用！)有上下文对象时，隐式绑定的规则就会把函数调用中的this绑定到这个上下文对象
    function foo(){
        console.log(this.a);
    }
    var obj={
        a:2,
        foo:foo//函数引用
    }
    console.log(obj.foo());//2 隐式绑定
3、隐式丢失
    当隐式绑定的函数丢失隐式绑定对象，就会引用默认绑定
    function foo(){
        console.log(this.a);
    }
    var obj={
        a:2,
        foo:foo //隐式绑定的函数
    }
    var obj1={
        a:4,
        obj:obj
    }
    // console.log(obj.foo());//2 显示绑定
    obj1.obj.foo();//2
4、显示绑定
    --call
    --apply
    --bind可以强行将函数的this指向改变
    obj1.obj.foo();//2
    function Fn(){
        console.log(this.a);
    }
    var o1={
        a:2
    }
    Fn.call(o1);//2
    Fn.apply(o1);
    var bar=Fn.bind(o1)
    bar();