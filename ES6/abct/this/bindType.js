// 1、隐式绑定
// 在一个对象内部包含一个指向函数的属性
function foo(){
    console.log(this.a);
}
var obj1={
    a:'your name',
    foo:foo//指向函数的属性
}
var a='global name';
var res=obj1.foo;
res();

// 隐私丢失
function test(){
    console.log(this.global);
}
var object1={
    global:56,
    test:test
}
var t=object1.test;
var global='window global';
t();
// 2、显示绑定
// 