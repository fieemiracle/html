// function A(){
//     let a="function name"
//     console.log(this.a);
// }

// var a="window name";
// A();

// function myFunction(){

//     this.someKey = 1;
//     this.inner = function(){
//       console.log(this);
//     }
//   }

//   const obj = new myFunction();
//   obj.inner()
// {someKey: 1, inner: ƒ} with myFunction prototype

// var Func=function(){
//     // this.a="我是构造函数的属性";
//     // this.fun=function(){
//     //     console.log(this);
//     // }
//     let this={
//         a:"我是构造函数的属性",
//         fun:function(){
//             console.log(this);
//         }
//     }
//     return this;
// }
// let myFunc=new Func();
// myFunc.fun();//Func { a: '我是构造函数的属性', fun: [Function (anonymous)] }

// function foo(){
//     var a="function"
//     setInterval(()=>{
//         console.log(this.a);
//     },1000)
// }
// var a="window"
// foo();

// var name = "window";
// var obj = {
//     name : "object",
//     func1: function () {
//         console.log(this.name)     
//     },
//     func2: function () {
//         var _this = this;//这里的 this 是调用 func2 的对象 obj
//         setTimeout( () => {
//             _this.func1()
//         },1000);
//     }
// };
// obj.func2() 

// var a = {
//     name1: "Cherry",
//     func1: function () {
//         console.log(this.name);
//     },
//     func2: function () {
//         setTimeout(function () {
//             this.func1();
//         }.apply(a), 500);
//     }
// };
// a.func2();

// var person = {
//     fullName: function() {
//         return this.firstName + " " + this.lastName;
//     }
// }
// var person1 = {
//     firstName:"Bill",
//     lastName: "Gates",
// }
// var person2 = {
//     firstName:"Steve",
//     lastName: "Jobs",
// }
// console.log(person.fullName.call(person1));
// console.log(person.fullName.apply(person1));
var Student=function(sex){
    this.sex=sex;
}
var student=new Student("girl");
console.log(student.sex);//girl