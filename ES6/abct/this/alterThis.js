// 改变this的指向
// 方法
// 1、使用 ES6 的箭头函数
// 2、在函数内部使用 _this = this
// 3、使用 apply、call、bind
// 4、new 实例化一个对象

// 给出这个例子
// var name = "windowsName";
// var a = {
//     name: "Cherry",
//     func1: function () {
//         console.log(this.name)
//     },
//     func2: function () {
//         setTimeout(function () {
//             this.func1();
//             // 最后调用 setTimeout 的对象是 window，但是在 window 中并没有 func1 函数。

//         }, 100);////严格模式下，定时器 this 还是指向 window
//     }
// };
// a.func2()     // this.func1 is not a function

// 1、使用ES6中的箭头函数改变this的指向：箭头函数的 this 始终指向函数定义时的 this，而非执行时
    // 箭头函数中没有 this 绑定，必须通过查找作用域链来决定其值，如果箭头函数被非箭头函数包含，则 this 绑定的是最近一层非箭头函数的 this
    // 否则，this 为 undefined
var name = "windowsName";
var a = {
    name : "Cherry",
    func1: function () {
        console.log(this.name)     
    },
    func2: function () {
        setTimeout( () => {
            this.func1()
        },100);
    }
};
a.func2()     // Cherry

// 2、在函数内部使用 _this = this
    // 将调用这个函数的对象保存在变量 _this 中，然后在函数中都使用这个 _this
    var name = "windowsName";
    var a = {
        name : "Cherry",
        func1: function () {
            console.log(this.name)     
        },
        func2: function () {
            var _this = this;//这里的 this 是调用 func2 的对象 a
            //var _this = this;可防止在 func2 中的 setTimeout 被 window 调用而导致的在 setTimeout 中的 this 为 window
            setTimeout( function() {
                _this.func1()
            },100);
        }
    };
    a.func2()       // Cherry

// 3、使用 apply、call、bind
    // 使用apply
    // var name1= "windowsName";

    // var a1 = {
    //     name1 : "Cherry",
    //     func1: function () {
    //         console.log(this.name1)
    //     },
    //     func2: function () {
    //         setTimeout(  function () {
    //             this.func1()
    //         }.apply(a1),500);
    //     }
    // };
    // a1.func2()            // Cherry

    // 使用call
    // var name2 = "windowsName";

    // var a2 = {
    //     name2 : "Cherry",
    //     func1: function () {
    //         console.log(this.name2)
    //     },
    //     func2: function () {
    //         setTimeout(  function () {
    //             this.func1()
    //         }.call(a2),200);
    //     }
    // };
    // a2.func2()            // Cherry

    // 使用bind
    // bind()方法创建一个新的函数, 当被调用时，将其this关键字设置为提供的值，在调用新函数时，在任何提供之前提供一个给定的参数序列。
    // 所以需要手动调用
    // var name3 = "windowsName";

    // var a3 = {
    //     name3 : "Cherry",
    //     func1: function () {
    //         console.log(this.name3)
    //     },
    //     func2: function () {
    //         setTimeout(  function () {
    //             this.func1()
    //         }.bind(a3)(),300);
    //     }
    // };
    // a3.func2()            // Cherry