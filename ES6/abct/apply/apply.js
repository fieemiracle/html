// 语法：fun.apply(thisArg, [argsArray])
    //thisArg：在 fun 函数运行时指定的 this 值。需要注意的是，指定的 this 值并不一定是该函数执行时真正的 this 值，
    //          如果这个函数处于非严格模式下，则指定为 null 或 undefined 时会自动指向全局对象（浏览器中就是window对象）
    //          同时值为原始值（数字，字符串，布尔值）的 this 会指向该原始值的自动包装对象。
    // argsArray：一个数组或者类数组对象，其中的数组元素将作为单独的参数传给 fun 函数。
    //            如果该参数的值为null 或 undefined，则表示不需要传入任何参数。
    //            从ECMAScript 5 开始可以使用类数组对象。浏览器兼容性请参阅本文底部内容。
    // apply() 方法调用一个函数, 其具有一个指定的this值，以及作为一个数组（或类似数组的对象）提供的参数

    // 使用apply
    // 可以用于将另一个对象作为参数调用对象方法
    // var name1= "windowsName";

    var a1 = {
        name1 : "Cherry",
        func1: function () {
            console.log(this.name1)
        },
        func2: function () {
            setTimeout(  function () {
                this.func1();
            }.apply(a1),500);
        }
    };
    a1.func2()