// 它可以用来调用所有者对象作为参数的方法。
// 通过 call()，您能够使用属于另一个对象的方法。

var person = {
	fullName: function() {
		return this.firstName + " " + this.lastName;
	}
}
var person1 = {
	firstName: "Bill",
	lastName: "Gates",
}
var person2 = {
	firstName: "Steve",
	lastName: "Jobs",
}
console.log(person.fullName.call(person1));
console.log(person.fullName.apply(person1));
// 将返回 "Bill Gates"

var obj = {
	user: 'Ducky',
	fn: function(a, b) {
		console.log(a + b);
		console.log(this.user);
	}
}
var b = obj.fn;
b.call(obj, 1, 2);
b.apply(obj, [1, 2, 3]); //3多余
var bar = b.bind(obj, 1, 2)
bar(3); //3多余

//   自定义call函数--借助隐式绑定的规则
Function.prototype.myCall = function(context) {
	context = context || 'window'; //解决b.call()问题
	// var context=arguments[0];
	var args = [...arguments].slice(0, 1);

	// const fn=this;//绑定this
	// context.fn=fn;
	const context.fn = this;
	const res = context.fn(...args);
	delete context.fn;
	return res;
}

function F(a, b) {
	console.log(a + b);
	console.log(this.a);
}
var obj = {
	a: 4,
	// fn:fn
}
// obj.fn();
fn.myCall(obj, 1, 2)
