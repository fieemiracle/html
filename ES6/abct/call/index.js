Object.prototype.toString.call()方法使用

// 前端开发项目中，常常会遇到判断一个变量的数据类型等操作，在 JavaScript 里使用 typeof 来判断数据类型，
// 只能区分基本类型，即 “number”，”string”，”undefined”，”boolean”，”object” 五种。对于数组、对象来说，其关系错综复杂，
// 使用 typeof 都会统一返回 “object” 字符串。此时，我们可以使用Object.prototype.toString.call(var) 能判断具体的类型数组；

// 1.判断基本类型：

     Object.prototype.toString.call(null);// ”[object Null]”
	 Object.prototype.toString.call(undefined);// ”[object Undefined]”
	 Object.prototype.toString.call(“abc”);// ”[object String]”
	 Object.prototype.toString.call(123);// ”[object Number]”
	 Object.prototype.toString.call(true);// ”[object Boolean]”
// 2.判断原生引用类型：

// a 函数类型

Function fn(){console.log(“test”);}
Object.prototype.toString.call(fn);//”[object Function]”
// b 日期类型

var date = new Date();
Object.prototype.toString.call(date);//”[object Date]”
// c 数组类型

var arr = [1,2,3];
Object.prototype.toString.call(arr);//”[object Array]”
// d 正则表达式

var reg = /[hbc]at/gi;
Object.prototype.toString.call(arr);//”[object Array]”
// e 自定义类型

function Person(name, age) {
    this.name = name;
    this.age = age;
}
var person = new Person("Rose", 18);
Object.prototype.toString.call(arr); //”[object Object]”
// 显然这种方法不能准确判断person是Person类的实例，而只能用instanceof 操作符来进行判断，如下所示：

console.log(person instanceof Person);//输出结果为true

// 3.判断原生JSON对象：

var isNativeJSON = window.JSON && Object.prototype.toString.call(JSON);
console.log(isNativeJSON);//输出结果为”[object JSON]”说明JSON是原生的，否则不是；
// 注意：Object.prototype.toString()本身是允许被修改的，
// 而我们目前所讨论的关于Object.prototype.toString()这个方法的应用都是假设toString()方法未被修改为前提的。

// Object.prototype.toString()会返回[object, [[class]]]的字符串,其中[[class]]会返回es定义的对象类型，
// 包含"Arguments", “Array”, “Boolean”, “Date”, “Error”, “Function”, “JSON”, “Math”, “Number”, “Object”, “RegExp”, 和 “String”；
// 再加上es5新增加的返回[object Undefined]和[object Null]

// 为什么需要Object.prototype?

// Object对象本身就有一个toString()方法，返回的是当前对象的字符串形式，原型上的toString()返回的才是我们真正需要的包含对象数据类型的字符串。

// 为什么需要call？
// 由于Object.prototype.toString()本身允许被修改，像Array、Boolean、Number的toString就被重写过，所以需要调用Object.prototype.toString.call(arg)来判断arg的类型，
// call将arg的上下文指向Object，所以arg执行了Object的toString方法。

// 至于call，就是改变对象的this指向，当一个对象想调用另一个对象的方法，可以通过call或者apply改变其this指向，将其this指向拥有此方法的对象，就可以调用该方法了

