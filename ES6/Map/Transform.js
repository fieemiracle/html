// map与其他数据结构的互相转化

// (1)Map 转为数组--使用扩展运算符（...）
const myMap = new Map()
    .set('name', 'Ducky')
    .set('age', 19)
    .set('sex', 'boy');

// const myMap = new Map()
//   .set(true, 7)
//   .set({foo: 3}, ['abc']);
console.log([...myMap]);

// (2)数组转为Map--将数组传入Map构造函数
let yourMap = new Map([['name', 'Ducky'], ['age', 19], ['sex', 'boy']]);
console.log(yourMap);

// (3)Map转为对象--如果所有 Map 的键都是字符串，它可以无损地转为对象；如果有非字符串的键名，那么这个键名会被转成字符串，再作为对象的键名
function strMapToObj(strMap) {
    let obj = Object.create(null);
    for (let [k, v] of strMap) {
        obj[k] = v;
    }
    console.log(obj);
    return obj;
}

const herMap = new Map()
    .set('yes', true)
    .set('no', false)
    .set(true, 78)
    .set({ foo: 3 }, ['abc']);
strMapToObj(herMap);

// (4)对象转为Map--对象转为 Map 可以通过Object.entries()
let myObject = {
    'name': 'Lucky',
    'age': 90
}
let objMap = new Map(Object.entries(myObject));
console.log(objMap);

// 自己实现一个转换函数
function objToStrMap(obj) {
    let strMap = new Map();
    for (let k of Object.keys(obj)) {
        strMap.set(k, obj[k]);
    }
    // return strMap;
    console.log(strMap);
}
objToStrMap({ yes: true, no: false })

// (5)Map转为JSON
// ---Map 的键名都是字符串，这时可以选择转为对象 JSON
function strMapToJson(strMap) {
    // return JSON.stringify(strMapToObj(strMap));
    console.log(JSON.stringify(strMapToObj(strMap)));
}
let hisMap = new Map().set('yes', true).set('no', false);
strMapToJson(hisMap);
// ---Map 的键名有非字符串，这时可以选择转为数组 JSON
function mapToArrayJson(map) {
    // return JSON.stringify([...map]);
    console.log(JSON.stringify([...map]));
}

let itsMap = new Map().set(true, 7).set({ foo: 3 }, ['abc']);
mapToArrayJson(itsMap);

// (6)JSON转为Map
// ---正常情况下，所有键名都是字符串
function jsonToStrMap(jsonStr) {
    // return objToStrMap(JSON.parse(jsonStr));
    console.log(objToStrMap(JSON.parse(jsonStr)));
}
jsonToStrMap('{"yes": true, "no": false}')
// ---JSON是一个数组，且每个数组成员是一个有两个成员的数组,则一一对应地转为 Map，这往往是 Map 转为数组 JSON 的逆操作
function jsonToMap(jsonStr) {
    // return new Map(JSON.parse(jsonStr));
    console.log(new Map(JSON.parse(jsonStr)));
}

jsonToMap('[[true,7],[{"foo":3},["abc"]]]')