// WeakMap 是一个键值集合，键不仅限于字符串
// Set和Map都可以用来生成新的 Map
// 语法
let map=new Map();
console.log(typeof map);

// 方法
let obj={
    p:'huang li xin'
};
map.set(obj,'your-name');//添加
console.log(map);//Map(1) { { p: 'huang li xin' } => 'your-name' }
let value=map.get(obj);
console.log(value);//your-name
map.has(obj);//true
map.delete(obj);//true
map.has(obj);//false

let myMap=new Map([['name','huang li xin'],['age','twenty']]);
myMap.size;//2
console.log(myMap);//Map(2) { 'name' => 'huang li xin', 'age' => 'twenty' }
myMap.clear();//清除所有成员，没有返回值

// 细节
// 1
let myMapp=new Map();
myMapp.set(1,'kkk');
myMapp.set(1,'lll');//Map(1) { 1 => 'lll' }
console.log(myMapp);

// 2
map.set(['a'], 555);
map.get(['a']) // undefined  表面是针对同一个键，但实际上这是两个不同的数组实例，内存地址是不一样的

const k1=['b'];
const k2=['b'];
map.set(k1,'123').set(k2,'1234');
console.log(map.get(k1),map.get(k2));//123 1234

// 如果 Map 的键是一个简单类型的值（数字、字符串、布尔值），则只要两个值严格相等，Map 将其视为一个键，比如0和-0就是一个键，
// 布尔值true和字符串true则是两个不同的键。另外，undefined和null也是两个不同的键。虽然NaN不严格相等于自身，但 Map 将其视为同一个键
