// Map 遍历方法
// Map.prototype.keys()：返回键名的遍历器。
// Map.prototype.values()：返回键值的遍历器。
// Map.prototype.entries()：返回所有成员的遍历器。
// Map.prototype.forEach()：遍历 Map 的所有成员

// size 属性
// Map.prototype.set(key, value)
// Map.prototype.get(key)
// Map.prototype.has(key)
// Map.prototype.delete(key)
// Map.prototype.clear()

// 遍历
const map = new Map([
    ['F', 'no'],
    ['T', 'yes'],
]);
for (let key of map.keys()) {
    console.log(key);//F T
}
for (let value of map.values()) {
    console.log(value);//no yes
}
for (let item of map.entries()) {
    console.log(item[0], item[1]);//F no,T yes
}
for (let [key, value] of map.entries()) {
    console.log(key, value);//F no,T yes
}
// 等同于使用map.entries()
for (let [key, value] of map) {
    console.log(key, value);//F no,T yes
}

// Map转换为数组--扩展运算符
const mapp = new Map([
    [1, 'one'],
    [2, 'two'],
    [3, 'three'],
]);

console.log([...mapp.keys()]);
console.log([...mapp.values()]);
console.log([...mapp.entries()]);
console.log([...mapp]);

// 结合数组的map方法、filter方法，可以实现 Map 的遍历和过滤（Map 本身没有map和filter方法
let findMap = new Map().set(1, 'your_name').set(3, 'your_age').set(6, 'your_home');
const findMap1 = new Map(
    [...findMap].filter(([item, index]) => item < 5)
);
console.log(findMap1);

const findMap2 = new Map(
    [...findMap].map(([item, index]) => [item * 2 , '--' + index])
);
console.log(findMap2);

// Map的forEach()遍历
findMap.forEach(function (value, key, findMap) {
    console.log("Key: %s, Value: %s", key, value);
});

// Map的forEach()的第二个参数可以绑定this
const reporter = {
    report: function (key, value) {
        console.log("Key: %s, Value: %s", key, value);
    }
};

findMap.forEach(function (value, key, findMap) {
    this.report(key, value);//this指向reporter
}, reporter);

