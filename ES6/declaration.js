// let
//（1）块作用域中，let 声明的变量只能作用到块作用域，var声明的变量可以作用到全局
{
    let a=10;
    var b=9;
}
console.log(b);//9
//console.log(a);//a is not defined

//（2） let没有声明提升,var有声明提升
console.log(name);//undefined
var name='Lucky';
// console.log(age);//ReferenceError: Cannot access 'age' before initialization
// let age=19;

//（3）let 存在暂时性死区
// 只要块级作用域内存在let命令，let所声明的变量就绑定这个块级作用域，不再受外部影响
// var tmp=123;
// if(true){
//     tmp='abc';
//     let tmp;//不能在let声明前使用变量
// }
// console.log(tmp);//ReferenceError: Cannot access 'tmp' before initialization

// (4)let 不允许重复声明一个变量，var可以重复声明
var date=2020;
var date=2022;
console.log(date);//2022

let day=29;//或者var day=29;
let day=30;
console.log(day);//SyntaxError: Identifier 'day' has already been declared

function A(a){
    {
        let a=3;
        console.log(this.a);//普通函数里面的this：浏览器--this指向window;node端--this.a==undefined,
    }
}

// const
//(1)const定义的变量是常量，不允许修改
const p=1;
p=2;
console.log(p);//SyntaxError: Identifier 'day' has already been declared

const arr=[];
arr.push(2);
console.log(arr);//[1]，这里arr可以添加值是因为，arr.push(2)操作并没有改变arr是数组，只是改变了arr的值