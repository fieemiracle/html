// 数组 解构赋值
let myArray = [1, 2, 3];
let [a, b, c] = myArray; //array destructuring assignment syntax复制代码
console.log(b);//2

// 忽略数组某些值
let [a1, , c1] = [12, 13, 14];
console.log(c1);//14

// 使用展开语法
let [a2, ...b2] = [1, 2, 3, 4, 5, 6];
console.log(Array.isArray(b2)); //true
console.log(b2);//[ 2, 3, 4, 5, 6 ]

let [a3, , ...b3] = [1, 2, 3, 4, 5, 6];
console.log(a3);//1
console.log(b3);//[ 3, 4, 5, 6 ]

// 默认参数值
let [a4, b4, c4 = 6] = [1, 3];
console.log(a4);//1
console.log(c4);//6
console.log(b4);//3

// 嵌套数组
let [a5, b5, [c5, d5]] = [1, 2, [3, 4]];
console.log(c5);//3

// 作为函数参数
function myFunction([a6, b6, c6 = 3]) {
    console.log(a6, b6, c6);
    //Output "1 2 3" 
}
myFunction([1, 2]);

var p = null;
var q = null;
console.log(p == q);


// 报错
// 如果等号的右边不是数组（或者严格地说，不是可遍历的结构，参见《Iterator》一章），那么将会报错
// let [foo] = 1;
// let [foo] = false;
// let [foo] = NaN;
// let [foo] = undefined;
// let [foo] = null;
// let [foo] = {};
// 等号右边的值，要么转为对象以后不具备 Iterator 接口（前五个表达式），要么本身就不具备 Iterator 接口（最后一个表达式）

let [x = 1] = [undefined];
console.log(x);//1
// 只有当一个数组成员严格等于undefined，默认值才会生效;===判断是否有值，没有值，就undefined,null不严格等于undefined
let [y = 1] = [null];
console.log(y);//null

function f() {
    console.log('aaa');
}
// 默认值是一个表达式，那么这个表达式是惰性求值的，即只有在用到的时候，才会求值
let [x = f()] = [1];

let x;
if ([1][0] === undefined) {
  x = f();
} else {
  x = [1][0];
}


let [x = 1, y = x] = [];     // x=1; y=1
let [x = 1, y = x] = [2];    // x=2; y=2
let [x = 1, y = x] = [1, 2]; // x=1; y=2
let [x = y, y = 1] = [];     // ReferenceError: y is not defined