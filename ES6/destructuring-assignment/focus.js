// 1 结构已经声明的变量--不可将大括号写在行首，避免引擎误认为是代码块，导致出错
let x;
({ x } = { x: 1 });
console.log(x);

// 2 结构赋值允许等号左边的模式不放任何变量
({} = [true, false]);
({} = 'abc');
({} = []);

// 3 由于数组是特殊对象，可以对数组进行对象属性的结构
let arr = [1, 2, 3];
let { 0: first, [arr.length - 1]: last } = arr;//属性名表达式
console.log(first, last);

// 4 字符串结构--类似数组对象
let [a, b, c, ...d] = 'helloKity';
let { length: len } = 'helloKity';
console.log(a, b, c, d);//h e l [ 'l', 'o', 'K', 'i', 't', 'y' ]
console.log(len);//9

// 5 数值和布尔值的解构赋值
// 规则：只要等号右边的值不是对象或数组，就先将其转为对象。由于undefined和null无法转为对象，所以对它们进行解构赋值，都会报错
// 数值和布尔值的包装对象都有toString属性，因此变量s都能取到值
let { toString: s1 } = 123;
s1 === Number.prototype.toString // true
let { toString: s2 } = true;
s2 === Boolean.prototype.toString // true

// 6 函数参数结构赋值
// function add([x, y]) {
//     return x + y;
// }
// add([1, 2]);//3

// 函数参数的解构也可以使用默认值。
function move({ x = 0, y = 0 } = {}) {//参数是一个对象，结构失败会等于默认值
    return [x, y];
}
move({ x: 3, y: 8 }); // [3, 8]
move({ x: 3 }); // [3, 0]
move({}); // [0, 0]
move(); // [0, 0]

// function move({ x, y } = { x: 0, y: 0 }) {//参数指定默认值，而不是变量指定默认值
//     return [x, y];
// }
// move({ x: 3, y: 8 }); // [3, 8]
// move({ x: 3 }); // [3, undefined]
// move({}); // [undefined, undefined]
// move(); // [0, 0]


// 7 不能使用圆括号的情况
// （1）变量声明语句
    // let [(a)] = [1];// 全部报错
    // let {x: (c)} = {};
    // let ({x: c}) = {};
// （2）函数参数（也属于变量声明）
    // function f([(z)]) { return z; }  // 报错
    // function f([z,(x)]) { return x; }// 报错
// （3）赋值语句
    // ({ p: a }) = { p: 42 };
    // ([a]) = [5];

// 8 可以使用圆括号的部分--赋值语句的菲模式部分
    [(b)] = [3]; // 正确
    ({ p: (d) } = {}); // 正确
    [(parseInt.prop)] = [3]; // 正确
