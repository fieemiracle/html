// 对象解构
// 与数组解构不同点：1、数组元素按次序排列，变量的值取决于它的位置；对象的属性没有次序，变量必须与属性同名

// 应用1  对象结构可以将现有对象的方法赋值到某个变量
let { pow, sin, cos } = Math;
console.log(pow(2, 3));//8
const { log } = console;
log('hello') // hello

// 应用二 对象的解构赋值的内部机制，先找同名属性，再赋给对应变量。真正被赋值的是后者(变量)，而不是前者(匹配模式)
let { foo: baz } = { foo: 'aaa', bar: 'bbb' };
console.log(baz);//aaa

let obj1 = { first: 'hello', last: 'world' };
let { first: f, last: l } = obj1;
console.log(f, l);//hello world

// 应用三 对象的嵌套
let obj2 = { p: ['Hello', { y: 'World' }] };
let { p: [x, { y }] } = obj2;//此时p是匹配模式
console.log(x, y);//hello world
// console.log(p);//ReferenceError: Cannot access 'p' before initialization

let obj3 = { p: ['Hello', { y1: 'World' }] };
let { p, p: [x1, { y1 }] } = obj3;//此时p是变量
console.log(x1, y1);//hello world
console.log(p);//[ 'Hello', { y1: 'World' } ] Object

const node = {loc: {start: {line: 1,column: 5}}};
let { loc, loc: { start }, loc: { start: { line } } } = node;
console.log(loc);//{start: {line: 1,column: 5}} Object
console.log(start);//{line: 1,column: 5}        Object
console.log(line);//1

// 应用四 对象的嵌套赋值
let obj = {};
let arr = [];
({ foo: obj.prop, bar: arr[0] } = { foo: 123, bar: true });//特别之处在于：foo的属性值是obj里的一个属性
console.log(obj);//{ prop: 123 }
console.log(arr);//[ true ]

// 应用四 对象的解构赋值可以取到继承属性
const obja = {};
const objb = { foo: 'bar' };
Object.setPrototypeOf(obja, objb);//obja的原型是objb
const { foo } = obj1;
console.log(foo);//bar

//应用五 默认值，生效条件：对象的属性值严格等于undefined
var {x: y = 3} = {};//y=3
var {x: y = 3} = {x: 5};//y=5

