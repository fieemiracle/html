// 解构赋值的用途
// 1 变换变量的值
let a = 1;
let b = 2;
[a, b] = [b, a];
console.log(a, b);//2 1

// 2 从函数返回多个值
// 返回一个数组
function example1() {
    return [1, 2, 3];
}
let [a1, b1, c1] = example1();

// 返回一个对象
function example2() {
    return {
        foo: 1,
        bar: 2
    };
}
let { foo, bar } = example2();

//3 函数参数的定义--解构赋值可以方便地将一组参数与变量名对应起来。
// 参数是一组有次序的值
function f1([x, y, z]) { console.log(x, y, z); }
f1([1, 2, 3]);//1 2 3

// 参数是一组无次序的值
function f2({ x, y, z }) { console.log(x, y, z); }
f2({ z: 3, y: 2, x: 1 });//1 2 3

//4 提取json值
let jsonData = {
    id: 42,
    status: "OK",
    data: [867, 5309]
};
let { id, status, data: number } = jsonData;
console.log(id, status, number);//42 OK [ 867, 5309 ]

//5 函数参数的默认值--避免了在函数体内部再写var foo = config.foo || 'default foo';
// jQuery.ajax = function (url, {
//     async = true,
//     beforeSend = function () {},
//     cache = true,
//     complete = function () {},
//     crossDomain = false,
//     global = true,
//     // ... more config
//   } = {}) {
//     // ... do stuff
//   }; 

//6 遍历Map结构
const map = new Map();
map.set('first', 'hello');
map.set('second', 'world');

// // 获取键名
// for (let [key] of map) { }
// // 获取键值
// for (let [, value] of map) { }

for (let [key, value] of map) {
    console.log(key + " is " + value);
}

//7输入模块的指定方法
const { SourceMapConsumer, SourceNode } = require("source-map");
