// promise语法
// let myPromise = new Promise(function(myResolve, myReject) {
//     // "Producing Code"（可能需要一些时间）
    
//       myResolve(); // 成功时
//       myReject();  // 出错时
//     });
    
//     // "Consuming Code" （必须等待一个兑现的承诺）
//     myPromise.then(
//       function(value) { /* 成功时的代码 */ },
//       function(error) { /* 出错时的代码 */ }
//     );

// 1
let myPromise=new Promise(function(myResolve,myReject){
    var num=6;
    if(num==6){
        myResolve();
    }else{
        myReject();
    }
});
myPromise.then(
    function(value){
        console.log('you are right!!');
    },
    function(error){
        console.log('error!!');
    }
)

// 使用回调函数--等待超时
// function myTest(){
//     console.log('here!!');
// }
// setInterval(myTest,2000);

// 使用promise--等待超时
// let myPromise1=new Promise(function(myResolve,myReject){
//     setInterval(myTest,2000);
// });
// function myTest(){
//     console.log('here!!');
// }

// 使用回调函数--等待文件