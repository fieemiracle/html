function a(){
    return new Promise((resolve,reject)=>{
        // console.log('resolve');
        resolve('ok')
        // reject('no')
    },1000)
}

function b(){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            // console.log('reject');
            // resolve('ok')
            reject('no')
        },1500)
    })
}
function c(){
    setTimeout(()=>{
         console.log('finally');
    },500)
   
}


// 情况一
a()
    .then((res)=>{
        console.log(res);
        return b()
    })
    .catch((err)=>{
        console.log(err);
    })
    .finally(c())

// 情况二
// a().finally(c)//有reject一定要catch???????????

// 情况三
// Promise.all([a(),b()]).then(c)
// Promise.race([a(),b()]).then(c)