// // 回调地狱
// //地狱回调
// setTimeout(function () {  //第一层
//     console.log(1);//等4秒打印1，然后执行下一个回调函数
//     setTimeout(function () {  //第二层
//         console.log(2);//等3秒打印2，然后执行下一个回调函数
//         setTimeout(function () {   //第三层
//             console.log(3);//等2秒打印3，然后执行下一个回调函数
//             setTimeout(function () {   //第三层
//                 console.log(4);//等1秒打印4
//             }, 1000)
//         }, 2000)
//     }, 3000)
// }, 4000)

// 状态不会二次改变
// let myPromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve('ok')
//         reject('no')
//     })
// }).then(
//     function (result) { console.log('resolved'); },
//     function (error) { console.log('reject'); }
// )


// catch（）
// const p = new Promise((resolve, reject) => {//p为Promise的实例
//     setTimeout(() => {
//         //设置 p 对象的状态为失败，并设置失败的值 
//         reject("出错啦!");
//     }, 1000)
// });
// p.then(function (value) { }, function (reason) {
//     // console.error(reason); 
//     console.log(reason);
// });
// p.catch(function (reason) {
//     // console.warn(reason);
//     console.log(reason);
// });

// catch（）
// const myPromise = new Promise(function(resolve, reject) {
//     throw new Error('出错啦');
//   });
//   promise.catch(function(error) {
//     console.log(error);
// });

// return非Promise对象
// const myPromise = new Promise((resolve, reject) => {//myPromise是Promise的实例对象
//     setTimeout(() => {
//         resolve('victory')
//         // reject('defeat')
//     })

// })
// let result = myPromise.then(
//     function (value) {
//         console.log(value);
//         return 'succeed';//如果回调函数中返回的结果是 非 promise 类型的属性，状态为成功，返回值为对象的成功值
//         //没有return 语句时，结果为Promise { <pending> }，有return语句时，返回该值

//         // throw new Error('出错啦!"); //因为 throw error 的缘故，代码被阻断执行
//         // throw'出错啦!;
//     },
//     function (reason) {
//         console.log(reason);
//     }
// )
// console.log(result);


// // promise.all()
// const myPromise1=new Promise((resolve,reject)=>{
//     resolve('sure');
// })
//     .then(result=>result)

// const myPromise2=new Promise((resolve,reject)=>{
//     reject('cancel')
// })
//     .then(result=>result)
//     .catch(error=>error)//myPromise2有自己的catch

// Promise.all([myPromise1,myPromise2])//myPromise1,myPromise2都处于成功状态
//     .then(result=>{console.log(result);})//走这里  [ 'sure', 'cancel' ]
//     .catch(error=>{console.log(error);})

// Promise.race()
// const myPromise1 = new Promise((resolve, reject) => {
//     setTimeout(()=>{
//        reject('cancel') 
//     },2000)
    
// })
//     .then(result => result)
//     .catch(error => error)
// const myPromise2 = new Promise((resolve, reject) => {
//     setTimeout(()=>{
//         resolve('sure');
//     },2000)
    
// })
//     .then(result => result)


// Promise.race([myPromise1, myPromise2])
//     .then(result => { console.log(result); })
//     .catch(error => { console.log(error); })

// Promise.resolve()
// let myString='hello';
// console.log(myString);//hello

// const myPromise=Promise.resolve(myString)//带参
// console.log(myPromise);//Promise { 'hello' }

// myString=Promise.resolve();//不带参，直接调用
// console.log(myString);//Promise { undefined }
// myString.then(result=>{//说明myString已经是Promise对象了，只有Promise对象才有.then
//     console.log(result);//undefined
// })

// Promise.reject()
// const myPromise=Promise.reject('error')
// myPromise.catch(error=>{
//     console.log(error);//error
// })

// // 以上代码等于
// const p=new Promise((resolve,reject)=>{
//     reject('error')
// })
// p.catch(error=>{
//     console.log(error);//error
// })
// // 或者
// p.then(function(){},function(error){console.log(error);})//error
// // 或者
// p.then(null,function(error){console.log(error);})//error
// // 或者
// p.then(undefined,function(error){console.log(error);})//error

// 引入fs模块
const fs=require('fs');

//方法一
// 读取text文件夹中的文件内容
// fs.readFile('.1.md',(err,data)=>{
//     // 如果地址错误，抛出异常
//     if(err) throw err;
//     // 如果成功，输出内容
//     console.log(data.toString());//toString将buffer转换为文字
// });

// //方法二  使用Promise封装
// const P=new Promise(function(resolve,reject){
//     fs.readFile('./1.md',(err,data)=>{
//         // 如果地址错误，抛出异常
//         if(err) reject(err) ;
//         // 如果成功，输出内容
//         resolve(data);//toString将buffer转换为文字
//     });
// });
// P.then(function(value){
//     console.log(value.toString());
// },function(reason){
//     console.log("defeat!!!!");
// });

let p=new Promise(function(resolve,reject){   
    resolve();
    return 'ok'
})
p.then(function(value){
    console.log('value');
},function(reason){})
