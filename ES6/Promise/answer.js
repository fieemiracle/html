// promise解决方式
// function fn(str) {
//     var promise = new Promise(function (success, error) { //success 是成功的方法  error是失败的方法  
//         //处理异步任务
//         var flag = true;
//         setTimeout(function () {
//             if (flag) {
//                 success(str)
//             }
//             else {
//                 error('失败')
//             }
//         })
//     })
//     return promise;
// }

// fn('张三')
//     .then((res) => { //then是成功执行的方法 返回的还是一个promise对象
//         console.log(res);//打印张三  res是执行
//         return fn('李四');
//     })
//     .then((res) => {
//         console.log(res);
//         return fn('王五')
//     })
//     .then((res) => {
//         console.log(res);
//     })
//     .catch((res) => { //catch是失败执行的方法
//         console.log(res);
//     })


//封装一个返回promise的异步任务
function fn(str) {
    var promise = new Promise(function (success, error) {
        var flag = true;
        setTimeout(function () {
            if (flag) {
                success(str)
            } else {
                error('处理失败')
            }
        })
    })
    return promise;
}

//封装一个执行上述异步任务的async函数
async function test() {
    var res1 = await fn('张三');  //await直接拿到fn()返回的promise的数据，并且赋值给res
    var res2 = await fn('李四');
    var res3 = await fn('王五');
    console.log(res1);
    console.log(res2);
    console.log(res3);
}
//执行函数
test();