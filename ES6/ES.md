一、let,var,const
    1、let，var，const的区别
    （1）作用域：var 声明的范围是函数作用域，let 和 const 声明的范围是块作用域
    （2）变量提升：var 声明的变量会被提升到函数作用域的顶部，let 和 const 声明的变量不存在提升，且具有暂时性死区特征
    （3）重复声明：var 允许在同一个作用域中重复声明同一个变量，let 和 const 不允许
    （5）window的属性：在全局作用域中使用 var 声明的变量会成为 window 对象的属性，let 和 const 声明的变量则不会
    （6）初始化：const 的行为与 let 基本相同，唯一一个重要的区别是，使用 const 声明的变量必须进行初始化，且不能被修改
    2、let
    （1）块作用域中，let 声明的变量只能作用到块作用域，var声明的变量可以作用到全局
        {
            let a=10;
            var b=9;
        }
        console.log(b);//9
        console.log(a);//a is not defined

    （2） let没有声明提升,var有声明提升
        console.log(name);//undefined
        var name='Lucky';
        console.log(age);//ReferenceError: Cannot access 'age' before initialization
        let age=19;

    （3）let 存在暂时性死区
        // 只要块级作用域内存在let命令，let所声明的变量就绑定这个块级作用域，不再受外部影响
        var tmp=123;
        if(true){
            tmp='abc';
            let tmp;
        }
        console.log(tmp);//ReferenceError: Cannot access 'tmp' before initialization
    
    （4）let 不允许重复声明一个变量，var可以重复声明
        var date=2020;
        var date=2022;
        console.log(date);//2022

        let day=29;//或者var day=29;
        let day=30;
        console.log(day);//SyntaxError: Identifier 'day' has already been declared

    3、const
     与let基本一致，唯一的不同是：
    （1）const定义的变量是常量，不允许修改
        const p=1;
        p=2;
        console.log(p);//SyntaxError: Identifier 'day' has already been declared

        const arr=[];
        arr.push(2);
        console.log(arr);//[1]，这里arr可以添加值是因为，arr.push(2)操作并没有改变arr是数组，只是改变了arr的值

二、解构赋值
    1、数组解构

    2、对象解构

    3、字符串解构
        // 字符串解构
        let [a,b,c,d,e]='hello';
        console.log(a,b,c,d,e);

三、Set和Map
    1、Object的键只能是字符串
    2、Set类似于数组，但是成员都是唯一，没有重复项；键名就是键值；weakSet

    3、Map的键可以是任何值