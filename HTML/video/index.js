var speed = document.querySelector('.speed');// 选择器
var bar = document.querySelector('.speed-bar');
// console.log(document.querySelector('.speed'));
speed.addEventListener('mousemove', function (e) {
    console.log(e);
    var persent=e.offsetY/speed.offsetHeight;
    // 鼠标的坐标：pageX,pageY;鼠标到容器的距离：offsetX,offsetY
    var height = Math.round( persent* 100) + '%';//math.round保留整数
    var playbackRate=persent*(3-0.5)+0.5
    console.log(e.offsetY);
    bar.style.height = height;
    bar.textContent=playbackRate.toFixed(2)+'x';

    var video=document.querySelector('.video');
    video.playbackRate=playbackRate;
})
