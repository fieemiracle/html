一、脱离文档流的方法：
    1、绝对定位  position:absolute
    2、浮动      float
    3、固定定位  position:fixed

二、分别：
    #float:left/right/auto
        作用:  #1、脱离文档流，
               #2、文本环绕，但是开启浮动的容器的文档流任然占据位置，有文字环绕效果
               #3、让块级元素同行显示
               #4、使行内元素可改变宽高，行内元素设置了浮动，该元素则变成了内敛块级标签，可以设置宽高
               #5、可以使用margin，但是不能使用margin: 0 auto;/*设置浮动后不能生效*/    
               #6、元素没有宽高时，宽高可以被内容撑开（与正常盒子一样）    

    #position:absolute
        作用：脱离文档流，开启绝对定位的容器不占据位置，此时文字没有环绕效果
               1、可以使用margin，但是不能使用margin: 0 auto;/*设置浮动后不能生效*/        
            
    #position:fixed
        作用：脱离文档流
        特点：不需要参照物，永远根据浏览器定位

三、清除浮动的方法：（清除浮动的核心理念就是想办法让浮动元素的父元素有高度）
    #1、给父容器设置高度（不推荐），给浮动元素的父辈容器添加高度

    #2、在设置浮动的最后一个元素后面，增加一个子容器，设置clear属性，，使用清除属性clear（内墙元素隔离法）
            clear:both/left/right

    #3、使用伪元素清除浮动(推荐)，给浮动元素的父辈容器添加伪元素，::after,如下：（内墙元素隔离法升级）
            ul{
                content: '';
                display: block/inline-block/table;
                clear: both;
            }
        
            使用clear清除浮动的不足：
                不足1: 浮动元素的父辈仍然没有高度
                不足2: 此时清除元素的margin-top失效，其他三个方向的margin有效

   #4、开启BFC容器（推荐）,给浮动元素的父辈容器添加
            （1）overflow:hidden/auto（非visible的值）(抵消浮动带来的影响)
            （2）display:inline-block / table-cell/ flex  /table(其他含有table) 
             (3)position:absolute/fixed

    #5、给下面被影响的容器添加clear:both，不足：父辈容器仍然没有高度

四、了解BFC（css2.0）
    BFC--Block Formatting Context（块级格式化上下文）
    #概念(范围)：一个BFC包含该上下文的所有子元素，但不包含创建了新BFC容器的内部元素
         一个元素不能同时处于两个bfc容器

    #效果：让处于bfc内部的元素与外部的元素相互隔离，使内外元素的定位（浏览器的渲染位置）不会相互影响
          1、重点:解决外边距重叠
          2、内部盒子会在垂直方向上一个接一个排列
          3、bfc容器在计算高度的时候，会连着浮动元素计算在内，所以可以借助bfc清除浮动

    视觉格式化模型--Box
    #BFC的创建方法：
        （1） 给开启浮动元素的父辈容器设置浮动，float:left/right
        （2） 给开启浮动元素的父辈容器开启定位，position:absolute/fixed
        （3） 给开启浮动元素的父辈容器设置display,
                行内块： display:inline-block
                表格单元：display:table-cell/table(属性值含有table的都可以)
                弹性布局:display:flex
        （4）给开启浮动元素的父辈容器设置overflow
                overflow:auto|hidden|overlay|scroll(非visible的值)

五、隐藏的方法
    1、display：none
    2、opcity:0.5

六、let，var，const的区别
    （1）作用域：var 声明的范围是函数作用域，let 和 const 声明的范围是块作用域
    （2）变量提升：var 声明的变量会被提升到函数作用域的顶部，let 和 const 声明的变量不存在提升，且具有暂时性死区特征
    （3）重复声明：var 允许在同一个作用域中重复声明同一个变量，let 和 const 不允许
    （5）window的属性：在全局作用域中使用 var 声明的变量会成为 window 对象的属性，let 和 const 声明的变量则不会
    （6）初始化：const 的行为与 let 基本相同，唯一一个重要的区别是，使用 const 声明的变量必须进行初始化，且不能被修改

七、setInterval和setTimeout的区别
    (1)setInterval() 方法可按照指定的周期（以毫秒计）来调用函数或计算表达式
    (2)setTimeout() 方法用于在指定的毫秒数后调用函数或计算表达式，setTimeout() 只执行一次
    
八、addEventListener和“on...”的区别

九、柯里化实现

十、防抖节流

十一、dom方法的区别
        getElementById()方法是全局方法，所以必须使用document节点；
        querySelector()方法不是全局方法，可以直接使用它的父容器节点
        offsetTop距离父容器顶部的高度  
        offsetLeft距离父容器左侧的宽度
        offsetWidth自己容器的宽度        
        offsetHeight自己容器的高度

十二、惰性求值

十三、async函数

十四、作用域（重要）
    1、引  擎
    2、编译器
    3、作用域
    （1）全局作用域
    （2）函数体作用域:隐藏内部实现
    （3）块级作用域--let作用域，代码块{}，for循环用let时的作用域，if,while,switch
        --标识符：能在作用域生效的变量。函数的参数，变量，函数名。函数体内部的标识符外部访问不到
        --函数声明：function foo(){}
        --函数表达式 var foo=function(){}
        --自执行函数 (function foo(){})()；自执行函数前面的语句必须有分号；通常用于隐藏作用域

    4、作用域链

    5、词法作用域：代码在执行之前需要被编译的过程
    （1）词法化
        规则:--父级作用域不能取子级作用域，但是子级作用域可以调用父级作用域的字符
             --遮蔽效应：执行阶段查找作用域是由内到外的，找到第一个就会停止查找
             --全局作用域里的全局变量会作用到全局对象（一般是window）上，node端没有window，浏览器有window

    6、欺骗词法作用域(让代码性能变差)
    （1）eval() 让原本不属于某个地方的代码好像原始就存在了那一样。非严格模式下，eval不会创建自己内部的作用域，所以eval(x)里的参数x不是标识符
            function foo(str,a){
                'use strict'//严格模式 1 2
                eval(str)//欺骗
                console.log(a,b);//非严格模式 1 4
            }
            var b=2;
            foo('var b=4',1);

    （2）with 当使用with修改一个对象中不存在的属性的时候，这个属性会被泄漏到全局

十五、双飞翼和圣杯（面试必备）--可举例（三列布局，瀑布流）

十六、预编译
    #发生在代码执行之前
    1、声明提升
        console.log(b);
        var b=123;//undefined

    2、函数声明整体提升
        test();//hello123
        function test(){
            var a=123;
            console.log('hello'+a);
        }

    #发生在函数执行之前--四部曲（重要）
    1、创建一个AO（Activation Object）
    2、找形参和变量声明，然后将形参和变量声明作为AO的属性名，属性值为undefined
    3、将实参和形参统一
    4、在函数体内找函数声明，将函数名作为AO对象的属性名，属性值予函数体

    #发生在全局（内层作用域可以访问外层作用域）
    1、创建DO对象
    2、找全局变量声明，将变量声明作为GO的属性名，属性值为undefined
    3、在全局找函数声明，将函数名作为GO对象的属性名，属性值赋予函数体

十七、作用域链---可以解释为什么内层访问到外层，而外层访问不到内层

    #根本原因：根据作用域链，函数执行完毕其内部的数据AO是否会被回收决定了能不能被访问到。

    #执行期上下文：当函数执行的时候，会创建一个称为执行期上下文的对象（AO对象），一个执行期上下文定义了一个函数执行时的环境
                 函数每次执行时，对应的执行上下文都是独一无二的，所以多次调用一个函数会导致创建多个执行期上下文，当函数执行完毕
                 它所产生的执行期上下文会被销毁。

    #查找变量：从作用域链的顶端依次往下查找
    #[[scope]]----作用域属性，也称为隐式属性,仅支持引擎自己访问。函数作用域，是不可访问的，其中存储了运行期上下文的结合。

    #作用域链：[[scope]]中所存储的执行期上下文对象的集合，这个集合呈链式链接，我们把这种链式链接叫做作用域链

        
